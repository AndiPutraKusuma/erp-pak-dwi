-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2017 at 02:21 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `retail01`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_alert_stok` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT tb1.nama_item,tb1.id_suplier,tb1.nama_item,tb1.`id_item`,tb1.tipe,tb1.satuan,SUM(tb1.`jumlah`) AS jumlah,tb1.hargaSatuan,tb1.link_photo,tb1.deskripsi
	FROM (SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_produksi`=penerimaan_barang.`id_produksi`
	INNER JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	where item_master.`id_pemilik`=_id_pemilik
	GROUP BY item_master.`id_item`
UNION
	SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	where item_master.`id_pemilik`=_id_pemilik
	 GROUP BY item_master.`id_item` ORDER BY nama_item ASC) AS tb1 WHERE tb1.jumlah<20 GROUP BY tb1.nama_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariCustomer` (`_keyword` VARCHAR(300))  BEGIN
	SELECT * FROM customer WHERE nama LIKE CONCAT('%',_keyword,'%') or id_customer like CONCAT('%',_keyword,'%') ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariPetugas` (`_keyword` VARCHAR(300))  BEGIN
	select * from petugas where nama like CONCAT('%',_keyword,'%') or id_petugas LIKE CONCAT('%',_keyword,'%');
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariProduk` (`cari_suplier` VARCHAR(200))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE nama_item LIKE CONCAT('%',cari_suplier,'%')
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariSuplier` (`_keyword` VARCHAR(300))  BEGIN
	SELECT * FROM suplier WHERE nama_suplier LIKE CONCAT('%',_keyword,'%') or id_suplier LIKE CONCAT('%',_keyword,'%');
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cari_suplierProduk` (`cari_suplier` VARCHAR(200))  BEGIN
	SELECT nama_suplier,suplier.`id_suplier`,item_master.`id_item`,nama_item,satuan,tipe, SUM(gudang.`jumlah`)AS jumlah,
	 link_photo,item_master.deskripsi,detail_penerimaan.hargaSatuan 
	FROM detail_penerimaan LEFT JOIN gudang 
	ON detail_penerimaan.`id_purchasing`=gudang.`id_purchasing` AND
	detail_penerimaan.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_penerimaan.`id_item`
	INNER JOIN penerimaan_barang ON penerimaan_barang.`id_po`=detail_penerimaan.`id_purchasing`
	INNER JOIN suplier ON suplier.`id_suplier`=penerimaan_barang.`id_suplier`
	 WHERE nama_item LIKE CONCAT('%',cari_suplier,'%') GROUP BY detail_penerimaan.`id_item`;
	 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cekService` (`_id_service` INT(11))  BEGIN
	set@cek=(select id_service from produkservice where id_service=_id_service);
	if(@cek) then
		select 1 as cek;
	else
		select -1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cekStok` (`_id_item` VARCHAR(100), `_id_supplier` INT(11), `_harga` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
SELECT gudang.`id_rec`,detail_suplier.`id_suplier`,detail_suplier.`id_item`,gudang.`jumlah`,hargaSatuan FROM penerimaan_barang 
INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
WHERE detail_suplier.`id_item`=_id_item and penerimaan_barang.`id_suplier`=_id_supplier  and gudang.`hargaSatuan`=_harga and penerimaan_barang.`id_pemilik`=_id_pemilik
ORDER BY gudang.`id_rec` asc LIMIT 1;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cek_akses` (`_email` VARCHAR(200))  BEGIN
    set@idPet=(select id_petugas from petugas where petugas.`email`=_email);
	select privilege from petugas where id_petugas=@idPet;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	set@cek=(select id_customer from penjualan where id_customer=_id_customer limit 1);
	IF(@cek) THEN
			SELECT -1 AS cek;
		ELSE
		DELETE FROM customer WHERE id_customer=_id_customer;
			SELECT 1 AS cek;
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteInstitusi` (`_id_institusi` VARCHAR(100))  BEGIN
   
	set@child=(select id_customer from customer where customer.`id_institut`=_id_institusi limit 1);
	if(@child) then
		select -1 as cek;
	else
		DELETE FROM institusi WHERE id_institusi=_id_institusi;
		SET@cek=(SELECT id_institusi FROM institusi WHERE id_institusi=_id_institusi);
		IF(@cek) THEN
			SELECT -1 AS cek;
		ELSE
			SELECT 1 AS cek;
		END IF;
	
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteService` (`_id_service` INT(11))  BEGIN
	
	delete from service where id_service=_id_service;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteSuplier` (`_id_suplier` INT(11))  BEGIN
	SET@cek=(SELECT id_suplier FROM purchasing WHERE id_suplier=_id_suplier LIMIT 1);
	IF(@cek) THEN
			SELECT -1 AS cek;
	ELSE
	delete from suplier where id_suplier=_id_suplier;
	select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_bom` (`_id_produk` VARCHAR(100))  BEGIN
	delete from bom where id_produk=_id_produk;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_item` (`_id_item` VARCHAR(100))  BEGIN
	set@cek=(select id_item from detail_suplier where id_item=_id_item limit 1);
	if(@cek) then
		select -1 as cek;
	else
		delete from item_master where id_item=_id_item;
		select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_satuan` (`_id_satuan` INT(11))  BEGIN
	set@child=(select satuan from item_master where satuan=_id_satuan);
	if(@child) then
		select -1 as cek;
	else
		delete from satuan where id_satuan=_id_satuan;
		set@cek=(select id_satuan from satuan where id_satuan=_id_satuan);
		if(@cek) then
			select -1 as cek;
		else
			select 1 as cek;
		end if;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_tipe` (`_id_tipe` INT(11))  BEGIN
	SET@child=(SELECT tipe FROM item_master WHERE satuan=_id_tipe);
	IF(@child) THEN
		SELECT -1 AS cek;
	ELSE
		DELETE FROM tipe_item WHERE id_tipe_item=_id_tipe;
		SET@cek=(SELECT id_tipe_item FROM tipe_item WHERE id_tipe_item=_id_tipe);
		IF(@cek) THEN
			SELECT -1 AS cek;
		ELSE
			SELECT 1 AS cek;
		END IF;
	END IF;
  
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deletPetugas` (`_id_petugas` VARCHAR(100))  BEGIN
	SET@cek=(SELECT id_petugas FROM penjualan WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek1=(SELECT id_petugas FROM purchasing WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek2=(SELECT id_petugas FROM service WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek3=(SELECT teknisi FROM solving WHERE teknisi=_id_petugas LIMIT 1);
	IF(@cek or @cek1 or @cek2 or @cek3 ) THEN
			SELECT -1 AS cek;
	ELSE
	delete from petugas where id_petugas=_id_petugas;
	select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_detail_pembelian` (`_id_pembelian` VARCHAR(100), `_id_produk` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11))  BEGIN
	insert into detail_pembelian (id_transaksi,id_produk,jumlah,harga) values(_id_pembelian,_id_produk,_jumlah,_harga);
	UPDATE produk SET jumalah=jumlah-_jumlah WHERE id_produk=_id_produk;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_detail_pengeluaran` (`_id_issue` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` INT(11), `_harga` INT(11), `_id_suplier` INT(11), `_id_rec` VARCHAR(100), `_kode` INT(11))  BEGIN
	
	insert into detail_pengeluaran(id_issue,id_item,jumlah,harga,id_suplier,id_rec)
	values(_id_issue,_id_item,_jumlah,_harga,_id_suplier,_id_rec);
	
	if(_kode=1) then
		SET@idSO=(SELECT id_so FROM pengeluaran_barang WHERE pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_penjualan WHERE id_so=@idSO);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		if(@count2>=@count1) then
			update penjualan set `status`=3 where id_so=@idSO;
			update pengeluaran_barang set `status`=3 where id_issue=_id_issue;
		end if;
	elseif(_kode=2) then
		set@idPro=(select id_produksi from pengeluaran_barang where pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_produksi WHERE id_produksi=@idPro);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		IF(@count2>=@count1) THEN
			UPDATE produksi SET `status`=3 WHERE id_produksi=@idPro;
			UPDATE pengeluaran_barang SET `status`=3 WHERE id_issue=_id_issue;
		END IF;
	ELSEIF(_kode=3) THEN
		SET@idPro=(SELECT pengeluaran_barang.`id_produkService` FROM pengeluaran_barang WHERE pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_produkservice WHERE id_produkService=@idPro);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		IF(@count2>=@count1) THEN
			UPDATE produkservice SET `status`=3 WHERE id_produkService=@idPro;
			UPDATE pengeluaran_barang SET `status`=3 WHERE id_issue=_id_issue;
		END IF;
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanDefect` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_def,id_rec,id_issue,defect.`id_item`,nama_item,nama_satuan,nama_tipe_item,jumlah,hargaSatuan,tanggal,keterangan FROM defect 
	INNER JOIN item_master ON item_master.`id_item`=defect.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE ((`status`=1 and defect.`tanggal`>=_tgl_awal and defect.`tanggal`<=_tgl_akhir)
	 OR (`status`=2 and defect.`tanggal`>=_tgl_awal and defect.`tanggal`<=_tgl_akhir)) and defect.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanPembelian` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penerimaan_barang.`id_rec` ,tanggal_receive,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir,nama_suplier
	FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON detail_penerimaan.`id_rec`=penerimaan_barang.`id_rec`
	INNER JOIN item_master ON detail_penerimaan.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penerimaan_barang.`id_petugas`=petugas.`id_petugas`
	INNER JOIN suplier ON penerimaan_barang.`id_suplier`=suplier.`id_suplier`
	where penerimaan_barang.`status`=3 and penerimaan_barang.`tanggal_receive`>=_tgl_awal 
	and penerimaan_barang.`tanggal_receive`<=_tgl_akhir and penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanPenjualan` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penjualan.`id_so`,tanggal,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir FROM detail_penjualan 
	INNER JOIN penjualan ON detail_penjualan.`id_so`=penjualan.`id_so`
	INNER JOIN item_master ON detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	where penjualan.`status`=3 and penjualan.`tanggal`>=_tgl_awal and penjualan.`tanggal`<=_tgl_akhir and penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanService` (`_tgl_awal` DATE, `_tgl_akhir` DATE)  BEGIN
	SELECT produkservice.`id_produkService`,produkservice.`tanggal`,petugas.`nama` AS nama_petugas,
	customer.`nama`AS nama_customer ,item_master.`id_item`,nama_item,nama_satuan,nama_tipe_item,detail_produkservice.`jumlah`
	FROM detail_produkservice
	INNER JOIN produkservice ON produkservice.`id_produkService`=detail_produkservice.`id_produkService`
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN item_master ON item_master.`id_item`=detail_produkservice.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE (produkservice.`status`=3 and produkservice.`tanggal`>=_tgl_awal) 
	and (produkservice.`status`=3 AND produkservice.`tanggal`<=_tgl_akhir);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_userId` (`_email` VARCHAR(200))  BEGIN
	select id_petugas from petugas where email=_email;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungCustomer` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(id_customer) as jumlah from customer where customer.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPembelian` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(purchasing.`id_po`) as jumlah from purchasing where purchasing.`id_pemilik`=_id_pemilik ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPembelianSukses` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(purchasing.`id_po`) AS jumlah FROM purchasing where purchasing.`status`=3 and purchasing.`id_pemilik`=_id_pemilik ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenerimaan` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penerimaan_barang.`id_rec`) AS jumlah FROM penerimaan_barang where penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPengeluaran` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(pengeluaran_barang.`id_issue`)AS jumlah FROM pengeluaran_barang where pengeluaran_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenjualan` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penjualan.`id_so`) AS jumlah FROM penjualan where penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenjualanSukses` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penjualan.`id_so`) AS jumlah FROM penjualan where penjualan.`status`=3 and penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungProduk` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(item_master.`id_item`) AS jumlah FROM item_master where item_master.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungProduksi` ()  BEGIN
	select count(produksi.`id_produksi`) as jumlah from produksi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungSupplier` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(id_suplier) as jumlah from suplier where suplier.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_inputProduk_supplier` (`_idSupplier` INT(11), `_id_item` VARCHAR(100), `_id_pemilik` VARCHAR(100), `_harga` INT(11))  BEGIN
	set@cek=(select id_suplier from detail_suplier where id_suplier=_idSupplier and id_item=_id_item);
	if(@cek) then
		select -1 as cek;
	else
		insert into detail_suplier(id_suplier,id_item,id_pemilik,harga) values(_idSupplier,_id_item,_id_pemilik,_harga);
		select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_bom` (`_id_produk` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE)  BEGIN
	
		insert into bom (id_produk,id_item,jumlah) values(_id_produk,_id_item,_jumlah);
		select 1 as cek;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_customer` (`_idCustomer` VARCHAR(100), `_id_institut` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` VARCHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	set@cek=(select id_customer from customer where id_customer=_idCustomer);
	if(@cek) then
		select -1 as cek;
	else
		insert into `customer` (id_customer,id_institut,nama,jenkel,alamat,hp,email,tgl,jabatan,id_pemilik) 
		values(_idCustomer,_id_institut,_nama,_jenkel,_alamat,_hp,_email,now(),_jabatan,_id_pemilik);
		select 0 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_defect` (`_id` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	IF(_kode=1)THEN
		INSERT INTO defect (id_rec,id_item,jumlah,hargaSatuan,keterangan,tanggal,id_pemilik) VALUES(_id,_id_item,_jumlah,_harga,1,now(),_id_pemilik);
	ELSEIF(_kode=2) THEN
		INSERT INTO defect (id_issue,id_item,jumlah,hargaSatuan,keterangan,tanggal,id_pemilik) VALUES(_id,_id_item,_jumlah,_harga,2,now(),_id_pemilik);
	ELSEIF(_kode=3) THEN
		INSERT INTO defect (id_issue,id_item,jumlah,hargaSatuan,keterangan,tanggal,id_pemilik) VALUES(_id,_id_item,_jumlah,_harga,3,now(),_id_pemilik);
	ELSEIF(_kode=4) THEN
		INSERT INTO defect (id_issue,id_item,jumlah,hargaSatuan,keterangan,tanggal,id_pemilik) VALUES(_id,_id_item,_jumlah,_harga,4,NOW(),_id_pemilik);
	END IF;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailKeluarGudang` (`_id_tran` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_id_suplier` INT(11))  BEGIN
	update detail_penjualan set jumlah_keluar=_jumlah where id_transaksi=_id_tran and id_item=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailPenjualan` (`_id_so` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_id_suplier` INT(11))  BEGIN
	INSERT INTO detail_penjualan (id_so,id_item,jumlah,harga,id_suplier)
	VALUES(_id_so,_id_item,_jumlah,_harga,_id_suplier);
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailProduksi` (`_id_produksi` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_hargaSatuan` INT(11), `_id_suplier` INT(11))  BEGIN
	INSERT INTO detail_produksi(id_produksi,id_item,jumlah,hargaSatuan,id_suplier) VALUES(_id_produksi,_id_item,_jumlah,_hargaSatuan,_id_suplier);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailPurchasing` (`_id_purchasing` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_hargaSatuan` INT(11))  BEGIN
	insert into detail_purchasing(id_purchasing,id_item,jumlah,hargaSatuan) values(_id_purchasing,_id_item,_jumlah,_hargaSatuan);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailServProduk` (`_id_tran` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_id_suplier` INT(11))  BEGIN
	INSERT INTO detail_produkservice(id_produkService,id_item,jumlah,harga,id_suplier) 
	VALUES(_id_tran,_id_item,_jumlah,_harga,_id_suplier);
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_gudang` (`_id_rec` VARCHAR(200), `_id_tran` VARCHAR(200), `_id_item` VARCHAR(200), `_jumlah` DOUBLE, `_harga` INT(11), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	if(_kode=1) then
	set@cekPO=(select jumlah from detail_purchasing where id_purchasing=_id_tran and id_item=_id_item);
	if (_jumlah>=@cekPO) then
		insert into gudang (id_rec,id_item,jumlah,hargaSatuan,id_pemilik) values(_id_rec,_id_item,_jumlah,_harga,_id_pemilik);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik);
		set@count1=(SELECT SUM(jumlah)  FROM detail_purchasing WHERE id_purchasing=_id_tran);
		set@count2=(select sum(jumlah) from gudang where id_rec=_id_rec);
		if (@count2>=@count1) then 
			update purchasing set `status`=3 where id_po=_id_tran;
			UPDATE penerimaan_barang SET `status`=3 WHERE id_rec=_id_rec;
		end if;
	else
		INSERT INTO gudang (id_rec,id_item,jumlah,hargaSatuan,`status`,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,(_jumlah-@cekPO),_id_pemilik);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,`status`,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,(_jumlah-@cekPO),_id_pemilik);
		INSERT INTO defect (id_rec,id_item,jumlah,hargaSatuan,tanggal,keterangan,`status`,id_pemilik) 
		VALUES(_id_rec,_id_item,(@cekPO-_jumlah),_harga,now(),1,1,_id_pemilik);
		UPDATE purchasing SET `status`=2 WHERE id_po=_id_tran;
		UPDATE penerimaan_barang SET `status`=2 WHERE id_rec=_id_rec; 
	end if;
	elseif(_kode=2)then
		INSERT INTO gudang (id_rec,id_item,jumlah,hargaSatuan,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik);
		SET@count1=(SELECT SUM(jumlah_item)  FROM produksi WHERE id_produksi=_id_tran);
		SET@count2=(SELECT SUM(jumlah) FROM gudang WHERE id_rec=_id_rec);
		IF (@count2>=@count1) THEN 
			UPDATE produksi SET `status`=6 WHERE id_produksi=_id_tran;
			UPDATE penerimaan_barang SET `status`=3 WHERE id_rec=_id_rec;
		END IF;
		
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_institusi` (`_id_institut` VARCHAR(100), `_nama` VARCHAR(300), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200))  BEGIN
	set@cek=(select `id_institusi` from institusi where `id_institusi`=_id_institut);
	if(@cek) then
		select -1 as cek;
	else
		INSERT INTO `institusi` (id_institusi,nama_institusi,alamat_institusi,telephone_institusi,email,tgl_registrasi) 
		VALUES(_id_institut,_nama,_alamat,_hp,_email,NOW());
		select 0 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_item` (`_id_item` VARCHAR(100), `_nama_item` VARCHAR(200), `_harga` INT(50), `_satuan` VARCHAR(50), `_deskripsi` TEXT, `_link_photo` VARCHAR(200), `id_pemilik` VARCHAR(200))  BEGIN
	SET@cek=(SELECT count(*) from item_master where id_item=_id_item);
	if (@cek) then
		select -1 as cek;
	else
	insert into item_master(id_item,nama_item,item_harga,satuan,deskripsi,link_photo,id_pemilik) values(_id_item,_nama_item,_harga,_satuan,_deskripsi,_link_photo,id_pemilik);
	select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_keluarGudang` (`_id_issue` VARCHAR(100), `_id` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` INT(11), `_total` BIGINT(20), `_tgl` DATETIME, `_kurir` VARCHAR(100), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	set@idPet=(select id_petugas from petugas where email=_email);
	if _kode=1 then
		insert into pengeluaran_barang(id_issue,id_so,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		values(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	elseif _kode=2 then
		INSERT INTO pengeluaran_barang(id_issue,id_produksi,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		VALUES(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	ELSEIF _kode=3 THEN
		INSERT INTO pengeluaran_barang(id_issue,id_produkService,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		VALUES(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_penerimaan` (`_id_rec` VARCHAR(100), `_id_po` VARCHAR(200), `_idPenerima` VARCHAR(200), `_tgl` DATE, `_idSuplier` INT(11), `_totalHarga` BIGINT(20), `_kurir` VARCHAR(100), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	if(_kode=1) then
		INSERT INTO penerimaan_barang (id_rec,id_po,id_petugas,tanggal_receive,id_suplier,totalHarga,kurir,id_pemilik) 
		VALUES(_id_rec,_id_po,_idPenerima,_tgl,_idSuplier,_totalHarga,_kurir,_id_pemilik);
	elseif(_kode=2) then
		INSERT INTO penerimaan_barang (id_rec,id_produksi,id_petugas,tanggal_receive,id_suplier,totalHarga,kurir) 
		VALUES(_id_rec,_id_po,_idPenerima,_tgl,_idSuplier,_totalHarga,_kurir);
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_penjualan` (`_id_so` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` VARCHAR(100), `_total` BIGINT(20), `_tgl` DATETIME, `_kurir` VARCHAR(100), `_ongkir` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SET@id_petugas=(SELECT id_petugas FROM petugas WHERE email=_email);
	INSERT INTO penjualan (id_so,id_petugas,id_customer,tanggal,total,kurir,ongkir,id_pemilik) 
	VALUES(_id_so,@id_petugas,_id_customer,_tgl,_total,_kurir,_ongkir,_id_pemilik);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_petugas` (`_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_email` VARCHAR(200), `_passwd` VARCHAR(200))  BEGIN
	set@cek=(select count(*) from petugas where id_petugas=_ktp);
	SET@cek2=(SELECT Count(*) email FROM petugas WHERE email=_email);
	if (@cek or @cek2) then
		select -1 as cek;
	else
		INSERT INTO `petugas` (id_petugas,nama,email,tgl,`password`,privilege,jabatan) 
		VALUES(_ktp,_nama,_email,now(),md5(_passwd),1,'user');
		select 0 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_produkService` (`_id` VARCHAR(100), `_id_service` INT(11), `_total` BIGINT(20), `_teknisi` VARCHAR(200))  BEGIN
	insert into produkservice(id_produkService,id_service,total,tanggal,teknisi)
	values(_id,_id_service,_total,now(),_teknisi);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_produksi` (`_id_po` VARCHAR(100), `_email` VARCHAR(200), `_jumlah` INT(11), `_tanggal_po` DATE, `_totalHarga` BIGINT(20), `_id_produk` VARCHAR(100))  BEGIN
	SET@idPetugas=(SELECT id_petugas FROM petugas WHERE email=_email);
	INSERT INTO produksi (id_produksi,id_petugas,tanggal_po,totalHarga,jumlah_item,id_produk) VALUES(_id_po,@idPetugas,_tanggal_po,_totalHarga,_jumlah,_id_produk);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_produksiToGudang` (`_id_rec` VARCHAR(100), `_id_po` VARCHAR(200), `_id_petugas` VARCHAR(200), `_id_suplier` INT(11), `_totalHarga` BIGINT(20), `_id_item` VARCHAR(200), `_jumlah` INT(11), `_harga` INT(11))  BEGIN
	insert into penerimaan_barang(id_rec,id_produksi,id_petugas,tanggal_receive,`status`,id_suplier,totalHarga)
	values(_id_rec,_id_po,_id_petugas,now(),3,_id_suplier,_totalHarga);
	insert into detail_penerimaan(id_produksi,id_item,jumlah,hargaSatuan)
	values(_id_po,_id_item,_jumlah,_harga);
	INSERT INTO gudang(id_produksi,id_item,jumlah,hargaSatuan)
	VALUES(_id_po,_id_item,_jumlah,_harga);
	update produksi set `status`=2 where id_produksi=_id_po;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_purchasing` (`_id_po` VARCHAR(100), `_id_suplier` INT(11), `_email` VARCHAR(200), `_tanggal_po` DATE, `_totalHarga` BIGINT(20), `_id_pemilik` VARCHAR(100), `_ongkir` INT(11))  BEGIN
	set@idPetugas=(select id_petugas from petugas where email=_email);
	insert into purchasing (id_po,id_suplier,id_petugas,tanggal_po,totalHarga,id_pemilik,ongkir) 
	values(_id_po,_id_suplier,@idPetugas,_tanggal_po,_totalHarga,_id_pemilik,_ongkir);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_return` (`_id_po` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11))  BEGIN
	set @cek=(select id_purchasing from gudang where id_purchasing=_id_po);
	if @cek then
		update gudang set jumalah=(jumlah+_jumlah);
		update defect set `status`=0 where id_purchasing=_id_po and id_item=_id_item;
	else
		INSERT INTO gudang (id_purchasing,id_item,jumlah,hargaSatuan) VALUES(_id_po,_id_item,_jumlah,_harga);
		UPDATE defect SET `status`=0 WHERE id_purchasing=_id_po AND id_item=_id_item;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_satuan` (`_nama_satuan` VARCHAR(50), `_kelompok` VARCHAR(50), `_deskripsi` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	insert into satuan (nama_satuan,kelompok_satuan,deskripsi_satuan,id_pemilik) values(_nama_satuan,_kelompok,_deskripsi,_id_pemilik);
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_service` (`_email` VARCHAR(200), `_id_customer` VARCHAR(100), `_subject` VARCHAR(500), `_keluhan` TEXT, `_tgl_open` DATETIME, `_status` INT(11))  BEGIN
	set@idpetugas=(select id_petugas from petugas where email=_email);
	INSERT INTO `service` (id_petugas,id_customer,`subject`,keluhan,tgl_open,`status`) 
	VALUES(@idpetugas,_id_customer,_subject,_keluhan,_tgl_open,_status);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_solving` (`_id_service` INT(11), `_tgl_solved` DATE, `_teknisi` VARCHAR(200), `_penyelesaian` TEXT, `_status` INT(11))  BEGIN
	insert into solving(id_service,teknisi,penyelesaian,tgl_solved) values(_id_service,_teknisi,_penyelesaian,_tgl_solved);
	update `service` set `status`=_status where id_service=_id_service;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_suplier` (`_nama` VARCHAR(200), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_deskripsi` TEXT, `_id_pemilik` VARCHAR(100))  BEGIN
	set@cek=(select id_suplier from suplier where lower(nama_suplier)=lower(_nama));
	if(@cek) then
		select -1 as cek;
	else
		INSERT INTO `suplier` (nama_suplier,alamat,hp,email,deskripsi,tgl,id_pemilik) VALUES(_nama,_alamat,_hp,_email,_deskripsi,now(),_id_pemilik);
		select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_tipe` (`_nama_tipe` VARCHAR(100), `_deskripsi` TEXT)  BEGIN
	insert into tipe_item(nama_tipe_item,deskripsi) values(_nama_tipe,_deskripsi);
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_defect` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_def,id_rec,id_issue,defect.`id_item`,nama_item,nama_satuan,nama_tipe_item,jumlah,hargaSatuan,tanggal,keterangan FROM defect 
	INNER JOIN item_master ON item_master.`id_item`=defect.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE ((`status`=1 AND defect.`tanggal`>=now() AND defect.`tanggal`<=now())
	 OR (`status`=2 AND defect.`tanggal`>=now() AND defect.`tanggal`<=now())) and defect.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_pembelian` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penerimaan_barang.`id_rec` ,tanggal_receive,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir,nama_suplier
	FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON detail_penerimaan.`id_rec`=penerimaan_barang.`id_rec`
	INNER JOIN item_master ON detail_penerimaan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penerimaan_barang.`id_petugas`=petugas.`id_petugas`
	INNER JOIN suplier ON penerimaan_barang.`id_suplier`=suplier.`id_suplier`
	WHERE penerimaan_barang.`status`=3 and penerimaan_barang.`tanggal_receive`>=now() AND penerimaan_barang.`tanggal_receive`<=now() and penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_penjualan` (`_id_pemilik` VARCHAR(100))  BEGIN
	select penjualan.`id_so`,tanggal,nama_item,sat.nama_satuan,jumlah,petugas.`nama` as nama_petugas,kurir from detail_penjualan 
	inner join penjualan on detail_penjualan.`id_so`=penjualan.`id_so`
	inner join item_master on detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	inner join petugas on penjualan.`id_petugas`=petugas.`id_petugas`
	WHERE penjualan.`status`=3 and penjualan.`tanggal`>=now() AND penjualan.`tanggal`<=now() and penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_service` ()  BEGIN
	SELECT produkservice.`id_produkService`,produkservice.`tanggal`,petugas.`nama` AS nama_petugas,
	customer.`nama`AS nama_customer ,item_master.`id_item`,nama_item,nama_satuan,nama_tipe_item,detail_produkservice.`jumlah`
	FROM detail_produkservice
	INNER JOIN produkservice ON produkservice.`id_produkService`=detail_produkservice.`id_produkService`
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN item_master ON item_master.`id_item`=detail_produkservice.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE (produkservice.`status`=3 AND produkservice.`tanggal`>=now()) 
	AND (produkservice.`status`=3 AND produkservice.`tanggal`<=now());
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listProduk_perSuplier` (`_id_suplier` INT(11))  BEGIN
	SELECT detail_suplier.`harga`,nama_item,detail_suplier.`id_item`,sat.nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	WHERE detail_suplier.`id_suplier`=_id_suplier GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_bahanProduksi` (`_id_produk` VARCHAR(100))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN bom ON bom.`id_item`=item_master.`id_item`
	WHERE bom.`id_produk`=_id_produk
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_bom` ()  BEGIN
	select item_master.`id_item`,nama_item,nama_satuan,nama_tipe_item,item_master.`deskripsi`,link_photo from bom 
	inner join item_master on item_master.`id_item`=bom.`id_produk`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe` 
	group by bom.`id_produk`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_customer` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer INNER JOIN institusi ON customer.`id_institut`=institusi.`id_institusi` 
	 where customer.`id_pemilik`=_id_pemilik order by nama asc;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_defect` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_def,id_rec,id_issue,nama_item,item_master.`id_item`,jumlah,hargaSatuan,nama_tipe_item,nama_satuan,`status`
	FROM defect INNER JOIN item_master ON defect.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where defect.`id_pemilik`=_id_pemilik;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_institusi` ()  BEGIN
	select * from institusi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_item` (IN `_id_pemilik` VARCHAR(100))  BEGIN
	select item_master.id_item ,nama_item, item_harga , link_photo , sat.nama_satuan as satuan ,item_master.deskripsi from item_master
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe` where item_master.`id_pemilik`=_id_pemilik order by nama_item asc;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_penerimaan` ()  BEGIN
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,1 as kode FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier` where `status`=1 or `status`=2 or `status`=3
union
	SELECT produksi.`id_petugas`,id_produksi,petugas.`nama` AS nama_petugas,"PT.Miconos" AS nama_suplier,tanggal_po,totalHarga,`status`,2 AS kode 
	FROM produksi 
	INNER JOIN petugas ON produksi.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON suplier.`id_suplier`=3 WHERE `status`=5 or `status`=6;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_pengeluaran` ()  BEGIN
	select penjualan.`id_so`,petugas.nama as nama_petugas,customer.`nama` as nama_customer,tanggal,total,kurir,`status`, 1 as kode
	from penjualan 
	inner join petugas on petugas.`id_petugas`=penjualan.`id_petugas`
	inner join customer on customer.`id_customer`=penjualan.`id_customer`
	where penjualan.`status`=1 or penjualan.`status`=2 or `status`=3
	union
	SELECT produksi.`id_produksi`,petugas.nama AS nama_petugas,"Produksi" as nama_customer,tanggal_po,totalHarga,NULL AS kurir,`status`,2 as kode
	FROM produksi
	INNER JOIN petugas ON petugas.`id_petugas`=produksi.`id_petugas`
	WHERE produksi.`status`=1 OR produksi.`status`=2 or `status`=3
	union 
	SELECT produkservice.`id_produkService`,petugas.nama AS nama_petugas,customer.`nama` as nama_customer,tanggal,total,"Service" AS kurir,produkservice.`status`,3 AS kode
	FROM produkservice
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	WHERE produkservice.`status`=1 or produkservice.`status`=2 or produkservice.`status`=3 ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_penjualan` ()  BEGIN
	
	select id_so,petugas.`nama` as nama_petugas,customer.`nama` as nama_customer,tanggal,total,kurir,`status` 
	from penjualan 
	inner join customer on penjualan.`id_customer`=customer.`id_customer` 
	inner join petugas on penjualan.`id_petugas`=petugas.`id_petugas` order by tanggal desc;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_petugas` ()  BEGIN
	SELECT * FROM petugas;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_produk` (`_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where item_master.`id_pemilik`=_id_pemilik
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_produksi` ()  BEGIN
	select id_produksi,produksi.`id_petugas`,id_produk,nama_item,petugas.`nama` as nama_petugas,tanggal_po,totalHarga,`status`,jumlah_item from produksi 
	inner join petugas on petugas.`id_petugas`=produksi.`id_petugas`
	inner join item_master on item_master.`id_item`=produksi.`id_produk`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_purchasing` (`nama` VARCHAR(200))  BEGIN
	select purchasing.`id_petugas`,id_po,petugas.`nama` as nama_petugas,nama_suplier,tanggal_po,totalHarga,`status` from purchasing 
	inner join petugas on purchasing.`id_petugas`=petugas.`id_petugas` 
	inner join suplier on purchasing.`id_suplier`=suplier.`id_suplier` where id_petugas=nama order by tanggal_po desc;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_satuan` (`_id_pemilik` VARCHAR(100))  BEGIN
	select * from satuan where satuan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_service` ()  BEGIN
	select id_service,`subject`,keluhan,tgl_open,`status`,customer.`nama` as nama_customer,customer.`id_customer`,petugas.`nama` as nama_petugas,service.`id_petugas`,service.`id_customer` 
	from service inner join customer on service.`id_customer`=customer.`id_customer`
	inner join petugas on service.`id_petugas`=petugas.`id_petugas`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_suplier` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT * FROM suplier where id_pemilik=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_tipeItem` ()  BEGIN
	select * from tipe_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_transaksiPerCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	select penjualan.`id_so`,tanggal,nama_item,jumlah,harga
	from detail_penjualan 
	inner join item_master on detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN penjualan on detail_penjualan.`id_so`=penjualan.`id_so`
	  where id_customer=_id_customer;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_unsolvedService` ()  BEGIN
	select * from service where `status` <> "3";
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_login` (`_email` VARCHAR(200), `_passwd` VARCHAR(50))  BEGIN
	set@cek=(select 1 from petugas where email=_email and `password`=md5(_passwd));
	if @cek=1 then
		select 1 as A;
	else select 0 as A;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_customer` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer INNER JOIN institusi ON customer.`id_institut`=institusi.`id_institusi`
	 where customer.`id_pemilik`=_id_pemilik
	 ORDER BY nama ASC LIMIT _limit offset _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_pembelian` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,ongkir FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier`
	where purchasing.`id_pemilik`=_id_pemilik
	ORDER BY tanggal_po DESC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penerimaan` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN SELECT * FROM(
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,1 AS kode,ongkir FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier` WHERE (`status`=1 OR `status`=2 OR `status`=3) and purchasing.id_pemilik=_id_pemilik
UNION
	SELECT produksi.`id_petugas`,id_produksi,petugas.`nama` AS nama_petugas,"PT.Miconos" AS nama_suplier,tanggal_po,totalHarga,`status`,2 AS kode,"1"
	FROM produksi 
	INNER JOIN petugas ON produksi.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON suplier.`id_suplier`=3 WHERE (`status`=5 OR `status`=6) AND produksi.id_pemilik=_id_pemilik) AS tab ORDER BY tab.tanggal_po DESC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_pengeluaran` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	select * from (SELECT penjualan.`id_so`,petugas.nama AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,kurir,`status`, 1 AS kode, ongkir
	FROM penjualan 
	INNER JOIN petugas ON petugas.`id_petugas`=penjualan.`id_petugas`
	INNER JOIN customer ON customer.`id_customer`=penjualan.`id_customer`
	WHERE (penjualan.`status`=1 OR penjualan.`status`=2 OR `status`=3) and penjualan.`id_pemilik`=_id_pemilik
	UNION
	SELECT produksi.`id_produksi`,petugas.nama AS nama_petugas,"Produksi" AS nama_customer,tanggal_po,totalHarga,NULL AS kurir,`status`,2 AS kode, "1"
	FROM produksi
	INNER JOIN petugas ON petugas.`id_petugas`=produksi.`id_petugas`
	WHERE (produksi.`status`=1 OR produksi.`status`=2 OR `status`=3) and produksi.`id_pemilik`=_id_pemilik
	UNION 
	SELECT produkservice.`id_produkService`,petugas.nama AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,"Service" AS kurir,produkservice.`status`,3 AS kode, "1"
	FROM produkservice
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	WHERE (produkservice.`status`=1 OR produkservice.`status`=2 OR produkservice.`status`=3) and produkservice.`id_pemilik`=_id_pemilik) as tab order by tab.tanggal desc LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penjualan` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_so,petugas.`nama` AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,kurir,`status` 
	FROM penjualan 
	INNER JOIN customer ON penjualan.`id_customer`=customer.`id_customer` 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	where penjualan.`id_pemilik`=_id_pemilik
	ORDER BY tanggal DESC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_produk` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where penerimaan_barang.`id_pemilik`= `_id_pemilik`
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_produksi` (`_start` INT(11), `_limit` INT(11))  BEGIN
	SELECT id_produksi,produksi.`id_petugas`,id_produk,nama_item,petugas.`nama` AS nama_petugas,tanggal_po,totalHarga,`status`,jumlah_item FROM produksi 
	INNER JOIN petugas ON petugas.`id_petugas`=produksi.`id_petugas`
	INNER JOIN item_master ON item_master.`id_item`=produksi.`id_produk` order by produksi.`tanggal_po` desc limit _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_supplier` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	select * from suplier where id_pemilik=_id_pemilik order by nama_suplier asc LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pembelian` (`_id_transaksi` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` INT(11), `_total` BIGINT(20), `_tgl` DATETIME)  BEGIN
	set@id_petugas=(select id_petugas from petugas where email=_email);
	insert into pembelian (id_transaksi,id_petugas,id_customer,tanggal,total) values(_id_transaksi,@id_petugas,_id_customer,_tgl,_total);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengurangan_stok` (`_id_rec` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE)  BEGIN
	UPDATE gudang SET jumlah=(jumlah-_jumlah) WHERE id_rec=_id_rec AND id_item=_id_item;
	set@cek=(select jumlah from gudang WHERE id_rec=_id_rec AND id_item=_id_item);
	set@set=(SELECT `status` FROM gudang WHERE id_rec=_id_rec AND id_item=_id_item);
	if(@cek<1 and @set=0)then
		delete from gudang WHERE id_rec=_id_rec AND id_item=_id_item;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_returning_defect` (`_id_def` INT(11), `_id` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_id_issue` VARCHAR(100))  BEGIN
	update defect set `status`=1,tanggal=now() where id_def=_id_def and id_item=_id_item AND hargaSatuan=_harga;
	set@cekGuna=(select keterangan from defect where id_def=_id_def and id_item=_id_item and hargaSatuan=_harga);
	SET@po=(SELECT id_po FROM penerimaan_barang WHERE penerimaan_barang.`id_rec`=_id);
	if(@cekGuna=1) then 
		update gudang set jumlah=jumlah+_jumlah, `status`=0 where id_rec=_id and id_item=_id_item AND hargaSatuan=_harga;
		UPDATE detail_penerimaan SET jumlah=jumlah+_jumlah, `status`=0 WHERE id_rec=_id AND id_item=_id_item AND hargaSatuan=_harga;
		update penerimaan_barang set `status`=3 where id_rec=_id;
		
		UPDATE purchasing SET `status`=3 where id_po=@po;
		update defect set `status`=0 where id_def=_id_def;
	elseif(@cekGuna=2) then
		set@rec=(select id_rec from detail_pengeluaran where id_issue=_id_issue and id_item=_id_item);
		UPDATE gudang SET jumlah=jumlah+_jumlah, `status`=0 WHERE id_rec=@rec AND id_item=_id_item AND hargaSatuan=_harga;
		UPDATE defect SET `status`=0 WHERE id_def=_id_def;
	ELSEIF(@cekGuna=3) THEN
		SET@rec=(SELECT id_rec FROM detail_pengeluaran WHERE id_issue=_id_issue AND id_item=_id_item);
		UPDATE gudang SET jumlah=jumlah+_jumlah, `status`=0 WHERE id_rec=@rec AND id_item=_id_item AND hargaSatuan=_harga;
		UPDATE defect SET `status`=0 WHERE id_def=_id_def;
	ELSEIF(@cekGuna=4) THEN
		SET@rec=(SELECT id_rec FROM detail_pengeluaran WHERE id_issue=_id_issue AND id_item=_id_item);
		UPDATE gudang SET jumlah=jumlah+_jumlah, `status`=0 WHERE id_rec=@rec AND id_item=_id_item AND hargaSatuan=_harga;
		UPDATE defect SET `status`=0 WHERE id_def=_id_def;
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianListProduk` (`_id_item` VARCHAR(100))  BEGIN
	SELECT nama_suplier,detail_suplier.`id_suplier`,nama_item,detail_suplier.`id_item`,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	inner join suplier on suplier.`id_suplier`=detail_suplier.`id_suplier`
	WHERE detail_suplier.`id_item`=_id_item GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianPembelian` (IN `_id_pur` VARCHAR(100))  BEGIN
	select DISTINCT  detail_purchasing.`id_item`,nama_item,nama_satuan,nama_tipe_item,detail_purchasing.jumlah,tab.`jumlah`as jml_keluar,detail_purchasing.hargaSatuan 
	from detail_purchasing inner join item_master on item_master.`id_item`=detail_purchasing.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	left join (select penerimaan_barang.`id_po`,jumlah,id_item from detail_penerimaan inner join penerimaan_barang on penerimaan_barang.`id_rec`=detail_penerimaan.`id_rec`
	where penerimaan_barang.`id_po`=_id_pur) as tab on tab.id_po=detail_purchasing.`id_purchasing` AND tab.id_item=detail_purchasing.`id_item`
	where detail_purchasing.`id_purchasing`=_id_pur ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianPenjualan` (`_id_tran` VARCHAR(100))  BEGIN
	select detail_penjualan.`id_item`,nama_item,nama_satuan,nama_tipe_item,detail_penjualan.jumlah,tab.`jumlah` as keluar,harga from detail_penjualan
	inner join item_master on item_master.`id_item`=detail_penjualan.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	LEFT JOIN (SELECT pengeluaran_barang.`id_so`,jumlah,id_item FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	WHERE pengeluaran_barang.`id_so`=_id_tran) AS tab ON tab.id_so=detail_penjualan.`id_so` AND tab.id_item=detail_penjualan.`id_item`
	where detail_penjualan.`id_so`=_id_tran;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianProduksi` (`_id_tran` VARCHAR(100))  BEGIN
	SELECT detail_produksi.id_produksi,detail_produksi.`id_item`,nama_item,hargaSatuan,nama_satuan,nama_tipe_item,detail_produksi.`jumlah`,tab.jumlah AS keluar
	FROM detail_produksi
	INNER JOIN item_master ON item_master.`id_item`=detail_produksi.`id_item`
	INNER JOIN satuan sat ON sat.`id_satuan`=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	LEFT JOIN (SELECT pengeluaran_barang.`id_produksi`,jumlah,id_item FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	WHERE pengeluaran_barang.`id_produksi`=_id_tran) AS tab ON tab.id_produksi=detail_produksi.`id_produksi` AND tab.id_item=detail_produksi.`id_item`
	WHERE detail_produksi.id_produksi=_id_tran; 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincian_bom` (`_id_produk` VARCHAR(100))  BEGIN
	select bom.`id_item`,nama_item,jumlah,nama_satuan,nama_tipe_item,link_photo,item_master.deskripsi from bom 
	inner join item_master on item_master.`id_item`=bom.`id_item`
	inner join satuan on satuan.`id_satuan`=item_master.`satuan`
	left join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where bom.`id_produk`=_id_produk;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincian_viewSO` (`_id_so` VARCHAR(100))  BEGIN
	select customer.`id_customer`,customer.`nama`as nama_customer,petugas.`nama`as nama_petugas,penjualan.`tanggal`,kurir,penjualan.`total`,penjualan.`ongkir` from penjualan
	inner join petugas on petugas.`id_petugas`=penjualan.`id_petugas`
	inner join customer on customer.`id_customer`=penjualan.`id_customer`
	where penjualan.`id_so`=_id_so;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_stok_gudang` (`_id_item` VARCHAR(100))  BEGIN
	SELECT "Masuk" AS asal,SUM(detail_penerimaan.`jumlah`)AS jumlah,nama_item FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON penerimaan_barang.`id_rec`=detail_penerimaan.`id_rec`
	inner join item_master on item_master.`id_item`=detail_penerimaan.`id_item`
	WHERE (penerimaan_barang.`status`=3 AND detail_penerimaan.`id_item`=_id_item) 
	or (penerimaan_barang.`status`=2 AND detail_penerimaan.`id_item`=_id_item)
	UNION 
	SELECT "Gudang" AS asal, SUM(gudang.`jumlah`) AS jumlah,nama_item FROM gudang
	inner join item_master on item_master.`id_item`=gudang.`id_item`
	WHERE gudang.`id_item`=_id_item
	UNION
	SELECT "Keluar" AS asal,SUM(detail_pengeluaran.`jumlah`) AS jumlah,nama_item FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	inner join item_master on item_master.`id_item`=detail_pengeluaran.`id_item`
	WHERE pengeluaran_barang.`status`=3 AND detail_pengeluaran.`id_item`=_id_item
	UNION
	SELECT "Rusak" AS asal, SUM(defect.`jumlah`)AS jumlah,nama_item FROM defect 
	inner join item_master on item_master.`id_item`=defect.`id_item`
	WHERE defect.`id_item`=_id_item AND defect.`status`>=1;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ubahPassword` (`_id_petugas` VARCHAR(100), `_pwdLama` VARCHAR(200), `_pwdBaru` VARCHAR(200))  BEGIN
	set@cek=(select id_petugas from petugas where id_petugas=_id_petugas and `password`=md5(_pwdLama));
	if (@cek) then
		update petugas set `password`=md5(_pwdBaru) where id_petugas=_id_petugas;
		select 1 as cek;
	else 
		select -1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateCustomer` (`_institusi` VARCHAR(100), `_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` CHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(200))  BEGIN
    
	 
	UPDATE customer SET id_institut=_institusi, nama=_nama, jenkel=_jenkel,alamat=_alamat, hp=_hp, email=_email,jabatan=_jabatan WHERE id_customer=_ktp;
	SELECT 1 AS cek;
	 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateInstitusi` (`_id_institusi` VARCHAR(200), `_nama` VARCHAR(300), `_alamat` VARCHAR(500), `_telephone` VARCHAR(30), `_email` VARCHAR(200))  BEGIN
	update institusi set nama_institusi=_nama,alamat_institusi=_alamat,telephone_institusi=_telephone,email=_email
	where id_institusi=_id_institusi;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateItem` (`_id_item` VARCHAR(100), `_nama_item` VARCHAR(200), `_tipe` VARCHAR(50), `_satuan` VARCHAR(50), `_deskripsi` TEXT)  BEGIN
	update item_master set nama_item=_nama_item,tipe=_tipe,satuan=_satuan,deskripsi=_deskripsi where id_item=_id_item;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updatePetugas` (`_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` CHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(200), `_passwd` VARCHAR(200))  BEGIN
    set@passwd=(select `password` from petugas where id_petugas=_ktp);
	if@passwd=(md5(_passwd)) then 
	 
	UPDATE petugas SET nama=_nama, jenkel=_jenkel,alamat=_alamat, hp=_hp, email=_email,jabatan=_jabatan WHERE id_petugas=_ktp;
	select 1 as cek;
	 else select -1 as cek; end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateSatuan` (`_id` INT(11), `_nama_satuan` VARCHAR(50), `_kelompok` VARCHAR(50), `_deskripsi` VARCHAR(100))  BEGIN
	update satuan set nama_satuan=_nama_satuan,kelompok_satuan=_kelompok,deskripsi_satuan=_deskripsi where id_satuan=_id;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateService` (`_id_serv` INT(11), `_id_customer` VARCHAR(100), `_subject` VARCHAR(500), `_keluhan` TEXT, `_status` INT(11))  BEGIN
	update service set id_customer=_id_customer,`subject`=_subject,keluhan=_keluhan,`status`=_status where id_service=_id_serv;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_pembelian` (`_id_pur` VARCHAR(100), `_status` INT(11))  BEGIN
	update purchasing set `status`=_status where id_po=_id_pur;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_penjualan` (`_id_so` VARCHAR(100), `_status` INT(11))  BEGIN
	update penjualan set `status`=_status where id_so=_id_so;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_produksi` (`_id_produksi` VARCHAR(100), `_status` INT(11))  BEGIN
	update produksi set `status`=_status where id_produksi=_id_produksi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateSuplier` (`_id_suplier` INT(11), `_nama` VARCHAR(200), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_deskripsi` TEXT)  BEGIN
	update suplier set nama_suplier=_nama,alamat=_alamat,hp=_hp,email=_email,deskripsi=_deskripsi where id_suplier=_id_suplier;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateTipe` (`_id` INT(11), `_nama_tipe` VARCHAR(100), `_deskripsi` TEXT)  BEGIN
	update tipe_item set nama_tipe_item=_nama_tipe,deskripsi=_deskripsi where id_tipe_item=_id;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_defect` (`_id_def` INT(11), `_id_item` VARCHAR(100), `_harga` INT(11))  BEGIN
	update defect set `status`=2 where id_def=_id_def and id_item=_id_item and hargaSatuan=_harga;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_privilege` (`_id_petugas` VARCHAR(200), `_privilege` INT(11))  BEGIN
	update petugas set privilege=_privilege where id_petugas=_id_petugas;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_uploadGmbrPetugas` (`_id_petugas` VARCHAR(200), `_nama_file` VARCHAR(200))  BEGIN
	update petugas set photo_link=_nama_file where id_petugas=_id_petugas;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_uploadGmbrSuplier` (`_id_suplier` INT(11), `_photo_link` VARCHAR(200))  BEGIN
	update suplier set photo_link=_photo_link where id_suplier=_id_suplier;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewCustomer_perInstitusi` (`_id_institusi` VARCHAR(200))  BEGIN
	select * from customer where customer.`id_institut`=_id_institusi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewPO` (`_id_po` VARCHAR(200))  BEGIN
	select id_purchasing,nama_item,item_master.`id_item`,jumlah,hargaSatuan,nama_tipe_item,nama_satuan,id_suplier,purchasing.id_petugas,totalHarga,1 as kode
	from detail_purchasing inner join purchasing on purchasing.`id_po`=detail_purchasing.`id_purchasing`
	inner join item_master on detail_purchasing.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where id_purchasing=_id_po;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewPRO` (`_id_pro` VARCHAR(100))  BEGIN
	SELECT detail_produksi.`id_suplier`,suplier.`nama_suplier`,detail_produksi.`id_item`,nama_item,nama_satuan,nama_tipe_item,hargaSatuan as harga,jumlah,2 as kode 
	FROM detail_produksi
	INNER JOIN suplier ON suplier.`id_suplier`=detail_produksi.`id_suplier`
	INNER JOIN item_master ON item_master.`id_item`=detail_produksi.`id_item`
	INNER JOIN satuan sat ON sat.`id_satuan`=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where detail_produksi.`id_produksi`=_id_pro;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewPROIN` (`_id_tran` VARCHAR(100))  BEGIN
	select suplier.`id_suplier`,nama_suplier,produksi.`id_produk` as id_item,nama_item,nama_satuan,nama_tipe_item,(totalHarga/jumlah_item) as hargaSatuan,jumlah_item as jumlah,id_petugas,2 as kode from produksi
	inner join suplier on suplier.`id_suplier`=3
	inner join item_master on item_master.`id_item`=produksi.`id_produk`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where produksi.`id_produksi`=_id_tran;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewSER` (`_id_tran` VARCHAR(100))  BEGIN
	select service.`id_customer`,detail_produkservice.`id_suplier`,nama_suplier,detail_produkservice.`id_produkService`,petugas.`nama`as nama_petugas,nama_item,detail_produkservice.`id_item`,nama_satuan,nama_tipe_item,
	tanggal,harga,produkservice.`status`,jumlah,3 as kode
	from detail_produkservice
	inner join produkservice on produkservice.`id_produkService`=produkservice.`id_produkService`
	inner join service on service.`id_service`=produkservice.`id_service`
	inner join petugas on petugas.`id_petugas`=produkservice.`teknisi`
	inner join item_master on item_master.`id_item`=detail_produkservice.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	inner join suplier on suplier.`id_suplier`=detail_produkservice.`id_suplier`
	where detail_produkservice.`id_produkService`=_id_tran group by detail_produkservice.`id_item`,detail_produkservice.`harga`,detail_produkservice.`id_suplier`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewSO` (`_id_so` VARCHAR(100))  BEGIN
	select detail_suplier.`harga` as harga_beli, nama_suplier,penjualan.`id_so`,nama_item,item_master.`id_item`,jumlah,nama_satuan,nama_tipe_item,detail_penjualan.harga,jumlah,detail_penjualan.`id_suplier`,1 as kode
	from detail_penjualan 
	inner join penjualan on penjualan.`id_so`=detail_penjualan.`id_so`
	inner join suplier on suplier.`id_suplier`=detail_penjualan.`id_suplier`
	inner join item_master on item_master.`id_item`=detail_penjualan.`id_item`
	INNER JOIN detail_suplier ON suplier.`id_suplier`=detail_suplier.`id_suplier` and detail_suplier.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where penjualan.`id_so`=_id_so;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer inner join institusi on customer.`id_institut`=institusi.`id_institusi` WHERE id_customer=_id_customer;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perInstitusi` (`_id_institusi` VARCHAR(200))  BEGIN
	select * from institusi where id_institusi=_id_institusi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perItem` (`_id_item` VARCHAR(100))  BEGIN
	SELECT item_master.`id_item`,nama_item,item_master.deskripsi,link_photo,
	sat.nama_satuan,nama_tipe_item,item_master.`satuan`,item_master.`tipe`
	FROM item_master INNER JOIN satuan ON satuan.`id_satuan`=item_master.`satuan`
	inner join satuan sat on sat.`id_satuan`=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE id_item=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perPetugas` (`_id_pet` VARCHAR(200))  BEGIN
	select * from petugas where id_petugas=_id_pet;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perProduk` (`_id_item` VARCHAR(100))  BEGIN
	select * from item_master inner join satuan on satuan.`id_satuan`=item_master.`satuan` where id_item=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perProdukServ` (`_id_service` INT(11))  BEGIN
	SELECT detail_produkservice.`id_produkService`,produkservice.`id_service`,detail_produkservice.`id_item`,
	nama_item,nama_satuan,nama_tipe_item,jumlah,harga,tanggal,link_photo FROM detail_produkservice
INNER JOIN produkservice ON produkservice.`id_produkService`=detail_produkservice.`id_produkService`
INNER JOIN item_master ON item_master.`id_item`=detail_produkservice.`id_item`
INNER JOIN satuan sat ON sat.`id_satuan`=item_master.`satuan`
INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
WHERE produkservice.`id_service`=_id_service;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perService` (`_id_service` INT(11))  BEGIN
	set@solving=(select id_service from service where id_service=_id_service and `status`=3);
	
	select service.id_service,petugas.`id_petugas`,service.`id_customer`,petugas.`nama` AS nama_petugas, customer.`nama` AS nama_customer,`subject`,keluhan,tgl_open,`status`
	from service
	INNER JOIN  customer ON service.`id_customer`=customer.`id_customer`
	INNER JOIN petugas ON service.`id_petugas`=petugas.`id_petugas` WHERE service.`id_service`=_id_service;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perSolving` (`_id_service` INT(11))  BEGIN
	select id_solving,solving.`id_service`,teknisi,petugas.`nama`as nama_petugas,penyelesaian,tgl_solved,service.`status`
	from solving 
	inner join petugas on petugas.`id_petugas`=solving.`teknisi`
	inner join service on service.`id_service`=solving.`id_service`
	where solving.id_service=_id_service;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perSuplier` (`_id_suplier` INT(11))  BEGIN
	SELECT * FROM suplier WHERE id_suplier=_id_suplier;
    END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `fc_tambah` (`_id_po` INT) RETURNS INT(11) BEGIN
	declare hasil int;
	set hasil = ( SELECT SUM(jumlah) FROM gudang WHERE id_purchasing=_id_po);
	return hasil;
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `bom`
--

CREATE TABLE `bom` (
  `id_produk` varchar(100) DEFAULT NULL,
  `id_item` varchar(100) DEFAULT NULL,
  `jumlah` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` varchar(100) NOT NULL,
  `id_institut` varchar(100) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `jenkel` varchar(3) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `tgl` date NOT NULL,
  `photo_link` varchar(500) NOT NULL DEFAULT 'default/users.png',
  `jabatan` varchar(100) NOT NULL,
  `id_pemilik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `id_institut`, `nama`, `jenkel`, `alamat`, `hp`, `email`, `tgl`, `photo_link`, `jabatan`, `id_pemilik`) VALUES
('000003', '1', 'grosir', 'L', 'sfasfasfas', '033535', 'andiputra3107@gmail.com', '2017-02-25', 'default/users.png', 'sfasfa', '5113100150'),
('00001', '1', 'grosir', 'L', 'Kejawan Gebang 6 23', '087849862859', 'pengepul1@pengepul.co.id', '2017-02-25', 'default/users.png', 'pengepul', '330172906940076'),
('00002', '1', 'Retail', 'P', 'Gebang Wetan 23D', '087849862777', 'pengepul2@pengepul.co.id', '2017-02-25', 'default/users.png', 'pengepul', '330172906940076'),
('00003', '1', 'Midi', 'L', 'Keputih tambak', '058485321456', 'pengepul3@pengepul.co.id', '2017-02-25', 'default/users.png', 'pengepul', '330172906940076'),
('10', '1', 'Eden Hazard', 'L', 'Jl. London no. 10', '08123456789', 'edenhazard@gmail.com', '2017-03-06', 'default/users.png', 'Manager', '5114100100'),
('123', '1', 'NadiaRahma', 'P', 'Depok', '081384781693', 'nadiarahma@gmail.com', '2017-02-27', 'default/users.png', 'Manager', '5114100110'),
('12345678', '1', 'Peter Parker', 'L', 'Jl SukaSenang No 20', '12345678', 'ppap@gmail.com', '2017-02-27', 'default/users.png', 'CEO', '5113100139'),
('12345678232', '1', 'Ahmad Zaenal', 'L', 'Jl.Gunung Wukir no.24', '085664497937', 'ahapedia@yahoo.com', '2017-03-06', 'default/users.png', 'Teknisi', '5113100043'),
('123456789', '1', 'Suparman', 'L', 'Surabaya', '082114657983', 'suparman@superman.com', '2017-02-27', 'default/users.png', 'Direktur', '5114100014'),
('13', '1', 'Thibaut Courtois', 'L', 'Jl. London no. 13', '08567891234', 'thibautcourtois@gmail.com', '2017-03-06', 'default/users.png', 'Manager', '5114100100'),
('1312124098151', '1', 'RIpat surata', 'L', 'keputih tegal timur no 1', '08231234151', 'ripat@gmail.com', '2017-02-27', 'default/users.png', 'manager', '5114100122'),
('13567', '1', 'Andy Yemima', 'L', 'Jalan semolowaru no 59', '083831929296', 'andygate777@gmail.com', '2017-03-06', 'default/users.png', 'manager', '5114100064'),
('15114100001', '1', 'Nezar', 'L', 'Keputih Perintis', '085677874321', 'nezarmahardika@gmail.com', '2017-03-06', 'default/users.png', 'Logistic', '5114100017'),
('15114100012', '1', 'Zahrah', 'P', 'Keputih Gg I', '08568787632', 'zahrahcitra@yahoo.co.id', '2017-03-06', 'default/users.png', 'Sales', '5114100017'),
('15114100030', '1', 'Bebet', 'P', 'Keputih Gg Pasar', '08123300989', 'desyrahmi@gmail.com', '2017-03-06', 'default/users.png', 'Accounting', '5114100017'),
('15114100130', '1', 'Ennay', 'P', 'Keputih Gg Pasar', '08987768987', 'nadiarahmatin@gmail.com', '2017-03-06', 'default/users.png', 'Accounting', '5114100017'),
('15114100172', '1', 'Kania', 'P', 'Mulyosari', '08123356090', 'kaniaamalia21@gmail.com', '2017-03-06', 'default/users.png', 'Sales', '5114100017'),
('15114100181', '1', 'Anne', 'P', 'Educity', '08221314563', 'anneannisa@yahoo.com', '2017-03-06', 'default/users.png', 'Sales', '5114100017'),
('19', '1', 'Diego Costa', 'L', 'Jl. London no. 19', '08234567891', 'diegocosta@gmail.com', '2017-03-06', 'default/users.png', 'Manager', '5114100100'),
('30', '1', 'David Luiz', 'L', 'Jl. London no. 30', '08345678912', 'davidluiz@gmail.com', '2017-03-06', 'default/users.png', 'Manager', '5114100100'),
('3174020305960003', '1', 'Adiwinoto Saptorenggo', 'L', 'Jl. Melati', '081267458042', 'saptorenggoadi@yahoo.co.id', '2017-02-27', 'default/users.png', 'Mahasiswa', '5114100164'),
('3467823648', '1', 'Wawan', 'L', 'Surabaya Manyar kertojoyo', '087654321467', 'Wawan@gmail.com', '2017-03-06', 'default/users.png', 'Pemimpin Wilayah', '5114100167'),
('5113100002', '1', 'Nyoman', 'L', 'Mulyoz', '123', '1@1.1', '2017-03-06', 'default/users.png', 'Manager', '5114100181'),
('5113100160', '1', 'Andi Putra', 'L', 'sdfaas', '12389217', 'shdksaj@jshdkjsah.com', '2017-03-06', 'default/users.png', 'ceo', '5113100028'),
('5113100161', '1', 'Andi Putra', 'L', 'jl teknik komputer 10', '089789654543', 'rectarenanda2@gmail.com', '2017-03-06', 'default/users.png', 'ceo', '5113100160'),
('5113100164', '1', 'Zaenal', 'L', 'jl teknik komputer 10', '12389217', 'andiputra31@gmail.com', '2017-03-06', 'default/users.png', 'aaaa', '1316100030'),
('5114100001', '1', 'Nezar', 'L', 'Surabaya Ngagel', '088888887777', 'nezar@gmail.com', '2017-02-27', 'default/users.png', 'Manager', '5114100012'),
('5114100012', '1', 'Zahrah Citra', 'P', 'Keputih 123 no. 45', '08211111111', 'zahrah@gmail.com', '2017-02-27', 'default/users.png', 'Pemilik', '5114100181'),
('51141000121', '1', 'Rara hafizha', 'P', 'Jl. Raya Keputih', '098289277135', 'rara@hisana.com', '2017-03-06', 'default/users.png', 'Owner hisana corp', '5114100001'),
('5114100017', '1', 'Nafia', 'P', 'Perumdos blok j 45', '088888777712', 'nafia@gmail.com', '2017-03-06', 'default/users.png', 'Sales', '5114100012'),
('51141000170', '1', 'Nafia R. Y', 'P', 'Blok J Perumdos ITS', '0872829293927', 'nafia@kitten.com', '2017-03-06', 'default/users.png', 'Owner Kitten', '5114100001'),
('5114100026', '1', 'Fandy', 'L', 'Jalan Teknik Arsitektur Blok J-2 Surabaya', '081234567899', 'fandyadityaw@gmail.com', '2017-03-06', 'default/users.png', 'Owner', '5114100073'),
('5114100035', '1', 'Pramudito H.', 'L', 'jl. pembalap no. 55', '08642195839', 'dito@blarblar.com', '2017-02-27', 'default/users.png', 'Owner Blarblar ', '5114100001'),
('5114100049', '1', 'Cahya Setya Adhi', 'L', 'Keputih Gang Makam No. 19, Sukolilo, Surabaya', '081878722113', 'cahyasetya@gmail.com', '2017-02-27', 'default/users.png', 'Direktur CV. Maju Jaya', '5114100071'),
('5114100068', '1', 'Buthoro Kunto Raharjo', 'L', 'Keputih Gang Makam No. 19A, Sukolilo, Surabaya', '087865630912', 'buthorothekuns@gmail.com', '2017-02-27', 'default/users.png', 'Owner Kafe DJI', '5114100071'),
('5114100070', '1', 'Prasetyo Nugrohadi', 'L', 'Surabaya', '+6287854444652', 'prstyngrhd@gmail.com', '2017-03-06', 'default/users.png', 'Owner', '5114100070'),
('5114100076', '1', 'Fina Andita', '-Pi', 'Jl Tanah Merah No.23 Surabaya', '857339744512323', 'fina22@yahoo.com', '2017-02-27', 'default/users.png', 'CMO', '511400704'),
('5114100077', '1', 'Muhammad Hanif Amrizal', 'L', 'Keputih Gg. 1 Surabaya', '081223344556', 'mhanifamrizal@gmail.com', '2017-03-06', 'default/users.png', 'Owner', '5114100073'),
('5114100097', '1', 'Sani', 'L', 'jl. Jemursari', '089333246221', 'sani@jomblo.com', '2017-02-27', 'default/users.png', 'Owner Toko Jomblo', '5114100001'),
('5114100108', '1', 'Randy Irawan', 'L', 'Jl. Ir Soekarno 71 Surabaya', '856609991232', 'RandyIrawan@gmail.com', '2017-02-27', 'default/users.png', 'Owner', '511400704'),
('5114100114', '1', 'lala', 'L', 'lala', '089131231311', 's@gmail.com', '2017-02-27', 'default/users.png', 'lala', '5114100118'),
('5114100130', '1', 'Rahmatin Nadia', 'P', 'Keputih', '087654322157', 'nadiarahmatin@gmail.com', '2017-02-27', 'default/users.png', 'Wiraswasta', '5114100172'),
('5114100171', '1', 'Glleen A. M.', 'L', 'jl. Kertajaya', '085697344566', 'glleen@fengshui.com', '2017-02-27', 'default/users.png', 'Owner Tok Fengshui', '5114100001'),
('5114100172', '1', 'Kania Amalia', 'P', 'Mulyosari Prima 123', '0822222222', 'kania@gmail.com', '2017-02-27', 'default/users.png', 'Manager', '5114100181'),
('5114100173', '1', 'Afif Ridho', 'L', 'jl. Fak -fak no. 46', '082312345677', 'afifridho@ambonstore.com', '2017-02-27', 'default/users.png', 'Owner Ambon Store', '5114100001'),
('5114100175', '1', 'Riansyap', 'L', 'Jl. Aqua no 1', '093658362836', 'riansya@cobamail.com', '2017-02-27', 'default/users.png', 'Mahasiswa', '5114100175'),
('5114100177', '1', 'Kania Amalia', '-Pi', 'jl. Surabayan Gg. 3', '081294395372', 'kania@tokosebelah.com', '2017-02-27', 'default/users.png', 'Owner Toko Sebelah', '5114100001'),
('5114100180', '1', 'Rizal Septiabunga', 'L', 'Keputih gang 3 56A', '085645303200', 'Rizal.bunga@gmail.com', '2017-03-06', 'default/users.png', 'Customer', '5114100065'),
('5114100181', '1', 'Anne', 'P', 'Mulyosari Sukolilo', '088666553344', 'anne@gmail.com', '2017-02-27', 'default/users.png', 'Sekertaris', '5114100012'),
('5114100188', '1', 'Ripat', 'L', 'Jln Prof Moh Yamin 72 surabaya', '08352664627', 'ripat@gmail.com', '2017-02-27', 'default/users.png', 'Boss', '5114100180'),
('5114100704', '1', 'Nur Maulidiah El Fajr', 'P', 'Jl. Raya Prapen No.20, Panjang Jiwo, Tenggilis Mejoyo, Kota SBY, Jawa Timur 60299', '081234567890', 'elva.shain@gmail.com', '2017-03-06', 'default/users.png', 'Owner', '5114100073'),
('5114100800', '1', 'Adiwinoto', 'L', 'Jl. Gebang utara', '08827188987', 'adi@wintolien.com', '2017-03-06', 'default/users.png', 'Owner Wintolien ', '5114100001'),
('5145784564843415', '1', 'Gunawangsa', 'L', 'Keputih Tegal Timur', '087853495958', 'Gunawangsa@gmail.com', '2017-03-06', 'default/users.png', 'Direktur', '5114100167'),
('5161239819232', '1', 'annisa nur aulia', 'P', 'jl raya pancasila', '0853123523', 'annisa@gmail.com', '2017-02-27', 'default/users.png', 'ibu rumah rangga', '5114100122'),
('5171011003960006', '1', 'Bu Surya', 'P', 'Jalan Teknik Arsitektur, Blok J No 2, Sukolilo, Surabaya', '082187719982', 'wbs@gmail.com', '2017-02-27', 'default/users.png', 'Owner', '5114100026'),
('5171011003960007', '1', 'Soma Sanubari', 'L', 'Jalan Teknik Komputer No 21, Sukolilo, Surabaya', '081338613143', 'soma.sanubary@gmail.com', '2017-02-27', 'default/users.png', 'owner', '5114100026'),
('5171011003960008', '1', 'Usoparta Anugrah', 'L', 'Jalan Raya Kenjeran no 21', '08563945126', 'usop.parta@gmail.com', '2017-02-27', 'default/users.png', 'Manager', '5114100026'),
('5171011003960010', '1', 'Aditya Permana', 'L', 'Jalan Arief Rahman Hakim no 09', '087962123778', 'adit.peno@gmail.com', '2017-02-27', 'default/users.png', 'Manager', '5114100026'),
('5171011003960011', '1', 'Budi Denali', 'L', 'Jalan Arief Rahman Hakim no 10', '089726225484', 'budi.denali@gmail.com', '2017-02-27', 'default/users.png', 'Manager', '5114100026'),
('5171011003960012', '1', 'Indah kus', 'P', 'Dukuh Kupang, no 21', '082187719981', 'indah.kus@gmail.com', '2017-02-27', 'default/users.png', 'Manager', '5114100026'),
('5171011003960013', '1', 'Jessica Mila', 'P', 'Jalan Nginden, No 32', '087829188212', 'jess.mila@gmail.com', '2017-02-27', 'default/users.png', 'Owner', '5114100026'),
('5171011003960014', '1', 'Pt . Air Segar Sby', 'L', 'Jalan Raya Juanda no 101', '089718921291', 'airsegar.sby@gmail.com', '2017-02-27', 'default/users.png', 'Manager', '5114100026'),
('521000001', '1', 'Felix Andrea', 'L', 'Jl. Rungkut Industri III No.9 Surabaya', '88809975434', 'felixand@gmail.com', '2017-02-27', 'default/users.png', 'COO', '511400704'),
('7', '1', 'N Golo Kante', 'L', 'Jl. London no. 7', '08456789123', 'ngolokante@gmail.com', '2017-03-06', 'default/users.png', 'Manager', '5114100100'),
('7655014565438895', '1', 'Sutejo Binti Aisyah', 'L', 'Surabaya', '0812345678919', 'sutejo.bintiaisyah@gmail.com', '2017-03-06', 'default/users.png', 'Manager', '5114100014'),
('87654321', '1', 'Hank Pym', 'L', 'Jl SedihSelalu No 12', '12457809', 'PHP@gmail.com', '2017-02-27', 'default/users.png', 'Boss', '5113100139'),
('923451761193475', '1', 'rizal arizal', 'L', 'keputih perintis no 3', '081234512341', 'riza@gmail.com', '2017-02-27', 'default/users.png', 'dokter', '5114100122'),
('adi', '1', 'winoto', 'L', 'gebang', '082252144777', 'adi.winoto@gmail.com', '2017-03-06', 'default/users.png', 'Pati', '5114100065'),
('co001', '1', 'Aldi wirawan', 'L', 'Jl. Keputih 2', '098768766567', 'aldi.wirawan@gmail.com', '2017-02-27', 'default/users.png', 'Owner', '5114100151'),
('co002', '1', 'Agus salim', 'L', 'Jl. Melati 4', '092345612367', 'agus.salim@gmail.com', '2017-02-27', 'default/users.png', 'Owner', '5114100151');

-- --------------------------------------------------------

--
-- Table structure for table `defect`
--

CREATE TABLE `defect` (
  `id_def` int(11) NOT NULL,
  `id_rec` varchar(100) DEFAULT NULL,
  `id_issue` varchar(100) DEFAULT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `hargaSatuan` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `tanggal` date NOT NULL,
  `keterangan` int(11) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `defect`
--

INSERT INTO `defect` (`id_def`, `id_rec`, `id_issue`, `id_item`, `jumlah`, `hargaSatuan`, `status`, `tanggal`, `keterangan`, `id_pemilik`) VALUES
(1, 'REC58bd06e055b03', NULL, 'Barang03', 2, 26000, 0, '2017-03-06', 1, '5114100181'),
(2, 'REC58bd0800c32ca', NULL, 'Barang01', 20, 500, 1, '2017-03-06', 1, '5114100118'),
(3, 'REC58bd08a657851', NULL, 'Barang03', 800, 27000, 0, '2017-03-06', 1, '5114100151'),
(4, NULL, 'ISSUE58bd08e4ea209', 'Barang02', 5, 25000, 1, '2017-03-06', 2, '5114100014'),
(5, 'REC58bd08f5ca947', NULL, 'Barang02', 1, 18000, 0, '2017-03-06', 1, '5114100175'),
(6, NULL, 'ISSUE58bd0963de78d', 'Barang03', 2, 32000, 0, '2017-03-06', 2, '5114100167'),
(7, 'REC58bd0949cf9e8', NULL, 'Barang03', 2500, 26000, 1, '2017-03-06', 1, '5114100050'),
(8, 'REC58bd0acf37423', NULL, 'Barang03', 1000, 26000, 1, '2017-03-06', 1, '5114100151'),
(9, NULL, 'ISSUE58bd0ad358754', 'Barang03', 2, 43000, 2, '2017-03-06', 2, '5114100012'),
(10, NULL, 'ISSUE58bd0af231205', 'Barang03', 2, 43000, 2, '2017-03-06', 2, '5114100012'),
(11, NULL, 'ISSUE58bd0b861feab', 'Barang03', 30, 30000, 0, '2017-03-06', 2, '5114100151'),
(12, 'REC58bd0ddc48289', NULL, 'Barang03', 100, 26000, 0, '2017-03-06', 1, '5114100151'),
(13, NULL, 'ISSUE58bd0e1d1344e', 'Barang03', 5, 30000, 1, '2017-03-06', 2, '5114100118'),
(14, NULL, 'ISSUE58bd110e9b0f9', 'Barang03', 2, 32000, 0, '2017-03-06', 2, '5114100017'),
(15, NULL, 'ISSUE58bd11de87f12', 'Barang01', 1, 1000, 1, '2017-03-06', 2, '5114100181'),
(16, 'REC58bd59a3b9c2a', NULL, 'Barang01', 20, 480, 0, '2017-03-06', 1, '5113100043');

-- --------------------------------------------------------

--
-- Table structure for table `detail_penerimaan`
--

CREATE TABLE `detail_penerimaan` (
  `id_rec` varchar(100) DEFAULT NULL,
  `id_produksi` varchar(100) DEFAULT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `hargaSatuan` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_penerimaan`
--

INSERT INTO `detail_penerimaan` (`id_rec`, `id_produksi`, `id_item`, `jumlah`, `hargaSatuan`, `status`, `id_pemilik`) VALUES
('REC58bd038c4c55c', NULL, 'Barang03', 175200, 27000, 0, '5114100070'),
('REC58bd042b8ae81', NULL, 'Barang03', 2500, 26000, 0, '5114100026'),
('REC58bd06032d650', NULL, 'Barang03', 25, 26000, 0, '5113100150'),
('REC58bd0601e7b5a', NULL, 'Barang02', 500, 18000, 0, '5114100001'),
('REC58bd061ca7f57', NULL, 'Barang02', 100, 19500, 0, '5114100014'),
('REC58bd0661b58b5', NULL, 'Barang03', 4, 31000, 0, '5114100175'),
('REC58bd06be53b0e', NULL, 'Barang01', 100, 480, 0, '5114100181'),
('REC58bd06cdd361f', NULL, 'Barang02', 1000, 18000, 0, '5114100065'),
('REC58bd06e055b03', NULL, 'Barang03', 9, 26000, 0, '5114100181'),
('REC58bd07120f0cc', NULL, 'Barang02', 100, 18000, 0, '5114100181'),
('REC58bd074f79a44', NULL, 'Barang01', 20, 480, 0, '5114100181'),
('REC58bd074f79a44', NULL, 'Barang02', 1, 20000, 0, '5114100181'),
('REC58bd074f79a44', NULL, 'Barang03', 2, 26000, 0, '5114100181'),
('REC58bd079760173', NULL, 'Barang03', 30, 26000, 0, '5114100167'),
('REC58bd0821a5dde', NULL, 'Barang03', 3750, 26000, 0, '5114100100'),
('REC58bd08238954a', NULL, 'Barang03', 50, 31000, 0, '5114100012'),
('REC58bd08376ecdc', NULL, 'Barang03', 2000, 27000, 0, '511400704'),
('REC58bd084b8a5fe', NULL, 'Barang02', 50, 19500, 0, '5114100014'),
('REC58bd085426ed7', NULL, 'Barang03', 3500, 26000, 0, '5114100064'),
('REC58bd084ab689a', NULL, 'Barang03', 490, 26000, 0, '5114100017'),
('REC58bd088e91231', NULL, 'Barang03', 600, 26000, 0, '5114100073'),
('REC58bd08a657851', NULL, 'Barang03', 600, 27000, 0, '5114100151'),
('REC58bd08f5ca947', NULL, 'Barang02', 10, 18000, 0, '5114100175'),
('REC58bd0949cf9e8', NULL, 'Barang03', 2500, 26000, -2500, '5114100050'),
('REC58bd0989b1ac2', NULL, 'Barang02', 500, 18000, 0, '5114100001'),
('REC58bd093224c06', NULL, 'Barang02', 10000, 22000, 0, '5114100164'),
('REC58bd0a27563ab', NULL, 'Barang03', 200, 30000, 0, '5114100012'),
('REC58bd0acf37423', NULL, 'Barang03', 3000, 26000, -1000, '5114100151'),
('REC58bd0b211411a', NULL, 'Barang01', 5000, 480, 0, '5114100065'),
('REC58bd0c2b6543f', NULL, 'Barang02', 150, 18000, 0, '5114100012'),
('REC58bd0c4c44159', NULL, 'Barang02', 150, 18000, 0, '5114100012'),
('REC58bd0c6a8d657', NULL, 'Barang03', 30, 26000, 0, '5114100167'),
('REC58bd0c8e3140d', NULL, 'Barang02', 200, 18000, 0, '5114100017'),
('REC58bd0d56887b9', NULL, 'Barang01', 1000, 500, 0, '5114100181'),
('REC58bd0ddc48289', NULL, 'Barang03', 400, 26000, 0, '5114100151'),
('REC58bd0e4ac0970', NULL, 'Barang02', 150, 18000, 0, '5114100012'),
('REC58bd0e4ac0970', NULL, 'Barang03', 100, 43000, 0, '5114100012'),
('REC58bd0e767b50f', NULL, 'Barang01', 500, 600, 0, '5114100012'),
('REC58bd11d49d46f', NULL, 'Barang03', 200, 30000, 0, '5114100012'),
('REC58bd2ca4f2fc8', NULL, 'Barang01', 40, 480, 0, '1316100030'),
('REC58bd2f98337e5', NULL, 'Barang01', 4, 500, 0, '1316100030'),
('REC58bd59a3b9c2a', NULL, 'Barang01', 500, 480, 0, '5113100043'),
('REC58bd606bd28de', NULL, 'Barang01', 2, 500, 0, '1316100030');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pengeluaran`
--

CREATE TABLE `detail_pengeluaran` (
  `id_issue` varchar(100) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` int(11) NOT NULL,
  `id_suplier` int(11) DEFAULT NULL,
  `id_rec` varchar(100) DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pengeluaran`
--

INSERT INTO `detail_pengeluaran` (`id_issue`, `id_item`, `jumlah`, `harga`, `id_suplier`, `id_rec`, `id_pemilik`) VALUES
('ISSUE58bd04a622943', 'Barang03', 175200, 32000, 5, 'REC58bd038c4c55c', NULL),
('ISSUE58bd0607a0882', 'Barang03', 500, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd061973ac5', 'Barang03', 100, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd062592d2d', 'Barang03', 50, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd0630f343e', 'Barang03', 25, 30000, 1, 'REC58bd06032d650', NULL),
('ISSUE58bd06346a06f', 'Barang03', 500, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd069bdb154', 'Barang02', 50, 25000, 5, 'REC58bd061ca7f57', NULL),
('ISSUE58bd06d77b135', 'Barang03', 250, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd06e7e1e8a', 'Barang03', 100, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd06f9ba744', 'Barang03', 100, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd0707729f4', 'Barang03', 250, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd07558d1c0', 'Barang02', 1000, 25000, 4, 'REC58bd06cdd361f', NULL),
('ISSUE58bd078fcc2bd', 'Barang03', 4, 58000, 3, 'REC58bd0661b58b5', NULL),
('ISSUE58bd0798294c6', 'Barang03', 300, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd079ab3c41', 'Barang02', 100, 25000, 4, 'REC58bd0601e7b5a', NULL),
('ISSUE58bd07a506f4d', 'Barang03', 50, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd07ae77278', 'Barang03', 300, 30000, 1, 'REC58bd042b8ae81', NULL),
('ISSUE58bd081e181fb', 'Barang03', 20, 32000, 1, 'REC58bd079760173', NULL),
('ISSUE58bd085591823', 'Barang02', 90, 25000, 4, 'REC58bd0601e7b5a', NULL),
('ISSUE58bd08962c863', 'Barang02', 85, 25000, 4, 'REC58bd0601e7b5a', NULL),
('ISSUE58bd08e301b57', 'Barang02', 95, 25000, 4, 'REC58bd0601e7b5a', NULL),
('ISSUE58bd08e4ea209', 'Barang02', 15, 25000, 5, 'REC58bd061ca7f57', NULL),
('ISSUE58bd08e8c68d0', 'Barang03', 100, 32000, 1, 'REC58bd084ab689a', NULL),
('ISSUE58bd0945cd145', 'Barang02', 85, 25000, 4, 'REC58bd0601e7b5a', NULL),
('ISSUE58bd094305947', 'Barang03', 25, 40000, 3, 'REC58bd08238954a', NULL),
('ISSUE58bd09480765a', 'Barang03', 2000, 35000, 1, 'REC58bd085426ed7', NULL),
('ISSUE58bd0963de78d', 'Barang03', 8, 32000, 1, 'REC58bd079760173', NULL),
('ISSUE58bd097ca105e', 'Barang01', 10, 1050, 2, 'REC58bd0800c32ca', NULL),
('ISSUE58bd09f1b1fa2', 'Barang02', 1, 25000, 2, 'REC58bd074f79a44', NULL),
('ISSUE58bd0a366c938', 'Barang03', 200, 31000, 5, 'REC58bd08376ecdc', NULL),
('ISSUE58bd0a37e8241', 'Barang03', 500, 33000, 1, 'REC58bd0821a5dde', NULL),
('ISSUE58bd0a7377adc', 'Barang02', 75, 25000, 4, 'REC58bd0989b1ac2', NULL),
('ISSUE58bd0aa9af7a0', 'Barang02', 20, 25000, 5, 'REC58bd061ca7f57', NULL),
('ISSUE58bd0ad358754', 'Barang03', 98, 43000, 4, 'REC58bd0a27563ab', NULL),
('ISSUE58bd0af231205', 'Barang03', 98, 43000, 4, 'REC58bd0a27563ab', NULL),
('ISSUE58bd0b0c52041', 'Barang03', 250, 33000, 1, 'REC58bd0821a5dde', NULL),
('ISSUE58bd0b1f7deb7', 'Barang02', 150, 25000, 4, 'REC58bd0989b1ac2', NULL),
('ISSUE58bd0b1eacc66', 'Barang03', 150, 31000, 5, 'REC58bd08376ecdc', NULL),
('ISSUE58bd0b5eced89', 'Barang03', 100, 30000, 5, 'REC58bd08a657851', NULL),
('ISSUE58bd0b7e83130', 'Barang03', 50, 32000, 1, 'REC58bd084ab689a', NULL),
('ISSUE58bd0b861feab', 'Barang03', 10, 30000, 5, 'REC58bd08a657851', NULL),
('ISSUE58bd0ba6e3f13', 'Barang01', 5000, 800, 1, 'REC58bd0b211411a', NULL),
('ISSUE58bd0bf98afaf', 'Barang01', -1, 1050, 2, 'REC58bd0800c32ca', NULL),
('ISSUE58bd0c13b6455', 'Barang01', 70, 1050, 2, 'REC58bd0800c32ca', NULL),
('ISSUE58bd0c130ad0f', 'Barang02', 75, 25000, 4, 'REC58bd0989b1ac2', NULL),
('ISSUE58bd0cb464cb7', 'Barang03', 100, 30000, 1, 'REC58bd088e91231', NULL),
('ISSUE58bd0ce44d329', 'Barang02', 150, 25000, 4, 'REC58bd0c2b6543f', NULL),
('ISSUE58bd0d40b715a', 'Barang02', 15, 23000, 4, 'REC58bd0c8e3140d', NULL),
('ISSUE58bd0e344c4b6', 'Barang01', 666, 1500, 2, 'REC58bd0d56887b9', NULL),
('ISSUE58bd0e50bb6a6', 'Barang03', 30, 32000, 1, 'REC58bd0c6a8d657', NULL),
('ISSUE58bd0e1d1344e', 'Barang03', 30, 30000, 1, 'REC58bd0cb63d3f5', NULL),
('ISSUE58bd0ee8bc99b', 'Barang03', 110, 32000, 1, 'REC58bd084ab689a', NULL),
('ISSUE58bd0fa948629', 'Barang01', 300, 1000, 3, 'REC58bd0e767b50f', NULL),
('ISSUE58bd102e2407a', 'Barang02', 60, 25000, 4, 'REC58bd0989b1ac2', NULL),
('ISSUE58bd110e9b0f9', 'Barang03', 148, 32000, 1, 'REC58bd084ab689a', NULL),
('ISSUE58bd112333385', 'Barang03', 2, 32000, 1, 'REC58bd084ab689a', NULL),
('ISSUE58bd11de87f12', 'Barang01', 99, 1000, 2, 'REC58bd0d56887b9', NULL),
('ISSUE58bd123d0f2e2', 'Barang03', 200, 35000, 4, 'REC58bd11d49d46f', NULL),
('ISSUE58bd13855d8d6', 'Barang02', 20, 23000, 4, 'REC58bd0c8e3140d', NULL),
('ISSUE58bd5ab167dd7', 'Barang01', 2, 800, 2, 'REC58bd2f98337e5', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `id_so` varchar(100) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` int(11) NOT NULL,
  `id_suplier` int(11) DEFAULT NULL,
  `jumlah_keluar` double DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_penjualan`
--

INSERT INTO `detail_penjualan` (`id_so`, `id_item`, `jumlah`, `harga`, `id_suplier`, `jumlah_keluar`, `id_pemilik`) VALUES
('SO58bd03ed5141a', 'Barang03', 175200, 32000, 5, NULL, NULL),
('SO58bd052be436e', 'Barang03', 500, 30000, 1, NULL, NULL),
('SO58bd055dddf55', 'Barang03', 50, 30000, 1, NULL, NULL),
('SO58bd058a7ac01', 'Barang03', 500, 30000, 1, NULL, NULL),
('SO58bd05c097c9a', 'Barang03', 100, 30000, 1, NULL, NULL),
('SO58bd06180cb2d', 'Barang03', 25, 30000, 1, NULL, NULL),
('SO58bd06532504c', 'Barang03', 250, 30000, 1, NULL, NULL),
('SO58bd065fcefd4', 'Barang02', 50, 25000, 5, NULL, NULL),
('SO58bd066f7348f', 'Barang03', 250, 30000, 1, NULL, NULL),
('SO58bd068e93a97', 'Barang03', 100, 30000, 1, NULL, NULL),
('SO58bd06b17dd9f', 'Barang03', 100, 30000, 1, NULL, NULL),
('SO58bd06e907ccf', 'Barang02', 100, 25000, 4, NULL, NULL),
('SO58bd0700102d1', 'Barang02', 1000, 25000, 4, NULL, NULL),
('SO58bd07339368f', 'Barang03', 50, 30000, 1, NULL, NULL),
('SO58bd074b88253', 'Barang03', 300, 30000, 1, NULL, NULL),
('SO58bd07461046a', 'Barang03', 4, 58000, 3, NULL, NULL),
('SO58bd076b5c1c8', 'Barang03', 300, 30000, 1, NULL, NULL),
('SO58bd07e10c319', 'Barang03', 20, 32000, 1, NULL, NULL),
('SO58bd082c3152e', 'Barang02', 90, 25000, 4, NULL, NULL),
('SO58bd083dbc056', 'Barang01', 100, 1000, 2, NULL, NULL),
('SO58bd083dbc056', 'Barang02', 1, 25000, 2, NULL, NULL),
('SO58bd08745c465', 'Barang02', 85, 25000, 4, NULL, NULL),
('SO58bd08a308ff6', 'Barang01', 20, 1000, 2, NULL, NULL),
('SO58bd08a308ff6', 'Barang02', 1, 25000, 2, NULL, NULL),
('SO58bd089828481', 'Barang03', 100, 32000, 1, NULL, NULL),
('SO58bd089e189aa', 'Barang03', 25, 40000, 3, NULL, NULL),
('SO58bd08a9c745e', 'Barang02', 20, 25000, 5, NULL, NULL),
('SO58bd08bebb08d', 'Barang02', 95, 25000, 4, NULL, NULL),
('SO58bd08f3d623b', 'Barang01', 20, 1000, 2, NULL, NULL),
('SO58bd08ef206aa', 'Barang03', 2000, 35000, 1, NULL, NULL),
('SO58bd090336caf', 'Barang02', 85, 25000, 4, NULL, NULL),
('SO58bd09195649d', 'Barang03', 100, 30000, 1, NULL, NULL),
('SO58bd0933a4a63', 'Barang03', 10, 30000, 5, NULL, NULL),
('SO58bd093c49330', 'Barang03', 10, 32000, 1, NULL, NULL),
('SO58bd094539acb', 'Barang01', 10, 1050, 2, NULL, NULL),
('SO58bd091be3862', 'Barang03', 200, 31000, 5, NULL, NULL),
('SO58bd09c0b61e5', 'Barang02', 10, 30000, 4, NULL, NULL),
('SO58bd0a02b76a7', 'Barang03', 500, 33000, 1, NULL, NULL),
('SO58bd0a1ebc2f5', 'Barang02', 75, 25000, 4, NULL, NULL),
('SO58bd0a84aa09b', 'Barang03', 100, 43000, 4, NULL, NULL),
('SO58bd0ab8311e6', 'Barang03', 150, 31000, 5, NULL, NULL),
('SO58bd0ad69d5a4', 'Barang03', 250, 33000, 1, NULL, NULL),
('SO58bd0ac2ceee5', 'Barang03', 50, 32000, 1, NULL, NULL),
('SO58bd0ae10f328', 'Barang02', 150, 25000, 4, NULL, NULL),
('SO58bd0aee3f6a8', 'Barang01', 100, 1000, 2, NULL, NULL),
('SO58bd0aee3f6a8', 'Barang02', 1, 25000, 2, NULL, NULL),
('SO58bd0b0c6afc4', 'Barang03', 200, 30000, 5, NULL, NULL),
('SO58bd0b14d42e7', 'Barang03', 1, 35000, 2, NULL, NULL),
('SO58bd0b6c5be6d', 'Barang01', 5000, 800, 1, NULL, NULL),
('SO58bd0b936b9e2', 'Barang02', 4000, 25000, 1, NULL, NULL),
('SO58bd0bb194c0d', 'Barang01', 70, 1050, 2, NULL, NULL),
('SO58bd0bd3f2693', 'Barang01', 120, 1000, 2, NULL, NULL),
('SO58bd0bdeb69bb', 'Barang02', 75, 25000, 4, NULL, NULL),
('SO58bd0c6dd118b', 'Barang03', 11, 30000, 2, NULL, NULL),
('SO58bd0c9314130', 'Barang02', 150, 25000, 4, NULL, NULL),
('SO58bd0d1564af8', 'Barang02', 15, 23000, 4, NULL, NULL),
('SO58bd0d84d984c', 'Barang03', 30, 32000, 1, NULL, NULL),
('SO58bd0d8804648', 'Barang03', 20, 30000, 1, NULL, NULL),
('SO58bd0dfcdc32d', 'Barang01', 666, 1500, 2, NULL, NULL),
('SO58bd0e353a6c5', 'Barang03', 110, 32000, 1, NULL, NULL),
('SO58bd0e6b79841', 'Barang03', 30, 3000, 5, NULL, NULL),
('SO58bd0eee16ce8', 'Barang01', 300, 1000, 3, NULL, NULL),
('SO58bd0eee16ce8', 'Barang03', 80, 50000, 4, NULL, NULL),
('SO58bd0eee16ce8', 'Barang02', 200, 25000, 4, NULL, NULL),
('SO58bd0f28c94d9', 'Barang01', 300, 1000, 3, NULL, NULL),
('SO58bd0ff1c2c2e', 'Barang02', 60, 25000, 4, NULL, NULL),
('SO58bd10d2496ae', 'Barang03', 150, 32000, 1, NULL, NULL),
('SO58bd10fcc6c33', 'Barang03', 100, 50000, 4, NULL, NULL),
('SO58bd12074e347', 'Barang03', 200, 35000, 4, NULL, NULL),
('SO58bd12d7b0cb6', 'Barang02', 20, 23000, 4, NULL, NULL),
('SO58bd2cc30c09a', 'Barang01', 30, 600, 1, NULL, NULL),
('SO58bd2fb760921', 'Barang01', 1, 600, 2, NULL, NULL),
('SO58bd2ff7834c7', 'Barang01', 1, 600, 2, NULL, NULL),
('SO58bd307f7425c', 'Barang01', 1, 100, 2, NULL, NULL),
('SO58bd30a37505b', 'Barang01', 1, 10, 2, NULL, NULL),
('SO58bd30d06b880', 'Barang01', 1, 10, 2, NULL, NULL),
('SO58bd360c205b7', 'Barang01', 1, 600, 2, NULL, NULL),
('SO58bd526aa61b2', 'Barang01', 1, 600, 2, NULL, NULL),
('SO58bd52b73cd60', 'Barang01', 1, 1000, 2, NULL, NULL),
('SO58bd52ec5c82b', 'Barang01', 1, 1000, 2, NULL, NULL),
('SO58bd58067cff7', 'Barang01', 2, 1000, 2, NULL, NULL),
('SO58bd59239f733', 'Barang01', 2, 600, 2, NULL, NULL),
('SO58bd5976cce7e', 'Barang01', 2, 600, 2, NULL, NULL),
('SO58bd59802838f', 'Barang01', 2, 600, 2, NULL, NULL),
('SO58bd59f0ab75e', 'Barang01', 10, 700, 1, NULL, NULL),
('SO58bd5a06047ee', 'Barang01', 2, 600, 2, NULL, NULL),
('SO58bd5a312a397', 'Barang01', 2, 600, 2, NULL, NULL),
('SO58bd5a81c0fb3', 'Barang01', 2, 800, 2, NULL, NULL),
('SO58bd60b199d36', 'Barang01', 3, 5000, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_produkservice`
--

CREATE TABLE `detail_produkservice` (
  `id_produkService` varchar(100) DEFAULT NULL,
  `id_item` varchar(100) DEFAULT NULL,
  `jumlah` double DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `id_suplier` int(11) DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_produksi`
--

CREATE TABLE `detail_produksi` (
  `id_produksi` varchar(100) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `hargaSatuan` int(11) NOT NULL,
  `id_suplier` int(11) DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_purchasing`
--

CREATE TABLE `detail_purchasing` (
  `id_purchasing` varchar(100) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `hargaSatuan` int(11) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_purchasing`
--

INSERT INTO `detail_purchasing` (`id_purchasing`, `id_item`, `jumlah`, `hargaSatuan`, `id_pemilik`) VALUES
('PO58bd03436a597', 'Barang03', 175200, 27000, NULL),
('PO58bd0392af760', 'Barang03', 2500, 26000, NULL),
('PO58bd0586ac13d', 'Barang03', 3750, 26000, NULL),
('PO58bd05a489836', 'Barang03', 10, 26000, NULL),
('PO58bd05c43165e', 'Barang02', 500, 18000, NULL),
('PO58bd05ebb41c3', 'Barang03', 25, 26000, NULL),
('PO58bd05e19b460', 'Barang02', 100, 19500, NULL),
('PO58bd0617b57e5', 'Barang01', 100, 480, NULL),
('PO58bd0629db011', 'Barang02', 100, 18000, NULL),
('PO58bd062356133', 'Barang03', 4, 31000, NULL),
('PO58bd06312e506', 'Barang03', 30, 26000, NULL),
('PO58bd064b19a51', 'Barang03', 1, 26000, NULL),
('PO58bd067c8e23b', 'Barang01', 10, 480, NULL),
('PO58bd067c8e23b', 'Barang03', 1, 26000, NULL),
('PO58bd067c8e23b', 'Barang02', 1, 20000, NULL),
('PO58bd069f2ee1f', 'Barang02', 1000, 18000, NULL),
('PO58bd07598cfe3', 'Barang03', 50, 30000, NULL),
('PO58bd07598cfe3', 'Barang03', 100, 31000, NULL),
('PO58bd077f71028', 'Barang01', 100, 500, NULL),
('PO58bd079ebf0c2', 'Barang03', 4000, 26000, NULL),
('PO58bd07a5bb892', 'Barang03', 2000, 27000, NULL),
('PO58bd07c88b0d4', 'Barang03', 50, 31000, NULL),
('PO58bd07df35c76', 'Barang03', 1000, 27000, NULL),
('PO58bd07ec3c864', 'Barang03', 490, 26000, NULL),
('PO58bd0811ba70a', 'Barang03', 600, 26000, NULL),
('PO58bd081a241f6', 'Barang02', 50, 19500, NULL),
('PO58bd080ebd94b', 'Barang03', 3500, 26000, NULL),
('PO58bd08bed232b', 'Barang03', 10, 26000, NULL),
('PO58bd08992c3c9', 'Barang02', 10000, 22000, NULL),
('PO58bd08d74a109', 'Barang02', 10, 18000, NULL),
('PO58bd0849a5565', 'Barang03', 5000, 26000, NULL),
('PO58bd0964461bd', 'Barang02', 500, 18000, NULL),
('PO58bd09ca5d312', 'Barang03', 200, 30000, NULL),
('PO58bd0a0da8e60', 'Barang03', 10, 32000, NULL),
('PO58bd0a0da8e60', 'Barang03', 30, 26000, NULL),
('PO58bd0a968088f', 'Barang01', 5000, 480, NULL),
('PO58bd0bf64d4a7', 'Barang03', 100, 43000, NULL),
('PO58bd0bf64d4a7', 'Barang02', 150, 18000, NULL),
('PO58bd0c2ceac1c', 'Barang03', 30, 26000, NULL),
('PO58bd0c5f96cf5', 'Barang03', 100, 26000, NULL),
('PO58bd0c5a2c980', 'Barang02', 200, 18000, NULL),
('PO58bd0d268cb2a', 'Barang01', 1000, 500, NULL),
('PO58bd0db1dd80e', 'Barang03', 400, 26000, NULL),
('PO58bd0df6880c4', 'Barang01', 500, 600, NULL),
('PO58bd11a032419', 'Barang03', 200, 30000, NULL),
('PO58bd2c6e6dc9c', 'Barang01', 40, 480, NULL),
('PO58bd2f851ac8d', 'Barang01', 4, 500, NULL),
('PO58bd5984ba34d', 'Barang01', 500, 480, NULL),
('PO58bd5e6fb06da', 'Barang01', 2, 500, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_suplier`
--

CREATE TABLE `detail_suplier` (
  `id_suplier` int(11) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL,
  `harga` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_suplier`
--

INSERT INTO `detail_suplier` (`id_suplier`, `id_item`, `id_pemilik`, `harga`) VALUES
(2, 'Barang01', '2017', 500),
(2, 'Barang03', '2017', 30000),
(2, 'Barang02', '2017', 20000),
(5, 'Barang01', '2017', 550),
(5, 'Barang03', '2017', 27000),
(5, 'Barang02', '2017', 19500),
(3, 'Barang01', '2017', 600),
(3, 'Barang03', '2017', 31000),
(3, 'Barang02', '2017', 20600),
(4, 'Barang01', '2017', 475),
(4, 'Barang03', '2017', 30000),
(4, 'Barang02', '2017', 18000),
(1, 'Barang01', '2017', 480),
(1, 'Barang03', '2017', 26000),
(1, 'Barang02', '2017', 22000);

-- --------------------------------------------------------

--
-- Table structure for table `gl_account`
--

CREATE TABLE `gl_account` (
  `id` int(11) NOT NULL,
  `coa_id` int(11) NOT NULL,
  `acc_code` int(20) NOT NULL,
  `acc_name` varchar(100) NOT NULL,
  `deletable` tinyint(1) NOT NULL DEFAULT '0',
  `other` tinyint(1) DEFAULT '0',
  `uid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gl_account`
--

INSERT INTO `gl_account` (`id`, `coa_id`, `acc_code`, `acc_name`, `deletable`, `other`, `uid`) VALUES
(1, 1, 1000, 'Kas', 0, 0, NULL),
(2, 1, 1001, 'Rekening Bank', 0, 0, NULL),
(3, 1, 1200, 'Piutang Usaha', 0, 0, NULL),
(4, 1, 1210, 'Piutang Lainnya', 0, 0, NULL),
(5, 1, 1300, 'Dana Belum Disetor', 0, 0, NULL),
(6, 1, 1400, 'Persediaan Barang', 0, 0, NULL),
(7, 1, 1500, 'Uang Muka Pembelian', 0, 0, NULL),
(8, 1, 1600, 'Pinjaman Karyawan', 0, 0, NULL),
(9, 1, 1700, 'Pinjaman Lainnya', 0, 0, NULL),
(10, 1, 1800, 'Aset Tetap', 0, 0, NULL),
(11, 1, 1801, 'Penyusutan Aset Tetap', 0, 0, NULL),
(12, 1, 1810, 'Aset Tak Berwujud', 0, 0, NULL),
(13, 1, 1820, 'Investasi', 0, 0, NULL),
(14, 1, 1900, 'PPN Masukan', 0, 0, NULL),
(15, 1, 1910, 'Pajak Masukan lainnya', 0, 0, NULL),
(16, 2, 2000, 'Hutang Usaha', 0, 0, NULL),
(17, 2, 2010, 'Hutang Gaji Karyawan', 0, 0, NULL),
(18, 2, 2020, 'Hutang Dividen', 0, 0, NULL),
(19, 2, 2030, 'Hutang Lainnya', 0, 0, NULL),
(20, 2, 2090, 'Uang Muka Penjualan', 0, 0, NULL),
(21, 2, 2100, 'Hutang Bank', 0, 0, NULL),
(22, 2, 2200, 'PPN Pengeluaran', 0, 0, NULL),
(23, 2, 2210, 'Pengeluaran Pajak Payroll', 0, 0, NULL),
(24, 2, 2230, 'Pengeluaran Pajak Penghasilan Usaha', 0, 0, NULL),
(25, 2, 2299, 'PPH Pengeluaran Lainnya', 0, 0, NULL),
(26, 2, 2910, 'Hutang Dari Pemegang Saham', 0, 0, NULL),
(27, 3, 3000, 'Modal Awal', 0, 0, NULL),
(28, 3, 3100, 'Laba Ditahan', 0, 0, NULL),
(29, 3, 3200, 'Dividen', 0, 0, NULL),
(30, 3, 3900, 'Ekuitas Saldo Awal', 0, 0, NULL),
(31, 4, 4000, 'Penjualan', 0, 0, NULL),
(32, 4, 4100, 'Diskon Penjualan', 0, 0, NULL),
(33, 4, 4200, 'Retur Penjualan', 0, 0, NULL),
(34, 5, 5000, 'Harga Pokok Penjualan', 0, 0, NULL),
(35, 5, 5100, 'Diskon Penjualan', 0, 0, NULL),
(36, 5, 5200, 'Retur Pembelian', 0, 0, NULL),
(37, 5, 5300, 'Pengiriman dan Pengangkutan', 0, 0, NULL),
(38, 5, 5900, 'Biaya Produksi', 0, 0, NULL),
(40, 6, 6000, 'Iklan & Promosi', 0, 0, NULL),
(41, 6, 6001, 'Piutang Tak Tertagih', 0, 0, NULL),
(42, 6, 6002, 'Bank', 0, 0, NULL),
(43, 6, 6003, 'Kontribusi Sosial', 0, 0, NULL),
(44, 6, 6004, 'Biaya Tenaga Kerja', 0, 0, NULL),
(45, 6, 6005, 'Komisi & Upah', 0, 0, NULL),
(46, 6, 6006, 'Biaya Pembuangan', 0, 0, NULL),
(47, 6, 6007, 'Iuran & Langganan', 0, 0, NULL),
(48, 6, 6008, 'Hiburan', 0, 0, NULL),
(49, 6, 6009, 'Makanan Hiburan', 0, 0, NULL),
(50, 6, 6010, 'Penyewaan Alat', 0, 0, NULL),
(51, 6, 6011, 'Asuransi', 0, 0, NULL),
(52, 6, 6012, 'Bunga Hutang', 0, 0, NULL),
(53, 6, 6013, 'Bahan Pekerjaan', 0, 0, NULL),
(54, 6, 6014, 'Legal & Profesional', 0, 0, NULL),
(55, 6, 6015, 'Pengobatan', 0, 0, NULL),
(56, 6, 6016, 'Biaya Kantor', 0, 0, NULL),
(57, 6, 6017, 'Biaya Administrasi & Umum Lainnya', 0, 0, NULL),
(58, 6, 6018, 'Sewa Tempat', 0, 0, NULL),
(59, 6, 6019, 'Pemeliharaan & Perbaikan Gedung', 0, 0, NULL),
(60, 6, 6020, 'Alat Tulis Kantor & Printing', 0, 0, NULL),
(61, 6, 6021, 'Bea Materai', 0, 0, NULL),
(62, 6, 6022, 'Pemborong', 0, 0, NULL),
(63, 6, 6023, 'Persediaan & Bahan', 0, 0, NULL),
(64, 6, 6024, 'Pajak & Lisensi', 0, 0, NULL),
(65, 6, 6025, 'Alat-alat', 0, 0, NULL),
(66, 6, 6026, 'Perjalanan & Transportasi', 0, 0, NULL),
(67, 6, 6027, 'Makanan Perjalanan', 0, 0, NULL),
(68, 6, 6028, 'Fasilitas/Utilitas', 0, 0, NULL),
(70, 7, 7000, 'Pendapatan Pengiriman & Pengangkutan', 0, 0, NULL),
(71, 7, 7100, 'Pendapatan Bunga & Jasa Giro', 0, 0, NULL),
(72, 7, 7900, 'Pendapatan Lainnya', 0, 0, NULL),
(73, 8, 8000, 'Pengeluaran Lainnya', 0, 0, NULL),
(74, 8, 8001, 'Biaya Amortisasi', 0, 0, NULL),
(75, 8, 8002, 'Biaya Penyusutan', 0, 0, NULL),
(77, 8, 8900, 'Penyesuaian Persediaan Barang', 0, 0, NULL),
(78, 6, 6029, 'Kendaraan & Mesin', 0, 0, NULL),
(79, 6, 6030, 'Denda & Hukuman', 0, 0, NULL),
(80, 6, 6031, 'Upah & Gaji', 0, 0, NULL),
(81, 6, 6032, 'Bonus Karyawan', 0, 0, NULL),
(82, 6, 6900, 'Pengeluaran Barang Rusak', 0, 0, NULL),
(83, 8, 8003, 'Untung/Rugi Pertukaran Kurs', 0, 0, NULL),
(101, 1, 1, 'Assets', 0, 0, NULL),
(102, 2, 2, 'Liabilities', 0, 0, NULL),
(103, 3, 3, 'Equity', 0, 0, NULL),
(104, 4, 4, 'Income', 0, 0, NULL),
(105, 5, 5, 'COGS', 0, 0, NULL),
(106, 6, 6, 'Expense', 0, 0, NULL),
(107, 7, 7, 'Other Income', 0, 0, NULL),
(108, 8, 8, 'Other Expense', 0, 0, NULL),
(158, 1, 1, 'Asset', 1, 0, 'user33'),
(159, 1, 50000, 'uang muka pembelian', 1, 0, 'user63'),
(160, 1, 15000, 'piutang usaha', 1, 0, 'user63'),
(161, 2, 3000, 'hutang gaji karyawan', 1, 0, 'user63'),
(162, 0, 0, '', 1, 0, 'user63'),
(163, 2, 1500, 'hutang dari pemegang saham', 1, 0, 'user62'),
(164, 0, 0, '', 1, 0, ''),
(165, 6, 6, 'Uang Muka Pembelian', 1, 0, 'user58'),
(166, 0, 0, '', 1, 0, 'user58'),
(167, 1, 1001, 'uanguang', 1, 0, 'tester'),
(168, 1, 10, 'Uang muka pembelian gudang', 1, 0, 'user10');

-- --------------------------------------------------------

--
-- Table structure for table `gl_group`
--

CREATE TABLE `gl_group` (
  `id` int(11) NOT NULL,
  `coa_number` varchar(10) NOT NULL,
  `coa_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gl_group`
--

INSERT INTO `gl_group` (`id`, `coa_number`, `coa_name`) VALUES
(1, '1', 'Assets'),
(2, '2', 'Liabilities'),
(3, '3', 'Equity'),
(4, '4', 'Income'),
(5, '5', 'COGS'),
(6, '6', 'Expenses'),
(7, '7', 'Other Income'),
(8, '8', 'Other Expense');

-- --------------------------------------------------------

--
-- Table structure for table `gl_has`
--

CREATE TABLE `gl_has` (
  `id` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `child` int(11) NOT NULL,
  `uid` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gl_has`
--

INSERT INTO `gl_has` (`id`, `parent`, `child`, `uid`) VALUES
(1, NULL, 1, NULL),
(2, NULL, 2, NULL),
(3, NULL, 3, NULL),
(4, NULL, 4, NULL),
(5, NULL, 5, NULL),
(6, NULL, 6, NULL),
(7, NULL, 7, NULL),
(8, NULL, 8, NULL),
(9, 1, 1000, NULL),
(10, 1000, 1001, NULL),
(11, 1, 1200, NULL),
(12, 1200, 1210, NULL),
(13, 1, 1300, NULL),
(14, 1, 1400, NULL),
(15, 1, 1500, NULL),
(16, 1, 1600, NULL),
(17, 1, 1700, NULL),
(18, 1, 1800, NULL),
(19, 1800, 1801, NULL),
(20, 1800, 1810, NULL),
(21, 1800, 1820, NULL),
(22, 1, 1900, NULL),
(23, 1900, 1910, NULL),
(24, 2, 2000, NULL),
(25, 2000, 2010, NULL),
(26, 2000, 2020, NULL),
(27, 2000, 2030, NULL),
(28, 2000, 2090, NULL),
(29, 2, 2100, NULL),
(30, 2, 2200, NULL),
(31, 2200, 2210, NULL),
(32, 2200, 2230, NULL),
(33, 2200, 2299, NULL),
(34, 2, 2910, NULL),
(35, 3, 3000, NULL),
(36, 3, 3100, NULL),
(37, 3, 3200, NULL),
(38, 3, 3900, NULL),
(39, 4, 4000, NULL),
(40, 4, 4100, NULL),
(41, 4, 4200, NULL),
(42, 5, 5000, NULL),
(43, 5, 5100, NULL),
(44, 5, 5200, NULL),
(45, 5, 5300, NULL),
(46, 5, 5900, NULL),
(47, 6, 6000, NULL),
(48, 6000, 6001, NULL),
(49, 6000, 6002, NULL),
(50, 6000, 6003, NULL),
(51, 6000, 6004, NULL),
(52, 6000, 6005, NULL),
(53, 6000, 6006, NULL),
(54, 6000, 6007, NULL),
(55, 6000, 6008, NULL),
(56, 6000, 6009, NULL),
(57, 6000, 6010, NULL),
(58, 6000, 6011, NULL),
(59, 6000, 6012, NULL),
(60, 6000, 6013, NULL),
(61, 6000, 6014, NULL),
(62, 6000, 6015, NULL),
(63, 6000, 6016, NULL),
(64, 6000, 6017, NULL),
(65, 6000, 6018, NULL),
(66, 6000, 6019, NULL),
(67, 6000, 6020, NULL),
(68, 6000, 6021, NULL),
(69, 6000, 6022, NULL),
(70, 6000, 6023, NULL),
(71, 6000, 6024, NULL),
(72, 6000, 6025, NULL),
(73, 6000, 6026, NULL),
(74, 6000, 6027, NULL),
(75, 6000, 6028, NULL),
(76, 6000, 6029, NULL),
(77, 6000, 6030, NULL),
(78, 6000, 6031, NULL),
(79, 6000, 6032, NULL),
(80, 6, 6900, NULL),
(81, 7, 7000, NULL),
(82, 7, 7100, NULL),
(83, 7, 7900, NULL),
(84, 8, 8000, NULL),
(85, 8000, 8001, NULL),
(86, 8000, 8002, NULL),
(87, 8000, 8003, NULL),
(88, 8, 8900, NULL),
(91, 1, 1, 'user33'),
(92, 1200, 50000, 'user63'),
(93, 1200, 15000, 'user63'),
(94, 2010, 3000, 'user63'),
(95, 0, 0, 'user63'),
(96, 2910, 1500, 'user62'),
(97, 0, 0, ''),
(98, 1500, 6, 'user58'),
(99, 0, 0, 'user58'),
(100, 1000, 1001, 'tester'),
(101, 1, 10, 'user10');

-- --------------------------------------------------------

--
-- Table structure for table `gl_journal_h`
--

CREATE TABLE `gl_journal_h` (
  `id` int(11) NOT NULL,
  `period_id` date NOT NULL,
  `journal_name` varchar(80) DEFAULT NULL,
  `notes` longtext,
  `Post` tinyint(1) DEFAULT '0',
  `uid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gl_journal_h`
--

INSERT INTO `gl_journal_h` (`id`, `period_id`, `journal_name`, `notes`, `Post`, `uid`) VALUES
(1, '2017-03-06', 'Peminjaman Modal', NULL, 0, '5114100026'),
(2, '2017-03-06', 'Peminjaman Modal2', NULL, 0, '5114100026'),
(3, '2017-03-06', 'Peminjaman Modal3', NULL, 0, '5114100100'),
(4, '2017-03-06', 'Peminjaman Modal4', NULL, 0, '5114100100'),
(5, '2017-03-06', 'Peminjaman Modal5', NULL, 0, '5114100070'),
(6, '2017-03-06', 'Peminjaman Modal6', NULL, 0, '5114100070'),
(7, '2017-03-06', 'Pembayaran Purchase 7', NULL, 0, '5114100070'),
(8, '2017-03-06', 'Pembayaran Purchase 8', NULL, 0, '5114100026'),
(9, '2017-03-06', 'Peminjaman Modal9', NULL, 0, '5114100175'),
(10, '2017-03-06', 'Pembayaran Invoice10', NULL, 0, '5114100070'),
(11, '2017-03-06', 'Peminjaman Modal11', NULL, 0, '5114100175'),
(12, '2017-03-06', 'Pembayaran Invoice12', NULL, 0, '5114100026'),
(13, '2017-03-06', 'Peminjaman Modal13', NULL, 0, '5114100167'),
(14, '2017-03-06', 'Peminjaman Modal14', NULL, 0, '5114100167'),
(15, '2017-03-06', 'Pembayaran Invoice15', NULL, 0, '5114100026'),
(16, '2017-03-06', 'Peminjaman Modal16', NULL, 0, '5114100181'),
(17, '2017-03-06', 'Peminjaman Modal17', NULL, 0, '5114100017'),
(18, '2017-03-06', 'Peminjaman Modal18', NULL, 0, '5114100014'),
(19, '2017-03-06', 'Peminjaman Modal19', NULL, 0, '5114100017'),
(20, '2017-03-06', 'Peminjaman Modal20', NULL, 0, '5114100014'),
(21, '2017-03-06', 'Peminjaman Modal21', NULL, 0, '5114100001'),
(22, '2017-03-06', 'Pembayaran Invoice22', NULL, 0, '5114100026'),
(23, '2017-03-06', 'Pembayaran Purchase 23', NULL, 0, '5114100100'),
(24, '2017-03-06', 'Peminjaman Modal24', NULL, 0, '5114100001'),
(25, '2017-03-06', 'Pembayaran Purchase 25', NULL, 0, '5114100181'),
(26, '2017-03-06', 'Peminjaman Modal26', NULL, 0, '5114100151'),
(27, '2017-03-06', 'Peminjaman Modal27', NULL, 0, '5114100151'),
(28, '2017-03-06', 'Pembayaran Modal28', NULL, 0, ''),
(29, '2017-03-06', 'Pembayaran Modal29', NULL, 0, ''),
(30, '2017-03-06', 'Peminjaman Modal30', NULL, 0, '5114100073'),
(31, '2017-03-06', 'Pembayaran Invoice31', NULL, 0, '5114100026'),
(32, '2017-03-06', 'Pembayaran Purchase 32', NULL, 0, '5114100001'),
(33, '2017-03-06', 'Peminjaman Modal33', NULL, 0, '5113100150'),
(34, '2017-03-06', 'Peminjaman Modal34', NULL, 0, '5114100065'),
(35, '2017-03-06', 'Peminjaman Modal35', NULL, 0, '5114100073'),
(36, '2017-03-06', 'Pembayaran Purchase 36', NULL, 0, '5113100150'),
(37, '2017-03-06', 'Peminjaman Modal37', NULL, 0, '5114100065'),
(38, '2017-03-06', 'Pembayaran Purchase 38', NULL, 0, '5114100014'),
(39, '2017-03-06', 'Peminjaman Modal39', NULL, 0, '5114100050'),
(40, '2017-03-06', 'Pembayaran Purchase 40', NULL, 0, '5114100181'),
(41, '2017-03-06', 'Pembayaran Invoice41', NULL, 0, '5113100150'),
(42, '2017-03-06', 'Pembayaran Purchase 42', NULL, 0, '5114100181'),
(43, '2017-03-06', 'Pembayaran Purchase 43', NULL, 0, '5114100175'),
(44, '2017-03-06', 'Peminjaman Modal44', NULL, 0, '5114100050'),
(45, '2017-03-06', 'Pembayaran Purchase 45', NULL, 0, '5114100167'),
(46, '2017-03-06', 'Pembayaran Purchase 46', NULL, 0, '5114100181'),
(47, '2017-03-06', 'Pembayaran Invoice47', NULL, 0, '5114100026'),
(48, '2017-03-06', 'Peminjaman Modal48', NULL, 0, '5114100012'),
(49, '2017-03-06', 'Pembayaran Invoice49', NULL, 0, '5114100014'),
(50, '2017-03-06', 'Pembayaran Invoice50', NULL, 0, '5114100026'),
(51, '2017-03-06', 'Pembayaran Purchase 51', NULL, 0, '5114100181'),
(52, '2017-03-06', 'Peminjaman Modal52', NULL, 0, '5114100012'),
(53, '2017-03-06', 'Pembayaran Invoice53', NULL, 0, '5114100026'),
(54, '2017-03-06', 'Pembayaran Modal54', NULL, 0, ''),
(55, '2017-03-06', 'Pembayaran Modal55', NULL, 0, ''),
(56, '2017-03-06', 'Pembayaran Modal56', NULL, 0, ''),
(57, '2017-03-06', 'Pembayaran Purchase 57', NULL, 0, '5114100065'),
(58, '2017-03-06', 'Pembayaran Modal58', NULL, 0, ''),
(59, '2017-03-06', 'Pembayaran Invoice59', NULL, 0, '5114100026'),
(60, '2017-03-06', 'Peminjaman Modal60', NULL, 0, '5114100012'),
(61, '2017-03-06', 'Peminjaman Modal61', NULL, 0, '5114100012'),
(62, '2017-03-06', 'Pembayaran Invoice62', NULL, 0, '5114100001'),
(63, '2017-03-06', 'Pembayaran Modal63', NULL, 0, ''),
(64, '2017-03-06', 'Pembayaran Invoice64', NULL, 0, '5114100065'),
(65, '2017-03-06', 'Pembayaran Invoice65', NULL, 0, '5114100026'),
(68, '2017-03-06', 'Pembayaran Invoice68', NULL, 0, '5114100026'),
(69, '2017-03-06', 'Pembayaran Invoice69', NULL, 0, '5114100175'),
(70, '2017-03-06', 'Peminjaman Modal70', NULL, 0, '511400704'),
(71, '2017-03-06', 'Peminjaman Modal71', NULL, 0, '511400704'),
(72, '2017-03-06', 'Pembayaran Purchase 72', NULL, 0, '5114100012'),
(73, '2017-03-06', 'Pembayaran Invoice73', NULL, 0, '5114100026'),
(74, '2017-03-06', 'Pembayaran Modal74', NULL, 0, ''),
(76, '2017-03-06', 'Pembayaran Purchase 76', NULL, 0, '5114100151'),
(77, '2017-03-06', 'Pembayaran Purchase 77', NULL, 0, '511400704'),
(78, '2017-03-06', 'Pembayaran Purchase 78', NULL, 0, '5114100012'),
(79, '2017-03-06', 'Pembayaran Purchase 79', NULL, 0, '5114100151'),
(80, '2017-03-06', 'Peminjaman Modal80', NULL, 0, '5114100064'),
(81, '2017-03-06', 'Pembayaran Invoice81', NULL, 0, '5114100167'),
(82, '2017-03-06', 'Peminjaman Modal82', NULL, 0, '5114100064'),
(83, '2017-03-06', 'Peminjaman Modal83', NULL, 0, '5114100164'),
(84, '2017-03-06', 'Peminjaman Modal84', NULL, 0, '5114100180'),
(85, '2017-03-06', 'Pembayaran Purchase 85', NULL, 0, '5114100017'),
(86, '2017-03-06', 'Peminjaman Modal86', NULL, 0, '5114100164'),
(87, '2017-03-06', 'Pembayaran Purchase 87', NULL, 0, '5114100073'),
(88, '2017-03-06', 'Pembayaran Purchase 87', NULL, 0, '5114100014'),
(89, '2017-03-06', 'Peminjaman Modal89', NULL, 0, '5114100180'),
(90, '2017-03-06', 'Pembayaran Purchase 90', NULL, 0, '5114100064'),
(91, '2017-03-06', 'Pembayaran Modal91', NULL, 0, ''),
(92, '2017-03-06', 'Pembayaran Invoice92', NULL, 0, '5114100001'),
(93, '2017-03-06', 'Pembayaran Invoice93', NULL, 0, '5114100181'),
(94, '2017-03-06', 'Pembayaran Invoice93', NULL, 0, '5114100181'),
(95, '2017-03-06', 'Peminjaman Modal95', NULL, 0, '5114100180'),
(96, '2017-03-06', 'Pembayaran Invoice96', NULL, 0, '5114100001'),
(97, '2017-03-06', 'Pembayaran Modal97', NULL, 0, ''),
(98, '2017-03-06', 'Pembayaran Invoice98', NULL, 0, '5114100181'),
(99, '2017-03-06', 'Pembayaran Invoice98', NULL, 0, '5114100181'),
(100, '2017-03-06', 'Pembayaran Invoice100', NULL, 0, '5114100017'),
(101, '2017-03-06', 'Pembayaran Invoice101', NULL, 0, '5114100012'),
(102, '2017-03-06', 'Pembayaran Invoice102', NULL, 0, '5114100014'),
(104, '2017-03-06', 'Pembayaran Purchase 104', NULL, 0, '5114100164'),
(105, '2017-03-06', 'Pembayaran Invoice105', NULL, 0, '5114100001'),
(106, '2017-03-06', 'Pembayaran Purchase 106', NULL, 0, '5114100175'),
(107, '2017-03-06', 'Pembayaran Invoice107', NULL, 0, '5114100181'),
(108, '2017-03-06', 'Pembayaran Invoice108', NULL, 0, '5114100064'),
(109, '2017-03-06', 'Pembayaran Purchase 109', NULL, 0, '5114100050'),
(110, '2017-03-06', 'Pembayaran Invoice110', NULL, 0, '5114100001'),
(111, '2017-03-06', 'Pembayaran Invoice111', NULL, 0, '5114100073'),
(112, '2017-03-06', 'Pembayaran Invoice112', NULL, 0, '5114100151'),
(113, '2017-03-06', 'Pembayaran Invoice113', NULL, 0, '5114100167'),
(115, '2017-03-06', 'Pembayaran Invoice115', NULL, 0, '511400704'),
(116, '2017-03-06', 'Pembayaran Purchase 116', NULL, 0, '5114100001'),
(117, '2017-03-06', 'Pembayaran Invoice117', NULL, 0, '5114100175'),
(118, '2017-03-06', 'Pembayaran Purchase 118', NULL, 0, '5114100012'),
(119, '2017-03-06', 'Pembayaran Invoice119', NULL, 0, '5114100100'),
(120, '2017-03-06', 'Pembayaran Purchase 120', NULL, 0, '5114100167'),
(121, '2017-03-06', 'Pembayaran Invoice121', NULL, 0, '5114100001'),
(122, '2017-03-06', 'Pembayaran Invoice122', NULL, 0, '5114100012'),
(123, '2017-03-06', 'Pembayaran Invoice123', NULL, 0, '511400704'),
(124, '2017-03-06', 'Pembayaran Invoice124', NULL, 0, '5114100100'),
(125, '2017-03-06', 'Pembayaran Invoice125', NULL, 0, '5114100017'),
(126, '2017-03-06', 'Pembayaran Invoice126', NULL, 0, '5114100001'),
(127, '2017-03-06', 'Pembayaran Invoice127', NULL, 0, '5114100181'),
(128, '2017-03-06', 'Pembayaran Invoice127', NULL, 0, '5114100181'),
(129, '2017-03-06', 'Pembayaran Purchase 129', NULL, 0, '5114100065'),
(130, '2017-03-06', 'Pembayaran Invoice130', NULL, 0, '5114100151'),
(131, '2017-03-06', 'Pembayaran Invoice131', NULL, 0, '5114100181'),
(132, '2017-03-06', 'Pembayaran Invoice132', NULL, 0, '5114100065'),
(133, '2017-03-06', 'Pembayaran Invoice133', NULL, 0, '5114100164'),
(135, '2017-03-06', 'Pembayaran Invoice135', NULL, 0, '5114100181'),
(136, '2017-03-06', 'Pembayaran Invoice136', NULL, 0, '5114100001'),
(137, '2017-03-06', 'Pembayaran Purchase 137', NULL, 0, '5114100012'),
(138, '2017-03-06', 'Pembayaran Purchase 138', NULL, 0, '5114100167'),
(140, '2017-03-06', 'Pembayaran Purchase 140', NULL, 0, '5114100017'),
(141, '2017-03-06', 'Pembayaran Invoice141', NULL, 0, '5114100181'),
(142, '2017-03-06', 'Pembayaran Invoice142', NULL, 0, '5114100012'),
(143, '2017-03-06', 'Peminjaman Modal143', NULL, 0, '5114100151'),
(144, '2017-03-06', 'Pembayaran Invoice144', NULL, 0, '5114100017'),
(145, '2017-03-06', 'Pembayaran Purchase 145', NULL, 0, '5114100181'),
(146, '2017-03-06', 'Pembayaran Invoice146', NULL, 0, '5114100167'),
(148, '2017-03-06', 'Pembayaran Purchase 148', NULL, 0, '5114100151'),
(149, '2017-03-06', 'Pembayaran Invoice149', NULL, 0, '5114100181'),
(150, '2017-03-06', 'Pembayaran Purchase 150', NULL, 0, '5114100012'),
(151, '2017-03-06', 'Pembayaran Invoice151', NULL, 0, '5114100017'),
(152, '2017-03-06', 'Pembayaran Invoice152', NULL, 0, '5114100151'),
(153, '2017-03-06', 'Pembayaran Invoice153', NULL, 0, '5114100012'),
(154, '2017-03-06', 'Pembayaran Invoice153', NULL, 0, '5114100012'),
(155, '2017-03-06', 'Pembayaran Invoice153', NULL, 0, '5114100012'),
(156, '2017-03-06', 'Pembayaran Invoice156', NULL, 0, '5114100012'),
(157, '2017-03-06', 'Pembayaran Invoice157', NULL, 0, '5114100001'),
(158, '2017-03-06', 'Pembayaran Invoice158', NULL, 0, '5114100017'),
(159, '2017-03-06', 'Pembayaran Invoice159', NULL, 0, '5114100012'),
(160, '2017-03-06', 'Pembayaran Purchase 160', NULL, 0, '5114100012'),
(161, '2017-03-06', 'Pembayaran Invoice161', NULL, 0, '5114100012'),
(162, '2017-03-06', 'Pembayaran Invoice162', NULL, 0, '5114100017'),
(163, '2017-03-06', 'Peminjaman Modal163', NULL, 0, '1316100030'),
(164, '2017-03-06', 'Peminjaman Modal164', NULL, 0, '1316100030'),
(166, '2017-03-06', 'Pembayaran Purchase 165', NULL, 0, '1316100030'),
(167, '2017-03-06', 'Pembayaran Invoice167', NULL, 0, '1316100030'),
(168, '2017-03-06', 'Pembayaran Purchase 168', NULL, 0, '1316100030'),
(169, '2017-03-06', 'Pembayaran Invoice169', NULL, 0, '1316100030'),
(170, '2017-03-06', 'Pembayaran Invoice170', NULL, 0, '1316100030'),
(171, '2017-03-06', 'Pembayaran Invoice171', NULL, 0, '1316100030'),
(172, '2017-03-06', 'Pembayaran Invoice172', NULL, 0, '1316100030'),
(173, '2017-03-06', 'Pembayaran Invoice173', NULL, 0, '1316100030'),
(174, '2017-03-06', 'Pembayaran Invoice174', NULL, 0, '1316100030'),
(175, '2017-03-06', 'Pembayaran Invoice175', NULL, 0, '1316100030'),
(176, '2017-03-06', 'Peminjaman Modal176', NULL, 0, '5113100043'),
(177, '2017-03-06', 'Pembayaran Invoice177', NULL, 0, '1316100030'),
(178, '2017-03-06', 'Pembayaran Invoice178', NULL, 0, '1316100030'),
(179, '2017-03-06', 'Pembayaran Purchase 179', NULL, 0, '5113100043'),
(180, '2017-03-06', 'Pembayaran Invoice180', NULL, 0, '1316100030'),
(181, '2017-03-06', 'Pembayaran Purchase 181', NULL, 0, '1316100030'),
(182, '2017-03-06', 'Peminjaman Modal182', NULL, 0, '1316100030'),
(183, '2017-03-06', 'Pembayaran Invoice183', NULL, 0, '1316100030');

-- --------------------------------------------------------

--
-- Table structure for table `gl_journal_l`
--

CREATE TABLE `gl_journal_l` (
  `id` int(20) NOT NULL,
  `journal_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `line_debit` bigint(20) DEFAULT '0',
  `line_credit` bigint(20) DEFAULT '0',
  `uid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gl_journal_l`
--

INSERT INTO `gl_journal_l` (`id`, `journal_id`, `acc_id`, `line_debit`, `line_credit`, `uid`) VALUES
(1, 0, 3000, 0, 4000000000, '5114100026'),
(2, 0, 1000, 4000000000, 0, '5114100026'),
(3, 2, 2100, 0, 1000000000, '5114100026'),
(4, 2, 1000, 1000000000, 0, '5114100026'),
(5, 3, 3000, 0, 3500000000, '5114100100'),
(6, 3, 1000, 3500000000, 0, '5114100100'),
(7, 4, 2100, 0, 1500000000, '5114100100'),
(8, 4, 1000, 1500000000, 0, '5114100100'),
(9, 5, 2100, 0, 1500000000, '5114100070'),
(10, 5, 1000, 1500000000, 0, '5114100070'),
(11, 6, 3000, 0, 4500000000, '5114100070'),
(12, 6, 1000, 4500000000, 0, '5114100070'),
(13, 7, 1400, 4730400000, 0, '5114100070'),
(14, 7, 1000, 0, 4730400000, '5114100070'),
(15, 8, 1400, 65000000, 0, '5114100026'),
(16, 8, 1000, 0, 65000000, '5114100026'),
(17, 9, 3000, 0, 200000000, '5114100175'),
(18, 9, 1000, 200000000, 0, '5114100175'),
(19, 10, 4000, 0, 0, '5114100070'),
(20, 10, 1000, 0, 0, '5114100070'),
(21, 10, 1400, 0, 0, '5114100070'),
(22, 10, 5000, 0, 0, '5114100070'),
(23, 11, 2100, 0, 50000000, '5114100175'),
(24, 11, 1000, 50000000, 0, '5114100175'),
(25, 12, 4000, 0, 0, '5114100026'),
(26, 12, 1000, 0, 0, '5114100026'),
(27, 12, 1400, 0, 0, '5114100026'),
(28, 12, 5000, 0, 0, '5114100026'),
(29, 13, 3000, 0, 800000000, '5114100167'),
(30, 13, 1000, 800000000, 0, '5114100167'),
(31, 14, 2100, 0, 200000000, '5114100167'),
(32, 14, 1000, 200000000, 0, '5114100167'),
(33, 15, 4000, 0, 0, '5114100026'),
(34, 15, 1000, 0, 0, '5114100026'),
(35, 15, 1400, 0, 0, '5114100026'),
(36, 15, 5000, 0, 0, '5114100026'),
(37, 16, 3000, 0, 1000000000, '5114100181'),
(38, 16, 1000, 1000000000, 0, '5114100181'),
(39, 17, 3000, 0, 1700000000, '5114100017'),
(40, 17, 1000, 1700000000, 0, '5114100017'),
(41, 18, 3000, 0, 800000000, '5114100014'),
(42, 18, 1000, 800000000, 0, '5114100014'),
(43, 19, 2100, 0, 2500000000, '5114100017'),
(44, 19, 1000, 2500000000, 0, '5114100017'),
(45, 20, 2100, 0, 1200000000, '5114100014'),
(46, 20, 1000, 1200000000, 0, '5114100014'),
(47, 21, 3000, 0, 700000000, '5114100001'),
(48, 21, 1000, 700000000, 0, '5114100001'),
(49, 22, 4000, 0, 0, '5114100026'),
(50, 22, 1000, 0, 0, '5114100026'),
(51, 22, 1400, 0, 0, '5114100026'),
(52, 22, 5000, 0, 0, '5114100026'),
(53, 23, 1400, 97500000, 0, '5114100100'),
(54, 23, 1000, 0, 97500000, '5114100100'),
(55, 24, 2100, 0, 1300000000, '5114100001'),
(56, 24, 1000, 1300000000, 0, '5114100001'),
(57, 25, 1400, 260000, 0, '5114100181'),
(58, 25, 1000, 0, 260000, '5114100181'),
(59, 26, 3000, 0, 1000000000, '5114100151'),
(60, 26, 1000, 1000000000, 0, '5114100151'),
(61, 27, 2100, 0, 1500000000, '5114100151'),
(62, 27, 1000, 1500000000, 0, '5114100151'),
(63, 28, 3000, 0, 0, ''),
(64, 28, 1000, 0, 0, ''),
(65, 29, 3000, 0, 0, ''),
(66, 29, 1000, 0, 0, ''),
(67, 30, 3000, 0, 4000000000, '5114100073'),
(68, 30, 1000, 4000000000, 0, '5114100073'),
(69, 31, 4000, 0, 0, '5114100026'),
(70, 31, 1000, 0, 0, '5114100026'),
(71, 31, 1400, 0, 0, '5114100026'),
(72, 31, 5000, 0, 0, '5114100026'),
(73, 32, 1400, 9000000, 0, '5114100001'),
(74, 32, 1000, 0, 9000000, '5114100001'),
(75, 33, 2100, 0, 10000000, '5113100150'),
(76, 33, 1000, 10000000, 0, '5113100150'),
(77, 34, 3000, 0, 1000000000, '5114100065'),
(78, 34, 1000, 1000000000, 0, '5114100065'),
(79, 35, 2100, 0, 1000000000, '5114100073'),
(80, 35, 1000, 1000000000, 0, '5114100073'),
(81, 36, 1400, 650000, 0, '5113100150'),
(82, 36, 1000, 0, 650000, '5113100150'),
(83, 37, 2100, 0, 500000000, '5114100065'),
(84, 37, 1000, 500000000, 0, '5114100065'),
(85, 38, 1400, 1950000, 0, '5114100014'),
(86, 38, 1000, 0, 1950000, '5114100014'),
(87, 39, 3000, 0, 2000000000, '5114100050'),
(88, 39, 1000, 2000000000, 0, '5114100050'),
(89, 40, 1400, 48000, 0, '5114100181'),
(90, 40, 1000, 0, 48000, '5114100181'),
(91, 41, 4000, 0, 0, '5113100150'),
(92, 41, 1000, 0, 0, '5113100150'),
(93, 41, 1400, 0, 0, '5113100150'),
(94, 41, 5000, 0, 0, '5113100150'),
(95, 42, 1400, 1800000, 0, '5114100181'),
(96, 42, 1000, 0, 1800000, '5114100181'),
(97, 43, 1400, 124000, 0, '5114100175'),
(98, 43, 1000, 0, 124000, '5114100175'),
(99, 44, 2100, 0, 1000000000, '5114100050'),
(100, 44, 1000, 1000000000, 0, '5114100050'),
(101, 45, 1400, 780000, 0, '5114100167'),
(102, 45, 1000, 0, 780000, '5114100167'),
(103, 46, 1400, 26000, 0, '5114100181'),
(104, 46, 1000, 0, 26000, '5114100181'),
(105, 47, 4000, 0, 0, '5114100026'),
(106, 47, 1000, 0, 0, '5114100026'),
(107, 47, 1400, 0, 0, '5114100026'),
(108, 47, 5000, 0, 0, '5114100026'),
(109, 48, 3000, 0, 0, '5114100012'),
(110, 48, 1000, 0, 0, '5114100012'),
(111, 49, 4000, 0, 0, '5114100014'),
(112, 49, 1000, 0, 0, '5114100014'),
(113, 49, 1400, 0, 0, '5114100014'),
(114, 49, 5000, 0, 0, '5114100014'),
(115, 50, 4000, 0, 0, '5114100026'),
(116, 50, 1000, 0, 0, '5114100026'),
(117, 50, 1400, 0, 0, '5114100026'),
(118, 50, 5000, 0, 0, '5114100026'),
(119, 51, 1400, 50800, 0, '5114100181'),
(120, 51, 1000, 0, 50800, '5114100181'),
(121, 52, 2100, 0, 1500000000, '5114100012'),
(122, 52, 1000, 1500000000, 0, '5114100012'),
(123, 53, 4000, 0, 0, '5114100026'),
(124, 53, 1000, 0, 0, '5114100026'),
(125, 53, 1400, 0, 0, '5114100026'),
(126, 53, 5000, 0, 0, '5114100026'),
(127, 54, 3000, 0, 0, ''),
(128, 54, 1000, 0, 0, ''),
(129, 55, 3000, 0, 0, ''),
(130, 55, 1000, 0, 0, ''),
(131, 56, 3000, 0, 0, ''),
(132, 56, 1000, 0, 0, ''),
(133, 57, 1400, 18000000, 0, '5114100065'),
(134, 57, 1000, 0, 18000000, '5114100065'),
(135, 58, 3000, 0, 0, ''),
(136, 58, 1000, 0, 0, ''),
(137, 59, 4000, 0, 0, '5114100026'),
(138, 59, 1000, 0, 0, '5114100026'),
(139, 59, 1400, 0, 0, '5114100026'),
(140, 59, 5000, 0, 0, '5114100026'),
(141, 60, 3000, 0, 3500000000, '5114100012'),
(142, 60, 1000, 3500000000, 0, '5114100012'),
(143, 61, 3000, 0, 0, '5114100012'),
(144, 61, 1000, 0, 0, '5114100012'),
(145, 62, 4000, 0, 0, '5114100001'),
(146, 62, 1000, 0, 0, '5114100001'),
(147, 62, 1400, 0, 0, '5114100001'),
(148, 62, 5000, 0, 0, '5114100001'),
(149, 63, 3000, 0, 0, ''),
(150, 63, 1000, 0, 0, ''),
(151, 64, 4000, 0, 0, '5114100065'),
(152, 64, 1000, 0, 0, '5114100065'),
(153, 64, 1400, 0, 0, '5114100065'),
(154, 64, 5000, 0, 0, '5114100065'),
(155, 65, 4000, 0, 0, '5114100026'),
(156, 65, 1000, 0, 0, '5114100026'),
(157, 65, 1400, 0, 0, '5114100026'),
(158, 65, 5000, 0, 0, '5114100026'),
(163, 68, 4000, 0, 0, '5114100026'),
(164, 68, 1000, 0, 0, '5114100026'),
(165, 68, 1400, 0, 0, '5114100026'),
(166, 68, 5000, 0, 0, '5114100026'),
(167, 69, 4000, 0, 0, '5114100175'),
(168, 69, 1000, 0, 0, '5114100175'),
(169, 69, 1400, 0, 0, '5114100175'),
(170, 69, 5000, 0, 0, '5114100175'),
(171, 70, 2100, 0, 200000000, '511400704'),
(172, 70, 1000, 200000000, 0, '511400704'),
(173, 71, 3000, 0, 500000000, '511400704'),
(174, 71, 1000, 500000000, 0, '511400704'),
(175, 72, 1400, 4600000, 0, '5114100012'),
(176, 72, 1000, 0, 4600000, '5114100012'),
(177, 73, 4000, 0, 0, '5114100026'),
(178, 73, 1000, 0, 0, '5114100026'),
(179, 73, 1400, 0, 0, '5114100026'),
(180, 73, 5000, 0, 0, '5114100026'),
(181, 74, 3000, 0, 0, ''),
(182, 74, 1000, 0, 0, ''),
(185, 76, 1400, 104000000, 0, '5114100151'),
(186, 76, 1000, 0, 104000000, '5114100151'),
(187, 77, 1400, 54000000, 0, '511400704'),
(188, 77, 1000, 0, 54000000, '511400704'),
(189, 78, 1400, 1550000, 0, '5114100012'),
(190, 78, 1000, 0, 1550000, '5114100012'),
(191, 79, 1400, 27000000, 0, '5114100151'),
(192, 79, 1000, 0, 27000000, '5114100151'),
(193, 80, 3000, 0, 2500000000, '5114100064'),
(194, 80, 1000, 2500000000, 0, '5114100064'),
(195, 81, 4000, 0, 0, '5114100167'),
(196, 81, 1000, 0, 0, '5114100167'),
(197, 81, 1400, 0, 0, '5114100167'),
(198, 81, 5000, 0, 0, '5114100167'),
(199, 82, 2100, 0, 1500000000, '5114100064'),
(200, 82, 1000, 1500000000, 0, '5114100064'),
(201, 83, 2100, 0, 50000000, '5114100164'),
(202, 83, 1000, 50000000, 0, '5114100164'),
(203, 84, 3000, 0, 0, '5114100180'),
(204, 84, 1000, 0, 0, '5114100180'),
(205, 85, 1400, 12740000, 0, '5114100017'),
(206, 85, 1000, 0, 12740000, '5114100017'),
(207, 86, 3000, 0, 200000000, '5114100164'),
(208, 86, 1000, 200000000, 0, '5114100164'),
(209, 87, 1400, 15600000, 0, '5114100073'),
(210, 87, 1000, 0, 15600000, '5114100073'),
(211, 87, 1400, 975000, 0, '5114100014'),
(212, 87, 1000, 0, 975000, '5114100014'),
(213, 89, 3000, 0, 250000000, '5114100180'),
(214, 89, 1000, 250000000, 0, '5114100180'),
(215, 90, 1400, 91000000, 0, '5114100064'),
(216, 90, 1000, 0, 91000000, '5114100064'),
(217, 91, 3000, 0, 0, ''),
(218, 91, 1000, 0, 0, ''),
(219, 92, 4000, 0, 0, '5114100001'),
(220, 92, 1000, 0, 0, '5114100001'),
(221, 92, 1400, 0, 0, '5114100001'),
(222, 92, 5000, 0, 0, '5114100001'),
(223, 93, 4000, 0, 0, '5114100181'),
(224, 93, 1000, 0, 0, '5114100181'),
(225, 93, 1400, 0, 0, '5114100181'),
(226, 93, 5000, 0, 0, '5114100181'),
(227, 93, 4000, 0, 0, '5114100181'),
(228, 93, 1000, 0, 0, '5114100181'),
(229, 93, 1400, 0, 0, '5114100181'),
(230, 93, 5000, 0, 0, '5114100181'),
(231, 95, 2100, 0, 100000000, '5114100180'),
(232, 95, 1000, 100000000, 0, '5114100180'),
(233, 96, 4000, 0, 0, '5114100001'),
(234, 96, 1000, 0, 0, '5114100001'),
(235, 96, 1400, 0, 0, '5114100001'),
(236, 96, 5000, 0, 0, '5114100001'),
(237, 97, 3000, 0, 0, ''),
(238, 97, 1000, 0, 0, ''),
(239, 98, 4000, 0, 0, '5114100181'),
(240, 98, 1000, 0, 0, '5114100181'),
(241, 98, 1400, 0, 0, '5114100181'),
(242, 98, 5000, 0, 0, '5114100181'),
(243, 98, 4000, 0, 0, '5114100181'),
(244, 98, 1000, 0, 0, '5114100181'),
(245, 98, 1400, 0, 0, '5114100181'),
(246, 98, 5000, 0, 0, '5114100181'),
(247, 100, 4000, 0, 0, '5114100017'),
(248, 100, 1000, 0, 0, '5114100017'),
(249, 100, 1400, 0, 0, '5114100017'),
(250, 100, 5000, 0, 0, '5114100017'),
(251, 101, 4000, 0, 0, '5114100012'),
(252, 101, 1000, 0, 0, '5114100012'),
(253, 101, 1400, 0, 0, '5114100012'),
(254, 101, 5000, 0, 0, '5114100012'),
(255, 102, 4000, 0, 0, '5114100014'),
(256, 102, 1000, 0, 0, '5114100014'),
(257, 102, 1400, 0, 0, '5114100014'),
(258, 102, 5000, 0, 0, '5114100014'),
(261, 104, 1400, 220000000, 0, '5114100164'),
(262, 104, 1000, 0, 220000000, '5114100164'),
(263, 105, 4000, 0, 0, '5114100001'),
(264, 105, 1000, 0, 0, '5114100001'),
(265, 105, 1400, 0, 0, '5114100001'),
(266, 105, 5000, 0, 0, '5114100001'),
(267, 106, 1400, 180000, 0, '5114100175'),
(268, 106, 1000, 0, 180000, '5114100175'),
(269, 107, 4000, 0, 0, '5114100181'),
(270, 107, 1000, 0, 0, '5114100181'),
(271, 107, 1400, 0, 0, '5114100181'),
(272, 107, 5000, 0, 0, '5114100181'),
(273, 108, 4000, 0, 0, '5114100064'),
(274, 108, 1000, 0, 0, '5114100064'),
(275, 108, 1400, 0, 0, '5114100064'),
(276, 108, 5000, 0, 0, '5114100064'),
(277, 109, 1400, 130000000, 0, '5114100050'),
(278, 109, 1000, 0, 130000000, '5114100050'),
(279, 110, 4000, 0, 0, '5114100001'),
(280, 110, 1000, 0, 0, '5114100001'),
(281, 110, 1400, 0, 0, '5114100001'),
(282, 110, 5000, 0, 0, '5114100001'),
(283, 111, 4000, 0, 0, '5114100073'),
(284, 111, 1000, 0, 0, '5114100073'),
(285, 111, 1400, 0, 0, '5114100073'),
(286, 111, 5000, 0, 0, '5114100073'),
(287, 112, 4000, 0, 0, '5114100151'),
(288, 112, 1000, 0, 0, '5114100151'),
(289, 112, 1400, 0, 0, '5114100151'),
(290, 112, 5000, 0, 0, '5114100151'),
(291, 113, 4000, 0, 0, '5114100167'),
(292, 113, 1000, 0, 0, '5114100167'),
(293, 113, 1400, 0, 0, '5114100167'),
(294, 113, 5000, 0, 0, '5114100167'),
(299, 115, 4000, 0, 0, '511400704'),
(300, 115, 1000, 0, 0, '511400704'),
(301, 115, 1400, 0, 0, '511400704'),
(302, 115, 5000, 0, 0, '511400704'),
(303, 116, 1400, 9000000, 0, '5114100001'),
(304, 116, 1000, 0, 9000000, '5114100001'),
(305, 117, 4000, 0, 0, '5114100175'),
(306, 117, 1000, 0, 0, '5114100175'),
(307, 117, 1400, 0, 0, '5114100175'),
(308, 117, 5000, 0, 0, '5114100175'),
(309, 118, 1400, 6000000, 0, '5114100012'),
(310, 118, 1000, 0, 6000000, '5114100012'),
(311, 119, 4000, 0, 0, '5114100100'),
(312, 119, 1000, 0, 0, '5114100100'),
(313, 119, 1400, 0, 0, '5114100100'),
(314, 119, 5000, 0, 0, '5114100100'),
(315, 120, 1400, 1100000, 0, '5114100167'),
(316, 120, 1000, 0, 1100000, '5114100167'),
(317, 121, 4000, 0, 0, '5114100001'),
(318, 121, 1000, 0, 0, '5114100001'),
(319, 121, 1400, 0, 0, '5114100001'),
(320, 121, 5000, 0, 0, '5114100001'),
(321, 122, 4000, 0, 0, '5114100012'),
(322, 122, 1000, 0, 0, '5114100012'),
(323, 122, 1400, 0, 0, '5114100012'),
(324, 122, 5000, 0, 0, '5114100012'),
(325, 123, 4000, 0, 0, '511400704'),
(326, 123, 1000, 0, 0, '511400704'),
(327, 123, 1400, 0, 0, '511400704'),
(328, 123, 5000, 0, 0, '511400704'),
(329, 124, 4000, 0, 0, '5114100100'),
(330, 124, 1000, 0, 0, '5114100100'),
(331, 124, 1400, 0, 0, '5114100100'),
(332, 124, 5000, 0, 0, '5114100100'),
(333, 125, 4000, 0, 0, '5114100017'),
(334, 125, 1000, 0, 0, '5114100017'),
(335, 125, 1400, 0, 0, '5114100017'),
(336, 125, 5000, 0, 0, '5114100017'),
(337, 126, 4000, 0, 0, '5114100001'),
(338, 126, 1000, 0, 0, '5114100001'),
(339, 126, 1400, 0, 0, '5114100001'),
(340, 126, 5000, 0, 0, '5114100001'),
(341, 127, 4000, 0, 0, '5114100181'),
(342, 127, 1000, 0, 0, '5114100181'),
(343, 127, 1400, 0, 0, '5114100181'),
(344, 127, 5000, 0, 0, '5114100181'),
(345, 127, 4000, 0, 0, '5114100181'),
(346, 127, 1000, 0, 0, '5114100181'),
(347, 127, 1400, 0, 0, '5114100181'),
(348, 127, 5000, 0, 0, '5114100181'),
(349, 129, 1400, 2400000, 0, '5114100065'),
(350, 129, 1000, 0, 2400000, '5114100065'),
(351, 130, 4000, 0, 0, '5114100151'),
(352, 130, 1000, 0, 0, '5114100151'),
(353, 130, 1400, 0, 0, '5114100151'),
(354, 130, 5000, 0, 0, '5114100151'),
(355, 131, 4000, 0, 0, '5114100181'),
(356, 131, 1000, 0, 0, '5114100181'),
(357, 131, 1400, 0, 0, '5114100181'),
(358, 131, 5000, 0, 0, '5114100181'),
(359, 132, 4000, 0, 0, '5114100065'),
(360, 132, 1000, 0, 0, '5114100065'),
(361, 132, 1400, 0, 0, '5114100065'),
(362, 132, 5000, 0, 0, '5114100065'),
(363, 133, 4000, 0, 0, '5114100164'),
(364, 133, 1000, 0, 0, '5114100164'),
(365, 133, 1400, 0, 0, '5114100164'),
(366, 133, 5000, 0, 0, '5114100164'),
(371, 135, 4000, 0, 0, '5114100181'),
(372, 135, 1000, 0, 0, '5114100181'),
(373, 135, 1400, 0, 0, '5114100181'),
(374, 135, 5000, 0, 0, '5114100181'),
(375, 136, 4000, 0, 0, '5114100001'),
(376, 136, 1000, 0, 0, '5114100001'),
(377, 136, 1400, 0, 0, '5114100001'),
(378, 136, 5000, 0, 0, '5114100001'),
(379, 137, 1400, 7000000, 0, '5114100012'),
(380, 137, 1000, 0, 7000000, '5114100012'),
(381, 138, 1400, 780000, 0, '5114100167'),
(382, 138, 1000, 0, 780000, '5114100167'),
(385, 140, 1400, 3600000, 0, '5114100017'),
(386, 140, 1000, 0, 3600000, '5114100017'),
(387, 141, 4000, 0, 0, '5114100181'),
(388, 141, 1000, 0, 0, '5114100181'),
(389, 141, 1400, 0, 0, '5114100181'),
(390, 141, 5000, 0, 0, '5114100181'),
(391, 142, 4000, 0, 0, '5114100012'),
(392, 142, 1000, 0, 0, '5114100012'),
(393, 142, 1400, 0, 0, '5114100012'),
(394, 142, 5000, 0, 0, '5114100012'),
(395, 143, 2100, 0, 5000000, '5114100151'),
(396, 143, 1000, 5000000, 0, '5114100151'),
(397, 144, 4000, 0, 0, '5114100017'),
(398, 144, 1000, 0, 0, '5114100017'),
(399, 144, 1400, 0, 0, '5114100017'),
(400, 144, 5000, 0, 0, '5114100017'),
(401, 145, 1400, 500000, 0, '5114100181'),
(402, 145, 1000, 0, 500000, '5114100181'),
(403, 146, 4000, 0, 0, '5114100167'),
(404, 146, 1000, 0, 0, '5114100167'),
(405, 146, 1400, 0, 0, '5114100167'),
(406, 146, 5000, 0, 0, '5114100167'),
(411, 148, 1400, 10400000, 0, '5114100151'),
(412, 148, 1000, 0, 10400000, '5114100151'),
(413, 149, 4000, 0, 0, '5114100181'),
(414, 149, 1000, 0, 0, '5114100181'),
(415, 149, 1400, 0, 0, '5114100181'),
(416, 149, 5000, 0, 0, '5114100181'),
(417, 150, 1400, 300000, 0, '5114100012'),
(418, 150, 1000, 0, 300000, '5114100012'),
(419, 151, 4000, 0, 0, '5114100017'),
(420, 151, 1000, 0, 0, '5114100017'),
(421, 151, 1400, 0, 0, '5114100017'),
(422, 151, 5000, 0, 0, '5114100017'),
(423, 152, 4000, 0, 0, '5114100151'),
(424, 152, 1000, 0, 0, '5114100151'),
(425, 152, 1400, 0, 0, '5114100151'),
(426, 152, 5000, 0, 0, '5114100151'),
(427, 153, 4000, 0, 0, '5114100012'),
(428, 153, 1000, 0, 0, '5114100012'),
(429, 153, 1400, 0, 0, '5114100012'),
(430, 153, 5000, 0, 0, '5114100012'),
(431, 153, 4000, 0, 0, '5114100012'),
(432, 153, 1000, 0, 0, '5114100012'),
(433, 153, 1400, 0, 0, '5114100012'),
(434, 153, 5000, 0, 0, '5114100012'),
(435, 153, 4000, 0, 0, '5114100012'),
(436, 153, 1000, 0, 0, '5114100012'),
(437, 153, 1400, 0, 0, '5114100012'),
(438, 153, 5000, 0, 0, '5114100012'),
(439, 156, 4000, 0, 0, '5114100012'),
(440, 156, 1000, 0, 0, '5114100012'),
(441, 156, 1400, 0, 0, '5114100012'),
(442, 156, 5000, 0, 0, '5114100012'),
(443, 157, 4000, 0, 0, '5114100001'),
(444, 157, 1000, 0, 0, '5114100001'),
(445, 157, 1400, 0, 0, '5114100001'),
(446, 157, 5000, 0, 0, '5114100001'),
(447, 158, 4000, 0, 0, '5114100017'),
(448, 158, 1000, 0, 0, '5114100017'),
(449, 158, 1400, 0, 0, '5114100017'),
(450, 158, 5000, 0, 0, '5114100017'),
(451, 159, 4000, 0, 0, '5114100012'),
(452, 159, 1000, 0, 0, '5114100012'),
(453, 159, 1400, 0, 0, '5114100012'),
(454, 159, 5000, 0, 0, '5114100012'),
(455, 160, 1400, 6000000, 0, '5114100012'),
(456, 160, 1000, 0, 6000000, '5114100012'),
(457, 161, 4000, 0, 0, '5114100012'),
(458, 161, 1000, 0, 0, '5114100012'),
(459, 161, 1400, 0, 0, '5114100012'),
(460, 161, 5000, 0, 0, '5114100012'),
(461, 162, 4000, 0, 0, '5114100017'),
(462, 162, 1000, 0, 0, '5114100017'),
(463, 162, 1400, 0, 0, '5114100017'),
(464, 162, 5000, 0, 0, '5114100017'),
(465, 163, 3000, 0, 1000000, '1316100030'),
(466, 163, 1000, 1000000, 0, '1316100030'),
(469, 165, 2100, 0, 2000000, '1316100030'),
(470, 165, 1000, 2000000, 0, '1316100030'),
(471, 165, 1400, 19200, 0, '1316100030'),
(472, 165, 1000, 0, 19200, '1316100030'),
(473, 167, 4000, 0, 0, '1316100030'),
(474, 167, 1000, 0, 0, '1316100030'),
(475, 167, 1400, 0, 0, '1316100030'),
(476, 167, 5000, 0, 0, '1316100030'),
(477, 168, 1400, 2000, 0, '1316100030'),
(478, 168, 1000, 0, 2000, '1316100030'),
(503, 176, 3000, 0, 10000000, '5113100043'),
(504, 176, 1000, 10000000, 0, '5113100043'),
(505, 179, 1400, 240000, 0, '5113100043'),
(506, 179, 1000, 0, 240000, '5113100043'),
(507, 180, 4000, 0, 1600, '1316100030'),
(508, 180, 1000, 1600, 0, '1316100030'),
(509, 180, 1400, 0, 1000, '1316100030'),
(510, 180, 5000, 1000, 0, '1316100030'),
(511, 181, 1400, 1000, 0, '1316100030'),
(512, 181, 1000, 0, 1000, '1316100030'),
(513, 182, 2100, 0, 1000000, '1316100030'),
(514, 182, 1000, 1000000, 0, '1316100030'),
(515, 183, 4000, 0, 15000, '1316100030'),
(516, 183, 1000, 15000, 0, '1316100030'),
(517, 183, 1400, 0, 1500, '1316100030'),
(518, 183, 5000, 1500, 0, '1316100030');

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `id_rec` varchar(100) DEFAULT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `hargaSatuan` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `id_produksi` varchar(100) DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`id_rec`, `id_item`, `jumlah`, `hargaSatuan`, `status`, `id_produksi`, `id_pemilik`) VALUES
('REC58bd061ca7f57', 'Barang02', 10, 19500, 0, NULL, '5114100014'),
('REC58bd06be53b0e', 'Barang01', 100, 480, 0, NULL, '5114100181'),
('REC58bd06e055b03', 'Barang03', 9, 26000, 0, NULL, '5114100181'),
('REC58bd07120f0cc', 'Barang02', 100, 18000, 0, NULL, '5114100181'),
('REC58bd074f79a44', 'Barang01', 20, 480, 0, NULL, '5114100181'),
('REC58bd074f79a44', 'Barang03', 2, 26000, 0, NULL, '5114100181'),
('REC58bd0800c32ca', 'Barang01', 2, 500, -20, NULL, '5114100118'),
('REC58bd0821a5dde', 'Barang03', 3000, 26000, 0, NULL, '5114100100'),
('REC58bd08238954a', 'Barang03', 25, 31000, 0, NULL, '5114100012'),
('REC58bd08376ecdc', 'Barang03', 1650, 27000, 0, NULL, '511400704'),
('REC58bd084b8a5fe', 'Barang02', 50, 19500, 0, NULL, '5114100014'),
('REC58bd085426ed7', 'Barang03', 1500, 26000, 0, NULL, '5114100064'),
('REC58bd088e91231', 'Barang03', 500, 26000, 0, NULL, '5114100073'),
('REC58bd08a657851', 'Barang03', 60, 27000, 0, NULL, '5114100151'),
('REC58bd0949cf9e8', 'Barang03', 2500, 26000, -2500, NULL, '5114100050'),
('REC58bd0989b1ac2', 'Barang02', 140, 18000, 0, NULL, '5114100001'),
('REC58bd093224c06', 'Barang02', 10000, 22000, 0, NULL, '5114100164'),
('REC58bd0acf37423', 'Barang03', 3000, 26000, -1000, NULL, '5114100151'),
('REC58bd0c8e3140d', 'Barang02', 165, 18000, 0, NULL, '5114100017'),
('REC58bd0cb63d3f5', 'Barang03', 115, 26000, 0, NULL, '5114100118'),
('REC58bd0d56887b9', 'Barang01', 234, 500, 0, NULL, '5114100181'),
('REC58bd0ddc48289', 'Barang03', 400, 26000, 0, NULL, '5114100151'),
('REC58bd0e4ac0970', 'Barang02', 150, 18000, 0, NULL, '5114100012'),
('REC58bd0e4ac0970', 'Barang03', 100, 43000, 0, NULL, '5114100012'),
('REC58bd2f98337e5', 'Barang01', 2, 500, 0, NULL, '1316100030'),
('REC58bd59a3b9c2a', 'Barang01', 500, 480, 0, NULL, '5113100043'),
('REC58bd606bd28de', 'Barang01', 2, 500, 0, NULL, '1316100030');

-- --------------------------------------------------------

--
-- Table structure for table `institusi`
--

CREATE TABLE `institusi` (
  `id_institusi` varchar(100) NOT NULL,
  `nama_institusi` varchar(300) NOT NULL,
  `alamat_institusi` varchar(500) NOT NULL,
  `telephone_institusi` varchar(30) NOT NULL,
  `email` varchar(200) NOT NULL,
  `tgl_registrasi` date NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institusi`
--

INSERT INTO `institusi` (`id_institusi`, `nama_institusi`, `alamat_institusi`, `telephone_institusi`, `email`, `tgl_registrasi`, `id_pemilik`) VALUES
('1', 'General', '', '', '', '0000-00-00', NULL),
('3573653633563738', 'CV.Karya Abadi', 'Jl.Karang Sari km.20 no.2', '03026283637', 'abadi@gmail.com', '2016-08-25', NULL),
('793520104102562', 'SMA N 1 Yogyakarta', 'jl.Karang wungu km.6', '0203635262', 'sman@gmail.com', '2016-08-01', NULL),
('793520104102580', 'Intel', 'Jl.Bonang Km 13 no.4', '030647462', 'info@itk.ac.id', '2016-08-02', NULL),
('7935201041030465', 'PT. Mekar Abadi', 'Jl.Tentara Pelajar Km 20 no.5', '(020)03038483848', 'cs@mekarabadi.co.id', '2016-08-06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_master`
--

CREATE TABLE `item_master` (
  `id_item` varchar(100) NOT NULL,
  `nama_item` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `tipe` int(11) NOT NULL DEFAULT '3',
  `satuan` int(11) NOT NULL,
  `link_photo` varchar(200) DEFAULT NULL,
  `id_petugas` varchar(100) DEFAULT NULL,
  `item_harga` int(50) DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_master`
--

INSERT INTO `item_master` (`id_item`, `nama_item`, `deskripsi`, `tipe`, `satuan`, `link_photo`, `id_petugas`, `item_harga`, `id_pemilik`) VALUES
('Barang01', 'Air Mineral gelas', 'Air Mineral Gelas', 3, 1, '', NULL, 500, '2017'),
('Barang02', 'Air Mineral kardus kecil', 'Air Mineral Karus Kecil', 3, 2, '', NULL, 500, '2017'),
('Barang03', 'Air Mineral kardus besar', 'Air Mineral Kardus Besar', 3, 3, '', NULL, 500, '2017');

-- --------------------------------------------------------

--
-- Table structure for table `modal`
--

CREATE TABLE `modal` (
  `id_modal` int(11) NOT NULL,
  `jenis_modal` int(11) NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `id_pemilik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modal`
--

INSERT INTO `modal` (`id_modal`, `jenis_modal`, `jumlah`, `status`, `id_pemilik`) VALUES
(1, 2, 4000000000, 0, '5114100026'),
(2, 1, 1000000000, 0, '5114100026'),
(3, 2, 3500000000, 0, '5114100100'),
(4, 1, 1500000000, 0, '5114100100'),
(5, 1, 1500000000, 0, '5114100070'),
(6, 2, 4500000000, 0, '5114100070'),
(7, 2, 200000000, 0, '5114100175'),
(8, 1, 50000000, 1, '5114100175'),
(9, 2, 800000000, 0, '5114100167'),
(10, 1, 200000000, 0, '5114100167'),
(11, 2, 1000000000, 0, '5114100181'),
(12, 2, 1700000000, 0, '5114100017'),
(13, 2, 800000000, 0, '5114100014'),
(14, 1, 2500000000, 0, '5114100017'),
(15, 1, 1200000000, 0, '5114100014'),
(16, 2, 700000000, 0, '5114100001'),
(17, 1, 1300000000, 0, '5114100001'),
(18, 2, 1000000000, 0, '5114100151'),
(19, 1, 1500000000, 1, '5114100151'),
(20, 2, 4000000000, 0, '5114100073'),
(21, 1, 10000000, 0, '5113100150'),
(22, 2, 1000000000, 0, '5114100065'),
(23, 1, 1000000000, 0, '5114100073'),
(24, 1, 500000000, 0, '5114100065'),
(25, 2, 2000000000, 0, '5114100050'),
(26, 1, 1000000000, 0, '5114100050'),
(27, 0, 0, 1, '5114100012'),
(28, 1, 1500000000, 1, '5114100012'),
(29, 2, 3500000000, 0, '5114100012'),
(30, 0, 0, 1, '5114100012'),
(31, 1, 15000000, 0, '5114100118'),
(32, 2, 10000000, 0, '5114100118'),
(33, 1, 200000000, 1, '511400704'),
(34, 2, 500000000, 0, '511400704'),
(35, 2, 2500000000, 0, '5114100064'),
(36, 1, 1500000000, 0, '5114100064'),
(37, 1, 50000000, 0, '5114100164'),
(38, 0, 0, 1, '5114100180'),
(39, 2, 200000000, 0, '5114100164'),
(40, 2, 250000000, 0, '5114100180'),
(41, 1, 100000000, 0, '5114100180'),
(42, 1, 5000000, 0, '5114100151'),
(43, 2, 1000000, 0, '1316100030'),
(45, 1, 2000000, 0, '1316100030'),
(46, 2, 10000000, 0, '5113100043'),
(47, 1, 1000000, 0, '1316100030');

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan_barang`
--

CREATE TABLE `penerimaan_barang` (
  `id_rec` varchar(100) NOT NULL,
  `id_po` varchar(100) DEFAULT NULL,
  `id_produksi` varchar(100) DEFAULT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `tanggal_receive` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `id_suplier` int(11) NOT NULL,
  `totalHarga` bigint(20) NOT NULL,
  `kurir` varchar(100) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penerimaan_barang`
--

INSERT INTO `penerimaan_barang` (`id_rec`, `id_po`, `id_produksi`, `id_petugas`, `tanggal_receive`, `status`, `id_suplier`, `totalHarga`, `kurir`, `id_pemilik`) VALUES
('REC58bd038c4c55c', 'PO58bd03436a597', NULL, '5114100070', '2017-03-06', 3, 5, 4730400000, 'Prasetyo', '5114100070'),
('REC58bd042b8ae81', 'PO58bd0392af760', NULL, '5114100026', '2017-03-05', 3, 1, 65000000, 'Self', '5114100026'),
('REC58bd0601e7b5a', 'PO58bd05c43165e', NULL, '5114100001', '2017-03-06', 3, 4, 9000000, 'GoBox', '5114100001'),
('REC58bd06032d650', 'PO58bd05ebb41c3', NULL, '5113100150', '2017-03-09', 3, 1, 650000, 'aaa', '5113100150'),
('REC58bd061ca7f57', 'PO58bd05e19b460', NULL, '5114100014', '2017-03-07', 3, 5, 1950000, 'Dio', '5114100014'),
('REC58bd0661b58b5', 'PO58bd062356133', NULL, '5114100175', '2017-03-07', 3, 3, 124000, 'Rifat', '5114100175'),
('REC58bd06be53b0e', 'PO58bd0617b57e5', NULL, '5114100181', '2017-03-05', 3, 1, 48000, 'jne', '5114100181'),
('REC58bd06cdd361f', 'PO58bd069f2ee1f', NULL, '5114100065', '2017-03-06', 3, 4, 18000000, 'Adi', '5114100065'),
('REC58bd06e055b03', 'PO58bd05a489836', NULL, '5114100181', '2017-03-05', 3, 1, 208000, 'wahana', '5114100181'),
('REC58bd07120f0cc', 'PO58bd0629db011', NULL, '5114100181', '2017-03-05', 3, 4, 1800000, 'wahana', '5114100181'),
('REC58bd074f79a44', 'PO58bd067c8e23b', NULL, '5114100181', '2017-03-05', 3, 2, 81600, 'jne', '5114100181'),
('REC58bd079760173', 'PO58bd06312e506', NULL, '5114100167', '2017-03-03', 3, 1, 780000, 'JNE', '5114100167'),
('REC58bd0800c32ca', 'PO58bd077f71028', NULL, '5114100118', '2017-03-06', 2, 2, 40000, 'jne', '5114100118'),
('REC58bd0821a5dde', 'PO58bd0586ac13d', NULL, '5114100100', '2017-03-06', 3, 1, 97500000, 'Go-Box', '5114100100'),
('REC58bd08238954a', 'PO58bd07c88b0d4', NULL, '5114100012', '2017-03-04', 3, 3, 1550000, 'jne', '5114100012'),
('REC58bd08376ecdc', 'PO58bd07a5bb892', NULL, '511400704', '2017-03-07', 3, 5, 54000000, 'irawan', '511400704'),
('REC58bd084ab689a', 'PO58bd07ec3c864', NULL, '5114100017', '2017-03-05', 3, 1, 12740000, 'Pribadi', '5114100017'),
('REC58bd084b8a5fe', 'PO58bd081a241f6', NULL, '5114100014', '2017-03-08', 3, 5, 975000, 'Pras', '5114100014'),
('REC58bd085426ed7', 'PO58bd080ebd94b', NULL, '5114100064', '2017-03-06', 3, 1, 91000000, 'gojek', '5114100064'),
('REC58bd088e91231', 'PO58bd0811ba70a', NULL, '5114100073', '2017-03-03', 3, 1, 15600000, '25000', '5114100073'),
('REC58bd08a657851', 'PO58bd07df35c76', NULL, '5114100151', '2017-03-08', 3, 5, 5400000, 'agus wirawan', '5114100151'),
('REC58bd08da00839', 'PO58bd08bed232b', NULL, '5114100118', '2017-03-06', 3, 1, 260000, 'jne', '5114100118'),
('REC58bd08f5ca947', 'PO58bd08d74a109', NULL, '5114100175', '2017-03-08', 3, 4, 162000, 'Rifat', '5114100175'),
('REC58bd093224c06', 'PO58bd08992c3c9', NULL, '5114100164', '2017-06-07', 3, 1, 220000000, 'Ani', '5114100164'),
('REC58bd0949cf9e8', 'PO58bd0849a5565', NULL, '5114100050', '2017-03-08', 2, 1, 65000000, 'Reynaldo', '5114100050'),
('REC58bd0989b1ac2', 'PO58bd0964461bd', NULL, '5114100001', '2017-03-07', 3, 4, 9000000, 'GoBox', '5114100001'),
('REC58bd0a27563ab', 'PO58bd09ca5d312', NULL, '5114100012', '2017-03-05', 3, 4, 6000000, 'jne', '5114100012'),
('REC58bd0a339f6a8', 'PO58bd0a0da8e60', NULL, '5114100167', '2017-02-06', 1, 1, 910000, 'JNE', '5114100167'),
('REC58bd0a798c870', 'PO58bd0a0da8e60', NULL, '5114100167', '2017-02-05', 1, 1, 910000, 'JNE', '5114100167'),
('REC58bd0abf1489e', 'PO58bd0a0da8e60', NULL, '5114100167', '2017-03-01', 1, 1, 910000, 'JNE', '5114100167'),
('REC58bd0acf37423', 'PO58bd079ebf0c2', NULL, '5114100151', '2017-03-28', 2, 1, 78000000, 'wawan', '5114100151'),
('REC58bd0ae7a21c7', 'PO58bd0a0da8e60', NULL, '5114100167', '2017-03-15', 1, 1, 910000, 'JNE', '5114100167'),
('REC58bd0b1672678', 'PO58bd0a0da8e60', NULL, '5114100167', '2017-03-14', 1, 1, 780000, 'JNE', '5114100167'),
('REC58bd0b211411a', 'PO58bd0a968088f', NULL, '5114100065', '2017-03-07', 3, 1, 2400000, 'adi', '5114100065'),
('REC58bd0b2a78618', 'PO58bd0a0da8e60', NULL, '5114100167', '2017-03-15', 1, 1, 780000, 'JNE', '5114100167'),
('REC58bd0b666cdec', 'PO58bd0a0da8e60', NULL, '5114100167', '2017-03-14', 1, 1, 1040000, 'JNE', '5114100167'),
('REC58bd0b7ac0618', 'PO58bd0a0da8e60', NULL, '5114100167', '2017-03-07', 1, 1, 320000, 'JNE', '5114100167'),
('REC58bd0bb5db8b7', 'PO58bd0a0da8e60', NULL, '5114100167', '2017-03-14', 1, 1, 780000, 'JNE', '5114100167'),
('REC58bd0c2b6543f', 'PO58bd0bf64d4a7', NULL, '5114100012', '2017-03-04', 1, 4, 2700000, 'Jne', '5114100012'),
('REC58bd0c4c44159', 'PO58bd0bf64d4a7', NULL, '5114100012', '2017-03-04', 1, 4, 2700000, 'Jne', '5114100012'),
('REC58bd0c6a8d657', 'PO58bd0c2ceac1c', NULL, '5114100167', '2017-03-14', 3, 1, 780000, 'JNE', '5114100167'),
('REC58bd0c8e3140d', 'PO58bd0c5a2c980', NULL, '5114100017', '2017-03-07', 3, 4, 3600000, 'Pribadi', '5114100017'),
('REC58bd0cb63d3f5', 'PO58bd0c5f96cf5', NULL, '5114100118', '2017-03-06', 3, 1, 3900000, 'jne', '5114100118'),
('REC58bd0d56887b9', 'PO58bd0d268cb2a', NULL, '5114100181', '2017-03-05', 3, 2, 500000, 'TEZ', '5114100181'),
('REC58bd0ddc48289', 'PO58bd0db1dd80e', NULL, '5114100151', '2017-03-27', 3, 1, 7800000, 'yoyo', '5114100151'),
('REC58bd0e4ac0970', 'PO58bd0bf64d4a7', NULL, '5114100012', '2017-03-04', 3, 4, 7000000, 'Jne', '5114100012'),
('REC58bd0e767b50f', 'PO58bd0df6880c4', NULL, '5114100012', '2017-03-04', 3, 3, 300000, 'Jne', '5114100012'),
('REC58bd11d49d46f', 'PO58bd11a032419', NULL, '5114100012', '2017-03-05', 3, 4, 6000000, 'Jne', '5114100012'),
('REC58bd2ca4f2fc8', 'PO58bd2c6e6dc9c', NULL, '1316100030', '2017-03-09', 3, 1, 19200, 'asd', '1316100030'),
('REC58bd2f98337e5', 'PO58bd2f851ac8d', NULL, '1316100030', '2017-03-12', 3, 2, 2000, 'CEO', '1316100030'),
('REC58bd59a3b9c2a', 'PO58bd5984ba34d', NULL, '5113100043', '2017-03-07', 3, 1, 230400, 'JNE', '5113100043'),
('REC58bd606bd28de', 'PO58bd5e6fb06da', NULL, '1316100030', '2017-03-06', 3, 2, 1000, 'setan', '1316100030');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_barang`
--

CREATE TABLE `pengeluaran_barang` (
  `id_issue` varchar(100) NOT NULL,
  `id_so` varchar(100) DEFAULT NULL,
  `id_produksi` varchar(100) DEFAULT NULL,
  `id_produkService` varchar(100) DEFAULT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `total` bigint(20) NOT NULL,
  `kurir` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '0',
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_barang`
--

INSERT INTO `pengeluaran_barang` (`id_issue`, `id_so`, `id_produksi`, `id_produkService`, `id_petugas`, `id_customer`, `tanggal`, `total`, `kurir`, `status`, `id_pemilik`) VALUES
('ISSUE58bd04a622943', 'SO58bd03ed5141a', NULL, NULL, '5114100070', '2147483647', '2017-03-20', 5606400000, 'Prasetyo', 3, '5114100070'),
('ISSUE58bd0607a0882', 'SO58bd058a7ac01', NULL, NULL, '5114100026', '2147483647', '2017-03-06', 15000000, 'Self', 3, '5114100026'),
('ISSUE58bd061973ac5', 'SO58bd05c097c9a', NULL, NULL, '5114100026', '2147483647', '2017-03-06', 3000000, 'Self', 3, '5114100026'),
('ISSUE58bd062592d2d', 'SO58bd055dddf55', NULL, NULL, '5114100026', '2147483647', '2017-03-06', 1500000, 'Self', 3, '5114100026'),
('ISSUE58bd0630f343e', 'SO58bd06180cb2d', NULL, NULL, '5113100150', '3', '2017-03-24', 750000, 'aaa', 3, '5113100150'),
('ISSUE58bd06346a06f', 'SO58bd052be436e', NULL, NULL, '5114100026', '2147483647', '2017-03-06', 15000000, 'Self', 3, '5114100026'),
('ISSUE58bd069bdb154', 'SO58bd065fcefd4', NULL, NULL, '5114100014', '123456789', '2017-03-08', 1250000, 'Dito', 3, '5114100014'),
('ISSUE58bd06d77b135', 'SO58bd066f7348f', NULL, NULL, '5114100026', '2147483647', '2017-03-09', 7500000, 'Self', 3, '5114100026'),
('ISSUE58bd06e7e1e8a', 'SO58bd06b17dd9f', NULL, NULL, '5114100026', '2147483647', '2017-03-09', 3000000, 'Self', 3, '5114100026'),
('ISSUE58bd06f9ba744', 'SO58bd068e93a97', NULL, NULL, '5114100026', '2147483647', '2017-03-08', 3000000, 'Self', 3, '5114100026'),
('ISSUE58bd0707729f4', 'SO58bd06532504c', NULL, NULL, '5114100026', '2147483647', '2017-03-09', 7500000, 'Self', 3, '5114100026'),
('ISSUE58bd07188ea44', 'SO58bd06e907ccf', NULL, NULL, '5114100001', '2147483647', '2017-03-06', 0, 'Pribadi', 0, '5114100001'),
('ISSUE58bd07558d1c0', 'SO58bd0700102d1', NULL, NULL, '5114100065', '2147483647', '2017-03-06', 25000000, 'Evan', 3, '5114100065'),
('ISSUE58bd078fcc2bd', 'SO58bd07461046a', NULL, NULL, '5114100175', '2147483647', '2017-03-08', 232000, 'Rifat', 3, '5114100175'),
('ISSUE58bd0798294c6', 'SO58bd074b88253', NULL, NULL, '5114100026', '2147483647', '2017-03-11', 9000000, 'Self', 3, '5114100026'),
('ISSUE58bd079ab3c41', 'SO58bd06e907ccf', NULL, NULL, '5114100001', '2147483647', '2017-03-06', 2500000, 'Pribadi', 3, '5114100001'),
('ISSUE58bd07a506f4d', 'SO58bd07339368f', NULL, NULL, '5114100026', '2147483647', '2017-03-11', 1500000, 'Self', 3, '5114100026'),
('ISSUE58bd07ae77278', 'SO58bd076b5c1c8', NULL, NULL, '5114100026', '2147483647', '2017-03-11', 9000000, 'Self', 3, '5114100026'),
('ISSUE58bd081e181fb', 'SO58bd07e10c319', NULL, NULL, '5114100167', '2147483647', '2017-03-04', 640000, 'JNE', 3, '5114100167'),
('ISSUE58bd085591823', 'SO58bd082c3152e', NULL, NULL, '5114100001', '2147483647', '2017-03-06', 2250000, 'Pribadi', 3, '5114100001'),
('ISSUE58bd08962c863', 'SO58bd08745c465', NULL, NULL, '5114100001', '2147483647', '2017-03-06', 2125000, 'Pribadi', 3, '5114100001'),
('ISSUE58bd08e301b57', 'SO58bd08bebb08d', NULL, NULL, '5114100001', '2147483647', '2017-03-06', 2375000, 'Pribadi', 3, '5114100001'),
('ISSUE58bd08e4ea209', 'SO58bd08a9c745e', NULL, NULL, '5114100014', '2147483647', '2017-03-09', 375000, 'Pras', 0, '5114100014'),
('ISSUE58bd08e8c68d0', 'SO58bd089828481', NULL, NULL, '5114100017', '2147483647', '2017-03-07', 3200000, 'Pribadi', 3, '5114100017'),
('ISSUE58bd08f1cf0f5', 'SO58bd08a9c745e', NULL, NULL, '5114100014', '2147483647', '2017-03-09', 0, 'Pras', 0, '5114100014'),
('ISSUE58bd0928c77d7', 'SO58bd090336caf', NULL, NULL, '5114100001', '2147483647', '2017-03-06', 0, 'GoBox', 0, '5114100001'),
('ISSUE58bd094305947', 'SO58bd089e189aa', NULL, NULL, '5114100012', '2147483647', '2017-03-05', 1000000, 'jne', 3, '5114100012'),
('ISSUE58bd0945cd145', 'SO58bd090336caf', NULL, NULL, '5114100001', '2147483647', '2017-03-06', 2125000, 'GoBox', 3, '5114100001'),
('ISSUE58bd09480765a', 'SO58bd08ef206aa', NULL, NULL, '5114100064', '13567', '2017-03-07', 70000000, 'gojek', 3, '5114100064'),
('ISSUE58bd0963de78d', 'SO58bd093c49330', NULL, NULL, '5114100167', '2147483647', '2017-03-06', 256000, 'JNE', 0, '5114100167'),
('ISSUE58bd097ca105e', 'SO58bd094539acb', NULL, NULL, '5114100118', '2147483647', '2017-03-06', 10500, 'jne', 3, '5114100118'),
('ISSUE58bd098b9cd12', 'SO58bd083dbc056', NULL, NULL, '5114100181', '2147483647', '2017-03-06', 124000, 'wahana', 0, '5114100181'),
('ISSUE58bd09da71a9b', 'SO58bd093c49330', NULL, NULL, '5114100167', '2147483647', '2017-03-06', 320000, 'JNE', 0, '5114100167'),
('ISSUE58bd09f1b1fa2', 'SO58bd083dbc056', NULL, NULL, '5114100181', '2147483647', '2017-03-06', 124000, 'wahana', 0, '5114100181'),
('ISSUE58bd0a031ed1a', 'SO58bd09c0b61e5', NULL, NULL, '5114100175', '2147483647', '2017-03-23', 300000, 'Rifat', 0, '5114100175'),
('ISSUE58bd0a366c938', 'SO58bd091be3862', NULL, NULL, '511400704', '521000001', '2017-03-06', 6200000, 'irawan', 3, '511400704'),
('ISSUE58bd0a37e8241', 'SO58bd0a02b76a7', NULL, NULL, '5114100100', '30', '2017-03-06', 16500000, 'Go-Box', 3, '5114100100'),
('ISSUE58bd0a45e90f2', 'SO58bd0a1ebc2f5', NULL, NULL, '5114100001', '2147483647', '2017-03-07', 1875000, 'Pribadi', 0, '5114100001'),
('ISSUE58bd0a6b84932', 'SO58bd08f3d623b', NULL, NULL, '5114100181', '2147483647', '2017-03-01', 143000, 'jne', 0, '5114100181'),
('ISSUE58bd0a7377adc', 'SO58bd0a1ebc2f5', NULL, NULL, '5114100001', '2147483647', '2017-03-07', 1875000, 'Pribadi', 3, '5114100001'),
('ISSUE58bd0a8a0494b', 'SO58bd09c0b61e5', NULL, NULL, '5114100175', '2147483647', '2017-03-15', 330000, 'Rifat', 0, '5114100175'),
('ISSUE58bd0aa3ceb47', 'SO58bd09c0b61e5', NULL, NULL, '5114100175', '2147483647', '2017-03-15', 0, 'Rifat', 0, '5114100175'),
('ISSUE58bd0aa9af7a0', 'SO58bd08a9c745e', NULL, NULL, '5114100014', '2147483647', '2017-03-31', 500000, 'Pras', 3, '5114100014'),
('ISSUE58bd0aba2e5eb', 'SO58bd083dbc056', NULL, NULL, '5114100181', '2147483647', '2017-03-06', 125000, 'wahana', 0, '5114100181'),
('ISSUE58bd0ad358754', 'SO58bd0a84aa09b', NULL, NULL, '5114100012', '2147483647', '2017-03-05', 4214000, 'jne', 0, '5114100012'),
('ISSUE58bd0af231205', 'SO58bd0a84aa09b', NULL, NULL, '5114100012', '2147483647', '2017-03-05', 4214000, 'jne', 0, '5114100012'),
('ISSUE58bd0b009652b', 'SO58bd0ac2ceee5', NULL, NULL, '5114100017', '2147483647', '2017-03-09', 0, 'Pribadi', 0, '5114100017'),
('ISSUE58bd0b0c52041', 'SO58bd0ad69d5a4', NULL, NULL, '5114100100', '19', '2017-03-07', 8250000, 'Go-Box', 3, '5114100100'),
('ISSUE58bd0b11d9cce', 'SO58bd0a84aa09b', NULL, NULL, '5114100012', '2147483647', '2017-03-05', 4300000, 'jne', 0, '5114100012'),
('ISSUE58bd0b1eacc66', 'SO58bd0ab8311e6', NULL, NULL, '511400704', '2147483647', '2017-03-13', 4650000, 'iwan', 3, '511400704'),
('ISSUE58bd0b1f7deb7', 'SO58bd0ae10f328', NULL, NULL, '5114100001', '2147483647', '2017-03-07', 3750000, 'Jasa Toko', 3, '5114100001'),
('ISSUE58bd0b264c660', 'SO58bd09c0b61e5', NULL, NULL, '5114100175', '2147483647', '2017-03-23', 300000, 'Rifat', 0, '5114100175'),
('ISSUE58bd0b3be1915', 'SO58bd0b14d42e7', NULL, NULL, '5114100181', '2147483647', '2017-03-06', 35000, 'zahrah', 0, '5114100181'),
('ISSUE58bd0b4a064aa', 'SO58bd09c0b61e5', NULL, NULL, '5114100175', '2147483647', '2017-04-08', 330000, 'Rifat', 0, '5114100175'),
('ISSUE58bd0b5e6f616', 'SO58bd09c0b61e5', NULL, NULL, '5114100175', '2147483647', '2017-03-28', 300000, 'Rifat', 0, '5114100175'),
('ISSUE58bd0b5eced89', 'SO58bd0b0c6afc4', NULL, NULL, '5114100151', '0', '2017-03-14', 3000000, 'yoyo', 0, '5114100151'),
('ISSUE58bd0b7e83130', 'SO58bd0ac2ceee5', NULL, NULL, '5114100017', '2147483647', '2017-03-09', 1600000, 'Pribadi', 3, '5114100017'),
('ISSUE58bd0b861feab', 'SO58bd0933a4a63', NULL, NULL, '5114100151', '0', '2017-03-29', 300000, 'wawan', 3, '5114100151'),
('ISSUE58bd0ba04dfb8', 'SO58bd0a84aa09b', NULL, NULL, '5114100012', '2147483647', '2017-03-05', 4300000, 'jne', 0, '5114100012'),
('ISSUE58bd0ba6e3f13', 'SO58bd0b6c5be6d', NULL, NULL, '5114100065', '0', '2017-03-07', 4000000, 'evan', 3, '5114100065'),
('ISSUE58bd0bca6e9db', 'SO58bd0a84aa09b', NULL, NULL, '5114100012', '2147483647', '2017-03-05', 4300000, 'jne', 0, '5114100012'),
('ISSUE58bd0bcbe8aaa', 'SO58bd0b0c6afc4', NULL, NULL, '5114100151', '0', '2017-03-05', 3000000, 'yoyo', 0, '5114100151'),
('ISSUE58bd0bdc373da', 'SO58bd0b0c6afc4', NULL, NULL, '5114100151', '0', '2017-03-13', 3000000, 'yoyo', 0, '5114100151'),
('ISSUE58bd0bf981b62', 'SO58bd0bd3f2693', NULL, NULL, '5114100181', '2147483647', '2017-03-07', 120000, 'ZAHRAH', 0, '5114100181'),
('ISSUE58bd0bf98afaf', 'SO58bd0bb194c0d', NULL, NULL, '5114100118', '2147483647', '2017-03-06', -1050, 'jne', 0, '5114100118'),
('ISSUE58bd0c130ad0f', 'SO58bd0bdeb69bb', NULL, NULL, '5114100001', '2147483647', '2017-03-07', 1875000, 'Jasa Toko', 3, '5114100001'),
('ISSUE58bd0c13b6455', 'SO58bd0bb194c0d', NULL, NULL, '5114100118', '2147483647', '2017-03-06', 73500, 'jne', 3, '5114100118'),
('ISSUE58bd0c35e1773', 'SO58bd09c0b61e5', NULL, NULL, '5114100175', '2147483647', '2017-03-22', 300000, 'Rifat', 0, '5114100175'),
('ISSUE58bd0c4f61a45', 'SO58bd0b0c6afc4', NULL, NULL, '5114100151', '0', '2017-03-08', 6000000, 'yoyo', 0, '5114100151'),
('ISSUE58bd0ca606ec4', 'SO58bd0c6dd118b', NULL, NULL, '5114100181', '2147483647', '2017-03-07', 330000, 'TEST COM', 0, '5114100181'),
('ISSUE58bd0cb464cb7', 'SO58bd09195649d', NULL, NULL, '5114100073', '2147483647', '2017-03-05', 3000000, 'Pribadi', 3, '5114100073'),
('ISSUE58bd0ce44d329', 'SO58bd0c9314130', NULL, NULL, '5114100012', '2147483647', '2017-03-05', 3750000, 'Jne', 3, '5114100012'),
('ISSUE58bd0d40b715a', 'SO58bd0d1564af8', NULL, NULL, '5114100017', '2147483647', '2017-03-09', 345000, 'Pribadi', 3, '5114100017'),
('ISSUE58bd0d5a84037', 'SO58bd09c0b61e5', NULL, NULL, '5114100175', '2147483647', '2017-03-06', 300000, 'Rifat', 0, '5114100175'),
('ISSUE58bd0dab32362', 'SO58bd0d84d984c', NULL, NULL, '5114100167', '2147483647', '2017-03-15', 0, 'JNE', 0, '5114100167'),
('ISSUE58bd0e1d1344e', 'SO58bd0d8804648', NULL, NULL, '5114100118', '2147483647', '2017-03-06', 1530000, 'jne', 3, '5114100118'),
('ISSUE58bd0e344c4b6', 'SO58bd0dfcdc32d', NULL, NULL, '5114100181', '2147483647', '2017-03-06', 999000, 'jne', 3, '5114100181'),
('ISSUE58bd0e50bb6a6', 'SO58bd0d84d984c', NULL, NULL, '5114100167', '2147483647', '2017-03-15', 960000, 'JNE', 3, '5114100167'),
('ISSUE58bd0ee8bc99b', 'SO58bd0e353a6c5', NULL, NULL, '5114100017', '2147483647', '2017-03-10', 3520000, 'Pribadi', 3, '5114100017'),
('ISSUE58bd0fa948629', 'SO58bd0eee16ce8', NULL, NULL, '5114100012', '2147483647', '2017-03-04', 9300000, 'Jne', 0, '5114100012'),
('ISSUE58bd0fdf85e18', 'SO58bd0f28c94d9', NULL, NULL, '5114100012', '2147483647', '2017-03-04', 300000, 'Jne', 0, '5114100012'),
('ISSUE58bd0ffb770a2', 'SO58bd0a84aa09b', NULL, NULL, '5114100012', '2147483647', '2017-03-03', 4300000, 'jne', 0, '5114100012'),
('ISSUE58bd102e2407a', 'SO58bd0ff1c2c2e', NULL, NULL, '5114100001', '2147483647', '2017-03-07', 1500000, 'GoBox', 3, '5114100001'),
('ISSUE58bd103e7e4bd', 'SO58bd0f28c94d9', NULL, NULL, '5114100012', '2147483647', '2017-03-04', 300000, 'Jne', 0, '5114100012'),
('ISSUE58bd10a6035c8', 'SO58bd0a84aa09b', NULL, NULL, '5114100012', '2147483647', '2017-03-06', 4300000, 'jne', 0, '5114100012'),
('ISSUE58bd110e9b0f9', 'SO58bd10d2496ae', NULL, NULL, '5114100017', '2147483647', '2017-03-06', 4736000, 'Pribadi', 0, '5114100017'),
('ISSUE58bd112333385', 'SO58bd10d2496ae', NULL, NULL, '5114100017', '2147483647', '2017-03-06', 64000, 'Pribadi', 0, '5114100017'),
('ISSUE58bd11349f553', 'SO58bd10d2496ae', NULL, NULL, '5114100017', '2147483647', '2017-03-06', 4800000, 'Pribadi', 0, '5114100017'),
('ISSUE58bd1140cdd67', 'SO58bd10fcc6c33', NULL, NULL, '5114100012', '2147483647', '2017-03-06', 5000000, 'Jne', 0, '5114100012'),
('ISSUE58bd117d69da6', 'SO58bd10d2496ae', NULL, NULL, '5114100017', '2147483647', '2017-03-06', 4800000, 'Pribadi', 0, '5114100017'),
('ISSUE58bd11af1d81d', 'SO58bd10d2496ae', NULL, NULL, '5114100017', '2147483647', '2017-03-06', 4800000, 'Pribadi', 0, '5114100017'),
('ISSUE58bd11de87f12', 'SO58bd0aee3f6a8', NULL, NULL, '5114100181', '2147483647', '2017-03-06', 99000, 'zahrah', 0, '5114100181'),
('ISSUE58bd123d0f2e2', 'SO58bd12074e347', NULL, NULL, '5114100012', '2147483647', '2017-03-05', 7000000, 'Jne', 3, '5114100012'),
('ISSUE58bd13855d8d6', 'SO58bd12d7b0cb6', NULL, NULL, '5114100017', '2147483647', '2017-03-06', 460000, 'Pribadi', 3, '5114100017'),
('ISSUE58bd2ce89b1ee', 'SO58bd2cc30c09a', NULL, NULL, '1316100030', '2147483647', '2017-03-16', 54000, 'CEO', 0, '1316100030'),
('ISSUE58bd2f2ca0b2f', 'SO58bd2cc30c09a', NULL, NULL, '1316100030', '2147483647', '2017-03-15', 18000, 'CEO', 0, '1316100030'),
('ISSUE58bd5ab167dd7', 'SO58bd5a81c0fb3', NULL, NULL, '1316100030', '2147483647', '2017-03-06', 1600, 'setan', 3, '1316100030');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id_so` varchar(100) NOT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `total` bigint(20) NOT NULL,
  `kurir` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '0',
  `tanggal_keluar` date DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL,
  `ongkir` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id_so`, `id_petugas`, `id_customer`, `tanggal`, `total`, `kurir`, `status`, `tanggal_keluar`, `id_pemilik`, `ongkir`) VALUES
('SO58bd03ed5141a', '5114100070', '5114100070', '2017-03-06', 5606400000, 'Prasetyo', 3, NULL, '5114100070', 0),
('SO58bd045f15c0b', '5114100026', '5171011003960014', '2017-03-06', 0, 'Self', 4, NULL, '5114100026', 0),
('SO58bd052be436e', '5114100026', '5171011003960010', '2017-03-06', 15000000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd055dddf55', '5114100026', '5171011003960006', '2017-03-07', 1500000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd058a7ac01', '5114100026', '5171011003960014', '2017-03-08', 15000000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd05c097c9a', '5114100026', '5171011003960011', '2017-03-08', 3000000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd06180cb2d', '5113100150', '000003', '2017-03-23', 750000, 'aaa', 3, NULL, '5113100150', 8000),
('SO58bd06532504c', '5114100026', '5171011003960012', '2017-03-09', 7500000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd065fcefd4', '5114100014', '123456789', '2017-03-08', 1250000, 'Dito', 3, NULL, '5114100014', 50000),
('SO58bd066f7348f', '5114100026', '5171011003960013', '2017-03-16', 7500000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd068e93a97', '5114100026', '5171011003960007', '2017-03-10', 3000000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd06b17dd9f', '5114100026', '5171011003960008', '2017-03-10', 3000000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd06e907ccf', '5114100001', '5114100097', '2017-03-06', 2500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58bd0700102d1', '5114100065', '5114100180', '2017-03-06', 25000000, 'Evan', 3, NULL, '5114100065', 100000),
('SO58bd070b86fae', '5114100167', '5145784564843415', '2017-03-03', 0, 'JNE', 0, NULL, '5114100167', 50000),
('SO58bd070ba3191', '5114100073', '5114100704', '2017-03-02', 0, 'Pribadi', 0, NULL, '5114100073', 25000),
('SO58bd07339368f', '5114100026', '5171011003960006', '2017-03-11', 1500000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd07461046a', '5114100175', '5114100175', '2017-03-07', 232000, 'Rifat', 3, NULL, '5114100175', 10000),
('SO58bd074b88253', '5114100026', '5171011003960012', '2017-03-11', 9000000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd076b5c1c8', '5114100026', '5171011003960013', '2017-03-11', 9000000, 'Self', 3, NULL, '5114100026', 0),
('SO58bd07e10c319', '5114100167', '5145784564843415', '2017-03-04', 640000, 'JNE', 3, NULL, '5114100167', 59981),
('SO58bd082c3152e', '5114100001', '5114100035', '2017-03-06', 2250000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58bd083dbc056', '5114100181', '5114100172', '2017-03-06', 125000, 'wahana', 2, NULL, '5114100181', 10000),
('SO58bd08745c465', '5114100001', '5114100177', '2017-03-06', 2125000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58bd089828481', '5114100017', '15114100012', '2017-03-07', 3200000, 'Pribadi', 3, NULL, '5114100017', 100000),
('SO58bd089e189aa', '5114100012', '5114100181', '2017-03-05', 1000000, 'jne', 3, NULL, '5114100012', 25000),
('SO58bd08a308ff6', '5114100181', '5114100172', '2017-03-05', 45000, 'jne', 2, NULL, '5114100181', 5000),
('SO58bd08a9c745e', '5114100014', '7655014565438895', '2017-03-09', 500000, 'Pras', 3, NULL, '5114100014', 20000),
('SO58bd08bebb08d', '5114100001', '5114100171', '2017-03-06', 2375000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58bd08ef206aa', '5114100064', '13567', '2017-03-07', 70000000, 'gojek', 3, NULL, '5114100064', 10000),
('SO58bd08f3d623b', '5114100181', '5114100172', '2017-03-05', 20000, 'jne', 2, NULL, '5114100181', 2000),
('SO58bd090336caf', '5114100001', '5114100173', '2017-03-06', 2125000, 'GoBox', 3, NULL, '5114100001', 100000),
('SO58bd09195649d', '5114100073', '5114100704', '2017-03-04', 3000000, 'Pribadi', 3, NULL, '5114100073', 25000),
('SO58bd091be3862', '511400704', '521000001', '2017-03-06', 6200000, 'irawan', 3, NULL, '511400704', 30000),
('SO58bd0933a4a63', '5114100151', 'co001', '2017-03-13', 300000, 'wawan', 3, NULL, '5114100151', 10000),
('SO58bd093c49330', '5114100167', '3467823648', '2017-03-05', 320000, 'JNE', 2, NULL, '5114100167', 50000),
('SO58bd094539acb', '5114100118', '5114100114', '2017-03-06', 10500, 'jne', 3, NULL, '5114100118', 10000),
('SO58bd09931e176', '5114100100', '30', '2017-03-06', 0, 'Go-Box', 4, NULL, '5114100100', 0),
('SO58bd09c0b61e5', '5114100175', '5114100175', '2017-03-09', 300000, 'Rifat', 2, NULL, '5114100175', 10000),
('SO58bd09d2919b9', '5114100164', '3174020305960003', '2017-06-12', 0, 'Janet', 4, NULL, '5114100164', 50000),
('SO58bd0a02b76a7', '5114100100', '30', '2017-03-06', 16500000, 'Go-Box', 3, NULL, '5114100100', 0),
('SO58bd0a1ebc2f5', '5114100001', '5114100035', '2017-03-07', 1875000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58bd0a84aa09b', '5114100012', '5114100001', '2017-03-06', 4300000, 'jne', 2, NULL, '5114100012', 40000),
('SO58bd0ab8311e6', '511400704', '5114100076', '2017-03-13', 4650000, 'iwan', 3, NULL, '511400704', 0),
('SO58bd0ac2ceee5', '5114100017', '15114100012', '2017-03-09', 1600000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58bd0ad69d5a4', '5114100100', '19', '2017-03-07', 8250000, 'Go-Box', 3, NULL, '5114100100', 0),
('SO58bd0ae10f328', '5114100001', '51141000121', '2017-03-07', 3750000, 'Jasa Toko', 3, NULL, '5114100001', 100000),
('SO58bd0aee3f6a8', '5114100181', '5114100012', '2017-03-06', 125000, 'zahrah', 2, NULL, '5114100181', 5000),
('SO58bd0b0c6afc4', '5114100151', 'co001', '2017-03-13', 6000000, 'yoyo', 2, NULL, '5114100151', 50000),
('SO58bd0b14d42e7', '5114100181', '5114100012', '2017-03-06', 35000, 'zahrah', 2, NULL, '5114100181', 5000),
('SO58bd0b1d5e3eb', '5114100100', '10', '2017-03-08', 0, 'Go-Box', 2, NULL, '5114100100', 0),
('SO58bd0b6c5be6d', '5114100065', 'adi', '2017-03-07', 4000000, 'evan', 3, NULL, '5114100065', 50000),
('SO58bd0b936b9e2', '5114100164', '3174020305960003', '2017-06-12', 100000000, 'Paul', 1, NULL, '5114100164', 20000),
('SO58bd0bb194c0d', '5114100118', '5114100114', '2017-03-06', 73500, 'jne', 3, NULL, '5114100118', 10000),
('SO58bd0bd3f2693', '5114100181', '5114100012', '2017-03-06', 120000, 'ZAHRAH', 2, NULL, '5114100181', 1212),
('SO58bd0bdeb69bb', '5114100001', '51141000170', '2017-03-07', 1875000, 'Jasa Toko', 3, NULL, '5114100001', 100000),
('SO58bd0c6dd118b', '5114100181', '5114100172', '2017-03-07', 330000, 'TEST COM', 2, NULL, '5114100181', 7000),
('SO58bd0c9314130', '5114100012', '5114100181', '2017-03-05', 3750000, 'Jne', 3, NULL, '5114100012', 40000),
('SO58bd0d1564af8', '5114100017', '15114100172', '2017-03-09', 345000, 'Pribadi', 3, NULL, '5114100017', 15000),
('SO58bd0d84d984c', '5114100167', '3467823648', '2017-03-15', 960000, 'JNE', 3, NULL, '5114100167', 200000),
('SO58bd0d8804648', '5114100118', '5114100114', '2017-03-06', 600000, 'jne', 3, NULL, '5114100118', 10000),
('SO58bd0dfcdc32d', '5114100181', '5113100002', '2017-03-06', 999000, 'jne', 3, NULL, '5114100181', 999),
('SO58bd0e353a6c5', '5114100017', '15114100172', '2017-03-11', 3520000, 'Pribadi', 3, NULL, '5114100017', 100000),
('SO58bd0e6b79841', '5114100151', 'co001', '2017-03-28', 90000, 'yoyo', 1, NULL, '5114100151', 50000),
('SO58bd0eee16ce8', '5114100012', '5114100017', '2017-03-04', 9300000, 'Jne', 2, NULL, '5114100012', 100000),
('SO58bd0f28c94d9', '5114100012', '5114100017', '2017-03-04', 300000, 'Jne', 2, NULL, '5114100012', 25000),
('SO58bd0ff1c2c2e', '5114100001', '5114100800', '2017-03-07', 1500000, 'GoBox', 3, NULL, '5114100001', 100000),
('SO58bd10d2496ae', '5114100017', '15114100130', '2017-03-06', 4800000, 'Pribadi', 2, NULL, '5114100017', 100000),
('SO58bd10fcc6c33', '5114100012', '5114100017', '2017-03-06', 5000000, 'Jne', 2, NULL, '5114100012', 20000),
('SO58bd12074e347', '5114100012', '5114100181', '2017-03-05', 7000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58bd12d7b0cb6', '5114100017', '15114100181', '2017-03-06', 460000, 'Pribadi', 3, NULL, '5114100017', 15000),
('SO58bd29d5c2ccf', '1316100030', '5113100164', '2017-03-14', 0, 'asd', 2, NULL, '1316100030', 0),
('SO58bd2cc30c09a', '1316100030', '5113100164', '2017-03-09', 18000, 'CEO', 2, NULL, '1316100030', 1000),
('SO58bd2fb760921', '1316100030', '5113100164', '2017-03-16', 600, 'ra sah', 0, NULL, '1316100030', 0),
('SO58bd2ff7834c7', '1316100030', '5113100164', '2017-03-13', 600, 'Nanda', 0, NULL, '1316100030', 0),
('SO58bd307f7425c', '1316100030', '5113100164', '2017-03-06', 100, 'Nanda', 0, NULL, '1316100030', 0),
('SO58bd30a37505b', '1316100030', '5113100164', '2017-03-06', 10, 'Nanda', 0, NULL, '1316100030', 0),
('SO58bd30d06b880', '1316100030', '5113100164', '2017-03-13', 10, 'Nanda', 0, NULL, '1316100030', 0),
('SO58bd360c205b7', '1316100030', '5113100164', '2017-03-06', 600, 'Umeh', 0, NULL, '1316100030', 0),
('SO58bd526aa61b2', '1316100030', '5113100164', '2017-03-06', 600, 'setan', 0, NULL, '1316100030', 3),
('SO58bd52b73cd60', '1316100030', '5113100164', '2017-03-06', 1000, 'setan', 0, NULL, '1316100030', 3),
('SO58bd52ec5c82b', '1316100030', '5113100164', '2017-03-16', 1000, 'setan', 0, NULL, '1316100030', 4),
('SO58bd58067cff7', '1316100030', '5113100164', '2017-03-06', 2000, 'setan', 0, NULL, '1316100030', 3),
('SO58bd59239f733', '1316100030', '5113100164', '2017-03-06', 1200, 'setan', 0, NULL, '1316100030', 100),
('SO58bd5976cce7e', '1316100030', '5113100164', '2017-03-06', 1200, 'setan', 0, NULL, '1316100030', 100),
('SO58bd59802838f', '1316100030', '5113100164', '2017-03-07', 1200, 'setan', 0, NULL, '1316100030', 200),
('SO58bd59f0ab75e', '5113100043', '12345678232', '2017-03-09', 7000, 'JNE', 0, NULL, '5113100043', 0),
('SO58bd5a06047ee', '1316100030', '5113100164', '2017-03-07', 1200, 'setan', 0, NULL, '1316100030', 200),
('SO58bd5a312a397', '1316100030', '5113100164', '2017-03-07', 1200, 'setan', 0, NULL, '1316100030', 200),
('SO58bd5a81c0fb3', '1316100030', '5113100164', '2017-03-06', 1600, 'setan', 3, NULL, '1316100030', 100),
('SO58bd60b199d36', '1316100030', '5113100164', '2017-03-06', 15000, 'setan', 2, NULL, '1316100030', 190);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` varchar(100) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `jenkel` varchar(3) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `jabatan` varchar(200) NOT NULL,
  `tgl` date NOT NULL,
  `photo_link` varchar(500) NOT NULL,
  `password` varchar(200) NOT NULL,
  `privilege` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama`, `jenkel`, `alamat`, `hp`, `email`, `jabatan`, `tgl`, `photo_link`, `password`, `privilege`) VALUES
('', '', '', '', '', '', 'user', '2017-02-27', '', 'd41d8cd98f00b204e9800998ecf8427e', 1),
('1', '1', '', '', '', 'sample@email.tst', 'user', '2017-02-27', '', '32cc5886dc1fa8c106a02056292c4654', 1),
('1316100030', 'Andi Putra', '', '', '', 'andiputra31@gmail.com', 'user', '2017-02-13', '', '5deb20a2cf8e246788f69beb9836f4f4', 1),
('330172906940003', 'Avatar Aang', 'L', 'Gg.kidul km 23 no 30', '081378376746', 'ang@miconos.co.id', 'Teknisi', '2016-07-01', 'file_1471317237.jpg', '827ccb0eea8a706c4c34a16891f84e7b', 3),
('330172906940004', 'Anjani Nugraha', 'P', 'Gg.kencana wungu', '08978376746', 'anjani@miconos.co.id', 'customer service', '2016-07-01', '', '827ccb0eea8a706c4c34a16891f84e7b', 0),
('330172906940005', 'Ahmad Zaenal Mustofa', 'L', 'Gg. Makam', '085664497937', 'mustofaahmad209@gmail.com', 'Teknis', '2016-07-20', '', '827ccb0eea8a706c4c34a16891f84e7b', 3),
('330172906940050', 'Burhanudin Rasyid', 'L', 'Jl.Bumi rejo', '0857363672527', 'hanumuslim@gmail.com', 'Korlap', '2016-08-16', '', '827ccb0eea8a706c4c34a16891f84e7b', 2),
('330172906940076', 'PT.MICONOS', 'L', 'jakal km.8 Blok A no 2', '0208363727', 'admin@miconos.co.id', 'Admin', '2016-08-19', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('330172906940090', 'Ahmad Hamdani R', 'L', 'Jl.Bumi rejo', '085664497937', 'hamdani@gmail.com', 'Marketing', '2016-08-25', '', '827ccb0eea8a706c4c34a16891f84e7b', 0),
('5113100028', 'Renanda Agustiantoro', '', '', '', 'rectarenanda@gmail.com', 'user', '2017-02-12', '', 'c0b7160f7d1e729044907cf93befa556', 1),
('5113100043', 'Ahmad Zaenal', 'L', 'Gg. Makam B No.5', '085664497937', 'mustofaahmad29@gmail.com', 'user', '2017-02-12', 'file_1486852246.jpg', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5113100084', 'Faizal Anugrah', '', '', '', 'faisalanugrah48@gmail.com', 'user', '2017-02-20', '', '735867f216f7e35c2adb91de82dbf3e9', 1),
('5113100139', 'Rigold Nainggolan', '', '', '', 'rigold95@gmail.com', 'user', '2017-02-13', '', '28cf59539e234128f244f0f0807786a4', 1),
('5113100150', 'Andi Putra Kusuma', 'L', 'lalalal', '13141515', 'andiputra3107@gmail.com', 'admin', '2017-02-06', '', '5deb20a2cf8e246788f69beb9836f4f4', 1),
('5113100160', 'Anggi', '', '', '', 'andi13@mhs.if.its.ac.id', '', '2017-02-06', '', '5deb20a2cf8e246788f69beb9836f4f4', 1),
('511400704', 'Elva Nur Maulidiah E', '', '', '', 'elva.shain@gmail.com', 'user', '2017-02-13', '', '17dc82d5a5bab463abb06df95903e46c', 1),
('5114100001', 'Muhammad Nezar Mahardika', 'L', 'Jl. Danau Semayang VIII c2c8', '08990352687', 'nezarmahardika1@gmail.com', 'perantara', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100008', 'Kadek Winda Dwiastini', '', '', '', 'winda1996@hotmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100012', 'Zahrah Citra Hafizha', '', '', '', 'rara.zch@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100014', 'Muhammad Hanif', 'L', 'Surabaya', '123', 'muhammadhanif.if@gmail.com', 'user', '2017-02-13', '', 'eb84c59a3f1d34016aa6debec30d284e', 1),
('5114100017', 'Nafia Rizky Yogayana', 'P', 'Perumdos ITS, Blok J-45', '082216612220', 'nafia.ry@gmail.com', 'user', '2017-02-13', '', 'fda428482f8a4278a3207f0cd0044554', 1),
('5114100025', 'I Putu Eka Wira Mahardika', '', '', '', 'putudikka@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100026', 'I Made Fandy Aditya Wirana', 'L', 'Jalan Raya Mulyosari, No 52B', '081338734041', 'fandyadityaw@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100035', 'Pramudito Hapriarso', '', '', '', 'ditosmileup@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100050', 'Andy Yohanes Hadiwijaya', '', '', '', 'andygate777@gmail.com', 'user', '2017-02-13', '', '3fc0a7acf087f549ac2b266baf94b8b1', 1),
('5114100051', 'Haidar Arya Prasetya', '', '', '', 'haidararya21@gmail.com', 'user', '2017-02-13', '', 'a8f5f167f44f4964e6c998dee827110c', 1),
('5114100064', 'Reynaldo Johanes', '', '', '', 'reynaldo.johanes13@gmail.com', 'user', '2017-02-13', '', 'a991cccf08aa2fe9a80586bb68cefa30', 1),
('5114100065', 'Anggit Yudhistira', 'L', 'Keputih gang 3 no 56A', '+6285640303100', 'anggityudhistira@gmail.com', 'user', '2017-02-13', '', 'aab02b8623a7a51b9cdca1a31ed30b59', 1),
('5114100070', 'Prasetyo Nugrohadi', 'L', 'Wisma Permai XI/3, Mulyorejo', '087854444653', 'prstyngrhd@gmail.com', 'user', '2017-02-13', '', '200820e3227815ed1756a6b531e7e0d2', 1),
('5114100071', 'Muhsin Bayu A.F', '', '', '', 'muhsin21baf@gmail.com', 'user', '2017-02-13', '', '41ed8ede4c41e52cf057edab8719ecda', 1),
('5114100072', 'Ahmad Ismail Harry Wicaksono', '', '', '', 'x5ahmadismail@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100073', 'Hendra Darmawan', 'L', 'Marina Emas Barat V/31 Surabaya', '081237275145', 'zavoxcool@gmail.com', 'user', '2017-02-13', '', 'e8d34f4832f3f432f095028b43708243', 1),
('5114100076', 'Muhammad Faishal Ilham', '', '', '', 'faishal15@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100078', 'Farhan ramadhana', '', '', '', 'farhanramadhana97@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100100', 'Steven Kurniawan', '', '', '', 'stevenkurkur@gmail.com', 'user', '2017-02-13', '', '1ab8a02a280ee45b93e915b66522f153', 1),
('5114100110', 'Rahmatin Nadia', 'P', 'keputih gg 2 no 19', '087878854753', 'nadiarahmatin@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100118', 'Muhammad Rifaullah', '', '', '', 'muhammad14@mhs.if.its.ac.id', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100122', 'Bayu Sektiaji', '', '', '', 'sektibayu@gmail.com', 'user', '2017-02-13', '', 'f9c15754846df9535f639c6891f8d4ab', 1),
('5114100124', 'Aufar Rizqi', '', '', '', 'aufarr@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100151', 'Riyadlatin Nufus', '', '', '', 'riyadlatin.nufus@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100164', 'Adiwinoto Saptorenggo', '', '', '', 'adiwinotosaptorenggo@gmail.com', 'user', '2017-02-13', '', '046c2ed1e56d192b5530f9c76e3e3d24', 1),
('5114100167', 'Aditya Gunawan', '', '', '', 'aditya.gunawan19th@gmail.com', 'user', '2017-02-13', '', '01cfcd4f6b8770febfb40cb906715822', 1),
('5114100172', 'Kania Amalia', 'P', 'Jl.Mulyosari Tengah VII no.46', '087881952727', 'kaniaamalia21@gmail.com', 'user', '2017-02-13', '', '2374a04ac1224c5ca8e439d61fc8e3b8', 1),
('5114100175', 'Riansya Pamusti', 'L', 'Keputih GG III No. 56a', '081220859750', 'riansyap@gmail.com', 'user', '2017-02-13', '', '61f098e9c7466c95129014e70f1315a3', 1),
('5114100180', 'Rizal Septiarakhman', '', '', '', 'rizalseptiarakhman@gmail.com', 'user', '2017-02-13', '', '218b09a6f2b5c1a47b718565adeae63a', 1),
('5114100181', 'Anne Annisa Aulia', 'P', 'Apartemen Educity Tower Harvard 0301', '082110227872', 'anneaaulia@gmail.com', 'user', '2017-02-13', 'file_1486965065.jpg', 'e10adc3949ba59abbe56e057f20f883e', 1),
('5114100702', 'Muhammad Ishar Kadir', '', '', '', 'ishar.muhammad7@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('5114100703', 'Ahmad Muzakka Ishaqi', '', '', '', 'jakopo130@gmail.com', 'user', '2017-02-13', '', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('513212442', 'andiputra', '', '', '', 'hahaha@hahaha.com', 'user', '2017-02-27', '', '5deb20a2cf8e246788f69beb9836f4f4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `produkservice`
--

CREATE TABLE `produkservice` (
  `id_produkService` varchar(100) NOT NULL,
  `id_service` int(11) NOT NULL,
  `total` bigint(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `teknisi` varchar(200) DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produksi`
--

CREATE TABLE `produksi` (
  `id_produksi` varchar(100) NOT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `id_produk` varchar(100) NOT NULL,
  `tanggal_po` date NOT NULL,
  `totalHarga` bigint(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `jumlah_item` int(11) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchasing`
--

CREATE TABLE `purchasing` (
  `id_po` varchar(100) NOT NULL,
  `id_suplier` int(11) NOT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `tanggal_po` date NOT NULL,
  `totalHarga` bigint(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `id_pemilik` varchar(200) DEFAULT NULL,
  `ongkir` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchasing`
--

INSERT INTO `purchasing` (`id_po`, `id_suplier`, `id_petugas`, `tanggal_po`, `totalHarga`, `status`, `id_pemilik`, `ongkir`) VALUES
('PO58bd03436a597', 5, '5114100070', '2017-03-06', 4730400000, 3, '5114100070', 0),
('PO58bd0392af760', 1, '5114100026', '2017-03-05', 65000000, 3, '5114100026', 0),
('PO58bd0586ac13d', 1, '5114100100', '2017-03-06', 97500000, 3, '5114100100', 0),
('PO58bd05a489836', 1, '5114100181', '2017-03-05', 260000, 3, '5114100181', 0),
('PO58bd05c43165e', 4, '5114100001', '2017-03-06', 9000000, 3, '5114100001', 100000),
('PO58bd05e19b460', 5, '5114100014', '2017-03-06', 1950000, 3, '5114100014', 0),
('PO58bd05ebb41c3', 1, '5113100150', '2017-03-08', 650000, 3, '5113100150', 0),
('PO58bd0617b57e5', 1, '5114100181', '2017-03-05', 48000, 3, '5114100181', 10000),
('PO58bd062356133', 3, '5114100175', '2017-03-06', 124000, 3, '5114100175', 10000),
('PO58bd0629db011', 4, '5114100181', '2017-03-05', 1800000, 3, '5114100181', 1000),
('PO58bd06312e506', 1, '5114100167', '2017-03-03', 780000, 3, '5114100167', 100000),
('PO58bd064b19a51', 1, '5114100181', '2017-03-05', 26000, 4, '5114100181', 4000),
('PO58bd067c8e23b', 2, '5114100181', '2017-03-03', 50800, 3, '5114100181', 9200),
('PO58bd069f2ee1f', 4, '5114100065', '2017-03-06', 18000000, 3, '5114100065', 100000),
('PO58bd07598cfe3', 3, '5114100012', '2017-03-04', 4600000, 4, '5114100012', 50000),
('PO58bd077f71028', 2, '5114100118', '2017-03-06', 50000, 2, '5114100118', 10000),
('PO58bd079ebf0c2', 1, '5114100151', '2017-03-01', 104000000, 2, '5114100151', 50000),
('PO58bd07a5bb892', 5, '511400704', '2017-03-06', 54000000, 3, '511400704', 50000),
('PO58bd07c88b0d4', 3, '5114100012', '2017-03-04', 1550000, 3, '5114100012', 50000),
('PO58bd07df35c76', 5, '5114100151', '2017-03-02', 27000000, 3, '5114100151', 50000),
('PO58bd07ec3c864', 1, '5114100017', '2017-03-05', 12740000, 3, '5114100017', 100000),
('PO58bd080ebd94b', 1, '5114100064', '2017-03-06', 91000000, 3, '5114100064', 100000),
('PO58bd0811ba70a', 1, '5114100073', '2017-03-02', 15600000, 3, '5114100073', 25000),
('PO58bd081a241f6', 5, '5114100014', '2017-03-07', 975000, 3, '5114100014', 20000),
('PO58bd0849a5565', 1, '5114100050', '2017-03-06', 130000000, 2, '5114100050', 200000),
('PO58bd08992c3c9', 1, '5114100164', '2017-06-07', 220000000, 3, '5114100164', 50000),
('PO58bd08bed232b', 1, '5114100118', '2017-03-06', 260000, 3, '5114100118', 10000),
('PO58bd08d74a109', 4, '5114100175', '2017-03-07', 180000, 3, '5114100175', 10000),
('PO58bd0964461bd', 4, '5114100001', '2017-03-06', 9000000, 3, '5114100001', 100000),
('PO58bd09ca5d312', 4, '5114100012', '2017-03-04', 6000000, 3, '5114100012', 30000),
('PO58bd0a0da8e60', 1, '5114100167', '2017-03-13', 1100000, 1, '5114100167', 100000),
('PO58bd0a968088f', 1, '5114100065', '2017-03-01', 2400000, 3, '5114100065', 50000),
('PO58bd0bf64d4a7', 4, '5114100012', '2017-03-04', 7000000, 3, '5114100012', 20000),
('PO58bd0c2ceac1c', 1, '5114100167', '2017-03-13', 780000, 3, '5114100167', 100000),
('PO58bd0c5a2c980', 4, '5114100017', '2017-03-07', 3600000, 3, '5114100017', 100000),
('PO58bd0c5f96cf5', 1, '5114100118', '2017-03-06', 2600000, 3, '5114100118', 10000),
('PO58bd0d268cb2a', 2, '5114100181', '2017-03-05', 500000, 3, '5114100181', 555),
('PO58bd0db1dd80e', 1, '5114100151', '2017-03-26', 10400000, 3, '5114100151', 30000),
('PO58bd0df6880c4', 3, '5114100012', '2017-03-03', 300000, 3, '5114100012', 30000),
('PO58bd11a032419', 4, '5114100012', '2017-03-05', 6000000, 3, '5114100012', 45000),
('PO58bd2c6e6dc9c', 1, '1316100030', '2017-03-08', 19200, 3, '1316100030', 1000),
('PO58bd2f851ac8d', 2, '1316100030', '2017-03-14', 2000, 3, '1316100030', 0),
('PO58bd5984ba34d', 1, '5113100043', '2017-03-07', 240000, 3, '5113100043', 20000),
('PO58bd5e6fb06da', 2, '1316100030', '2017-03-07', 1000, 3, '1316100030', 0);

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id_satuan` int(11) NOT NULL,
  `nama_satuan` varchar(50) NOT NULL,
  `kelompok_satuan` varchar(50) NOT NULL,
  `deskripsi_satuan` varchar(100) DEFAULT NULL,
  `id_petugas` varchar(100) DEFAULT NULL,
  `id_pemilik` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`, `kelompok_satuan`, `deskripsi_satuan`, `id_petugas`, `id_pemilik`) VALUES
(1, 'Bijian', 'Retail', 'Penjualan per buah', NULL, '2017'),
(2, 'Kardus Kecil', 'Midi', 'Penjualan per kardus kecil', NULL, '2017'),
(3, 'Kardus besar', 'Grosir', 'Penjualan per kardus besar', NULL, '2017');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id_service` int(11) NOT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `keluhan` text NOT NULL,
  `tgl_open` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `solving`
--

CREATE TABLE `solving` (
  `id_solving` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  `teknisi` varchar(100) NOT NULL,
  `penyelesaian` text NOT NULL,
  `tgl_solved` date NOT NULL,
  `id_petugas` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `id_suplier` int(11) NOT NULL,
  `nama_suplier` varchar(200) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `tgl` datetime NOT NULL,
  `photo_link` varchar(200) NOT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`id_suplier`, `nama_suplier`, `alamat`, `hp`, `email`, `deskripsi`, `tgl`, `photo_link`, `id_pemilik`) VALUES
(1, 'Sinar Abadi', 'Gebang Wetan 23D', '087849862859', 'toko1@toko.co.id', 'supplier gebang', '2017-02-25 14:57:47', '', '2017'),
(2, 'Lancar Jaya', 'Kejawan Gebang 6 23', '087849862777', 'toko2@toko.co.id', 'supplier Kejawan', '2017-02-25 14:58:18', '', '2017'),
(3, 'Nikmat', 'Gebang Wetan', '031 784921', 'toko3@toko.co.id', 'supplier gebang', '2017-02-25 14:59:01', '', '2017'),
(4, 'Sakinah', 'Keputih', '031456123', 'toko4@toko.co.id', 'supplier keputih', '2017-02-25 14:59:35', '', '2017'),
(5, 'Maju Terus', 'Keputih tambak', '0584865', 'toko5@toko.co.id', 'supplier keputih tambak', '2017-02-25 15:00:42', '', '2017');

-- --------------------------------------------------------

--
-- Table structure for table `tipe_item`
--

CREATE TABLE `tipe_item` (
  `id_tipe_item` int(11) NOT NULL,
  `nama_tipe_item` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_item`
--

INSERT INTO `tipe_item` (`id_tipe_item`, `nama_tipe_item`, `deskripsi`, `id_pemilik`) VALUES
(3, 'finish', 'serah wis', '2017');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bom`
--
ALTER TABLE `bom`
  ADD KEY `fk_idProduk` (`id_produk`),
  ADD KEY `fk_idItem` (`id_item`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`),
  ADD KEY `fk_id_institute` (`id_institut`);

--
-- Indexes for table `defect`
--
ALTER TABLE `defect`
  ADD PRIMARY KEY (`id_def`),
  ADD KEY `fk_id_po` (`id_rec`),
  ADD KEY `id_item` (`id_item`),
  ADD KEY `fk_idissueDefect` (`id_issue`);

--
-- Indexes for table `detail_penerimaan`
--
ALTER TABLE `detail_penerimaan`
  ADD KEY `fk_id_po` (`id_rec`),
  ADD KEY `fk_idItemgudang` (`id_item`),
  ADD KEY `fk_idPRO` (`id_produksi`);

--
-- Indexes for table `detail_pengeluaran`
--
ALTER TABLE `detail_pengeluaran`
  ADD KEY `FK_id_transaksi` (`id_issue`),
  ADD KEY `fk_id_produk` (`id_item`),
  ADD KEY `fk_idSupDetPengeluaran` (`id_suplier`),
  ADD KEY `fk_idrecIssue` (`id_rec`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD KEY `FK_id_transaksi` (`id_so`),
  ADD KEY `fk_id_produk` (`id_item`),
  ADD KEY `fk_idSupDetPenjualan` (`id_suplier`);

--
-- Indexes for table `detail_produkservice`
--
ALTER TABLE `detail_produkservice`
  ADD KEY `fk_idServis` (`id_produkService`),
  ADD KEY `fk_idProduk` (`id_item`),
  ADD KEY `fk_idSuplierProServ` (`id_suplier`);

--
-- Indexes for table `detail_produksi`
--
ALTER TABLE `detail_produksi`
  ADD KEY `fk_idpo` (`id_produksi`),
  ADD KEY `fkitempo` (`id_item`),
  ADD KEY `fk_idSupli` (`id_suplier`);

--
-- Indexes for table `detail_purchasing`
--
ALTER TABLE `detail_purchasing`
  ADD KEY `fk_idpo` (`id_purchasing`),
  ADD KEY `fkitempo` (`id_item`);

--
-- Indexes for table `detail_suplier`
--
ALTER TABLE `detail_suplier`
  ADD KEY `fk_id_suplier` (`id_suplier`),
  ADD KEY `fk_id_itemdetailSuplier` (`id_item`);

--
-- Indexes for table `gl_account`
--
ALTER TABLE `gl_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coa_g_id` (`coa_id`),
  ADD KEY `coa_id` (`coa_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `acc_code` (`id`);

--
-- Indexes for table `gl_group`
--
ALTER TABLE `gl_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gl_has`
--
ALTER TABLE `gl_has`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `gl_journal_h`
--
ALTER TABLE `gl_journal_h`
  ADD PRIMARY KEY (`id`),
  ADD KEY `period_code` (`period_id`),
  ADD KEY `journal_name` (`journal_name`),
  ADD KEY `uid` (`uid`) USING BTREE;

--
-- Indexes for table `gl_journal_l`
--
ALTER TABLE `gl_journal_l`
  ADD PRIMARY KEY (`id`),
  ADD KEY `journal_code` (`journal_id`),
  ADD KEY `coa_number` (`acc_id`),
  ADD KEY `cd_id` (`line_credit`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD KEY `fk_idItemGudang` (`id_item`),
  ADD KEY `fk_idProGudang` (`id_produksi`),
  ADD KEY `fk_idPurGudang` (`id_rec`);

--
-- Indexes for table `institusi`
--
ALTER TABLE `institusi`
  ADD PRIMARY KEY (`id_institusi`);

--
-- Indexes for table `item_master`
--
ALTER TABLE `item_master`
  ADD PRIMARY KEY (`id_item`),
  ADD KEY `fk_tipe` (`tipe`),
  ADD KEY `fk_satuan` (`satuan`);

--
-- Indexes for table `modal`
--
ALTER TABLE `modal`
  ADD PRIMARY KEY (`id_modal`);

--
-- Indexes for table `penerimaan_barang`
--
ALTER TABLE `penerimaan_barang`
  ADD PRIMARY KEY (`id_rec`),
  ADD KEY `fk_idporeceive` (`id_po`),
  ADD KEY `fk_idproreceive` (`id_produksi`);

--
-- Indexes for table `pengeluaran_barang`
--
ALTER TABLE `pengeluaran_barang`
  ADD PRIMARY KEY (`id_issue`),
  ADD KEY `FK_idPetugas` (`id_petugas`),
  ADD KEY `FK_id_custom` (`id_customer`),
  ADD KEY `fk_isoIssue` (`id_so`),
  ADD KEY `fk_proIssue` (`id_produksi`),
  ADD KEY `fk_proServIssue` (`id_produkService`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_so`),
  ADD KEY `FK_idPetugas` (`id_petugas`),
  ADD KEY `FK_id_custom` (`id_customer`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `produkservice`
--
ALTER TABLE `produkservice`
  ADD PRIMARY KEY (`id_produkService`),
  ADD KEY `fk_idServis` (`id_service`),
  ADD KEY `fk_idPetugasServ` (`teknisi`);

--
-- Indexes for table `produksi`
--
ALTER TABLE `produksi`
  ADD PRIMARY KEY (`id_produksi`);

--
-- Indexes for table `purchasing`
--
ALTER TABLE `purchasing`
  ADD PRIMARY KEY (`id_po`),
  ADD KEY `fk_Idsuplier` (`id_suplier`),
  ADD KEY `fk_idPet` (`id_petugas`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id_service`),
  ADD KEY `FK_id_custome` (`id_petugas`),
  ADD KEY `FK_id_customer` (`id_customer`);

--
-- Indexes for table `solving`
--
ALTER TABLE `solving`
  ADD PRIMARY KEY (`id_solving`),
  ADD KEY `fk_idTeknisi` (`teknisi`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indexes for table `tipe_item`
--
ALTER TABLE `tipe_item`
  ADD PRIMARY KEY (`id_tipe_item`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `defect`
--
ALTER TABLE `defect`
  MODIFY `id_def` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `gl_account`
--
ALTER TABLE `gl_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT for table `gl_group`
--
ALTER TABLE `gl_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `gl_has`
--
ALTER TABLE `gl_has`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `gl_journal_h`
--
ALTER TABLE `gl_journal_h`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;
--
-- AUTO_INCREMENT for table `gl_journal_l`
--
ALTER TABLE `gl_journal_l`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=519;
--
-- AUTO_INCREMENT for table `modal`
--
ALTER TABLE `modal`
  MODIFY `id_modal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `solving`
--
ALTER TABLE `solving`
  MODIFY `id_solving` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `id_suplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tipe_item`
--
ALTER TABLE `tipe_item`
  MODIFY `id_tipe_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bom`
--
ALTER TABLE `bom`
  ADD CONSTRAINT `fk_idItem` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idProduk` FOREIGN KEY (`id_produk`) REFERENCES `item_master` (`id_item`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `fk_id_institute` FOREIGN KEY (`id_institut`) REFERENCES `institusi` (`id_institusi`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `defect`
--
ALTER TABLE `defect`
  ADD CONSTRAINT `fk_idRecDefect` FOREIGN KEY (`id_rec`) REFERENCES `penerimaan_barang` (`id_rec`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_item` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idissueDefect` FOREIGN KEY (`id_issue`) REFERENCES `pengeluaran_barang` (`id_issue`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `detail_penerimaan`
--
ALTER TABLE `detail_penerimaan`
  ADD CONSTRAINT `fk_idItempenerimaan` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idPRO` FOREIGN KEY (`id_produksi`) REFERENCES `produksi` (`id_produksi`) ON DELETE NO ACTION ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_idPur` FOREIGN KEY (`id_rec`) REFERENCES `penerimaan_barang` (`id_rec`) ON DELETE NO ACTION ON UPDATE SET NULL;

--
-- Constraints for table `detail_pengeluaran`
--
ALTER TABLE `detail_pengeluaran`
  ADD CONSTRAINT `fk_idIssue` FOREIGN KEY (`id_issue`) REFERENCES `pengeluaran_barang` (`id_issue`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idItemIssue` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idSupDetPengeluaran` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idrecIssue` FOREIGN KEY (`id_rec`) REFERENCES `penerimaan_barang` (`id_rec`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD CONSTRAINT `Fk_idItemSO` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idSo` FOREIGN KEY (`id_so`) REFERENCES `penjualan` (`id_so`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idSupDetPenjualan` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `detail_produkservice`
--
ALTER TABLE `detail_produkservice`
  ADD CONSTRAINT `fk_idItemProdServ` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idSuplierProServ` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idprodServ` FOREIGN KEY (`id_produkService`) REFERENCES `produkservice` (`id_produkService`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `detail_produksi`
--
ALTER TABLE `detail_produksi`
  ADD CONSTRAINT `fk_idItemDetailProduksi` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idProduksi` FOREIGN KEY (`id_produksi`) REFERENCES `produksi` (`id_produksi`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idSupli` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `detail_purchasing`
--
ALTER TABLE `detail_purchasing`
  ADD CONSTRAINT `fk_idpo` FOREIGN KEY (`id_purchasing`) REFERENCES `purchasing` (`id_po`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fkitempo` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON UPDATE CASCADE;

--
-- Constraints for table `detail_suplier`
--
ALTER TABLE `detail_suplier`
  ADD CONSTRAINT `fk_id_itemdetailSuplier` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_suplier` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON UPDATE CASCADE;

--
-- Constraints for table `gudang`
--
ALTER TABLE `gudang`
  ADD CONSTRAINT `fk_idItemGudang` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idProGudang` FOREIGN KEY (`id_produksi`) REFERENCES `produksi` (`id_produksi`) ON DELETE NO ACTION ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_idPurGudang` FOREIGN KEY (`id_rec`) REFERENCES `penerimaan_barang` (`id_rec`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `item_master`
--
ALTER TABLE `item_master`
  ADD CONSTRAINT `fk_satuan` FOREIGN KEY (`satuan`) REFERENCES `satuan` (`id_satuan`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `penerimaan_barang`
--
ALTER TABLE `penerimaan_barang`
  ADD CONSTRAINT `fk_idporeceive` FOREIGN KEY (`id_po`) REFERENCES `purchasing` (`id_po`) ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_idproreceive` FOREIGN KEY (`id_produksi`) REFERENCES `produksi` (`id_produksi`) ON UPDATE SET NULL;

--
-- Constraints for table `pengeluaran_barang`
--
ALTER TABLE `pengeluaran_barang`
  ADD CONSTRAINT `fk_isoIssue` FOREIGN KEY (`id_so`) REFERENCES `penjualan` (`id_so`) ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_proIssue` FOREIGN KEY (`id_produksi`) REFERENCES `produksi` (`id_produksi`) ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_proServIssue` FOREIGN KEY (`id_produkService`) REFERENCES `produkservice` (`id_produkService`) ON DELETE NO ACTION ON UPDATE SET NULL;

--
-- Constraints for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `FK_idPetugas` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_id_custom` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `produkservice`
--
ALTER TABLE `produkservice`
  ADD CONSTRAINT `fk_idPetugasServ` FOREIGN KEY (`teknisi`) REFERENCES `petugas` (`id_petugas`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idServis` FOREIGN KEY (`id_service`) REFERENCES `service` (`id_service`) ON UPDATE CASCADE;

--
-- Constraints for table `purchasing`
--
ALTER TABLE `purchasing`
  ADD CONSTRAINT `fk_Idsuplier` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idPet` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `FK_id_customer` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_id_petugas` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON UPDATE CASCADE;

--
-- Constraints for table `solving`
--
ALTER TABLE `solving`
  ADD CONSTRAINT `fk_idTeknisi` FOREIGN KEY (`teknisi`) REFERENCES `petugas` (`id_petugas`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
