-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2017 at 06:21 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `retail01`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_alert_stok` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT tb1.nama_item,tb1.id_suplier,tb1.nama_item,tb1.`id_item`,tb1.tipe,tb1.satuan,SUM(tb1.`jumlah`) AS jumlah,tb1.hargaSatuan,tb1.link_photo,tb1.deskripsi
	FROM (SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_produksi`=penerimaan_barang.`id_produksi`
	INNER JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	where item_master.`id_pemilik`=_id_pemilik
	GROUP BY item_master.`id_item`
UNION
	SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	where item_master.`id_pemilik`=_id_pemilik
	 GROUP BY item_master.`id_item` ORDER BY nama_item ASC) AS tb1 WHERE tb1.jumlah<20 GROUP BY tb1.nama_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariCustomer` (`_keyword` VARCHAR(300))  BEGIN
	SELECT * FROM customer WHERE nama LIKE CONCAT('%',_keyword,'%') or id_customer like CONCAT('%',_keyword,'%') ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariPetugas` (`_keyword` VARCHAR(300))  BEGIN
	select * from petugas where nama like CONCAT('%',_keyword,'%') or id_petugas LIKE CONCAT('%',_keyword,'%');
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariProduk` (`cari_suplier` VARCHAR(200))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE nama_item LIKE CONCAT('%',cari_suplier,'%')
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cariSuplier` (`_keyword` VARCHAR(300))  BEGIN
	SELECT * FROM suplier WHERE nama_suplier LIKE CONCAT('%',_keyword,'%') or id_suplier LIKE CONCAT('%',_keyword,'%');
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cari_suplierProduk` (`cari_suplier` VARCHAR(200))  BEGIN
	SELECT nama_suplier,suplier.`id_suplier`,item_master.`id_item`,nama_item,satuan,tipe, SUM(gudang.`jumlah`)AS jumlah,
	 link_photo,item_master.deskripsi,detail_penerimaan.hargaSatuan 
	FROM detail_penerimaan LEFT JOIN gudang 
	ON detail_penerimaan.`id_purchasing`=gudang.`id_purchasing` AND
	detail_penerimaan.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_penerimaan.`id_item`
	INNER JOIN penerimaan_barang ON penerimaan_barang.`id_po`=detail_penerimaan.`id_purchasing`
	INNER JOIN suplier ON suplier.`id_suplier`=penerimaan_barang.`id_suplier`
	 WHERE nama_item LIKE CONCAT('%',cari_suplier,'%') GROUP BY detail_penerimaan.`id_item`;
	 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cekService` (`_id_service` INT(11))  BEGIN
	set@cek=(select id_service from produkservice where id_service=_id_service);
	if(@cek) then
		select 1 as cek;
	else
		select -1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cekStok` (IN `_id_item` VARCHAR(100), IN `_id_supplier` INT(11), IN `_harga` INT(11), IN `_id_pemilik` VARCHAR(100))  BEGIN
SELECT gudang.`id_rec`,detail_suplier.`id_suplier`,detail_suplier.`id_item`,gudang.`jumlah`,hargaSatuan FROM penerimaan_barang 
INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
WHERE detail_suplier.`id_item`=_id_item and penerimaan_barang.`id_suplier`=_id_supplier  and gudang.`hargaSatuan`=_harga and penerimaan_barang.`id_pemilik`=_id_pemilik
ORDER BY gudang.`id_rec` asc ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cek_akses` (`_email` VARCHAR(200))  BEGIN
    set@idPet=(select id_petugas from petugas where petugas.`email`=_email);
	select privilege from petugas where id_petugas=@idPet;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	set@cek=(select id_customer from penjualan where id_customer=_id_customer limit 1);
	IF(@cek) THEN
			SELECT -1 AS cek;
		ELSE
		DELETE FROM customer WHERE id_customer=_id_customer;
			SELECT 1 AS cek;
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteInstitusi` (`_id_institusi` VARCHAR(100))  BEGIN
   
	set@child=(select id_customer from customer where customer.`id_institut`=_id_institusi limit 1);
	if(@child) then
		select -1 as cek;
	else
		DELETE FROM institusi WHERE id_institusi=_id_institusi;
		SET@cek=(SELECT id_institusi FROM institusi WHERE id_institusi=_id_institusi);
		IF(@cek) THEN
			SELECT -1 AS cek;
		ELSE
			SELECT 1 AS cek;
		END IF;
	
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteService` (`_id_service` INT(11))  BEGIN
	
	delete from service where id_service=_id_service;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteSuplier` (`_id_suplier` INT(11))  BEGIN
	SET@cek=(SELECT id_suplier FROM purchasing WHERE id_suplier=_id_suplier LIMIT 1);
	IF(@cek) THEN
			SELECT -1 AS cek;
	ELSE
	delete from suplier where id_suplier=_id_suplier;
	select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_bom` (`_id_produk` VARCHAR(100))  BEGIN
	delete from bom where id_produk=_id_produk;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_item` (`_id_item` VARCHAR(100))  BEGIN
	set@cek=(select id_item from detail_suplier where id_item=_id_item limit 1);
	if(@cek) then
		select -1 as cek;
	else
		delete from item_master where id_item=_id_item;
		select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_satuan` (`_id_satuan` INT(11))  BEGIN
	set@child=(select satuan from item_master where satuan=_id_satuan);
	if(@child) then
		select -1 as cek;
	else
		delete from satuan where id_satuan=_id_satuan;
		set@cek=(select id_satuan from satuan where id_satuan=_id_satuan);
		if(@cek) then
			select -1 as cek;
		else
			select 1 as cek;
		end if;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_tipe` (`_id_tipe` INT(11))  BEGIN
	SET@child=(SELECT tipe FROM item_master WHERE satuan=_id_tipe);
	IF(@child) THEN
		SELECT -1 AS cek;
	ELSE
		DELETE FROM tipe_item WHERE id_tipe_item=_id_tipe;
		SET@cek=(SELECT id_tipe_item FROM tipe_item WHERE id_tipe_item=_id_tipe);
		IF(@cek) THEN
			SELECT -1 AS cek;
		ELSE
			SELECT 1 AS cek;
		END IF;
	END IF;
  
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deletPetugas` (`_id_petugas` VARCHAR(100))  BEGIN
	SET@cek=(SELECT id_petugas FROM penjualan WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek1=(SELECT id_petugas FROM purchasing WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek2=(SELECT id_petugas FROM service WHERE id_petugas=_id_petugas LIMIT 1);
	SET@cek3=(SELECT teknisi FROM solving WHERE teknisi=_id_petugas LIMIT 1);
	IF(@cek or @cek1 or @cek2 or @cek3 ) THEN
			SELECT -1 AS cek;
	ELSE
	delete from petugas where id_petugas=_id_petugas;
	select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_detail_pembelian` (`_id_pembelian` VARCHAR(100), `_id_produk` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11))  BEGIN
	insert into detail_pembelian (id_transaksi,id_produk,jumlah,harga) values(_id_pembelian,_id_produk,_jumlah,_harga);
	UPDATE produk SET jumalah=jumlah-_jumlah WHERE id_produk=_id_produk;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_detail_pengeluaran` (`_id_issue` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` INT(11), `_harga` INT(11), `_id_suplier` INT(11), `_id_rec` VARCHAR(100), `_kode` INT(11))  BEGIN
	
	insert into detail_pengeluaran(id_issue,id_item,jumlah,harga,id_suplier,id_rec)
	values(_id_issue,_id_item,_jumlah,_harga,_id_suplier,_id_rec);
	
	if(_kode=1) then
		SET@idSO=(SELECT id_so FROM pengeluaran_barang WHERE pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_penjualan WHERE id_so=@idSO);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		if(@count2>=@count1) then
			update penjualan set `status`=3 where id_so=@idSO;
			update pengeluaran_barang set `status`=3 where id_issue=_id_issue;
		end if;
	elseif(_kode=2) then
		set@idPro=(select id_produksi from pengeluaran_barang where pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_produksi WHERE id_produksi=@idPro);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		IF(@count2>=@count1) THEN
			UPDATE produksi SET `status`=3 WHERE id_produksi=@idPro;
			UPDATE pengeluaran_barang SET `status`=3 WHERE id_issue=_id_issue;
		END IF;
	ELSEIF(_kode=3) THEN
		SET@idPro=(SELECT pengeluaran_barang.`id_produkService` FROM pengeluaran_barang WHERE pengeluaran_barang.`id_issue`=_id_issue);
		SET@count1=(SELECT SUM(jumlah)  FROM detail_produkservice WHERE id_produkService=@idPro);
		SET@count2=(SELECT SUM(jumlah) FROM detail_pengeluaran WHERE id_issue=_id_issue);
		IF(@count2>=@count1) THEN
			UPDATE produkservice SET `status`=3 WHERE id_produkService=@idPro;
			UPDATE pengeluaran_barang SET `status`=3 WHERE id_issue=_id_issue;
		END IF;
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanDefect` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_def,id_rec,id_issue,defect.`id_item`,nama_item,nama_satuan,nama_tipe_item,jumlah,hargaSatuan,tanggal,keterangan FROM defect 
	INNER JOIN item_master ON item_master.`id_item`=defect.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE ((`status`=1 and defect.`tanggal`>=_tgl_awal and defect.`tanggal`<=_tgl_akhir)
	 OR (`status`=2 and defect.`tanggal`>=_tgl_awal and defect.`tanggal`<=_tgl_akhir)) and defect.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanPembelian` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penerimaan_barang.`id_rec` ,tanggal_receive,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir,nama_suplier
	FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON detail_penerimaan.`id_rec`=penerimaan_barang.`id_rec`
	INNER JOIN item_master ON detail_penerimaan.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penerimaan_barang.`id_petugas`=petugas.`id_petugas`
	INNER JOIN suplier ON penerimaan_barang.`id_suplier`=suplier.`id_suplier`
	where penerimaan_barang.`status`=3 and penerimaan_barang.`tanggal_receive`>=_tgl_awal 
	and penerimaan_barang.`tanggal_receive`<=_tgl_akhir and penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanPenjualan` (`_tgl_awal` DATE, `_tgl_akhir` DATE, `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penjualan.`id_so`,tanggal,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir FROM detail_penjualan 
	INNER JOIN penjualan ON detail_penjualan.`id_so`=penjualan.`id_so`
	INNER JOIN item_master ON detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	where penjualan.`status`=3 and penjualan.`tanggal`>=_tgl_awal and penjualan.`tanggal`<=_tgl_akhir and penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_filter_laporanService` (`_tgl_awal` DATE, `_tgl_akhir` DATE)  BEGIN
	SELECT produkservice.`id_produkService`,produkservice.`tanggal`,petugas.`nama` AS nama_petugas,
	customer.`nama`AS nama_customer ,item_master.`id_item`,nama_item,nama_satuan,nama_tipe_item,detail_produkservice.`jumlah`
	FROM detail_produkservice
	INNER JOIN produkservice ON produkservice.`id_produkService`=detail_produkservice.`id_produkService`
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN item_master ON item_master.`id_item`=detail_produkservice.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE (produkservice.`status`=3 and produkservice.`tanggal`>=_tgl_awal) 
	and (produkservice.`status`=3 AND produkservice.`tanggal`<=_tgl_akhir);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_userId` (`_email` VARCHAR(200))  BEGIN
	select id_petugas from petugas where email=_email;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungCustomer` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(id_customer) as jumlah from customer where customer.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPembelian` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(purchasing.`id_po`) as jumlah from purchasing where purchasing.`id_pemilik`=_id_pemilik ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPembelianSukses` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(purchasing.`id_po`) AS jumlah FROM purchasing where purchasing.`status`=3 and purchasing.`id_pemilik`=_id_pemilik ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenerimaan` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penerimaan_barang.`id_rec`) AS jumlah FROM penerimaan_barang where penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPengeluaran` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(pengeluaran_barang.`id_issue`)AS jumlah FROM pengeluaran_barang where pengeluaran_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenjualan` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penjualan.`id_so`) AS jumlah FROM penjualan where penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungPenjualanSukses` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(penjualan.`id_so`) AS jumlah FROM penjualan where penjualan.`status`=3 and penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungProduk` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT COUNT(item_master.`id_item`) AS jumlah FROM item_master where item_master.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungProduksi` ()  BEGIN
	select count(produksi.`id_produksi`) as jumlah from produksi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hitungSupplier` (`_id_pemilik` VARCHAR(100))  BEGIN
	select count(id_suplier) as jumlah from suplier where suplier.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_inputProduk_supplier` (`_idSupplier` INT(11), `_id_item` VARCHAR(100), `_id_pemilik` VARCHAR(100), `_harga` INT(11))  BEGIN
	set@cek=(select id_suplier from detail_suplier where id_suplier=_idSupplier and id_item=_id_item);
	if(@cek) then
		select -1 as cek;
	else
		insert into detail_suplier(id_suplier,id_item,id_pemilik,harga) values(_idSupplier,_id_item,_id_pemilik,_harga);
		select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_bom` (`_id_produk` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE)  BEGIN
	
		insert into bom (id_produk,id_item,jumlah) values(_id_produk,_id_item,_jumlah);
		select 1 as cek;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_customer` (`_idCustomer` VARCHAR(100), `_id_institut` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` VARCHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	set@cek=(select id_customer from customer where id_customer=_idCustomer);
	if(@cek) then
		select -1 as cek;
	else
		insert into `customer` (id_customer,id_institut,nama,jenkel,alamat,hp,email,tgl,jabatan,id_pemilik) 
		values(_idCustomer,_id_institut,_nama,_jenkel,_alamat,_hp,_email,now(),_jabatan,_id_pemilik);
		select 0 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_defect` (`_id` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	IF(_kode=1)THEN
		INSERT INTO defect (id_rec,id_item,jumlah,hargaSatuan,keterangan,tanggal,id_pemilik) VALUES(_id,_id_item,_jumlah,_harga,1,now(),_id_pemilik);
	ELSEIF(_kode=2) THEN
		INSERT INTO defect (id_issue,id_item,jumlah,hargaSatuan,keterangan,tanggal,id_pemilik) VALUES(_id,_id_item,_jumlah,_harga,2,now(),_id_pemilik);
	ELSEIF(_kode=3) THEN
		INSERT INTO defect (id_issue,id_item,jumlah,hargaSatuan,keterangan,tanggal,id_pemilik) VALUES(_id,_id_item,_jumlah,_harga,3,now(),_id_pemilik);
	ELSEIF(_kode=4) THEN
		INSERT INTO defect (id_issue,id_item,jumlah,hargaSatuan,keterangan,tanggal,id_pemilik) VALUES(_id,_id_item,_jumlah,_harga,4,NOW(),_id_pemilik);
	END IF;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailKeluarGudang` (`_id_tran` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_id_suplier` INT(11))  BEGIN
	update detail_penjualan set jumlah_keluar=_jumlah where id_transaksi=_id_tran and id_item=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailPenjualan` (`_id_so` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_id_suplier` INT(11))  BEGIN
	INSERT INTO detail_penjualan (id_so,id_item,jumlah,harga,id_suplier)
	VALUES(_id_so,_id_item,_jumlah,_harga,_id_suplier);
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailProduksi` (`_id_produksi` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_hargaSatuan` INT(11), `_id_suplier` INT(11))  BEGIN
	INSERT INTO detail_produksi(id_produksi,id_item,jumlah,hargaSatuan,id_suplier) VALUES(_id_produksi,_id_item,_jumlah,_hargaSatuan,_id_suplier);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailPurchasing` (`_id_purchasing` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_hargaSatuan` INT(11))  BEGIN
	insert into detail_purchasing(id_purchasing,id_item,jumlah,hargaSatuan) values(_id_purchasing,_id_item,_jumlah,_hargaSatuan);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_detailServProduk` (`_id_tran` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_id_suplier` INT(11))  BEGIN
	INSERT INTO detail_produkservice(id_produkService,id_item,jumlah,harga,id_suplier) 
	VALUES(_id_tran,_id_item,_jumlah,_harga,_id_suplier);
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_gudang` (`_id_rec` VARCHAR(200), `_id_tran` VARCHAR(200), `_id_item` VARCHAR(200), `_jumlah` DOUBLE, `_harga` INT(11), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	if(_kode=1) then
	set@cekPO=(select jumlah from detail_purchasing where id_purchasing=_id_tran and id_item=_id_item);
	if (_jumlah>=@cekPO) then
		insert into gudang (id_rec,id_item,jumlah,hargaSatuan,id_pemilik) values(_id_rec,_id_item,_jumlah,_harga,_id_pemilik);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik);
		set@count1=(SELECT SUM(jumlah)  FROM detail_purchasing WHERE id_purchasing=_id_tran);
		set@count2=(select sum(jumlah) from gudang where id_rec=_id_rec);
		if (@count2>=@count1) then 
			update purchasing set `status`=3 where id_po=_id_tran;
			UPDATE penerimaan_barang SET `status`=3 WHERE id_rec=_id_rec;
		end if;
	else
		INSERT INTO gudang (id_rec,id_item,jumlah,hargaSatuan,`status`,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,(_jumlah-@cekPO),_id_pemilik);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,`status`,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,(_jumlah-@cekPO),_id_pemilik);
		INSERT INTO defect (id_rec,id_item,jumlah,hargaSatuan,tanggal,keterangan,`status`,id_pemilik) 
		VALUES(_id_rec,_id_item,(@cekPO-_jumlah),_harga,now(),1,1,_id_pemilik);
		UPDATE purchasing SET `status`=2 WHERE id_po=_id_tran;
		UPDATE penerimaan_barang SET `status`=2 WHERE id_rec=_id_rec; 
	end if;
	elseif(_kode=2)then
		INSERT INTO gudang (id_rec,id_item,jumlah,hargaSatuan,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik);
		INSERT INTO detail_penerimaan (id_rec,id_item,jumlah,hargaSatuan,id_pemilik) VALUES(_id_rec,_id_item,_jumlah,_harga,_id_pemilik);
		SET@count1=(SELECT SUM(jumlah_item)  FROM produksi WHERE id_produksi=_id_tran);
		SET@count2=(SELECT SUM(jumlah) FROM gudang WHERE id_rec=_id_rec);
		IF (@count2>=@count1) THEN 
			UPDATE produksi SET `status`=6 WHERE id_produksi=_id_tran;
			UPDATE penerimaan_barang SET `status`=3 WHERE id_rec=_id_rec;
		END IF;
		
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_institusi` (`_id_institut` VARCHAR(100), `_nama` VARCHAR(300), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200))  BEGIN
	set@cek=(select `id_institusi` from institusi where `id_institusi`=_id_institut);
	if(@cek) then
		select -1 as cek;
	else
		INSERT INTO `institusi` (id_institusi,nama_institusi,alamat_institusi,telephone_institusi,email,tgl_registrasi) 
		VALUES(_id_institut,_nama,_alamat,_hp,_email,NOW());
		select 0 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_item` (`_id_item` VARCHAR(100), `_nama_item` VARCHAR(200), `_harga` INT(50), `_satuan` VARCHAR(50), `_deskripsi` TEXT, `_link_photo` VARCHAR(200), `id_pemilik` VARCHAR(200))  BEGIN
	SET@cek=(SELECT count(*) from item_master where id_item=_id_item);
	if (@cek) then
		select -1 as cek;
	else
	insert into item_master(id_item,nama_item,item_harga,satuan,deskripsi,link_photo,id_pemilik) values(_id_item,_nama_item,_harga,_satuan,_deskripsi,_link_photo,id_pemilik);
	select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_keluarGudang` (`_id_issue` VARCHAR(100), `_id` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` INT(11), `_total` BIGINT(20), `_tgl` DATETIME, `_kurir` VARCHAR(100), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	set@idPet=(select id_petugas from petugas where email=_email);
	if _kode=1 then
		insert into pengeluaran_barang(id_issue,id_so,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		values(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	elseif _kode=2 then
		INSERT INTO pengeluaran_barang(id_issue,id_produksi,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		VALUES(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	ELSEIF _kode=3 THEN
		INSERT INTO pengeluaran_barang(id_issue,id_produkService,id_petugas,id_customer,total,kurir,tanggal,id_pemilik)
		VALUES(_id_issue,_id,@idPet,_id_customer,_total,_kurir,_tgl,_id_pemilik);
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_penerimaan` (`_id_rec` VARCHAR(100), `_id_po` VARCHAR(200), `_idPenerima` VARCHAR(200), `_tgl` DATE, `_idSuplier` INT(11), `_totalHarga` BIGINT(20), `_kurir` VARCHAR(100), `_kode` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	if(_kode=1) then
		INSERT INTO penerimaan_barang (id_rec,id_po,id_petugas,tanggal_receive,id_suplier,totalHarga,kurir,id_pemilik) 
		VALUES(_id_rec,_id_po,_idPenerima,_tgl,_idSuplier,_totalHarga,_kurir,_id_pemilik);
	elseif(_kode=2) then
		INSERT INTO penerimaan_barang (id_rec,id_produksi,id_petugas,tanggal_receive,id_suplier,totalHarga,kurir) 
		VALUES(_id_rec,_id_po,_idPenerima,_tgl,_idSuplier,_totalHarga,_kurir);
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_penjualan` (`_id_so` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` VARCHAR(100), `_total` BIGINT(20), `_tgl` DATETIME, `_kurir` VARCHAR(100), `_ongkir` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SET@id_petugas=(SELECT id_petugas FROM petugas WHERE email=_email);
	INSERT INTO penjualan (id_so,id_petugas,id_customer,tanggal,total,kurir,ongkir,id_pemilik) 
	VALUES(_id_so,@id_petugas,_id_customer,_tgl,_total,_kurir,_ongkir,_id_pemilik);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_petugas` (`_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_email` VARCHAR(200), `_passwd` VARCHAR(200))  BEGIN
	set@cek=(select count(*) from petugas where id_petugas=_ktp);
	SET@cek2=(SELECT Count(*) email FROM petugas WHERE email=_email);
	if (@cek or @cek2) then
		select -1 as cek;
	else
		INSERT INTO `petugas` (id_petugas,nama,email,tgl,`password`,privilege,jabatan) 
		VALUES(_ktp,_nama,_email,now(),md5(_passwd),1,'user');
		select 0 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_produkService` (`_id` VARCHAR(100), `_id_service` INT(11), `_total` BIGINT(20), `_teknisi` VARCHAR(200))  BEGIN
	insert into produkservice(id_produkService,id_service,total,tanggal,teknisi)
	values(_id,_id_service,_total,now(),_teknisi);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_produksi` (`_id_po` VARCHAR(100), `_email` VARCHAR(200), `_jumlah` INT(11), `_tanggal_po` DATE, `_totalHarga` BIGINT(20), `_id_produk` VARCHAR(100))  BEGIN
	SET@idPetugas=(SELECT id_petugas FROM petugas WHERE email=_email);
	INSERT INTO produksi (id_produksi,id_petugas,tanggal_po,totalHarga,jumlah_item,id_produk) VALUES(_id_po,@idPetugas,_tanggal_po,_totalHarga,_jumlah,_id_produk);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_produksiToGudang` (`_id_rec` VARCHAR(100), `_id_po` VARCHAR(200), `_id_petugas` VARCHAR(200), `_id_suplier` INT(11), `_totalHarga` BIGINT(20), `_id_item` VARCHAR(200), `_jumlah` INT(11), `_harga` INT(11))  BEGIN
	insert into penerimaan_barang(id_rec,id_produksi,id_petugas,tanggal_receive,`status`,id_suplier,totalHarga)
	values(_id_rec,_id_po,_id_petugas,now(),3,_id_suplier,_totalHarga);
	insert into detail_penerimaan(id_produksi,id_item,jumlah,hargaSatuan)
	values(_id_po,_id_item,_jumlah,_harga);
	INSERT INTO gudang(id_produksi,id_item,jumlah,hargaSatuan)
	VALUES(_id_po,_id_item,_jumlah,_harga);
	update produksi set `status`=2 where id_produksi=_id_po;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_purchasing` (`_id_po` VARCHAR(100), `_id_suplier` INT(11), `_email` VARCHAR(200), `_tanggal_po` DATE, `_totalHarga` BIGINT(20), `_id_pemilik` VARCHAR(100), `_ongkir` INT(11))  BEGIN
	set@idPetugas=(select id_petugas from petugas where email=_email);
	insert into purchasing (id_po,id_suplier,id_petugas,tanggal_po,totalHarga,id_pemilik,ongkir) 
	values(_id_po,_id_suplier,@idPetugas,_tanggal_po,_totalHarga,_id_pemilik,_ongkir);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_return` (`_id_po` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11))  BEGIN
	set @cek=(select id_purchasing from gudang where id_purchasing=_id_po);
	if @cek then
		update gudang set jumalah=(jumlah+_jumlah);
		update defect set `status`=0 where id_purchasing=_id_po and id_item=_id_item;
	else
		INSERT INTO gudang (id_purchasing,id_item,jumlah,hargaSatuan) VALUES(_id_po,_id_item,_jumlah,_harga);
		UPDATE defect SET `status`=0 WHERE id_purchasing=_id_po AND id_item=_id_item;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_satuan` (`_nama_satuan` VARCHAR(50), `_kelompok` VARCHAR(50), `_deskripsi` VARCHAR(100), `_id_pemilik` VARCHAR(100))  BEGIN
	insert into satuan (nama_satuan,kelompok_satuan,deskripsi_satuan,id_pemilik) values(_nama_satuan,_kelompok,_deskripsi,_id_pemilik);
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_service` (`_email` VARCHAR(200), `_id_customer` VARCHAR(100), `_subject` VARCHAR(500), `_keluhan` TEXT, `_tgl_open` DATETIME, `_status` INT(11))  BEGIN
	set@idpetugas=(select id_petugas from petugas where email=_email);
	INSERT INTO `service` (id_petugas,id_customer,`subject`,keluhan,tgl_open,`status`) 
	VALUES(@idpetugas,_id_customer,_subject,_keluhan,_tgl_open,_status);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_solving` (`_id_service` INT(11), `_tgl_solved` DATE, `_teknisi` VARCHAR(200), `_penyelesaian` TEXT, `_status` INT(11))  BEGIN
	insert into solving(id_service,teknisi,penyelesaian,tgl_solved) values(_id_service,_teknisi,_penyelesaian,_tgl_solved);
	update `service` set `status`=_status where id_service=_id_service;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_suplier` (`_nama` VARCHAR(200), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_deskripsi` TEXT, `_id_pemilik` VARCHAR(100))  BEGIN
	set@cek=(select id_suplier from suplier where lower(nama_suplier)=lower(_nama));
	if(@cek) then
		select -1 as cek;
	else
		INSERT INTO `suplier` (nama_suplier,alamat,hp,email,deskripsi,tgl,id_pemilik) VALUES(_nama,_alamat,_hp,_email,_deskripsi,now(),_id_pemilik);
		select 1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_input_tipe` (`_nama_tipe` VARCHAR(100), `_deskripsi` TEXT)  BEGIN
	insert into tipe_item(nama_tipe_item,deskripsi) values(_nama_tipe,_deskripsi);
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_defect` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_def,id_rec,id_issue,defect.`id_item`,nama_item,nama_satuan,nama_tipe_item,jumlah,hargaSatuan,tanggal,keterangan FROM defect 
	INNER JOIN item_master ON item_master.`id_item`=defect.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE ((`status`=1 AND defect.`tanggal`>=now() AND defect.`tanggal`<=now())
	 OR (`status`=2 AND defect.`tanggal`>=now() AND defect.`tanggal`<=now())) and defect.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_pembelian` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT penerimaan_barang.`id_rec` ,tanggal_receive,nama_item,sat.nama_satuan,jumlah,petugas.`nama` AS nama_petugas,kurir,nama_suplier
	FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON detail_penerimaan.`id_rec`=penerimaan_barang.`id_rec`
	INNER JOIN item_master ON detail_penerimaan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	INNER JOIN petugas ON penerimaan_barang.`id_petugas`=petugas.`id_petugas`
	INNER JOIN suplier ON penerimaan_barang.`id_suplier`=suplier.`id_suplier`
	WHERE penerimaan_barang.`status`=3 and penerimaan_barang.`tanggal_receive`>=now() AND penerimaan_barang.`tanggal_receive`<=now() and penerimaan_barang.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_penjualan` (`_id_pemilik` VARCHAR(100))  BEGIN
	select penjualan.`id_so`,tanggal,nama_item,sat.nama_satuan,jumlah,petugas.`nama` as nama_petugas,kurir from detail_penjualan 
	inner join penjualan on detail_penjualan.`id_so`=penjualan.`id_so`
	inner join item_master on detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan` 
	inner join petugas on penjualan.`id_petugas`=petugas.`id_petugas`
	WHERE penjualan.`status`=3 and penjualan.`tanggal`>=now() AND penjualan.`tanggal`<=now() and penjualan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_laporan_service` ()  BEGIN
	SELECT produkservice.`id_produkService`,produkservice.`tanggal`,petugas.`nama` AS nama_petugas,
	customer.`nama`AS nama_customer ,item_master.`id_item`,nama_item,nama_satuan,nama_tipe_item,detail_produkservice.`jumlah`
	FROM detail_produkservice
	INNER JOIN produkservice ON produkservice.`id_produkService`=detail_produkservice.`id_produkService`
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN item_master ON item_master.`id_item`=detail_produkservice.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE (produkservice.`status`=3 AND produkservice.`tanggal`>=now()) 
	AND (produkservice.`status`=3 AND produkservice.`tanggal`<=now());
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listProduk_perSuplier` (`_id_suplier` INT(11))  BEGIN
	SELECT detail_suplier.`harga`,nama_item,detail_suplier.`id_item`,sat.nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan` 
	WHERE detail_suplier.`id_suplier`=_id_suplier GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_bahanProduksi` (`_id_produk` VARCHAR(100))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,tipe,satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN bom ON bom.`id_item`=item_master.`id_item`
	WHERE bom.`id_produk`=_id_produk
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_bom` ()  BEGIN
	select item_master.`id_item`,nama_item,nama_satuan,nama_tipe_item,item_master.`deskripsi`,link_photo from bom 
	inner join item_master on item_master.`id_item`=bom.`id_produk`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe` 
	group by bom.`id_produk`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_customer` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer INNER JOIN institusi ON customer.`id_institut`=institusi.`id_institusi` 
	 where customer.`id_pemilik`=_id_pemilik order by nama asc;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_defect` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_def,id_rec,id_issue,nama_item,item_master.`id_item`,jumlah,hargaSatuan,nama_tipe_item,nama_satuan,`status`
	FROM defect INNER JOIN item_master ON defect.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where defect.`id_pemilik`=_id_pemilik;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_institusi` ()  BEGIN
	select * from institusi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_item` (IN `_id_pemilik` VARCHAR(100))  BEGIN
	select item_master.id_item ,nama_item, item_harga , link_photo , sat.nama_satuan as satuan ,item_master.deskripsi from item_master
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe` where item_master.`id_pemilik`=_id_pemilik order by nama_item asc;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_penerimaan` ()  BEGIN
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,1 as kode FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier` where `status`=1 or `status`=2 or `status`=3
union
	SELECT produksi.`id_petugas`,id_produksi,petugas.`nama` AS nama_petugas,"PT.Miconos" AS nama_suplier,tanggal_po,totalHarga,`status`,2 AS kode 
	FROM produksi 
	INNER JOIN petugas ON produksi.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON suplier.`id_suplier`=3 WHERE `status`=5 or `status`=6;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_pengeluaran` ()  BEGIN
	select penjualan.`id_so`,petugas.nama as nama_petugas,customer.`nama` as nama_customer,tanggal,total,kurir,`status`, 1 as kode
	from penjualan 
	inner join petugas on petugas.`id_petugas`=penjualan.`id_petugas`
	inner join customer on customer.`id_customer`=penjualan.`id_customer`
	where penjualan.`status`=1 or penjualan.`status`=2 or `status`=3
	union
	SELECT produksi.`id_produksi`,petugas.nama AS nama_petugas,"Produksi" as nama_customer,tanggal_po,totalHarga,NULL AS kurir,`status`,2 as kode
	FROM produksi
	INNER JOIN petugas ON petugas.`id_petugas`=produksi.`id_petugas`
	WHERE produksi.`status`=1 OR produksi.`status`=2 or `status`=3
	union 
	SELECT produkservice.`id_produkService`,petugas.nama AS nama_petugas,customer.`nama` as nama_customer,tanggal,total,"Service" AS kurir,produkservice.`status`,3 AS kode
	FROM produkservice
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	WHERE produkservice.`status`=1 or produkservice.`status`=2 or produkservice.`status`=3 ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_penjualan` ()  BEGIN
	
	select id_so,petugas.`nama` as nama_petugas,customer.`nama` as nama_customer,tanggal,total,kurir,`status` 
	from penjualan 
	inner join customer on penjualan.`id_customer`=customer.`id_customer` 
	inner join petugas on penjualan.`id_petugas`=petugas.`id_petugas` order by tanggal desc;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_petugas` ()  BEGIN
	SELECT * FROM petugas;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_produk` (`_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where item_master.`id_pemilik`=_id_pemilik
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC;
 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_produksi` ()  BEGIN
	select id_produksi,produksi.`id_petugas`,id_produk,nama_item,petugas.`nama` as nama_petugas,tanggal_po,totalHarga,`status`,jumlah_item from produksi 
	inner join petugas on petugas.`id_petugas`=produksi.`id_petugas`
	inner join item_master on item_master.`id_item`=produksi.`id_produk`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_purchasing` (`nama` VARCHAR(200))  BEGIN
	select purchasing.`id_petugas`,id_po,petugas.`nama` as nama_petugas,nama_suplier,tanggal_po,totalHarga,`status` from purchasing 
	inner join petugas on purchasing.`id_petugas`=petugas.`id_petugas` 
	inner join suplier on purchasing.`id_suplier`=suplier.`id_suplier` where id_petugas=nama order by tanggal_po desc;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_satuan` (`_id_pemilik` VARCHAR(100))  BEGIN
	select * from satuan where satuan.`id_pemilik`=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_service` ()  BEGIN
	select id_service,`subject`,keluhan,tgl_open,`status`,customer.`nama` as nama_customer,customer.`id_customer`,petugas.`nama` as nama_petugas,service.`id_petugas`,service.`id_customer` 
	from service inner join customer on service.`id_customer`=customer.`id_customer`
	inner join petugas on service.`id_petugas`=petugas.`id_petugas`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_suplier` (`_id_pemilik` VARCHAR(100))  BEGIN
	SELECT * FROM suplier where id_pemilik=_id_pemilik;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_tipeItem` ()  BEGIN
	select * from tipe_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_transaksiPerCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	select penjualan.`id_so`,tanggal,nama_item,jumlah,harga
	from detail_penjualan 
	inner join item_master on detail_penjualan.`id_item`=item_master.`id_item`
	INNER JOIN penjualan on detail_penjualan.`id_so`=penjualan.`id_so`
	  where id_customer=_id_customer;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_list_unsolvedService` ()  BEGIN
	select * from service where `status` <> "3";
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_login` (`_email` VARCHAR(200), `_passwd` VARCHAR(50))  BEGIN
	set@cek=(select 1 from petugas where email=_email and `password`=md5(_passwd));
	if @cek=1 then
		select 1 as A;
	else select 0 as A;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_customer` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer INNER JOIN institusi ON customer.`id_institut`=institusi.`id_institusi`
	 where customer.`id_pemilik`=_id_pemilik
	 ORDER BY nama ASC LIMIT _limit offset _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_pembelian` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,ongkir FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier`
	where purchasing.`id_pemilik`=_id_pemilik
	ORDER BY tanggal_po DESC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penerimaan` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN SELECT * FROM(
	SELECT purchasing.`id_petugas`,id_po,petugas.`nama` AS nama_petugas,nama_suplier,tanggal_po,totalHarga,`status`,1 AS kode,ongkir FROM purchasing 
	INNER JOIN petugas ON purchasing.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON purchasing.`id_suplier`=suplier.`id_suplier` WHERE (`status`=1 OR `status`=2 OR `status`=3) and purchasing.id_pemilik=_id_pemilik
UNION
	SELECT produksi.`id_petugas`,id_produksi,petugas.`nama` AS nama_petugas,"PT.Miconos" AS nama_suplier,tanggal_po,totalHarga,`status`,2 AS kode,"1"
	FROM produksi 
	INNER JOIN petugas ON produksi.`id_petugas`=petugas.`id_petugas` 
	INNER JOIN suplier ON suplier.`id_suplier`=3 WHERE (`status`=5 OR `status`=6) AND produksi.id_pemilik=_id_pemilik) AS tab ORDER BY tab.tanggal_po DESC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_pengeluaran` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	select * from (SELECT penjualan.`id_so`,petugas.nama AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,kurir,`status`, 1 AS kode, ongkir
	FROM penjualan 
	INNER JOIN petugas ON petugas.`id_petugas`=penjualan.`id_petugas`
	INNER JOIN customer ON customer.`id_customer`=penjualan.`id_customer`
	WHERE (penjualan.`status`=1 OR penjualan.`status`=2 OR `status`=3) and penjualan.`id_pemilik`=_id_pemilik
	UNION
	SELECT produksi.`id_produksi`,petugas.nama AS nama_petugas,"Produksi" AS nama_customer,tanggal_po,totalHarga,NULL AS kurir,`status`,2 AS kode, "1"
	FROM produksi
	INNER JOIN petugas ON petugas.`id_petugas`=produksi.`id_petugas`
	WHERE (produksi.`status`=1 OR produksi.`status`=2 OR `status`=3) and produksi.`id_pemilik`=_id_pemilik
	UNION 
	SELECT produkservice.`id_produkService`,petugas.nama AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,"Service" AS kurir,produkservice.`status`,3 AS kode, "1"
	FROM produkservice
	INNER JOIN service ON service.`id_service`=produkservice.`id_service`
	INNER JOIN customer ON customer.`id_customer`=service.`id_customer`
	INNER JOIN petugas ON petugas.`id_petugas`=produkservice.`teknisi`
	WHERE (produkservice.`status`=1 OR produkservice.`status`=2 OR produkservice.`status`=3) and produkservice.`id_pemilik`=_id_pemilik) as tab order by tab.tanggal desc LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_penjualan` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_so,petugas.`nama` AS nama_petugas,customer.`nama` AS nama_customer,tanggal,total,kurir,`status` 
	FROM penjualan 
	INNER JOIN customer ON penjualan.`id_customer`=customer.`id_customer` 
	INNER JOIN petugas ON penjualan.`id_petugas`=petugas.`id_petugas`
	where penjualan.`id_pemilik`=_id_pemilik
	ORDER BY tanggal DESC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_produk` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where penerimaan_barang.`id_pemilik`= `_id_pemilik`
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_produksi` (`_start` INT(11), `_limit` INT(11))  BEGIN
	SELECT id_produksi,produksi.`id_petugas`,id_produk,nama_item,petugas.`nama` AS nama_petugas,tanggal_po,totalHarga,`status`,jumlah_item FROM produksi 
	INNER JOIN petugas ON petugas.`id_petugas`=produksi.`id_petugas`
	INNER JOIN item_master ON item_master.`id_item`=produksi.`id_produk` order by produksi.`tanggal_po` desc limit _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pageList_supplier` (`_start` INT(11), `_limit` INT(11), `_id_pemilik` VARCHAR(100))  BEGIN
	select * from suplier where id_pemilik=_id_pemilik order by nama_suplier asc LIMIT _limit OFFSET _start;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pembelian` (`_id_transaksi` VARCHAR(100), `_email` VARCHAR(100), `_id_customer` INT(11), `_total` BIGINT(20), `_tgl` DATETIME)  BEGIN
	set@id_petugas=(select id_petugas from petugas where email=_email);
	insert into pembelian (id_transaksi,id_petugas,id_customer,tanggal,total) values(_id_transaksi,@id_petugas,_id_customer,_tgl,_total);
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengurangan_stok` (`_id_rec` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE)  BEGIN
	UPDATE gudang SET jumlah=(jumlah-_jumlah) WHERE id_rec=_id_rec AND id_item=_id_item;
	set@cek=(select jumlah from gudang WHERE id_rec=_id_rec AND id_item=_id_item);
	set@set=(SELECT `status` FROM gudang WHERE id_rec=_id_rec AND id_item=_id_item);
	if(@cek<1 and @set=0)then
		delete from gudang WHERE id_rec=_id_rec AND id_item=_id_item;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_returning_defect` (`_id_def` INT(11), `_id` VARCHAR(100), `_id_item` VARCHAR(100), `_jumlah` DOUBLE, `_harga` INT(11), `_id_issue` VARCHAR(100))  BEGIN
	update defect set `status`=1,tanggal=now() where id_def=_id_def and id_item=_id_item AND hargaSatuan=_harga;
	set@cekGuna=(select keterangan from defect where id_def=_id_def and id_item=_id_item and hargaSatuan=_harga);
	SET@po=(SELECT id_po FROM penerimaan_barang WHERE penerimaan_barang.`id_rec`=_id);
	if(@cekGuna=1) then 
		update gudang set jumlah=jumlah+_jumlah, `status`=0 where id_rec=_id and id_item=_id_item AND hargaSatuan=_harga;
		UPDATE detail_penerimaan SET jumlah=jumlah+_jumlah, `status`=0 WHERE id_rec=_id AND id_item=_id_item AND hargaSatuan=_harga;
		update penerimaan_barang set `status`=3 where id_rec=_id;
		
		UPDATE purchasing SET `status`=3 where id_po=@po;
		update defect set `status`=0 where id_def=_id_def;
	elseif(@cekGuna=2) then
		set@rec=(select id_rec from detail_pengeluaran where id_issue=_id_issue and id_item=_id_item);
		UPDATE gudang SET jumlah=jumlah+_jumlah, `status`=0 WHERE id_rec=@rec AND id_item=_id_item AND hargaSatuan=_harga;
		UPDATE defect SET `status`=0 WHERE id_def=_id_def;
	ELSEIF(@cekGuna=3) THEN
		SET@rec=(SELECT id_rec FROM detail_pengeluaran WHERE id_issue=_id_issue AND id_item=_id_item);
		UPDATE gudang SET jumlah=jumlah+_jumlah, `status`=0 WHERE id_rec=@rec AND id_item=_id_item AND hargaSatuan=_harga;
		UPDATE defect SET `status`=0 WHERE id_def=_id_def;
	ELSEIF(@cekGuna=4) THEN
		SET@rec=(SELECT id_rec FROM detail_pengeluaran WHERE id_issue=_id_issue AND id_item=_id_item);
		UPDATE gudang SET jumlah=jumlah+_jumlah, `status`=0 WHERE id_rec=@rec AND id_item=_id_item AND hargaSatuan=_harga;
		UPDATE defect SET `status`=0 WHERE id_def=_id_def;
	end if;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianListProduk` (`_id_item` VARCHAR(100))  BEGIN
	SELECT nama_suplier,detail_suplier.`id_suplier`,nama_item,detail_suplier.`id_item`,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,item_master.deskripsi 
	FROM penerimaan_barang 
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	inner join suplier on suplier.`id_suplier`=detail_suplier.`id_suplier`
	WHERE detail_suplier.`id_item`=_id_item GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianPembelian` (IN `_id_pur` VARCHAR(100))  BEGIN
	select DISTINCT  detail_purchasing.`id_item`,nama_item,nama_satuan,nama_tipe_item,detail_purchasing.jumlah,tab.`jumlah`as jml_keluar,detail_purchasing.hargaSatuan 
	from detail_purchasing inner join item_master on item_master.`id_item`=detail_purchasing.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	left join (select penerimaan_barang.`id_po`,jumlah,id_item from detail_penerimaan inner join penerimaan_barang on penerimaan_barang.`id_rec`=detail_penerimaan.`id_rec`
	where penerimaan_barang.`id_po`=_id_pur) as tab on tab.id_po=detail_purchasing.`id_purchasing` AND tab.id_item=detail_purchasing.`id_item`
	where detail_purchasing.`id_purchasing`=_id_pur ;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianPenjualan` (`_id_tran` VARCHAR(100))  BEGIN
	select detail_penjualan.`id_item`,nama_item,nama_satuan,nama_tipe_item,detail_penjualan.jumlah,tab.`jumlah` as keluar,harga from detail_penjualan
	inner join item_master on item_master.`id_item`=detail_penjualan.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	LEFT JOIN (SELECT pengeluaran_barang.`id_so`,jumlah,id_item FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	WHERE pengeluaran_barang.`id_so`=_id_tran) AS tab ON tab.id_so=detail_penjualan.`id_so` AND tab.id_item=detail_penjualan.`id_item`
	where detail_penjualan.`id_so`=_id_tran;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincianProduksi` (`_id_tran` VARCHAR(100))  BEGIN
	SELECT detail_produksi.id_produksi,detail_produksi.`id_item`,nama_item,hargaSatuan,nama_satuan,nama_tipe_item,detail_produksi.`jumlah`,tab.jumlah AS keluar
	FROM detail_produksi
	INNER JOIN item_master ON item_master.`id_item`=detail_produksi.`id_item`
	INNER JOIN satuan sat ON sat.`id_satuan`=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	LEFT JOIN (SELECT pengeluaran_barang.`id_produksi`,jumlah,id_item FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	WHERE pengeluaran_barang.`id_produksi`=_id_tran) AS tab ON tab.id_produksi=detail_produksi.`id_produksi` AND tab.id_item=detail_produksi.`id_item`
	WHERE detail_produksi.id_produksi=_id_tran; 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincian_bom` (`_id_produk` VARCHAR(100))  BEGIN
	select bom.`id_item`,nama_item,jumlah,nama_satuan,nama_tipe_item,link_photo,item_master.deskripsi from bom 
	inner join item_master on item_master.`id_item`=bom.`id_item`
	inner join satuan on satuan.`id_satuan`=item_master.`satuan`
	left join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where bom.`id_produk`=_id_produk;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rincian_viewSO` (`_id_so` VARCHAR(100))  BEGIN
	select customer.`id_customer`,customer.`nama`as nama_customer,petugas.`nama`as nama_petugas,penjualan.`tanggal`,kurir,penjualan.`total`,penjualan.`ongkir` from penjualan
	inner join petugas on petugas.`id_petugas`=penjualan.`id_petugas`
	inner join customer on customer.`id_customer`=penjualan.`id_customer`
	where penjualan.`id_so`=_id_so;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_stok_gudang` (`_id_item` VARCHAR(100))  BEGIN
	SELECT "Masuk" AS asal,SUM(detail_penerimaan.`jumlah`)AS jumlah,nama_item FROM detail_penerimaan 
	INNER JOIN penerimaan_barang ON penerimaan_barang.`id_rec`=detail_penerimaan.`id_rec`
	inner join item_master on item_master.`id_item`=detail_penerimaan.`id_item`
	WHERE (penerimaan_barang.`status`=3 AND detail_penerimaan.`id_item`=_id_item) 
	or (penerimaan_barang.`status`=2 AND detail_penerimaan.`id_item`=_id_item)
	UNION 
	SELECT "Gudang" AS asal, SUM(gudang.`jumlah`) AS jumlah,nama_item FROM gudang
	inner join item_master on item_master.`id_item`=gudang.`id_item`
	WHERE gudang.`id_item`=_id_item
	UNION
	SELECT "Keluar" AS asal,SUM(detail_pengeluaran.`jumlah`) AS jumlah,nama_item FROM detail_pengeluaran 
	INNER JOIN pengeluaran_barang ON pengeluaran_barang.`id_issue`=detail_pengeluaran.`id_issue`
	inner join item_master on item_master.`id_item`=detail_pengeluaran.`id_item`
	WHERE pengeluaran_barang.`status`=3 AND detail_pengeluaran.`id_item`=_id_item
	UNION
	SELECT "Rusak" AS asal, SUM(defect.`jumlah`)AS jumlah,nama_item FROM defect 
	inner join item_master on item_master.`id_item`=defect.`id_item`
	WHERE defect.`id_item`=_id_item AND defect.`status`>=1;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ubahPassword` (`_id_petugas` VARCHAR(100), `_pwdLama` VARCHAR(200), `_pwdBaru` VARCHAR(200))  BEGIN
	set@cek=(select id_petugas from petugas where id_petugas=_id_petugas and `password`=md5(_pwdLama));
	if (@cek) then
		update petugas set `password`=md5(_pwdBaru) where id_petugas=_id_petugas;
		select 1 as cek;
	else 
		select -1 as cek;
	end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateCustomer` (`_institusi` VARCHAR(100), `_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` CHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(200))  BEGIN
    
	 
	UPDATE customer SET id_institut=_institusi, nama=_nama, jenkel=_jenkel,alamat=_alamat, hp=_hp, email=_email,jabatan=_jabatan WHERE id_customer=_ktp;
	SELECT 1 AS cek;
	 
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateInstitusi` (`_id_institusi` VARCHAR(200), `_nama` VARCHAR(300), `_alamat` VARCHAR(500), `_telephone` VARCHAR(30), `_email` VARCHAR(200))  BEGIN
	update institusi set nama_institusi=_nama,alamat_institusi=_alamat,telephone_institusi=_telephone,email=_email
	where id_institusi=_id_institusi;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateItem` (`_id_item` VARCHAR(100), `_nama_item` VARCHAR(200), `_tipe` VARCHAR(50), `_satuan` VARCHAR(50), `_deskripsi` TEXT)  BEGIN
	update item_master set nama_item=_nama_item,tipe=_tipe,satuan=_satuan,deskripsi=_deskripsi where id_item=_id_item;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updatePetugas` (`_ktp` VARCHAR(100), `_nama` VARCHAR(200), `_jenkel` CHAR(3), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_jabatan` VARCHAR(200), `_passwd` VARCHAR(200))  BEGIN
    set@passwd=(select `password` from petugas where id_petugas=_ktp);
	if@passwd=(md5(_passwd)) then 
	 
	UPDATE petugas SET nama=_nama, jenkel=_jenkel,alamat=_alamat, hp=_hp, email=_email,jabatan=_jabatan WHERE id_petugas=_ktp;
	select 1 as cek;
	 else select -1 as cek; end if;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateSatuan` (`_id` INT(11), `_nama_satuan` VARCHAR(50), `_kelompok` VARCHAR(50), `_deskripsi` VARCHAR(100))  BEGIN
	update satuan set nama_satuan=_nama_satuan,kelompok_satuan=_kelompok,deskripsi_satuan=_deskripsi where id_satuan=_id;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateService` (`_id_serv` INT(11), `_id_customer` VARCHAR(100), `_subject` VARCHAR(500), `_keluhan` TEXT, `_status` INT(11))  BEGIN
	update service set id_customer=_id_customer,`subject`=_subject,keluhan=_keluhan,`status`=_status where id_service=_id_serv;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_pembelian` (`_id_pur` VARCHAR(100), `_status` INT(11))  BEGIN
	update purchasing set `status`=_status where id_po=_id_pur;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_penjualan` (`_id_so` VARCHAR(100), `_status` INT(11))  BEGIN
	update penjualan set `status`=_status where id_so=_id_so;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateStatus_produksi` (`_id_produksi` VARCHAR(100), `_status` INT(11))  BEGIN
	update produksi set `status`=_status where id_produksi=_id_produksi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateSuplier` (`_id_suplier` INT(11), `_nama` VARCHAR(200), `_alamat` VARCHAR(500), `_hp` VARCHAR(20), `_email` VARCHAR(200), `_deskripsi` TEXT)  BEGIN
	update suplier set nama_suplier=_nama,alamat=_alamat,hp=_hp,email=_email,deskripsi=_deskripsi where id_suplier=_id_suplier;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateTipe` (`_id` INT(11), `_nama_tipe` VARCHAR(100), `_deskripsi` TEXT)  BEGIN
	update tipe_item set nama_tipe_item=_nama_tipe,deskripsi=_deskripsi where id_tipe_item=_id;
	select 1 as cek;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_defect` (`_id_def` INT(11), `_id_item` VARCHAR(100), `_harga` INT(11))  BEGIN
	update defect set `status`=2 where id_def=_id_def and id_item=_id_item and hargaSatuan=_harga;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_privilege` (`_id_petugas` VARCHAR(200), `_privilege` INT(11))  BEGIN
	update petugas set privilege=_privilege where id_petugas=_id_petugas;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_uploadGmbrPetugas` (`_id_petugas` VARCHAR(200), `_nama_file` VARCHAR(200))  BEGIN
	update petugas set photo_link=_nama_file where id_petugas=_id_petugas;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_uploadGmbrSuplier` (`_id_suplier` INT(11), `_photo_link` VARCHAR(200))  BEGIN
	update suplier set photo_link=_photo_link where id_suplier=_id_suplier;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewCustomer_perInstitusi` (`_id_institusi` VARCHAR(200))  BEGIN
	select * from customer where customer.`id_institut`=_id_institusi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewPO` (`_id_po` VARCHAR(200))  BEGIN
	select id_purchasing,nama_item,item_master.`id_item`,jumlah,hargaSatuan,nama_tipe_item,nama_satuan,id_suplier,purchasing.id_petugas,totalHarga,1 as kode
	from detail_purchasing inner join purchasing on purchasing.`id_po`=detail_purchasing.`id_purchasing`
	inner join item_master on detail_purchasing.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where id_purchasing=_id_po;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewPRO` (`_id_pro` VARCHAR(100))  BEGIN
	SELECT detail_produksi.`id_suplier`,suplier.`nama_suplier`,detail_produksi.`id_item`,nama_item,nama_satuan,nama_tipe_item,hargaSatuan as harga,jumlah,2 as kode 
	FROM detail_produksi
	INNER JOIN suplier ON suplier.`id_suplier`=detail_produksi.`id_suplier`
	INNER JOIN item_master ON item_master.`id_item`=detail_produksi.`id_item`
	INNER JOIN satuan sat ON sat.`id_satuan`=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	where detail_produksi.`id_produksi`=_id_pro;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewPROIN` (`_id_tran` VARCHAR(100))  BEGIN
	select suplier.`id_suplier`,nama_suplier,produksi.`id_produk` as id_item,nama_item,nama_satuan,nama_tipe_item,(totalHarga/jumlah_item) as hargaSatuan,jumlah_item as jumlah,id_petugas,2 as kode from produksi
	inner join suplier on suplier.`id_suplier`=3
	inner join item_master on item_master.`id_item`=produksi.`id_produk`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where produksi.`id_produksi`=_id_tran;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewSER` (`_id_tran` VARCHAR(100))  BEGIN
	select service.`id_customer`,detail_produkservice.`id_suplier`,nama_suplier,detail_produkservice.`id_produkService`,petugas.`nama`as nama_petugas,nama_item,detail_produkservice.`id_item`,nama_satuan,nama_tipe_item,
	tanggal,harga,produkservice.`status`,jumlah,3 as kode
	from detail_produkservice
	inner join produkservice on produkservice.`id_produkService`=produkservice.`id_produkService`
	inner join service on service.`id_service`=produkservice.`id_service`
	inner join petugas on petugas.`id_petugas`=produkservice.`teknisi`
	inner join item_master on item_master.`id_item`=detail_produkservice.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	inner join suplier on suplier.`id_suplier`=detail_produkservice.`id_suplier`
	where detail_produkservice.`id_produkService`=_id_tran group by detail_produkservice.`id_item`,detail_produkservice.`harga`,detail_produkservice.`id_suplier`;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_viewSO` (`_id_so` VARCHAR(100))  BEGIN
	select detail_suplier.`harga` as harga_beli, nama_suplier,penjualan.`id_so`,nama_item,item_master.`id_item`,jumlah,nama_satuan,nama_tipe_item,detail_penjualan.harga,jumlah,detail_penjualan.`id_suplier`,1 as kode
	from detail_penjualan 
	inner join penjualan on penjualan.`id_so`=detail_penjualan.`id_so`
	inner join suplier on suplier.`id_suplier`=detail_penjualan.`id_suplier`
	inner join item_master on item_master.`id_item`=detail_penjualan.`id_item`
	INNER JOIN detail_suplier ON suplier.`id_suplier`=detail_suplier.`id_suplier` and detail_suplier.`id_item`=item_master.`id_item`
	inner join satuan sat on sat.id_satuan=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	where penjualan.`id_so`=_id_so;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perCustomer` (`_id_customer` VARCHAR(100))  BEGIN
	SELECT institusi.`nama_institusi`,id_customer,id_institusi,nama,jenkel,alamat,hp,customer.email,tgl,photo_link,jabatan
	 FROM customer inner join institusi on customer.`id_institut`=institusi.`id_institusi` WHERE id_customer=_id_customer;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perInstitusi` (`_id_institusi` VARCHAR(200))  BEGIN
	select * from institusi where id_institusi=_id_institusi;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perItem` (`_id_item` VARCHAR(100))  BEGIN
	SELECT item_master.`id_item`,nama_item,item_master.deskripsi,link_photo,
	sat.nama_satuan,nama_tipe_item,item_master.`satuan`,item_master.`tipe`
	FROM item_master INNER JOIN satuan ON satuan.`id_satuan`=item_master.`satuan`
	inner join satuan sat on sat.`id_satuan`=item_master.`satuan`
	inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE id_item=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perPetugas` (`_id_pet` VARCHAR(200))  BEGIN
	select * from petugas where id_petugas=_id_pet;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perProduk` (`_id_item` VARCHAR(100))  BEGIN
	select * from item_master inner join satuan on satuan.`id_satuan`=item_master.`satuan` where id_item=_id_item;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perProdukServ` (`_id_service` INT(11))  BEGIN
	SELECT detail_produkservice.`id_produkService`,produkservice.`id_service`,detail_produkservice.`id_item`,
	nama_item,nama_satuan,nama_tipe_item,jumlah,harga,tanggal,link_photo FROM detail_produkservice
INNER JOIN produkservice ON produkservice.`id_produkService`=detail_produkservice.`id_produkService`
INNER JOIN item_master ON item_master.`id_item`=detail_produkservice.`id_item`
INNER JOIN satuan sat ON sat.`id_satuan`=item_master.`satuan`
INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
WHERE produkservice.`id_service`=_id_service;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perService` (`_id_service` INT(11))  BEGIN
	set@solving=(select id_service from service where id_service=_id_service and `status`=3);
	
	select service.id_service,petugas.`id_petugas`,service.`id_customer`,petugas.`nama` AS nama_petugas, customer.`nama` AS nama_customer,`subject`,keluhan,tgl_open,`status`
	from service
	INNER JOIN  customer ON service.`id_customer`=customer.`id_customer`
	INNER JOIN petugas ON service.`id_petugas`=petugas.`id_petugas` WHERE service.`id_service`=_id_service;
	
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perSolving` (`_id_service` INT(11))  BEGIN
	select id_solving,solving.`id_service`,teknisi,petugas.`nama`as nama_petugas,penyelesaian,tgl_solved,service.`status`
	from solving 
	inner join petugas on petugas.`id_petugas`=solving.`teknisi`
	inner join service on service.`id_service`=solving.`id_service`
	where solving.id_service=_id_service;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_view_perSuplier` (`_id_suplier` INT(11))  BEGIN
	SELECT * FROM suplier WHERE id_suplier=_id_suplier;
    END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `fc_tambah` (`_id_po` INT) RETURNS INT(11) BEGIN
	declare hasil int;
	set hasil = ( SELECT SUM(jumlah) FROM gudang WHERE id_purchasing=_id_po);
	return hasil;
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `id_so` varchar(100) NOT NULL,
  `id_item` varchar(100) NOT NULL,
  `jumlah` double NOT NULL,
  `harga` int(11) NOT NULL,
  `id_suplier` int(11) DEFAULT NULL,
  `jumlah_keluar` double DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_penjualan`
--

INSERT INTO `detail_penjualan` (`id_so`, `id_item`, `jumlah`, `harga`, `id_suplier`, `jumlah_keluar`, `id_pemilik`) VALUES
('SO58cf5f7610ebc', 'Barang01', 20, 500, 1, NULL, NULL),
('SO58cf6326f3332', 'Barang03', 120000, 30000, 1, NULL, NULL),
('SO58cf63791bf88', 'Barang03', 500, 30000, 1, NULL, NULL),
('SO58cf63a3ce429', 'Barang03', 500, 30000, 1, NULL, NULL),
('SO58cf63af6f5b9', 'Barang03', 200, 28500, 5, NULL, NULL),
('SO58cf63b3627b1', 'Barang03', 36000, 29500, 1, NULL, NULL),
('SO58cf63d1e2863', 'Barang03', 100, 26700, 1, NULL, NULL),
('SO58cf653af3952', 'Barang03', 125000, 2800, 1, NULL, NULL),
('SO58cf6567e2f96', 'Barang03', 200, 32000, 2, NULL, NULL),
('SO58cf659693bcc', 'Barang03', 150, 30500, 2, NULL, NULL),
('SO58cf65da3aa29', 'Barang02', 450, 25000, 4, NULL, NULL),
('SO58cf661c1d15e', 'Barang03', 180, 37200, 1, NULL, NULL),
('SO58cf65fe1de33', 'Barang03', 10000, 39000, 3, NULL, NULL),
('SO58cf66455ee30', 'Barang03', 36000, 29500, 1, NULL, NULL),
('SO58cf660445b02', 'Barang03', 90000, 31000, 1, NULL, NULL),
('SO58cf6666ea851', 'Barang03', 60000, 31000, 1, NULL, NULL),
('SO58cf6699a0152', 'Barang03', 175200, 30000, 5, NULL, NULL),
('SO58cf66a4a112f', 'Barang03', 36000, 29500, 1, NULL, NULL),
('SO58cf66d7a1756', 'Barang03', 150, 37200, 1, NULL, NULL),
('SO58cf672dad74a', 'Barang03', 5900, 31000, 1, NULL, NULL),
('SO58cf66f017339', 'Barang02', 500, 25000, 4, NULL, NULL),
('SO58cf6745ec774', 'Barang03', 9000, 30000, 1, NULL, NULL),
('SO58cf675d80ec8', 'Barang03', 250, 37200, 1, NULL, NULL),
('SO58cf67a6cc4df', 'Barang02', 490, 25000, 4, NULL, NULL),
('SO58cf67e83f654', 'Barang03', 51000, 26000, 1, NULL, NULL),
('SO58cf67e83f654', 'Barang03', 9000, 30000, 1, NULL, NULL),
('SO58cf6851dfbf6', 'Barang02', 550, 25000, 4, NULL, NULL),
('SO58cf6837359e1', 'Barang01', 2500, 1000, 2, NULL, NULL),
('SO58cf686cdd72f', 'Barang03', 100, 39000, 1, NULL, NULL),
('SO58cf6860b2251', 'Barang03', 44000, 39500, 3, NULL, NULL),
('SO58cf68ac855c3', 'Barang03', 1000, 33500, 1, NULL, NULL),
('SO58cf68d7228d4', 'Barang03', 1000, 35000, 1, NULL, NULL),
('SO58cf68ea0b43d', 'Barang02', 460, 25000, 4, NULL, NULL),
('SO58cf68fe4d737', 'Barang03', 5000, 35000, 1, NULL, NULL),
('SO58cf690c94ed3', 'Barang03', 17000, 31200, 1, NULL, NULL),
('SO58cf6925e2b9e', 'Barang03', 9000, 30000, 1, NULL, NULL),
('SO58cf69706b6a8', 'Barang03', 300, 38000, 1, NULL, NULL),
('SO58cf69865839c', 'Barang03', 6000, 35000, 1, NULL, NULL),
('SO58cf69cbecf5b', 'Barang03', 53100, 31000, 1, NULL, NULL),
('SO58cf69e0ecfcd', 'Barang03', 18000, 30000, 1, NULL, NULL),
('SO58cf69e7d6778', 'Barang03', 1200, 33500, 1, NULL, NULL),
('SO58cf69ce8da95', 'Barang03', 15000, 29900, 1, NULL, NULL),
('SO58cf6a0ff2653', 'Barang02', 600, 25000, 4, NULL, NULL),
('SO58cf6a131fd08', 'Barang03', 27000, 35000, 5, NULL, NULL),
('SO58cf6a2c37291', 'Barang03', 10000, 34000, 1, NULL, NULL),
('SO58cf6a3f860da', 'Barang02', 175000, 27000, 4, NULL, NULL),
('SO58cf6a5b733c3', 'Barang03', 18000, 30000, 1, NULL, NULL),
('SO58cf6ab4f32f4', 'Barang03', 120000, 30000, 1, NULL, NULL),
('SO58cf6abbc2d94', 'Barang03', 12000, 30900, 1, NULL, NULL),
('SO58cf6aefc2de7', 'Barang01', 10000, 1000, 2, NULL, NULL),
('SO58cf6aeea0a63', 'Barang02', 530, 25000, 4, NULL, NULL),
('SO58cf6b0e30566', 'Barang03', 500, 33500, 1, NULL, NULL),
('SO58cf6b1a16d5e', 'Barang03', 17000, 28900, 1, NULL, NULL),
('SO58cf6b1d5a2ae', 'Barang01', 12000, 1000, 2, NULL, NULL),
('SO58cf6b3528314', 'Barang03', 50, 39000, 1, NULL, NULL),
('SO58cf6b50704cf', 'Barang03', 27000, 36000, 5, NULL, NULL),
('SO58cf6b4715237', 'Barang01', 2000, 1000, 2, NULL, NULL),
('SO58cf6b68b8bf3', 'Barang03', 180, 37000, 1, NULL, NULL),
('SO58cf6b7ec19aa', 'Barang03', 5000, 35000, 1, NULL, NULL),
('SO58cf6b8291e0c', 'Barang03', 20500, 31800, 1, NULL, NULL),
('SO58cf6b849d74a', 'Barang01', 10000, 1000, 2, NULL, NULL),
('SO58cf6b9e6e186', 'Barang03', 15000, 30000, 1, NULL, NULL),
('SO58cf6b7c442c1', 'Barang03', 120000, 32000, 1, NULL, NULL),
('SO58cf6bbd1b9f7', 'Barang01', 10000, 1000, 2, NULL, NULL),
('SO58cf6bb60ac68', 'Barang03', 1300, 33500, 1, NULL, NULL),
('SO58cf6bd1405d9', 'Barang03', 135, 37750, 1, NULL, NULL),
('SO58cf6c11c25ea', 'Barang03', 7000, 35000, 1, NULL, NULL),
('SO58cf6c2663a1c', 'Barang01', 10000, 1000, 2, NULL, NULL),
('SO58cf6c369edd5', 'Barang03', 21000, 29900, 1, NULL, NULL),
('SO58cf6c4dc7f07', 'Barang03', 80, 38900, 1, NULL, NULL),
('SO58cf6c4fed599', 'Barang03', 18000, 32000, 1, NULL, NULL),
('SO58cf6c61085c3', 'Barang03', 130000, 26500, 1, NULL, NULL),
('SO58cf6c77a5ff1', 'Barang03', 175200, 25000, 1, NULL, NULL),
('SO58cf6c9e3fc40', 'Barang02', 400, 25000, 4, NULL, NULL),
('SO58cf6cb4b0405', 'Barang03', 2000, 33500, 1, NULL, NULL),
('SO58cf6cb90ef4f', 'Barang01', 400000, 600, 4, NULL, NULL),
('SO58cf6cc867cb9', 'Barang03', 18000, 32000, 1, NULL, NULL),
('SO58cf6cbe95be3', 'Barang03', 30000, 39000, 4, NULL, NULL),
('SO58cf6cdfcaeaf', 'Barang03', 6000, 35000, 1, NULL, NULL),
('SO58cf6d1824187', 'Barang03', 500, 35000, 1, NULL, NULL),
('SO58cf6d2a9edb3', 'Barang01', 420, 1775, 4, NULL, NULL),
('SO58cf6d4320831', 'Barang02', 1000, 25000, 4, NULL, NULL),
('SO58cf6d57e2f82', 'Barang03', 500, 33500, 1, NULL, NULL),
('SO58cf6d3fc8bbe', 'Barang03', 30200, 32000, 1, NULL, NULL),
('SO58cf6d291c94e', 'Barang03', 4000, 35000, 1, NULL, NULL),
('SO58cf6d89e66be', 'Barang03', 24000, 40000, 4, NULL, NULL),
('SO58cf6d97a41ea', 'Barang03', 280, 37800, 1, NULL, NULL),
('SO58cf6dcecea9d', 'Barang03', 100000, 40000, 1, NULL, NULL),
('SO58cf6e0082e35', 'Barang03', 3500, 33500, 1, NULL, NULL),
('SO58cf6e37248d2', 'Barang03', 18000, 32000, 1, NULL, NULL),
('SO58cf6e4c198e1', 'Barang03', 10000, 31000, 1, NULL, NULL),
('SO58cf6e5977ee0', 'Barang03', 1000, 33500, 1, NULL, NULL),
('SO58cf6e727d961', 'Barang01', 420, 1775, 4, NULL, NULL),
('SO58cf6e74bab07', 'Barang03', 54000, 29500, 1, NULL, NULL),
('SO58cf6ea7aaab3', 'Barang03', 100000, 40000, 1, NULL, NULL),
('SO58cf6ecbcfe00', 'Barang03', 1000, 33500, 1, NULL, NULL),
('SO58cf6ec367f1f', 'Barang03', 35000, 38500, 2, NULL, NULL),
('SO58cf6eed9324f', 'Barang03', 19800, 30800, 1, NULL, NULL),
('SO58cf6ef641a2f', 'Barang02', 20, 24000, 4, NULL, NULL),
('SO58cf6f283a1ac', 'Barang03', 80, 38900, 1, NULL, NULL),
('SO58cf6f4793b34', 'Barang03', 18000, 30000, 1, NULL, NULL),
('SO58cf6edf8d144', 'Barang01', 480, 1000, 4, NULL, NULL),
('SO58cf6f44ab378', 'Barang03', 19000, 37500, 2, NULL, NULL),
('SO58cf6f5ddab2c', 'Barang03', 3500, 35000, 1, NULL, NULL),
('SO58cf6f7d631ec', 'Barang03', 54000, 29500, 1, NULL, NULL),
('SO58cf6f892fa62', 'Barang01', 15000, 1000, 2, NULL, NULL),
('SO58cf6f98de429', 'Barang03', 30000, 33500, 1, NULL, NULL),
('SO58cf6fb996b2f', 'Barang03', 120, 38000, 1, NULL, NULL),
('SO58cf6fbe1bb3b', 'Barang01', 350000, 600, 4, NULL, NULL),
('SO58cf6fd82c92c', 'Barang03', 140000, 35000, 5, NULL, NULL),
('SO58cf6fe63310c', 'Barang03', 95, 38900, 1, NULL, NULL),
('SO58cf6ffe3dd7b', 'Barang01', 15000, 1000, 2, NULL, NULL),
('SO58cf7002d61b7', 'Barang01', 480, 1000, 4, NULL, NULL),
('SO58cf700e112ae', 'Barang02', 17000, 24000, 3, NULL, NULL),
('SO58cf70671eff9', 'Barang03', 3000, 33500, 1, NULL, NULL),
('SO58cf7080cbe9c', 'Barang03', 100000, 28000, 1, NULL, NULL),
('SO58cf70808955c', 'Barang03', 37800, 32500, 1, NULL, NULL),
('SO58cf7090ce31f', 'Barang02', 1500, 25000, 4, NULL, NULL),
('SO58cf709e72fa6', 'Barang03', 216000, 30000, 1, NULL, NULL),
('SO58cf70a71db46', 'Barang03', 2500, 35000, 1, NULL, NULL),
('SO58cf70ab93661', 'Barang03', 1350, 33500, 1, NULL, NULL),
('SO58cf70e9564e0', 'Barang03', 12500, 28000, 1, NULL, NULL),
('SO58cf710083291', 'Barang02', 520, 25000, 4, NULL, NULL),
('SO58cf70f2bb986', 'Barang02', 5000, 24000, 1, NULL, NULL),
('SO58cf71121ecd5', 'Barang01', 480, 1000, 4, NULL, NULL),
('SO58cf7100dc54f', 'Barang03', 5000, 33500, 1, NULL, NULL),
('SO58cf713440b89', 'Barang03', 19000, 35000, 1, NULL, NULL),
('SO58cf715b21cf7', 'Barang03', 10000, 35000, 1, NULL, NULL),
('SO58cf717c07793', 'Barang03', 10000, 35000, 1, NULL, NULL),
('SO58cf718917668', 'Barang01', 480, 900, 4, NULL, NULL),
('SO58cf71bc7f83e', 'Barang02', 17000, 25000, 3, NULL, NULL),
('SO58cf71d725af3', 'Barang03', 430, 35200, 1, NULL, NULL),
('SO58cf71e21f18d', 'Barang03', 5000, 33500, 1, NULL, NULL),
('SO58cf71cf758d1', 'Barang03', 5000, 36000, 1, NULL, NULL),
('SO58cf71ee384cb', 'Barang02', 450, 25000, 4, NULL, NULL),
('SO58cf72703d933', 'Barang03', 10000, 33500, 1, NULL, NULL),
('SO58cf72ab3c3d5', 'Barang03', 25, 39000, 5, NULL, NULL),
('SO58cf7354203ea', 'Barang03', 30, 39000, 5, NULL, NULL),
('SO58cf737ec6a37', 'Barang01', 420, 1775, 4, NULL, NULL),
('SO58cf7384d90fc', 'Barang02', 170000, 24000, 3, NULL, NULL),
('SO58cf73b061bfe', 'Barang03', 7000, 33500, 1, NULL, NULL),
('SO58cf741814202', 'Barang03', 5000, 33500, 1, NULL, NULL),
('SO58cf7434a4643', 'Barang01', 420, 1775, 4, NULL, NULL),
('SO58cf748e10ed6', 'Barang03', 2500, 33500, 1, NULL, NULL),
('SO58cf74b0d7436', 'Barang03', 25, 39000, 5, NULL, NULL),
('SO58cf74d66e8d8', 'Barang03', 5500, 33500, 1, NULL, NULL),
('SO58cf74e9aae97', 'Barang03', 120, 37800, 1, NULL, NULL),
('SO58cf74f7a30cd', 'Barang02', 3000, 24000, 1, NULL, NULL),
('SO58cf75008ecbe', 'Barang03', 35000, 37500, 1, NULL, NULL),
('SO58cf751d41ecf', 'Barang03', 250, 37600, 1, NULL, NULL),
('SO58cf7529244b7', 'Barang03', 10000, 33500, 1, NULL, NULL),
('SO58cf753a1b75b', 'Barang02', 20, 24000, 5, NULL, NULL),
('SO58cf75bd5569b', 'Barang03', 10000, 33500, 1, NULL, NULL),
('SO58cf75e204d0d', 'Barang03', 350, 35750, 1, NULL, NULL),
('SO58cf760c0c53d', 'Barang03', 280, 35750, 5, NULL, NULL),
('SO58cf75f1b08b5', 'Barang01', 420, 1775, 4, NULL, NULL),
('SO58cf767035389', 'Barang02', 600, 25000, 4, NULL, NULL),
('SO58cf769c41f83', 'Barang03', 10000, 30000, 5, NULL, NULL),
('SO58cf76bfc26a3', 'Barang03', 25000, 40500, 3, NULL, NULL),
('SO58cf76f5942a6', 'Barang03', 1000, 34000, 5, NULL, NULL),
('SO58cf76fb515af', 'Barang03', 10000, 33500, 1, NULL, NULL),
('SO58cf772405cac', 'Barang02', 450, 25000, 4, NULL, NULL),
('SO58cf7730d6374', 'Barang03', 500, 36000, 5, NULL, NULL),
('SO58cf7761dbf9b', 'Barang03', 10000, 33500, 1, NULL, NULL),
('SO58cf77c757da3', 'Barang02', 30, 24000, 5, NULL, NULL),
('SO58cf77cb2c59b', 'Barang03', 10000, 33500, 1, NULL, NULL),
('SO58cf77b163a87', 'Barang03', 10000, 35000, 1, NULL, NULL),
('SO58cf77c789ca0', 'Barang03', 29000, 40000, 3, NULL, NULL),
('SO58cf77e1eb4b8', 'Barang01', 480, 1000, 4, NULL, NULL),
('SO58cf77efe71fc', 'Barang01', 480, 1000, 4, NULL, NULL),
('SO58cf7803a64ef', 'Barang03', 10000, 35000, 1, NULL, NULL),
('SO58cf7813a5150', 'Barang03', 10000, 33500, 1, NULL, NULL),
('SO58cf780b630f2', 'Barang02', 600, 25000, 4, NULL, NULL),
('SO58cf78379e151', 'Barang03', 5000, 35000, 1, NULL, NULL),
('SO58cf78e842686', 'Barang02', 1990, 24000, 1, NULL, NULL),
('SO58cf79a1dd650', 'Barang01', 13000, 1000, 2, NULL, NULL),
('SO58cf79ae260a7', 'Barang03', 2000, 33600, 1, NULL, NULL),
('SO58cf79ff18e1d', 'Barang03', 640, 35900, 1, NULL, NULL),
('SO58cf7a2e478fc', 'Barang03', 140, 37800, 5, NULL, NULL),
('SO58cf7a2d6df8c', 'Barang01', 13000, 1000, 2, NULL, NULL),
('SO58cf7a7415df4', 'Barang01', 13000, 1000, 2, NULL, NULL),
('SO58cf7af14c976', 'Barang01', 13000, 1000, 2, NULL, NULL),
('SO58cf7bccf1a54', 'Barang03', 100000, 40000, 1, NULL, NULL),
('SO58cf7bca210bf', 'Barang01', 2500, 900, 4, NULL, NULL),
('SO58cf7c0b48952', 'Barang03', 100000, 40000, 1, NULL, NULL),
('SO58cf7c023500f', 'Barang01', 25000, 1000, 2, NULL, NULL),
('SO58cf7b9fb5e94', 'Barang03', 18000, 38000, 5, NULL, NULL),
('SO58cf7c38a64ba', 'Barang01', 25000, 1000, 2, NULL, NULL),
('SO58cf7c593b6e0', 'Barang03', 60, 38000, 1, NULL, NULL),
('SO58cf7c5c2d8d0', 'Barang01', 7000, 1000, 2, NULL, NULL),
('SO58cf7c9e25ed7', 'Barang03', 100000, 40000, 1, NULL, NULL),
('SO58cf7c9f0e90f', 'Barang03', 18000, 38500, 5, NULL, NULL),
('SO58cf7ccbaee79', 'Barang03', 100000, 40000, 1, NULL, NULL),
('SO58cf7d68b9f8c', 'Barang03', 5000, 32000, 1, NULL, NULL),
('SO58cf7d9e4dbf4', 'Barang03', 200000, 35000, 1, NULL, NULL),
('SO58cf7db49a773', 'Barang03', 10000, 37000, 5, NULL, NULL),
('SO58cf7d9e0f8f7', 'Barang03', 18000, 39500, 5, NULL, NULL),
('SO58cf7dd5e59bd', 'Barang03', 5000, 32000, 1, NULL, NULL),
('SO58cf7dee92e9d', 'Barang03', 20000, 35000, 1, NULL, NULL),
('SO58cf7deadb73f', 'Barang02', 130000, 25000, 5, NULL, NULL),
('SO58cf7deadb73f', 'Barang02', 33000, 24000, 3, NULL, NULL),
('SO58cf7e17dd9d6', 'Barang03', 100000, 35000, 1, NULL, NULL),
('SO58cf7e25d463f', 'Barang03', 5000, 35000, 1, NULL, NULL),
('SO58cf7e4ac7a62', 'Barang03', 100000, 30000, 1, NULL, NULL),
('SO58cf7e3d2375b', 'Barang03', 200, 37200, 1, NULL, NULL),
('SO58cf7e42bf8d8', 'Barang03', 10000, 32000, 1, NULL, NULL),
('SO58cf7e6f2c0d3', 'Barang03', 500, 36800, 1, NULL, NULL),
('SO58cf7ea395761', 'Barang03', 5000, 35000, 1, NULL, NULL),
('SO58cf7eb895363', 'Barang03', 40, 39000, 1, NULL, NULL),
('SO58cf7ed6dea27', 'Barang03', 20000, 32000, 1, NULL, NULL),
('SO58cf7f06c6c2f', 'Barang03', 320, 35000, 1, NULL, NULL),
('SO58cf7f249d6db', 'Barang03', 40000, 31000, 1, NULL, NULL),
('SO58cf7f41de03f', 'Barang03', 20000, 32000, 1, NULL, NULL),
('SO58cf7fc416c7d', 'Barang03', 10000, 32000, 1, NULL, NULL),
('SO58cf7fd410490', 'Barang03', 12000, 36000, 1, NULL, NULL),
('SO58cf7fcca0baa', 'Barang03', 18000, 35000, 1, NULL, NULL),
('SO58cf7ffd896f7', 'Barang03', 10000, 32000, 1, NULL, NULL),
('SO58cf8003133ac', 'Barang03', 13000, 37000, 1, NULL, NULL),
('SO58cf80314e578', 'Barang03', 40000, 33000, 1, NULL, NULL),
('SO58cf804716452', 'Barang03', 7000, 32000, 1, NULL, NULL),
('SO58cf801056102', 'Barang02', 24500, 150000, 2, NULL, NULL),
('SO58cf8061f32e9', 'Barang03', 18000, 36500, 1, NULL, NULL),
('SO58cf8075e0c2f', 'Barang03', 7000, 32000, 1, NULL, NULL),
('SO58cf8086702fb', 'Barang03', 100000, 33000, 1, NULL, NULL),
('SO58cf80b70a69b', 'Barang03', 5000, 32000, 1, NULL, NULL),
('SO58cf80be35f65', 'Barang03', 15000, 37000, 5, NULL, NULL),
('SO58cf80d788746', 'Barang02', 150000, 24500, 2, NULL, NULL),
('SO58cf80e65c797', 'Barang03', 10000, 40000, 5, NULL, NULL),
('SO58cf8114cd2da', 'Barang03', 18000, 37000, 1, NULL, NULL),
('SO58cf81d244031', 'Barang03', 50000, 33000, 1, NULL, NULL),
('SO58cf81ef2820d', 'Barang03', 12500, 37000, 1, NULL, NULL),
('SO58cf82120d36b', 'Barang03', 12500, 37000, 1, NULL, NULL),
('SO58cf823d52375', 'Barang03', 100000, 32000, 1, NULL, NULL),
('SO58cf824c15866', 'Barang03', 12000, 32000, 5, NULL, NULL),
('SO58cf82ac281fa', 'Barang03', 20000, 32000, 5, NULL, NULL),
('SO58cf831d7291f', 'Barang03', 15000, 32000, 5, NULL, NULL),
('SO58cf8370f01c0', 'Barang03', 15000, 32000, 5, NULL, NULL),
('SO58cf83b216931', 'Barang03', 10000, 32000, 5, NULL, NULL),
('SO58cf83e6aa63e', 'Barang03', 450, 34800, 1, NULL, NULL),
('SO58cf841cad7f7', 'Barang03', 15, 26000, 1, NULL, NULL),
('SO58cf84327c55a', 'Barang02', 180000, 26000, 1, NULL, NULL),
('SO58cf8443286be', 'Barang03', 20, 36000, 1, NULL, NULL),
('SO58cf84740c9fc', 'Barang03', 20000, 37000, 5, NULL, NULL),
('SO58cf8419a9bcc', 'Barang03', 13500, 34500, 1, NULL, NULL),
('SO58cf849cdf9ca', 'Barang03', 5000, 38000, 5, NULL, NULL),
('SO58cf84f1b7488', 'Barang03', 10000, 36000, 1, NULL, NULL),
('SO58cf84e9a4b8c', 'Barang03', 13500, 35000, 1, NULL, NULL),
('SO58cf851f7a488', 'Barang03', 20000, 36000, 1, NULL, NULL),
('SO58cf855a6210b', 'Barang03', 13500, 38500, 1, NULL, NULL),
('SO58cf858d8d2f0', 'Barang03', 300, 35200, 1, NULL, NULL),
('SO58cf859d4b2d9', 'Barang03', 15000, 35000, 1, NULL, NULL),
('SO58cf85be4349a', 'Barang03', 40, 35700, 1, NULL, NULL),
('SO58cf85d30f09c', 'Barang03', 15000, 37000, 1, NULL, NULL),
('SO58cf85e07e89d', 'Barang03', 15, 34200, 1, NULL, NULL),
('SO58cf85f015c92', 'Barang03', 13500, 38000, 1, NULL, NULL),
('SO58cf862231529', 'Barang03', 21000, 32000, 5, NULL, NULL),
('SO58cf864ce581d', 'Barang03', 27000, 32000, 5, NULL, NULL),
('SO58cf8898b5ac9', 'Barang01', 300000, 600, 4, NULL, NULL),
('SO58cf88d0e59c3', 'Barang03', 108000, 29000, 1, NULL, NULL),
('SO58cf88e6cc149', 'Barang01', 250000, 600, 4, NULL, NULL),
('SO58cf893e28912', 'Barang01', 250000, 600, 4, NULL, NULL),
('SO58cf89e7193bc', 'Barang01', 250000, 600, 4, NULL, NULL),
('SO58cf8aff4daa1', 'Barang01', 600000, 600, 1, NULL, NULL),
('SO58cf8b4e1cdcd', 'Barang01', 250000, 600, 1, NULL, NULL),
('SO58cf8c12bdf13', 'Barang01', 350000, 600, 4, NULL, NULL),
('SO58cf8c66ec035', 'Barang01', 400000, 600, 4, NULL, NULL),
('SO58cf8d1e10768', 'Barang01', 420000, 600, 4, NULL, NULL),
('SO58cf8d586eaa4', 'Barang01', 400000, 600, 4, NULL, NULL),
('SO58cf913c313d6', 'Barang03', 30000, 34500, 1, NULL, NULL),
('SO58cf93815ba7a', 'Barang03', 20000, 36000, 5, NULL, NULL),
('SO58cf952fa926f', 'Barang03', 12300, 34800, 1, NULL, NULL),
('SO58cf96922f6a5', 'Barang03', 35000, 35400, 5, NULL, NULL),
('SO58cf985100942', 'Barang03', 25000, 35800, 1, NULL, NULL),
('SO58cf993d675b5', 'Barang03', 15000, 36200, 1, NULL, NULL),
('SO58cf9a7b95ac8', 'Barang03', 8000, 35750, 5, NULL, NULL),
('SO58cf9bc2232aa', 'Barang03', 15000, 35000, 1, NULL, NULL),
('SO58cf9c14a0d7a', 'Barang03', 15000, 37000, 1, NULL, NULL),
('SO58cf9dcd2982c', 'Barang03', 13000, 35600, 1, NULL, NULL),
('SO58cf9e66bdb5c', 'Barang03', 10000, 40000, 2, NULL, NULL),
('SO58cf9ec7c123b', 'Barang03', 10000, 40500, 2, NULL, NULL),
('SO58cf9f364f8ed', 'Barang03', 17000, 40800, 2, NULL, NULL),
('SO58cf9f9624f02', 'Barang03', 17000, 39800, 2, NULL, NULL),
('SO58cfbe8410052', 'Barang03', 15000, 29900, 1, NULL, NULL),
('SO58cfbf2b08e6c', 'Barang03', 20000, 28900, 1, NULL, NULL),
('SO58d068a586f81', 'Barang03', 10000, 31900, 1, NULL, NULL),
('SO58d0691b8026b', 'Barang03', 15000, 29900, 1, NULL, NULL),
('SO58d06993b2f27', 'Barang03', 13000, 30900, 1, NULL, NULL),
('SO58d069f6a6fbf', 'Barang03', 17000, 28900, 1, NULL, NULL),
('SO58d06b85a69fc', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d06c03c889c', 'Barang03', 35000, 28900, 1, NULL, NULL),
('SO58d06caddf1db', 'Barang03', 25000, 30900, 1, NULL, NULL),
('SO58d06d2a9f139', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d06d66bb19f', 'Barang03', 5200, 30000, 1, NULL, NULL),
('SO58d06dd25a377', 'Barang03', 6000, 31000, 1, NULL, NULL),
('SO58d06dc4251dd', 'Barang03', 24000, 31900, 1, NULL, NULL),
('SO58d06e128f7c8', 'Barang03', 6000, 30000, 1, NULL, NULL),
('SO58d06e365a879', 'Barang03', 36000, 28900, 1, NULL, NULL),
('SO58d06f3a0d4b6', 'Barang03', 58000, 30000, 1, NULL, NULL),
('SO58d06f34b785e', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d06f70be35a', 'Barang03', 50000, 29000, 1, NULL, NULL),
('SO58d06fcfd812b', 'Barang03', 50000, 33000, 1, NULL, NULL),
('SO58d06fe98b405', 'Barang03', 34000, 28900, 1, NULL, NULL),
('SO58d07078de243', 'Barang01', 420, 1775, 4, NULL, NULL),
('SO58d070bda4e05', 'Barang03', 26000, 30900, 1, NULL, NULL),
('SO58d0712001b90', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d071d21884f', 'Barang03', 25000, 30900, 1, NULL, NULL),
('SO58d072199e148', 'Barang03', 35000, 28900, 1, NULL, NULL),
('SO58d0735375345', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d073a1ce2a3', 'Barang03', 40000, 28900, 1, NULL, NULL),
('SO58d073f7e6fb9', 'Barang03', 20000, 31900, 1, NULL, NULL),
('SO58d0745a44def', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d074c624130', 'Barang03', 35000, 28900, 1, NULL, NULL),
('SO58d07535d4b9a', 'Barang03', 25000, 31900, 1, NULL, NULL),
('SO58d076194f281', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d0766b8600a', 'Barang03', 25000, 31900, 1, NULL, NULL),
('SO58d076cea7499', 'Barang03', 35000, 28900, 1, NULL, NULL),
('SO58d0778e4c735', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d077fe4f182', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d07843d236e', 'Barang03', 30000, 29900, 1, NULL, NULL),
('SO58d07b2d5c8f7', 'Barang01', 3000, 900, 4, NULL, NULL),
('SO58d07c0176b10', 'Barang03', 400, 37800, 1, NULL, NULL),
('SO58d07ca2780c1', 'Barang01', 5000, 950, 4, NULL, NULL),
('SO58d07d18ed4c5', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d07d6884695', 'Barang03', 8900, 36000, 5, NULL, NULL),
('SO58d07d9bcd6dc', 'Barang03', 7800, 36200, 5, NULL, NULL),
('SO58d07e2327851', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d07e42e8b16', 'Barang03', 3200, 36900, 5, NULL, NULL),
('SO58d07e784a15c', 'Barang03', 5000, 36750, 4, NULL, NULL),
('SO58d07f47a6ea1', 'Barang03', 5000, 36750, 4, NULL, NULL),
('SO58d07f7a1a31e', 'Barang03', 100, 36750, 5, NULL, NULL),
('SO58d0809e6151c', 'Barang01', 2700, 1000, 4, NULL, NULL),
('SO58d0813ad5709', 'Barang01', 6000, 1000, 4, NULL, NULL),
('SO58d08145eac30', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d0819b807f3', 'Barang03', 10000, 35800, 4, NULL, NULL),
('SO58d081d869c25', 'Barang03', 1200, 37000, 4, NULL, NULL),
('SO58d0820632b96', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d082b0b25da', 'Barang03', 5800, 36000, 4, NULL, NULL),
('SO58d08335e2cdd', 'Barang03', 11000, 35800, 5, NULL, NULL),
('SO58d0834c66451', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d083c7a993b', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d08478f37ee', 'Barang03', 3000, 36000, 4, NULL, NULL),
('SO58d0849d4dfd5', 'Barang03', 2500, 36800, 5, NULL, NULL),
('SO58d084c4e0787', 'Barang03', 1160, 37600, 5, NULL, NULL),
('SO58d0851143828', 'Barang01', 3500, 900, 4, NULL, NULL),
('SO58d08535148fd', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d0869ba515b', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d087277a5ee', 'Barang01', 4000, 1100, 4, NULL, NULL),
('SO58d08752c5c0a', 'Barang03', 41300, 35000, 1, NULL, NULL),
('SO58d0875ec8f1a', 'Barang03', 25000, 34300, 1, NULL, NULL),
('SO58d0878abdb3d', 'Barang03', 3340, 36000, 5, NULL, NULL),
('SO58d087c00ec9a', 'Barang03', 5000, 36000, 1, NULL, NULL),
('SO58d087c61c4ea', 'Barang03', 80000, 27000, 1, NULL, NULL),
('SO58d08809092d9', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d08815ed6a3', 'Barang03', 18000, 35000, 1, NULL, NULL),
('SO58d088176adc7', 'Barang03', 30000, 31000, 1, NULL, NULL),
('SO58d088ace4f88', 'Barang03', 21200, 27000, 1, NULL, NULL),
('SO58d088b71c8e9', 'Barang01', 840, 1125, 4, NULL, NULL),
('SO58d0893293e88', 'Barang03', 70000, 29000, 1, NULL, NULL),
('SO58d08982b4712', 'Barang03', 2000, 36000, 1, NULL, NULL),
('SO58d089b523334', 'Barang03', 15000, 33900, 5, NULL, NULL),
('SO58d089cef07bd', 'Barang03', 38500, 28000, 1, NULL, NULL),
('SO58d089f8626c0', 'Barang01', 6500, 950, 4, NULL, NULL),
('SO58d08a2d37a40', 'Barang03', 41500, 27500, 1, NULL, NULL),
('SO58d08a1609cc2', 'Barang03', 5000, 35680, 5, NULL, NULL),
('SO58d08abbc0acd', 'Barang03', 20000, 25500, 1, NULL, NULL),
('SO58d08ab7da9e7', 'Barang03', 120000, 26500, 1, NULL, NULL),
('SO58d08ae24c78b', 'Barang01', 1000, 1500, 4, NULL, NULL),
('SO58d08b539ffe5', 'Barang03', 61700, 28500, 1, NULL, NULL),
('SO58d08bbd85f84', 'Barang01', 8000, 1500, 4, NULL, NULL),
('SO58d08b6ba36b2', 'Barang03', 20000, 31000, 1, NULL, NULL),
('SO58d08bd17781f', 'Barang03', 26100, 26500, 1, NULL, NULL),
('SO58d08c03059bb', 'Barang03', 20000, 31000, 1, NULL, NULL),
('SO58d08c695aecd', 'Barang03', 20000, 30000, 1, NULL, NULL),
('SO58d090913ce51', 'Barang03', 60000, 30000, 1, NULL, NULL),
('SO58d099c63b237', 'Barang01', 2000, 1500, 4, NULL, NULL),
('SO58d09b56e8109', 'Barang01', 6000, 1500, 4, NULL, NULL),
('SO58d09bce82f5d', 'Barang01', 6000, 1500, 4, NULL, NULL),
('SO58d09c501e1c9', 'Barang01', 4000, 1500, 4, NULL, NULL),
('SO58d09c99cfb3e', 'Barang01', 3360, 1125, 4, NULL, NULL),
('SO58d09ca1004d3', 'Barang01', 500, 1500, 4, NULL, NULL),
('SO58d09d47ef2c5', 'Barang01', 400, 1500, 4, NULL, NULL),
('SO58d09dfb3415d', 'Barang01', 3360, 1125, 4, NULL, NULL),
('SO58d09f343551a', 'Barang01', 4000, 1500, 4, NULL, NULL),
('SO58d09fba2396f', 'Barang01', 3000, 1500, 4, NULL, NULL),
('SO58d0a0435e771', 'Barang01', 1000, 1500, 4, NULL, NULL),
('SO58d0a0fa9fb63', 'Barang01', 3360, 1125, 4, NULL, NULL),
('SO58d0a1d8e4207', 'Barang01', 3360, 1125, 4, NULL, NULL),
('SO58d0a2b49d95c', 'Barang01', 1000, 1500, 4, NULL, NULL),
('SO58d0a33b8806c', 'Barang01', 1000, 1500, 4, NULL, NULL),
('SO58d0a33caa8e5', 'Barang01', 6720, 1125, 4, NULL, NULL),
('SO58d0a3827c933', 'Barang01', 6720, 1125, 4, NULL, NULL),
('SO58d0c318d1f34', 'Barang02', 900, 25000, 4, NULL, NULL),
('SO58d0c39f329e1', 'Barang02', 1500, 25000, 4, NULL, NULL),
('SO58d0c3cdaf219', 'Barang02', 1700, 25000, 4, NULL, NULL),
('SO58d0c4098bfb1', 'Barang02', 900, 25000, 4, NULL, NULL),
('SO58d0c3e7de572', 'Barang03', 60000, 31000, 1, NULL, NULL),
('SO58d0c4430f77a', 'Barang02', 600, 25000, 4, NULL, NULL),
('SO58d0c43a94d8e', 'Barang03', 60000, 30500, 1, NULL, NULL),
('SO58d0c4927538e', 'Barang02', 300, 25000, 4, NULL, NULL),
('SO58d0c5d13aa06', 'Barang01', 960, 1000, 4, NULL, NULL),
('SO58d0c6ad7d045', 'Barang03', 150000, 26500, 1, NULL, NULL),
('SO58d0c71880793', 'Barang02', 3000, 25000, 4, NULL, NULL),
('SO58d0c74901313', 'Barang03', 15000, 32000, 1, NULL, NULL),
('SO58d0c7b9f1b67', 'Barang03', 15000, 32000, 1, NULL, NULL),
('SO58d0c88dbeeea', 'Barang02', 1500, 25000, 4, NULL, NULL),
('SO58d0c8fdcf435', 'Barang01', 3360, 1000, 4, NULL, NULL),
('SO58d0c9497ce1a', 'Barang03', 70000, 28000, 1, NULL, NULL),
('SO58d0c97b0d197', 'Barang01', 3360, 900, 4, NULL, NULL),
('SO58d0c997c7f81', 'Barang03', 70000, 27000, 1, NULL, NULL),
('SO58d0c9b540f7d', 'Barang03', 20000, 32000, 5, NULL, NULL),
('SO58d0c9c9a61f5', 'Barang01', 3360, 1000, 4, NULL, NULL),
('SO58d0c9db6feb3', 'Barang03', 40000, 30000, 1, NULL, NULL),
('SO58d0ca1638312', 'Barang01', 1440, 1000, 4, NULL, NULL),
('SO58d0ca1638312', 'Barang01', 1920, 1000, 1, NULL, NULL),
('SO58d0ca9a4a192', 'Barang03', 50, 35000, 5, NULL, NULL),
('SO58d0cb183b25a', 'Barang01', 3600, 1000, 4, NULL, NULL),
('SO58d0cb2fc14a6', 'Barang01', 10, 5000, 4, NULL, NULL),
('SO58d0cb5e63b7c', 'Barang01', 3600, 800, 4, NULL, NULL),
('SO58d0cbb60cd85', 'Barang01', 7200, 900, 4, NULL, NULL),
('SO58d0cbd9ab009', 'Barang03', 20000, 32000, 5, NULL, NULL),
('SO58d0cc6339215', 'Barang01', 7200, 900, 4, NULL, NULL),
('SO58d0ccbfbffcd', 'Barang01', 7200, 1000, 4, NULL, NULL),
('SO58d0cd0e5edd0', 'Barang03', 28000, 32000, 5, NULL, NULL),
('SO58d0cd329eb6b', 'Barang03', 28000, 32000, 5, NULL, NULL),
('SO58d0cd885cd84', 'Barang01', 14400, 900, 4, NULL, NULL),
('SO58d0ce15c70dd', 'Barang01', 14400, 800, 4, NULL, NULL),
('SO58d0ce9d30ff1', 'Barang01', 14400, 1000, 4, NULL, NULL),
('SO58d0cfb71888e', 'Barang03', 15000, 30000, 1, NULL, NULL),
('SO58d0d0061c924', 'Barang03', 15000, 30000, 1, NULL, NULL),
('SO58d0d073ef2e6', 'Barang03', 18000, 30000, 1, NULL, NULL),
('SO58d0d19deee77', 'Barang03', 30000, 30000, 1, NULL, NULL),
('SO58d0d1edaf070', 'Barang03', 18000, 30000, 1, NULL, NULL),
('SO58d0d282320ff', 'Barang03', 5000, 30000, 1, NULL, NULL),
('SO58d0d2e22951c', 'Barang03', 23000, 30000, 1, NULL, NULL),
('SO58d0d31dd7917', 'Barang03', 20000, 30000, 1, NULL, NULL),
('SO58d0d549ac209', 'Barang03', 25000, 36000, 2, NULL, NULL),
('SO58d0d59794e0d', 'Barang03', 29000, 36000, 2, NULL, NULL),
('SO58d0d66667e4d', 'Barang03', 36000, 36000, 2, NULL, NULL),
('SO58d0d6a676dae', 'Barang03', 36000, 36000, 2, NULL, NULL),
('SO58d0e2b094fcc', 'Barang02', 1000, 24000, 4, NULL, NULL),
('SO58d0e306c690f', 'Barang02', 1000, 24000, 4, NULL, NULL),
('SO58d0e3bab5140', 'Barang02', 2300, 24000, 4, NULL, NULL),
('SO58d0e3eb48778', 'Barang02', 1000, 24000, 4, NULL, NULL),
('SO58d0e4e752ff7', 'Barang02', 2000, 24000, 4, NULL, NULL),
('SO58d0e52775365', 'Barang02', 2500, 24000, 4, NULL, NULL),
('SO58d0e5a694113', 'Barang02', 3000, 24000, 4, NULL, NULL),
('SO58d0e5e83c657', 'Barang02', 3000, 24000, 4, NULL, NULL),
('SO58d0f0f7b4f06', 'Barang03', 18000, 36000, 2, NULL, NULL),
('SO58d11b825d0b0', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d11d0e1347e', 'Barang02', 5500, 25000, 4, NULL, NULL),
('SO58d11da8c3cb4', 'Barang02', 4000, 25000, 4, NULL, NULL),
('SO58d11e79ad08c', 'Barang02', 5500, 25000, 4, NULL, NULL),
('SO58d11ff766310', 'Barang02', 4500, 25000, 4, NULL, NULL),
('SO58d1258698ff7', 'Barang03', 12500, 36000, 1, NULL, NULL),
('SO58d125a6a2e34', 'Barang03', 12500, 37000, 1, NULL, NULL),
('SO58d125c8814fc', 'Barang03', 10000, 36000, 1, NULL, NULL),
('SO58d125e5ce874', 'Barang03', 15000, 37000, 1, NULL, NULL),
('SO58d12671892b4', 'Barang03', 20000, 35000, 1, NULL, NULL),
('SO58d1268da758a', 'Barang03', 15000, 36000, 1, NULL, NULL),
('SO58d126bc7bcf2', 'Barang03', 15000, 36000, 1, NULL, NULL),
('SO58d126db76cae', 'Barang03', 15000, 37000, 1, NULL, NULL),
('SO58d127263d068', 'Barang02', 6000, 25000, 4, NULL, NULL),
('SO58d1286960481', 'Barang01', 12000, 1000, 2, NULL, NULL),
('SO58d12947edaf7', 'Barang03', 13000, 40000, 5, NULL, NULL),
('SO58d129645361d', 'Barang03', 12000, 38000, 5, NULL, NULL),
('SO58d1299822fb5', 'Barang01', 12000, 1000, 2, NULL, NULL),
('SO58d129c738541', 'Barang03', 15000, 36000, 1, NULL, NULL),
('SO58d129ff552e2', 'Barang03', 20000, 35000, 1, NULL, NULL),
('SO58d12a46d81d9', 'Barang03', 10000, 34000, 1, NULL, NULL),
('SO58d12a5b69458', 'Barang02', 4000, 25000, 4, NULL, NULL),
('SO58d12a632c092', 'Barang03', 10000, 36000, 1, NULL, NULL),
('SO58d12a4978208', 'Barang01', 12000, 1000, 2, NULL, NULL),
('SO58d12abec80fe', 'Barang03', 15000, 37000, 1, NULL, NULL),
('SO58d12b0094faa', 'Barang01', 15000, 1000, 2, NULL, NULL),
('SO58d12b6fb5bb7', 'Barang01', 15000, 1000, 2, NULL, NULL),
('SO58d12ba3d0d86', 'Barang01', 20000, 1000, 2, NULL, NULL),
('SO58d12bc42cb9c', 'Barang01', 20000, 1000, 2, NULL, NULL),
('SO58d12d49e1c6c', 'Barang01', 20000, 1000, 2, NULL, NULL),
('SO58d1312e23794', 'Barang01', 7000, 950, 1, NULL, NULL),
('SO58d131530dfe7', 'Barang01', 7000, 950, 1, NULL, NULL),
('SO58d1317d9b470', 'Barang01', 10000, 950, 1, NULL, NULL),
('SO58d131a5a7147', 'Barang01', 10000, 950, 1, NULL, NULL),
('SO58d131f3b5174', 'Barang01', 15000, 950, 1, NULL, NULL),
('SO58d13218a1b28', 'Barang01', 25000, 950, 1, NULL, NULL),
('SO58d13246da610', 'Barang01', 20000, 950, 1, NULL, NULL),
('SO58d1333c47dc3', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d133b9ccc8f', 'Barang01', 30000, 950, 1, NULL, NULL),
('SO58d133d4efd58', 'Barang01', 30000, 950, 1, NULL, NULL),
('SO58d133eb308b6', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d1341ae10ee', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d1342549d25', 'Barang01', 25000, 950, 1, NULL, NULL),
('SO58d1346142c46', 'Barang01', 25000, 950, 1, NULL, NULL),
('SO58d1352c62557', 'Barang01', 35000, 950, 1, NULL, NULL),
('SO58d13558bcabb', 'Barang01', 35000, 950, 1, NULL, NULL),
('SO58d13670ee217', 'Barang01', 20000, 950, 1, NULL, NULL),
('SO58d13694781e4', 'Barang01', 20000, 950, 1, NULL, NULL),
('SO58d136acbbc80', 'Barang01', 20000, 950, 1, NULL, NULL),
('SO58d1372fd7dec', 'Barang01', 20000, 950, 1, NULL, NULL),
('SO58d13772d4009', 'Barang01', 20000, 950, 1, NULL, NULL),
('SO58d137c6720f7', 'Barang01', 7600, 950, 1, NULL, NULL),
('SO58d1387ec5668', 'Barang01', 13800, 950, 1, NULL, NULL),
('SO58d138ae7ded3', 'Barang01', 40000, 900, 4, NULL, NULL),
('SO58d138b3ba8dd', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d138f936065', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13973d9582', 'Barang01', 80000, 900, 4, NULL, NULL),
('SO58d139a11d55b', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13a0690036', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13a5607847', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13a440210b', 'Barang01', 30000, 1000, 4, NULL, NULL),
('SO58d13a8944651', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13a9f0cef2', 'Barang01', 30000, 1000, 4, NULL, NULL),
('SO58d13aecc2ccc', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13afda13d7', 'Barang01', 45000, 1000, 4, NULL, NULL),
('SO58d13b172acc6', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13b1a39eb7', 'Barang01', 40000, 1000, 4, NULL, NULL),
('SO58d13b71bbbf5', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13b7603c4c', 'Barang01', 37800, 1000, 4, NULL, NULL),
('SO58d13c325a8df', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13c864a2a1', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13cd5a839b', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13d04a3630', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13d444b3bd', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13db151db8', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13e19c0a65', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13e6ce537b', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d13fd935d11', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1410ee1acd', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d14155d4246', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d1418a55289', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d141ed22a4d', 'Barang03', 50000, 31000, 1, NULL, NULL),
('SO58d1428ba5743', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d142dd20482', 'Barang02', 5000, 25000, 4, NULL, NULL),
('SO58d143810b37e', 'Barang03', 7000, 30000, 1, NULL, NULL),
('SO58d144942dd7d', 'Barang03', 51000, 31000, 1, NULL, NULL),
('SO58d144e64923b', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1451f1d5e0', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1475685ef7', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d147ae6399e', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d147fbce462', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1485aad391', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1496c96a1b', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d14a5ef239c', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d14b721f24d', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d14c3c6cd30', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d14d4e924e8', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d14d7518653', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1513095447', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1516de4569', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1524fd6450', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d152e034f45', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d153281dd66', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d1538d8b4a2', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d153aa410d0', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d153d2c98f9', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d1542b1eba4', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d1547c85d16', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d154aa41ee7', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d154ebcb29a', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d1555322b10', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1556ad5523', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d155a436d23', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d155a934bf7', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d15614b540f', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d1561e120f2', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d1566ce087e', 'Barang02', 5000, 26000, 4, NULL, NULL),
('SO58d157c3f11d0', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d15868782f8', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d3835050b32', 'Barang03', 300, 30000, 5, NULL, NULL),
('SO58d38394d3a68', 'Barang03', 600, 29000, 5, NULL, NULL),
('SO58d3855391075', 'Barang03', 100, 30000, 5, NULL, NULL),
('SO58d3866f5a4e0', 'Barang03', 400, 29000, 5, NULL, NULL),
('SO58d3912a34d6f', 'Barang03', 300, 30000, 5, NULL, NULL),
('SO58d3933776bdf', 'Barang03', 900, 30000, 5, NULL, NULL),
('SO58d5e0860cd0a', 'Barang03', 200, 30000, 5, NULL, NULL),
('SO58d604fb30a9f', 'Barang03', 900, 29000, 5, NULL, NULL),
('SO58d605866c13f', 'Barang03', 800, 29000, 5, NULL, NULL),
('SO58d605dc1c327', 'Barang03', 600, 28000, 5, NULL, NULL),
('SO58d609579898f', 'Barang03', 500, 28000, 5, NULL, NULL),
('SO58d871d74cd82', 'Barang01', 100, 600, 2, NULL, NULL),
('SO58d87378a24e3', 'Barang01', 10, 600, 2, NULL, NULL),
('SO58d87c8104512', 'Barang03', 95800, 31500, 1, NULL, NULL),
('SO58d87ce948a55', 'Barang03', 92000, 33500, 1, NULL, NULL),
('SO58d87db89909b', 'Barang03', 85000, 30000, 1, NULL, NULL),
('SO58d87e1a4b00b', 'Barang03', 102800, 31000, 1, NULL, NULL),
('SO58d87eef6ed58', 'Barang03', 93000, 29000, 1, NULL, NULL),
('SO58d87f5419226', 'Barang03', 94800, 27500, 1, NULL, NULL),
('SO58d8803b80bbd', 'Barang03', 65000, 25500, 1, NULL, NULL),
('SO58d880a435f97', 'Barang03', 122800, 29500, 1, NULL, NULL),
('SO58d881ba6e8f1', 'Barang03', 94000, 28000, 1, NULL, NULL),
('SO58d882182668f', 'Barang03', 93800, 26500, 1, NULL, NULL),
('SO58d885b47e51a', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d885eea02c3', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d8862329359', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d88659c597f', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d8868c0da3f', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d886bee3827', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d886f2e834d', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d8878fac340', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d887bca2a0c', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d887fbcf229', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d8882cbe090', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d88862c408a', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d8888bd2a2b', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d888bb01291', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d888f18f63d', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d8891f08ff9', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d8894909c83', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d889be7d7bd', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d889e87fc58', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d88a1e82af2', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d88a4632ccb', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d88a6e2e37e', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d88a99654a0', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d88ad0c7cf0', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d88afad5039', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d88b28385ca', 'Barang03', 27000, 31000, 1, NULL, NULL),
('SO58d88b512ad74', 'Barang03', 27000, 30000, 1, NULL, NULL),
('SO58d88ddbc54d2', 'Barang01', 11000, 1000, 4, NULL, NULL),
('SO58d88e0dd98ff', 'Barang01', 11500, 1000, 4, NULL, NULL),
('SO58d88e8c80833', 'Barang01', 7000, 1100, 4, NULL, NULL),
('SO58d88fbb5026c', 'Barang01', 17500, 900, 4, NULL, NULL),
('SO58d88ff21f5f8', 'Barang01', 20000, 875, 4, NULL, NULL),
('SO58d890c7b3639', 'Barang01', 6700, 1000, 4, NULL, NULL),
('SO58d890f78f66d', 'Barang01', 7300, 1300, 4, NULL, NULL),
('SO58d89129d8ea5', 'Barang01', 6500, 1150, 4, NULL, NULL),
('SO58d8923718011', 'Barang01', 38000, 1050, 4, NULL, NULL),
('SO58d8929830961', 'Barang01', 20984, 1500, 4, NULL, NULL),
('SO58d895d3d66c0', 'Barang02', 180000, 26000, 4, NULL, NULL),
('SO58d896271495f', 'Barang02', 18000, 26000, 4, NULL, NULL),
('SO58d89678ed1f7', 'Barang02', 150000, 27000, 4, NULL, NULL),
('SO58d896d842b98', 'Barang03', 15000, 35500, 1, NULL, NULL),
('SO58d89733ba872', 'Barang03', 23000, 35350, 1, NULL, NULL),
('SO58d8973b55c90', 'Barang02', 150000, 28000, 4, NULL, NULL),
('SO58d897b659458', 'Barang03', 3000, 36000, 5, NULL, NULL),
('SO58d897d8c718a', 'Barang03', 2000, 35800, 1, NULL, NULL),
('SO58d897d0c9eed', 'Barang02', 150000, 29000, 4, NULL, NULL),
('SO58d897d8ccfd9', 'Barang03', 800, 29000, 5, NULL, NULL),
('SO58d898569d0ad', 'Barang03', 18000, 35000, 1, NULL, NULL),
('SO58d898474a2c3', 'Barang03', 350, 30000, 5, NULL, NULL),
('SO58d8994999fb6', 'Barang03', 60000, 40000, 1, NULL, NULL),
('SO58d899af5dcea', 'Barang03', 40000, 34000, 1, NULL, NULL),
('SO58d89a46796ae', 'Barang03', 100000, 35000, 1, NULL, NULL),
('SO58d89afd65ceb', 'Barang02', 200, 24000, 5, NULL, NULL),
('SO58d89b57a2897', 'Barang02', 500, 24000, 4, NULL, NULL),
('SO58d89b42e17bb', 'Barang03', 150000, 36000, 2, NULL, NULL),
('SO58d89b851c0d1', 'Barang02', 500, 24000, 5, NULL, NULL),
('SO58d89b88f1737', 'Barang03', 120000, 40000, 1, NULL, NULL),
('SO58d89bbd7f34d', 'Barang03', 160000, 43000, 2, NULL, NULL),
('SO58d89e033122a', 'Barang03', 200000, 39000, 1, NULL, NULL),
('SO58d8a038f1d77', 'Barang03', 150000, 39000, 1, NULL, NULL),
('SO58d8a07f0f7de', 'Barang03', 100000, 40000, 1, NULL, NULL),
('SO58d8a18ea0cd3', 'Barang03', 100000, 40000, 1, NULL, NULL),
('SO58d8a1ce2f492', 'Barang03', 60000, 38000, 1, NULL, NULL),
('SO58d8a28513efc', 'Barang03', 130000, 40000, 1, NULL, NULL),
('SO58d8a31d95fd9', 'Barang03', 10000, 40000, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id_so` varchar(100) NOT NULL,
  `id_petugas` varchar(100) NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `total` bigint(20) NOT NULL,
  `kurir` varchar(100) NOT NULL,
  `status` int(11) DEFAULT '0',
  `tanggal_keluar` date DEFAULT NULL,
  `id_pemilik` varchar(200) DEFAULT NULL,
  `ongkir` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id_so`, `id_petugas`, `id_customer`, `tanggal`, `total`, `kurir`, `status`, `tanggal_keluar`, `id_pemilik`, `ongkir`) VALUES
('SO58cf5f7610ebc', '5113100150', '000003', '2017-03-28', 10000, 'nanda', 3, NULL, '5113100150', 0),
('SO58cf6326f3332', '5114100026', '5171011003960014', '2017-03-22', 3600000000, 'toko', 3, NULL, '5114100026', 0),
('SO58cf63791bf88', '5114100026', '5171011003960006', '2017-03-23', 15000000, 'toko', 4, NULL, '5114100026', 0),
('SO58cf63a3ce429', '5114100026', '5171011003960006', '2017-03-24', 15000000, 'toko', 3, NULL, '5114100026', 0),
('SO58cf63af6f5b9', '5113100150', '000003', '2017-04-28', 5700000, 'aaa', 3, NULL, '5113100150', 0),
('SO58cf63b3627b1', '5114100073', '5114100026', '2017-03-14', 1062000000, 'Pribadi', 3, NULL, '5114100073', 100000),
('SO58cf63d1e2863', '5113100150', '000003', '2017-03-28', 2670000, 'aaaa', 3, NULL, '5113100150', 0),
('SO58cf653af3952', '5114100026', '5171011003960014', '2017-04-06', 350000000, 'toko', 3, NULL, '5114100026', 0),
('SO58cf6567e2f96', '1316100030', '5113100164', '2017-03-28', 6400000, 'aaa', 2, NULL, '1316100030', 0),
('SO58cf659693bcc', '1316100030', '5113100164', '2017-04-28', 4575000, 'aaaa', 3, NULL, '1316100030', 0),
('SO58cf65da3aa29', '5114100001', '511510193302', '2017-03-01', 11250000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf65fe1de33', '5114100012', '5114100181', '2017-03-22', 390000000, 'jne', 3, NULL, '5114100012', 50000),
('SO58cf660445b02', '5114100151', 'co002', '2017-03-31', 2790000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58cf661c1d15e', '5114100181', '5114100172', '2017-03-01', 6696000, 'Tossa', 3, NULL, '5114100181', 20000),
('SO58cf66455ee30', '5114100073', '5114100077', '2017-03-21', 1062000000, 'Pribadi', 3, NULL, '5114100073', 100000),
('SO58cf6666ea851', '5114100026', '5171011003960007', '2017-05-17', 1860000000, 'Toko', 3, NULL, '5114100026', 0),
('SO58cf6699a0152', '5114100070', '5114100070', '2017-03-21', 5256000000, 'pras', 3, NULL, '5114100070', 0),
('SO58cf66a4a112f', '5114100073', '5114100101', '2017-03-21', 1062000000, 'Pribadi', 3, NULL, '5114100073', 100000),
('SO58cf66d7a1756', '5114100181', '5113100002', '2017-03-01', 5580000, 'Tossa', 3, NULL, '5114100181', 15000),
('SO58cf66f017339', '5114100001', '5114100097', '2017-03-01', 12500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf672dad74a', '5114100026', '5171011003960006', '2017-05-25', 182900000, 'toko', 3, NULL, '5114100026', 0),
('SO58cf6745ec774', '5114100051', '5116100009', '2017-03-21', 270000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58cf675d80ec8', '5114100181', '5114100172', '2017-03-02', 9300000, 'Tossa', 3, NULL, '5114100181', 20000),
('SO58cf67a6cc4df', '5114100001', '51141000121', '2017-03-01', 12250000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf67e83f654', '5114100051', '5116100004', '2017-03-21', 1596000000, 'Doomling', 2, NULL, '5114100051', 100000),
('SO58cf6837359e1', '5114100172', '5114100130', '2017-03-03', 2500000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf6851dfbf6', '5114100001', '5114100035', '2017-03-01', 13750000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf6860b2251', '5114100012', '5114100017', '2017-03-22', 1738000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf686cdd72f', '5114100181', '5114100012', '2017-03-03', 3900000, 'Tossa', 3, NULL, '5114100181', 10000),
('SO58cf687bcfd2d', '5114100172', '5114100069', '2017-03-04', 0, 'PD.Jaya', 0, NULL, '5114100172', 5000),
('SO58cf68ac855c3', '5114100017', '15114100012', '2017-03-02', 33500000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf68d7228d4', '5114100110', '432453254', '2017-03-01', 35000000, 'Akbar', 3, NULL, '5114100110', 30000),
('SO58cf68ea0b43d', '5114100001', '51141000170', '2017-03-01', 11500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf68fe4d737', '5114100110', '123', '2017-03-01', 175000000, 'Akbar', 3, NULL, '5114100110', 30000),
('SO58cf690bf2483', '5114100051', '5116100002', '2017-03-21', 0, 'Doomling', 1, NULL, '5114100051', 100000),
('SO58cf6925e2b9e', '5114100051', '5116100002', '2017-03-21', 270000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58cf69706b6a8', '5114100181', '5113100002', '2017-03-04', 11400000, 'Tossa', 3, NULL, '5114100181', 25000),
('SO58cf69865839c', '5114100110', '123456', '2017-03-02', 210000000, 'Akbar', 3, NULL, '5114100110', 30000),
('SO58cf69cbecf5b', '5114100026', '5171011003960012', '2017-05-11', 1646100000, 'toko', 3, NULL, '5114100026', 0),
('SO58cf69ce8da95', '5114100100', '30', '2017-03-03', 448500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58cf69e0ecfcd', '5114100051', '5116100010', '2017-03-21', 540000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58cf69e7d6778', '5114100017', '15114100181', '2017-03-02', 40200000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf6a0ff2653', '5114100001', '5114100177', '2017-03-01', 15000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf6a131fd08', '5114100012', '5114100001', '2017-03-24', 945000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf6a2c37291', '5114100110', '432453254', '2017-03-04', 340000000, 'Akbar', 3, NULL, '5114100110', 35000),
('SO58cf6a3f860da', '5114100065', '5114100180', '2017-03-22', 4725000000, 'rey', 3, NULL, '5114100065', 5000000),
('SO58cf6a5b733c3', '5114100051', '5116100015', '2017-03-21', 540000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58cf6ab4f32f4', '5114100026', '5171011003960011', '2017-06-16', 3600000000, 'Toko', 3, NULL, '5114100026', 0),
('SO58cf6abbc2d94', '5114100100', '19', '2017-03-05', 370800000, 'UBER', 3, NULL, '5114100100', 0),
('SO58cf6ad79f89c', '5114100181', '5114100012', '2017-03-05', 0, 'Tossa', 0, NULL, '5114100181', 5000),
('SO58cf6aeea0a63', '5114100001', '5114100171', '2017-03-01', 13250000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf6aefc2de7', '5114100172', '5114100135', '2017-03-05', 10000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf6b0e30566', '5114100017', '15114100130', '2017-03-03', 16750000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf6b1a16d5e', '5114100100', '10', '2017-03-07', 491300000, 'UBER', 3, NULL, '5114100100', 0),
('SO58cf6b1d5a2ae', '5114100172', '5114100191', '2017-03-05', 12000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf6b3528314', '5114100181', '5114100012', '2017-03-05', 1950000, 'Tossa', 3, NULL, '5114100181', 5000),
('SO58cf6b4715237', '5114100172', '5114100069', '2017-03-04', 2000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf6b50704cf', '5114100012', '5114100181', '2017-03-25', 972000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf6b68b8bf3', '5114100181', '5114100172', '2017-03-05', 6660000, 'Tossa', 3, NULL, '5114100181', 7500),
('SO58cf6b7c442c1', '5114100151', 'co001', '2018-04-30', 3840000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58cf6b7ec19aa', '5114100110', '123456', '2017-03-06', 175000000, 'Akbar', 3, NULL, '5114100110', 15000),
('SO58cf6b849d74a', '5114100172', '5114100135', '2017-03-11', 10000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf6b9e6e186', '5114100100', '7', '2017-03-09', 450000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58cf6bb60ac68', '5114100017', '15114100001', '2017-03-03', 43550000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf6bbd1b9f7', '5114100172', '5114100135', '2017-03-18', 10000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf6bd1405d9', '5114100181', '5113100002', '2017-03-06', 5096250, 'Tossa', 3, NULL, '5114100181', 7500),
('SO58cf6be3cd632', '5114100172', '5114100135', '2017-03-25', 0, 'PD.Jaya', 0, NULL, '5114100172', 5000),
('SO58cf6c11c25ea', '5114100110', '432453254', '2017-03-07', 245000000, 'Akbar', 3, NULL, '5114100110', 30000),
('SO58cf6c2663a1c', '5114100172', '5114100135', '2017-03-25', 10000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf6c369edd5', '5114100100', '13', '2017-03-11', 627900000, 'UBER', 3, NULL, '5114100100', 0),
('SO58cf6c3f7f1ca', '5114100172', '5114100135', '2017-03-31', 0, 'PD.Jaya', 0, NULL, '5114100172', 5000),
('SO58cf6c4dc7f07', '5114100181', '5114100012', '2017-03-06', 3112000, 'Tossa', 3, NULL, '5114100181', 5000),
('SO58cf6c4fed599', '5114100051', '5114100013', '2017-03-21', 576000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58cf6c61085c3', '5114100026', '5171011003960008', '2017-07-21', 3445000000, 'toko', 3, NULL, '5114100026', 0),
('SO58cf6c77a5ff1', '5114100070', '5114100070', '2017-04-05', 4380000000, 'pras', 3, NULL, '5114100070', 0),
('SO58cf6c9e3fc40', '5114100001', '5114100173', '2017-03-01', 10000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf6cb4b0405', '5114100017', '4114100012', '2017-03-03', 67000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf6cb90ef4f', '5114100035', '081231248235832', '2017-03-27', 240000000, 'Wintolien', 3, NULL, '5114100035', 150000),
('SO58cf6cbe95be3', '5114100012', '5114100017', '2017-04-02', 1170000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf6cc867cb9', '5114100051', '5116100017', '2017-03-21', 576000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58cf6cdfcaeaf', '5114100110', '432453254', '2017-03-08', 210000000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58cf6d1824187', '5114100181', '5114100170', '2017-03-07', 17500000, 'Kontainer', 3, NULL, '5114100181', 100000),
('SO58cf6d291c94e', '5114100110', '123', '2017-03-09', 140000000, 'Akbar', 3, NULL, '5114100110', 30000),
('SO58cf6d2a9edb3', '5114100175', '5114100164', '2017-01-03', 745500, 'Doni', 3, NULL, '5114100175', 10000),
('SO58cf6d4320831', '5114100001', '5114100800', '2017-03-01', 25000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf6d57e2f82', '5114100017', '15114100030', '2017-03-03', 16750000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf6d89e66be', '5114100012', '5114100001', '2017-04-02', 960000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf6d97a41ea', '5114100181', '5113100002', '2017-03-08', 10584000, 'Tossa', 3, NULL, '5114100181', 15000),
('SO58cf6dcecea9d', '5114100071', '5114100068', '2017-03-21', 4000000000, 'Dito', 3, NULL, '5114100071', 0),
('SO58cf6e0082e35', '5114100017', '4114100017', '2017-03-06', 117250000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf6e37248d2', '5114100051', '5116100011', '2017-03-21', 576000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58cf6e4c198e1', '5114100100', '99', '2017-03-13', 310000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58cf6e58b2916', '5114100014', '123456789', '2017-03-20', 0, 'Adit', 2, NULL, '5114100014', 20000),
('SO58cf6e5977ee0', '5114100181', '5114100172', '2017-03-12', 33500000, 'Kontainer', 3, NULL, '5114100181', 120000),
('SO58cf6e727d961', '5114100175', '5114100175', '2017-01-05', 745500, 'Doni', 3, NULL, '5114100175', 10000),
('SO58cf6e74bab07', '5114100073', '5114100071', '2017-03-27', 1593000000, 'Pribadi', 3, NULL, '5114100073', 150000),
('SO58cf6ea7aaab3', '5114100071', '5114100049', '2017-03-22', 4000000000, 'Dito', 3, NULL, '5114100071', 0),
('SO58cf6ec367f1f', '5114100012', '5114100181', '2017-04-04', 1347500000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf6ecbcfe00', '5114100017', '15114100172', '2017-03-06', 33500000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf6edf8d144', '5114100167', '3467823648', '2017-03-04', 480000, 'Hanip', 3, NULL, '5114100167', 40000),
('SO58cf6edf92a08', '5114100073', '5114100704', '2017-03-27', 0, 'Pribadi', 4, NULL, '5114100073', 150000),
('SO58cf6ef641a2f', '5114100014', '123456789', '2017-03-20', 480000, 'Adit', 3, NULL, '5114100014', 20000),
('SO58cf6f283a1ac', '5114100181', '5114100012', '2017-03-09', 3112000, 'Tossa', 3, NULL, '5114100181', 5000),
('SO58cf6f44ab378', '5114100012', '5114100017', '2017-04-05', 712500000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf6f4793b34', '5114100051', '5116100012', '2017-03-21', 540000000, 'Doomling', 2, NULL, '5114100051', 100000),
('SO58cf6f5ddab2c', '5114100110', '123', '2017-03-10', 122500000, 'Akbar', 3, NULL, '5114100110', 25000),
('SO58cf6f7d631ec', '5114100073', '5114100704', '2017-03-27', 1593000000, 'Pribadi', 3, NULL, '5114100073', 150000),
('SO58cf6f892fa62', '5114100172', '5114100191', '2017-03-12', 15000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf6f98de429', '5114100017', '12345678909', '2017-03-09', 1005000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf6fb1c76f0', '5114100172', '5114100191', '2017-03-19', 0, 'PD.Jaya', 0, NULL, '5114100172', 5000),
('SO58cf6fb996b2f', '5114100181', '5114100170', '2017-03-10', 4560000, 'Kontainer', 3, NULL, '5114100181', 11000),
('SO58cf6fbe1bb3b', '5114100035', '52349235832958293', '2017-03-28', 210000000, 'Wintolien', 2, NULL, '5114100035', 150000),
('SO58cf6fd82c92c', '5114100151', '34562345', '2019-05-28', 4900000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58cf6fe63310c', '5114100181', '5114100012', '2017-03-11', 3695500, 'Tossa', 3, NULL, '5114100181', 130000),
('SO58cf6ffe3dd7b', '5114100172', '5114100191', '2017-03-26', 15000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf7002d61b7', '5114100167', '5145784564843415', '2017-03-05', 480000, 'Hanip', 3, NULL, '5114100167', 40000),
('SO58cf700e112ae', '5114100065', 'adi', '2017-04-13', 408000000, 'Evan', 3, NULL, '5114100065', 4999997),
('SO58cf7052ebe67', '5114100172', '5114100191', '2017-03-31', 0, 'PD.Jaya', 0, NULL, '5114100172', 5000),
('SO58cf70671eff9', '5114100017', '15114100172', '2017-03-10', 100500000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf7080cbe9c', '5114100026', '5171011003960012', '2017-04-20', 2800000000, 'toko', 3, NULL, '5114100026', 0),
('SO58cf7090ce31f', '5114100001', '5114100037', '2017-03-02', 37500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf709e72fa6', '5114100073', '5114100077', '2017-04-03', 6480000000, 'Pribadi', 3, NULL, '5114100073', 750000),
('SO58cf70a71db46', '5114100110', '34353256', '2017-03-11', 87500000, 'Akbar', 3, NULL, '5114100110', 35000),
('SO58cf70ab93661', '5114100181', '5113100002', '2017-03-13', 45225000, 'Kontainer', 3, NULL, '5114100181', 125000),
('SO58cf70c59813c', '5114100071', '5114100049', '2017-04-09', 0, 'Dito', 2, NULL, '5114100071', 0),
('SO58cf70e9564e0', '5114100026', '5171011003960013', '2017-04-30', 350000000, 'toko', 3, NULL, '5114100026', 0),
('SO58cf70f2bb986', '5114100164', '3174020305960003', '2017-03-30', 120000000, 'Janet', 3, NULL, '5114100164', 10000),
('SO58cf710083291', '5114100001', '5114100083', '2017-03-02', 13000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf7100dc54f', '5114100017', '4114100012', '2017-03-12', 167500000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf71121ecd5', '5114100167', '4382479832', '2017-03-06', 480000, 'Hanip', 3, NULL, '5114100167', 40000),
('SO58cf713440b89', '5114100012', '5114100131', '2017-05-06', 665000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf715ad1539', '5114100175', '5114100164', '2017-01-10', 0, 'Doni', 2, NULL, '5114100175', 10000),
('SO58cf715b21cf7', '5114100110', '7387136319', '2017-03-14', 350000000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58cf717c07793', '5114100110', '432453254', '2017-03-16', 350000000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58cf718917668', '5114100167', '3467823648', '2017-03-07', 432000, 'Hanip', 3, NULL, '5114100167', 20000),
('SO58cf71bc7f83e', '5114100065', 'adi', '2017-04-18', 425000000, 'wintolien', 4, NULL, '5114100065', 5000000),
('SO58cf71cf758d1', '5114100110', '123456', '2017-03-18', 180000000, 'Akbar', 3, NULL, '5114100110', 30000),
('SO58cf71d725af3', '5114100181', '5114100170', '2017-03-14', 15136000, 'Tossa', 3, NULL, '5114100181', 123000),
('SO58cf71e21f18d', '5114100017', '15114100030', '2017-03-12', 167500000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf71ee384cb', '5114100001', '5114100040', '2017-03-02', 11250000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf72703d933', '5114100017', '4114100017', '2017-03-18', 335000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf72ab3c3d5', '5114100181', '5113100002', '2017-03-14', 975000, 'Kontainer', 3, NULL, '5114100181', 5000),
('SO58cf7354203ea', '5114100181', '5114100172', '2017-03-15', 1170000, 'Tossa', 3, NULL, '5114100181', 5000),
('SO58cf737ec6a37', '5114100175', '5114100164', '2017-01-11', 745500, 'Doni', 3, NULL, '5114100175', 10000),
('SO58cf7384d90fc', '5114100065', 'adi', '2017-04-20', 4080000000, 'muhsin', 3, NULL, '5114100065', 5000000),
('SO58cf73b061bfe', '5114100017', '15114100130', '2017-03-20', 234500000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf741814202', '5114100017', '15114100172', '2017-03-22', 167500000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf7434a4643', '5114100175', '5114100175', '2017-01-13', 745500, 'Doni', 3, NULL, '5114100175', 10000),
('SO58cf748e10ed6', '5114100017', '12345678909', '2017-03-24', 83750000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf74b0d7436', '5114100181', '5114100170', '2017-03-15', 975000, 'Tossa', 3, NULL, '5114100181', 5000),
('SO58cf74d66e8d8', '5114100017', '15114100012', '2017-03-24', 184250000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf74e9aae97', '5114100181', '5113100002', '2017-03-16', 4536000, 'Tossa', 3, NULL, '5114100181', 7500),
('SO58cf74f7a30cd', '5114100164', '0987654321', '2017-03-31', 72000000, 'Janet', 2, NULL, '5114100164', 10000),
('SO58cf75008ecbe', '5114100012', '5114100131', '2017-05-07', 1312500000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf751d41ecf', '5114100181', '5114100172', '2017-03-17', 9400000, 'Kontainer', 3, NULL, '5114100181', 100000),
('SO58cf7529244b7', '5114100017', '4114100012', '2017-03-26', 335000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf753a1b75b', '5114100014', '7655014565438895', '2017-03-21', 480000, 'Anggit', 3, NULL, '5114100014', 20000),
('SO58cf75bd5569b', '5114100017', '15114100181', '2017-03-27', 335000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf75e204d0d', '5114100181', '5114100012', '2017-03-18', 12512500, 'Kontainer', 3, NULL, '5114100181', 28000),
('SO58cf75f1b08b5', '5114100175', '5114100164', '2017-01-17', 745500, 'Doni', 3, NULL, '5114100175', 10000),
('SO58cf760c0c53d', '5114100181', '5114100012', '2017-03-18', 10010000, 'Kontainer', 3, NULL, '5114100181', 28000),
('SO58cf767035389', '5114100001', '5114100097', '2017-03-02', 15000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf769c41f83', '5114100151', 'co002', '2020-06-09', 300000000, 'wawan', 3, NULL, '5114100151', 50000),
('SO58cf76bfc26a3', '5114100012', '5114100033', '2017-05-08', 1012500000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf76f5942a6', '5114100181', '5114100170', '2017-03-23', 34000000, 'Kontainer', 3, NULL, '5114100181', 250000),
('SO58cf76fb515af', '5114100017', '15114100030', '2017-03-28', 335000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf772405cac', '5114100001', '5114100035', '2017-03-02', 11250000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf7730d6374', '5114100181', '5114100172', '2017-03-24', 18000000, 'Kontainer', 3, NULL, '5114100181', 180000),
('SO58cf7761dbf9b', '5114100017', '15114100130', '2017-03-29', 335000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf77b163a87', '5114100110', '74776466628', '2017-03-21', 350000000, 'Akbar', 3, NULL, '5114100110', 25000),
('SO58cf77c757da3', '5114100014', '123456789', '2017-03-22', 720000, 'Adit', 3, NULL, '5114100014', 20000),
('SO58cf77c789ca0', '5114100012', '5114100033', '2017-05-09', 1160000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf77cb2c59b', '5114100017', '12345678909', '2017-03-30', 335000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf77e1eb4b8', '5114100118', '5114100114', '2017-03-21', 480000, 'gojek', 3, NULL, '5114100118', 0),
('SO58cf77efe71fc', '5114100167', '4382479832', '2017-03-08', 480000, 'hanip', 3, NULL, '5114100167', 40000),
('SO58cf7803a64ef', '5114100110', '34353256', '2017-03-22', 350000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf780b630f2', '5114100001', '511510193302', '2017-03-02', 15000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58cf7813a5150', '5114100017', '4114100012', '2017-03-31', 335000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf78379e151', '5114100110', '432453254', '2017-03-23', 175000000, 'Akbar', 3, NULL, '5114100110', 30000),
('SO58cf78e842686', '5114100164', '74678833', '2017-04-03', 47760000, 'Janet', 3, NULL, '5114100164', 10000),
('SO58cf79a1dd650', '5114100172', '5114100085', '2017-03-06', 13000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf79ae260a7', '5114100181', '5113100002', '2017-03-24', 67200000, 'Kontainer', 3, NULL, '5114100181', 250000),
('SO58cf79c5e25b5', '5114100172', '5114100085', '2017-03-13', 0, 'PD.Jaya', 0, NULL, '5114100172', 5000),
('SO58cf79ff18e1d', '5114100181', '5114100012', '2017-03-26', 22976000, 'jne', 3, NULL, '5114100181', 100000),
('SO58cf7a2d6df8c', '5114100172', '5114100085', '2017-03-13', 13000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf7a2e478fc', '5114100181', '5114100172', '2017-03-27', 5292000, 'Tossa', 3, NULL, '5114100181', 50000),
('SO58cf7a7415df4', '5114100172', '5114100085', '2017-03-20', 13000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf7af14c976', '5114100172', '5114100085', '2017-03-27', 13000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf7b9fb5e94', '5114100012', '5114100181', '2017-06-03', 684000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf7bccf1a54', '5114100071', '5114100049', '2017-04-12', 4000000000, 'Dito', 3, NULL, '5114100071', 0),
('SO58cf7c023500f', '5114100172', '5114100185', '2017-03-15', 25000000, 'PD.Jaya', 3, NULL, '5114100172', 10000),
('SO58cf7c0b48952', '5114100071', '5114100068', '2017-04-27', 4000000000, 'Dito', 3, NULL, '5114100071', 0),
('SO58cf7c38a64ba', '5114100172', '5114100185', '2017-03-22', 25000000, 'PD.Jaya', 2, NULL, '5114100172', 10000),
('SO58cf7c593b6e0', '5114100181', '5114100170', '2017-03-31', 2280000, 'wahana', 3, NULL, '5114100181', 90000),
('SO58cf7c5c2d8d0', '5114100172', '5114100069', '2017-03-23', 7000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58cf7c9e25ed7', '5114100071', '5114100068', '2017-05-02', 4000000000, 'Dito', 3, NULL, '5114100071', 0),
('SO58cf7c9f0e90f', '5114100012', '5114100017', '2017-06-03', 693000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf7ccbaee79', '5114100071', '5114100049', '2017-05-03', 4000000000, 'Dito', 3, NULL, '5114100071', 0),
('SO58cf7d68b9f8c', '5114100017', '15114100030', '2017-04-02', 160000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf7d9e0f8f7', '5114100012', '5114100001', '2017-06-04', 711000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf7d9e4dbf4', '5114100071', '5114100068', '2017-06-04', 7000000000, 'Dito', 3, NULL, '5114100071', 0),
('SO58cf7db49a773', '5114100110', '7387136319', '2017-04-05', 370000000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58cf7dd5e59bd', '5114100017', '15114100181', '2017-04-04', 160000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf7deadb73f', '5114100065', 'adi', '2017-05-09', 4042000000, 'kurkur', 3, NULL, '5114100065', 6000000),
('SO58cf7dee92e9d', '5114100110', '34353256', '2017-04-08', 700000000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58cf7e17dd9d6', '5114100071', '5114100068', '2017-07-03', 3500000000, 'Dito', 3, NULL, '5114100071', 0),
('SO58cf7e25d463f', '5114100110', '123', '2017-03-09', 175000000, 'Akbar', 4, NULL, '5114100110', 35000),
('SO58cf7e3d2375b', '5114100181', '5113100002', '2017-04-05', 7440000, 'Kontainer', 3, NULL, '5114100181', 150000),
('SO58cf7e42bf8d8', '5114100017', '4114100012', '2017-04-06', 320000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf7e4ac7a62', '5114100071', '5114100049', '2017-07-14', 3000000000, 'Dito', 3, NULL, '5114100071', 0),
('SO58cf7e6f2c0d3', '5114100181', '5114100172', '2017-04-08', 18400000, 'Kontainer', 3, NULL, '5114100181', 125000),
('SO58cf7ea395761', '5114100110', '123', '2017-04-07', 175000000, 'Akbar', 3, NULL, '5114100110', 35000),
('SO58cf7eb895363', '5114100181', '5114100170', '2017-04-09', 1560000, 'Kontainer', 3, NULL, '5114100181', 10000),
('SO58cf7ed6dea27', '5114100017', '12345678909', '2017-03-09', 640000000, 'Pribadi', 0, NULL, '5114100017', 50000),
('SO58cf7f06c6c2f', '5114100181', '5114100012', '2017-04-13', 11200000, 'Kontainer', 3, NULL, '5114100181', 125000),
('SO58cf7f249d6db', '5114100151', 'co001', '2020-06-25', 1240000000, 'wawan', 3, NULL, '5114100151', 50000),
('SO58cf7f41de03f', '5114100017', '12345678909', '2017-04-09', 640000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf7fc416c7d', '5114100017', '4114100017', '2017-04-12', 320000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf7fcca0baa', '5114100012', '5114100001', '2017-06-07', 630000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf7fd410490', '5114100110', '123', '2017-04-11', 432000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf7ffd896f7', '5114100017', '15114100130', '2017-04-13', 320000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf8003133ac', '5114100110', '7387136319', '2017-04-13', 481000000, 'Akbar', 3, NULL, '5114100110', 35000),
('SO58cf801056102', '5114100065', '5114100180', '2017-06-14', 3675000000, 'adit', 2, NULL, '5114100065', 4000000),
('SO58cf80314e578', '5114100151', 'co001', '2020-06-27', 1320000000, 'wawan', 3, NULL, '5114100151', 50000),
('SO58cf804716452', '5114100017', '15114100001', '2017-03-15', 224000000, 'Pribadi', 0, NULL, '5114100017', 50000),
('SO58cf8061f32e9', '5114100012', '5114100131', '2017-06-07', 657000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf8075e0c2f', '5114100017', '15114100001', '2017-04-15', 224000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf8086702fb', '5114100151', 'co001', '2020-06-29', 3300000000, 'wawan', 3, NULL, '5114100151', 50000),
('SO58cf80b70a69b', '5114100017', '4114100012', '2017-04-15', 160000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf80be35f65', '5114100110', '123456', '2017-04-19', 555000000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58cf80d788746', '5114100065', '5114100180', '2017-06-15', 3675000000, 'adit', 3, NULL, '5114100065', 4000000),
('SO58cf80e65c797', '5114100110', '432453254', '2017-04-20', 400000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf8114cd2da', '5114100012', '5114100033', '2017-06-08', 666000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf81d244031', '5114100151', 'co001', '2021-07-25', 1650000000, 'wawan', 3, NULL, '5114100151', 50000),
('SO58cf81ef2820d', '5114100110', '34353256', '2017-04-26', 462500000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58cf82120d36b', '5114100110', '123', '2017-04-29', 462500000, 'Akbar', 3, NULL, '5114100110', 35000),
('SO58cf823d52375', '5114100151', '34562345', '2021-07-29', 3200000000, 'wawan', 3, NULL, '5114100151', 50000),
('SO58cf824c15866', '5114100017', '15114100012', '2017-04-20', 384000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf82ac281fa', '5114100017', '12345678909', '2017-04-22', 640000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58cf831d7291f', '5114100017', '15114100172', '2017-04-24', 480000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf8370f01c0', '5114100017', '15114100030', '2017-04-26', 480000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf83b216931', '5114100017', '15114100130', '2017-04-29', 320000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf83e6aa63e', '5114100181', '5114100170', '2017-04-16', 15660000, 'Kontainer', 3, NULL, '5114100181', 90000),
('SO58cf8419a9bcc', '5114100012', '5114100033', '2017-07-02', 465750000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf841cad7f7', '5114100181', '5114100172', '2017-04-17', 390000, 'Tossa', 3, NULL, '5114100181', 20000),
('SO58cf84327c55a', '5114100065', '5114100180', '2017-07-05', 4680000000, 'rifat', 3, NULL, '5114100065', 6000000),
('SO58cf8443286be', '5114100181', '5113100002', '2017-04-18', 720000, 'Tossa', 3, NULL, '5114100181', 10000),
('SO58cf84740c9fc', '5114100110', '432453254', '2017-05-10', 740000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf849cdf9ca', '5114100110', '34353256', '2017-05-13', 190000000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58cf84e9a4b8c', '5114100012', '5114100001', '2017-07-03', 472500000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf84f1b7488', '5114100110', '123', '2017-05-16', 360000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf851f7a488', '5114100110', '123456', '2017-05-18', 720000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf855a6210b', '5114100012', '5114100131', '2017-07-04', 519750000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf858d8d2f0', '5114100181', '5113100002', '2017-04-23', 10560000, 'Kontainer', 3, NULL, '5114100181', 100000),
('SO58cf859d4b2d9', '5114100110', '74776466628', '2017-05-16', 525000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf85be4349a', '5114100181', '5114100012', '2017-04-28', 1428000, 'Tossa', 3, NULL, '5114100181', 10000),
('SO58cf85d30f09c', '5114100110', '432453254', '2017-05-24', 555000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf85e07e89d', '5114100181', '5114100170', '2017-04-29', 513000, 'Tossa', 3, NULL, '5114100181', 8000),
('SO58cf85f015c92', '5114100012', '5114100181', '2017-07-05', 513000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf862231529', '5114100017', '12345678909', '2017-05-02', 672000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf864ce581d', '5114100017', '15114100181', '2017-05-07', 864000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58cf8898b5ac9', '5114100035', '52349235832958293', '2018-04-14', 180000000, 'faiq', 3, NULL, '5114100035', 150000),
('SO58cf88d0e59c3', '5114100073', '5114100101', '2017-03-11', 3132000000, 'Pribadi', 2, NULL, '5114100073', 300000),
('SO58cf88e6cc149', '5114100035', '081231248235832', '2017-03-14', 150000000, 'Wintolien', 0, NULL, '5114100035', 150000),
('SO58cf893e28912', '5114100035', '081231248235832', '2018-04-16', 150000000, 'wintolien2', 3, NULL, '5114100035', 150000),
('SO58cf89e7193bc', '5114100035', '2398523749237987', '2018-04-18', 150000000, 'faiq', 3, NULL, '5114100035', 200000),
('SO58cf8aff4daa1', '5114100035', '2398523749237987', '2019-05-09', 360000000, 'faiq', 3, NULL, '5114100035', 250000),
('SO58cf8b4e1cdcd', '5114100035', '523532458599', '2019-05-19', 150000000, 'trastian', 2, NULL, '5114100035', 300000),
('SO58cf8c12bdf13', '5114100035', '081231248235832', '2020-06-08', 210000000, 'botak', 3, NULL, '5114100035', 260000),
('SO58cf8c66ec035', '5114100035', '52349235832958293', '2020-06-11', 240000000, 'wintolien2', 3, NULL, '5114100035', 250000),
('SO58cf8d1e10768', '5114100035', '2398523749237987', '2021-07-16', 252000000, 'botak', 3, NULL, '5114100035', 250000),
('SO58cf8d586eaa4', '5114100035', '081231248235832', '2021-07-19', 240000000, 'Wintolien', 3, NULL, '5114100035', 300000),
('SO58cf913c313d6', '5114100181', '5114100170', '2017-03-19', 1035000000, 'Kontainer', 3, NULL, '5114100181', 300000),
('SO58cf93815ba7a', '5114100181', '5113100002', '2017-03-30', 720000000, 'Kontainer', 3, NULL, '5114100181', 300000),
('SO58cf952fa926f', '5114100181', '5114100012', '2017-03-22', 428040000, 'Kontainer', 3, NULL, '5114100181', 300000),
('SO58cf96922f6a5', '5114100181', '5114100012', '2017-04-01', 1239000000, 'Kontainer', 3, NULL, '5114100181', 300000),
('SO58cf985100942', '5114100181', '5114100170', '2017-04-06', 895000000, 'Kontainer', 3, NULL, '5114100181', 450000),
('SO58cf993d675b5', '5114100181', '5113100002', '2017-04-12', 543000000, 'Kontainer', 3, NULL, '5114100181', 378000),
('SO58cf9a7b95ac8', '5114100181', '5114100170', '2017-04-25', 286000000, 'Kontainer', 3, NULL, '5114100181', 320000),
('SO58cf9bc2232aa', '5114100110', '34353256', '2017-05-29', 525000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf9c14a0d7a', '5114100110', '74776466628', '2017-05-31', 555000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58cf9dcd2982c', '5114100181', '5114100172', '2017-05-01', 462800000, 'Kontainer', 3, NULL, '5114100181', 356000),
('SO58cf9e66bdb5c', '5114100012', '5114100131', '2017-07-08', 400000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf9ec7c123b', '5114100012', '5114100017', '2017-07-09', 405000000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf9f364f8ed', '5114100012', '5114100001', '2017-07-10', 693600000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cf9f9624f02', '5114100012', '5114100181', '2017-07-11', 676600000, 'Jne', 3, NULL, '5114100012', 50000),
('SO58cfbe8410052', '5114100100', '30', '2017-03-15', 448500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58cfbf2b08e6c', '5114100100', '19', '2017-03-17', 578000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d068a586f81', '5114100100', '10', '2017-03-19', 319000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d0691b8026b', '5114100100', '7', '2017-03-21', 448500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d06993b2f27', '5114100100', '13', '2017-03-23', 401700000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d069f6a6fbf', '5114100100', '99', '2017-03-25', 491300000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d06b85a69fc', '5114100100', '99', '2017-04-03', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d06c03c889c', '5114100100', '30', '2017-04-05', 1011500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d06caddf1db', '5114100100', '19', '2017-04-07', 772500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d06d2a9f139', '5114100100', '10', '2017-04-09', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d06d66bb19f', '5114100070', '5114100070', '2017-05-03', 156000000, 'Prasetyo', 3, NULL, '5114100070', 0),
('SO58d06dc4251dd', '5114100100', '7', '2017-04-11', 765600000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d06dd25a377', '5114100070', '5114100070', '2017-06-01', 186000000, 'Prasetyo', 3, NULL, '5114100070', 0),
('SO58d06e128f7c8', '5114100070', '5114100070', '2017-07-01', 180000000, 'Prasetyo', 3, NULL, '5114100070', 0),
('SO58d06e365a879', '5114100100', '13', '2017-04-13', 1040400000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d06f34b785e', '5114100100', '99', '2017-05-03', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d06f3a0d4b6', '5114100070', '5114100070', '2017-05-05', 1740000000, 'Prasetyo', 3, NULL, '5114100070', 0),
('SO58d06f70be35a', '5114100070', '5114100070', '2017-06-07', 1450000000, 'Prasetyo', 3, NULL, '5114100070', 0),
('SO58d06fcfd812b', '5114100070', '5114100070', '2017-07-04', 1650000000, 'Prasetyo', 3, NULL, '5114100070', 0),
('SO58d06fe98b405', '5114100100', '30', '2017-05-05', 982600000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d07078de243', '5114100175', '5114100175', '2017-01-20', 745500, 'Doni', 3, NULL, '5114100175', 10000),
('SO58d070bda4e05', '5114100100', '19', '2017-05-07', 803400000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d0712001b90', '5114100100', '10', '2017-05-09', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d071d21884f', '5114100100', '7', '2017-05-11', 772500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d072199e148', '5114100100', '13', '2017-05-13', 1011500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d0735375345', '5114100100', '99', '2017-06-03', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d073a1ce2a3', '5114100100', '30', '2017-06-05', 1156000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d073f7e6fb9', '5114100100', '19', '2017-06-07', 638000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d0745a44def', '5114100100', '10', '2017-06-09', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d074c624130', '5114100100', '7', '2017-06-11', 1011500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d07535d4b9a', '5114100100', '13', '2017-06-13', 797500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d076194f281', '5114100100', '99', '2017-07-03', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d0766b8600a', '5114100100', '30', '2017-07-05', 797500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d076cea7499', '5114100100', '19', '2017-07-07', 1011500000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d0778e4c735', '5114100100', '10', '2017-07-09', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d077fe4f182', '5114100100', '7', '2017-07-11', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d07843d236e', '5114100100', '13', '2017-07-13', 897000000, 'UBER', 3, NULL, '5114100100', 0),
('SO58d07c0176b10', '5114100181', '5114100170', '2017-05-02', 15120000, 'Tossa', 3, NULL, '5114100181', 150000),
('SO58d07d18ed4c5', '5114100175', '5114100164', '2017-01-24', 945000, 'Doni', 3, NULL, '5114100175', 10000),
('SO58d07d6884695', '5114100181', '5114100012', '2017-05-04', 320400000, 'Kontainer', 3, NULL, '5114100181', 230000),
('SO58d07d9bcd6dc', '5114100181', '5113100002', '2017-05-05', 282360000, 'Kontainer', 3, NULL, '5114100181', 300000),
('SO58d07e2327851', '5114100175', '5114100175', '2017-01-26', 945000, 'Doni', 3, NULL, '5114100175', 10000),
('SO58d07e42e8b16', '5114100181', '5114100170', '2017-05-06', 118080000, 'Kontainer', 3, NULL, '5114100181', 180000),
('SO58d07e784a15c', '5114100181', '5114100172', '2017-03-08', 183750000, 'Kontainer', 0, NULL, '5114100181', 230000),
('SO58d07f47a6ea1', '5114100181', '5114100172', '2017-05-08', 183750000, 'Kontainer', 3, NULL, '5114100181', 230000),
('SO58d07f7a1a31e', '5114100181', '5114100172', '2017-05-09', 3675000, 'Tossa', 3, NULL, '5114100181', 30000),
('SO58d08145eac30', '5114100175', '5114100164', '2017-02-03', 945000, 'Doni', 3, NULL, '5114100175', 15000),
('SO58d0819b807f3', '5114100181', '5114100012', '2017-05-12', 358000000, 'Kontainer', 3, NULL, '5114100181', 230000),
('SO58d081d869c25', '5114100181', '5113100002', '2017-05-13', 44400000, 'Kontainer', 3, NULL, '5114100181', 135000),
('SO58d0820632b96', '5114100175', '5114100175', '2017-02-04', 945000, 'Doni', 3, NULL, '5114100175', 15000),
('SO58d082b0b25da', '5114100181', '5114100172', '2017-05-15', 208800000, 'Kontainer', 3, NULL, '5114100181', 230000),
('SO58d08335e2cdd', '5114100181', '5114100170', '2017-05-17', 393800000, 'Kontainer', 3, NULL, '5114100181', 320000),
('SO58d0834c66451', '5114100175', '5114100164', '2017-02-10', 945000, 'Doni', 3, NULL, '5114100175', 15000),
('SO58d083c7a993b', '5114100175', '5114100175', '2017-02-13', 945000, 'Doni', 3, NULL, '5114100175', 15000),
('SO58d08478f37ee', '5114100181', '5114100012', '2017-05-25', 108000000, 'Kontainer', 3, NULL, '5114100181', 200000),
('SO58d0849d4dfd5', '5114100181', '5113100002', '2017-05-29', 92000000, 'Kontainer', 3, NULL, '5114100181', 180000),
('SO58d084c4e0787', '5114100181', '5114100172', '2017-05-30', 43616000, 'Kontainer', 3, NULL, '5114100181', 123000),
('SO58d08535148fd', '5114100175', '5114100164', '2017-02-16', 945000, 'Doni', 3, NULL, '5114100175', 15000),
('SO58d0869ba515b', '5114100175', '5114100175', '2017-02-19', 945000, 'Doni', 3, NULL, '5114100175', 15000),
('SO58d0875ec8f1a', '5114100181', '5114100170', '2017-06-03', 857500000, 'Kontainer', 3, NULL, '5114100181', 340000),
('SO58d0878abdb3d', '5114100181', '5114100172', '2017-06-06', 120240000, 'Kontainer', 3, NULL, '5114100181', 390000),
('SO58d087c00ec9a', '5114100181', '5114100172', '2017-06-06', 180000000, 'Kontainer', 3, NULL, '5114100181', 189000),
('SO58d087c61c4ea', '5114100050', '10172', '2017-04-05', 2160000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d08809092d9', '5114100175', '5114100164', '2017-02-27', 945000, 'Doni', 3, NULL, '5114100175', 15000),
('SO58d08815ed6a3', '5114100181', '5114100012', '2017-06-06', 630000000, 'Kontainer', 3, NULL, '5114100181', 410000),
('SO58d088176adc7', '5114100050', '10171', '2017-04-14', 930000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d088b71c8e9', '5114100175', '5114100175', '2017-02-28', 945000, 'Doni', 3, NULL, '5114100175', 15000),
('SO58d0893293e88', '5114100050', '10172', '2017-04-25', 2030000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d08982b4712', '5114100181', '5114100172', '2017-06-11', 72000000, 'Kontainer', 3, NULL, '5114100181', 180000),
('SO58d089b523334', '5114100181', '5113100002', '2017-06-13', 508500000, 'Kontainer', 3, NULL, '5114100181', 310000),
('SO58d08a1609cc2', '5114100181', '5114100170', '2017-06-20', 178400000, 'Kontainer', 3, NULL, '5114100181', 123000),
('SO58d08ab7da9e7', '5114100050', '10172', '2017-05-09', 3180000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d08ae24c78b', '5114100118', '5114100114', '2017-03-29', 1500000, 'jne', 3, NULL, '5114100118', 20000),
('SO58d08b6ba36b2', '5114100050', '10171', '2017-05-15', 620000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d08bbd85f84', '5114100118', '5114100114', '2017-04-04', 12000000, 'gojek', 3, NULL, '5114100118', 30000),
('SO58d08c03059bb', '5114100050', '10171', '2017-05-19', 620000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d08c695aecd', '5114100050', '10172', '2017-05-24', 600000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d090913ce51', '5114100050', '10172', '2017-06-01', 1800000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d099c63b237', '5114100118', '5114100114', '2017-04-25', 3000000, 'gojek', 3, NULL, '5114100118', 10000),
('SO58d09b56e8109', '5114100118', '5114100114', '2017-04-28', 9000000, 'gojek', 3, NULL, '5114100118', 3000),
('SO58d09bce82f5d', '5114100118', '5114100114', '2017-05-11', 9000000, 'gojek', 3, NULL, '5114100118', 20000),
('SO58d09c501e1c9', '5114100118', '5114100114', '2017-05-25', 6000000, 'gojek', 3, NULL, '5114100118', 3000),
('SO58d09c99cfb3e', '5114100175', '5114100164', '2017-03-08', 3780000, 'Doni', 3, NULL, '5114100175', 30000),
('SO58d09ca1004d3', '5114100118', '5114100114', '2017-06-01', 750000, 'gojek', 3, NULL, '5114100118', 1000),
('SO58d09d47ef2c5', '5114100118', '5114100114', '2017-06-13', 600000, 'gojek', 3, NULL, '5114100118', 1000),
('SO58d09dfb3415d', '5114100175', '5114100175', '2017-03-23', 3780000, 'Doni', 3, NULL, '5114100175', 30000),
('SO58d09f343551a', '5114100118', '5114100114', '2017-06-28', 6000000, 'gojek', 3, NULL, '5114100118', 20000),
('SO58d09fba2396f', '5114100118', '5114100114', '2017-07-06', 4500000, 'gojek', 3, NULL, '5114100118', 1000),
('SO58d0a0435e771', '5114100118', '5114100114', '2017-07-07', 1500000, 'gojek', 3, NULL, '5114100118', 2000),
('SO58d0a0fa9fb63', '5114100175', '5114100164', '2017-04-11', 3780000, 'Doni', 3, NULL, '5114100175', 30000),
('SO58d0a1d8e4207', '5114100175', '5114100175', '2017-04-26', 3780000, 'Doni', 3, NULL, '5114100175', 30000),
('SO58d0a2b49d95c', '5114100118', '5114100114', '2017-07-14', 1500000, 'gojek', 3, NULL, '5114100118', 999),
('SO58d0a33b8806c', '5114100118', '5114100114', '2017-08-03', 1500000, 'gojek', 3, NULL, '5114100118', 1000),
('SO58d0a33caa8e5', '5114100175', '5114100164', '2017-04-19', 7560000, 'Doni', 0, NULL, '5114100175', 60000),
('SO58d0a3827c933', '5114100175', '5114100164', '2017-05-15', 7560000, 'Doni', 3, NULL, '5114100175', 60000),
('SO58d0c318d1f34', '5114100001', '5114100171', '2017-03-02', 22500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d0c39f329e1', '5114100001', '5114100173', '2017-03-03', 37500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d0c3cdaf219', '5114100001', '5114100800', '2017-03-03', 42500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d0c3e7de572', '5114100050', '10171', '2017-06-13', 1860000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d0c4098bfb1', '5114100001', '5114100083', '2017-03-03', 22500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d0c43a94d8e', '5114100050', '10172', '2017-06-22', 1830000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d0c4430f77a', '5114100001', '5114100040', '2017-03-03', 15000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d0c4927538e', '5114100001', '51141000121', '2017-03-03', 7500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d0c5d13aa06', '5114100167', '4382479832', '2017-03-15', 960000, 'rian', 3, NULL, '5114100167', 50000),
('SO58d0c6ad7d045', '5114100050', '10172', '2017-07-03', 3975000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d0c71880793', '5114100001', '5114100044', '2017-03-04', 75000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d0c74901313', '5114100050', '10171', '2017-07-16', 480000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d0c7b9f1b67', '5114100050', '10172', '2017-07-23', 480000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d0c85f492fd', '5114100001', '5114100035', '2017-03-04', 0, 'Pribadi', 2, NULL, '5114100001', 0),
('SO58d0c88dbeeea', '5114100001', '5114100037', '2017-03-04', 37500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d0c8fdcf435', '5114100167', '4382479832', '2017-03-25', 3360000, 'rian', 3, NULL, '5114100167', 20000),
('SO58d0c9497ce1a', '5114100050', '10172', '2017-08-06', 1960000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d0c97b0d197', '5114100167', '3467823648', '2017-03-31', 3024000, 'rian', 3, NULL, '5114100167', 50000),
('SO58d0c997c7f81', '5114100050', '10171', '2017-08-14', 1890000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d0c9b540f7d', '5114100017', '15114100030', '2017-05-13', 640000000, 'Pribadi', 0, NULL, '5114100017', 0),
('SO58d0c9c9a61f5', '5114100167', '3467823648', '2017-04-07', 3360000, 'rian', 3, NULL, '5114100167', 50000),
('SO58d0c9db6feb3', '5114100050', '10172', '2017-08-21', 1200000000, 'Reynaldo', 3, NULL, '5114100050', 0),
('SO58d0ca1638312', '5114100167', '3467823648', '2017-04-14', 3360000, 'rian', 3, NULL, '5114100167', 50000),
('SO58d0ca9a4a192', '5113100150', '000003', '2017-03-29', 1750000, 'aaa', 0, NULL, '5113100150', 0),
('SO58d0cb183b25a', '5114100167', '4382479832', '2017-04-21', 3600000, 'rian', 3, NULL, '5114100167', 50000),
('SO58d0cb5e63b7c', '5114100167', '3467823648', '2017-04-28', 2880000, 'fian', 3, NULL, '5114100167', 50000),
('SO58d0cbb60cd85', '5114100167', '4382479832', '2017-05-07', 6480000, 'rian', 3, NULL, '5114100167', 50000),
('SO58d0cbd9ab009', '5114100017', '15114100030', '2017-05-13', 640000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0cc6339215', '5114100167', '4382479832', '2017-05-21', 6480000, 'jene', 3, NULL, '5114100167', 50000),
('SO58d0ccbfbffcd', '5114100167', '4382479832', '2017-06-04', 7200000, 'rian', 3, NULL, '5114100167', 50000),
('SO58d0cd0e5edd0', '5114100017', '12345678909', '2017-03-18', 896000000, 'Pribadi', 0, NULL, '5114100017', 0),
('SO58d0cd329eb6b', '5114100017', '12345678909', '2017-05-18', 896000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0cd885cd84', '5114100167', '4382479832', '2017-06-14', 12960000, 'rian', 3, NULL, '5114100167', 50000),
('SO58d0ce15c70dd', '5114100167', '3467823648', '2017-07-12', 11520000, 'jian', 3, NULL, '5114100167', 50000),
('SO58d0ce9d30ff1', '5114100167', '5145784564843415', '2017-08-16', 14400000, 'rian', 3, NULL, '5114100167', 50000),
('SO58d0cfb71888e', '5114100017', '4114100012', '2017-06-03', 450000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0d0061c924', '5114100017', '4114100017', '2017-06-06', 450000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58d0d073ef2e6', '5114100017', '15114100130', '2017-06-11', 540000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0d19deee77', '5114100017', '15114100001', '2017-06-18', 900000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0d1edaf070', '5114100017', '15114100181', '2017-06-21', 540000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58d0d282320ff', '5114100017', '15114100172', '2017-06-26', 150000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0d2e22951c', '5114100017', '4114100012', '2017-06-29', 690000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0d31dd7917', '5114100017', '12345678909', '2017-06-30', 600000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58d0d549ac209', '5114100017', '4114100012', '2017-07-03', 900000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0d59794e0d', '5114100017', '15114100001', '2017-07-13', 1044000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0d66667e4d', '5114100017', '1029384756', '2017-07-16', 1296000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0d6a676dae', '5114100017', '15114100130', '2017-07-20', 1296000000, 'Pribadi', 3, NULL, '5114100017', 0),
('SO58d0e2b094fcc', '5114100164', '0987654321', '2017-04-26', 24000000, 'Janet', 3, NULL, '5114100164', 10000),
('SO58d0e306c690f', '5114100164', '74678833', '2017-04-27', 24000000, 'Ani', 3, NULL, '5114100164', 10000),
('SO58d0e3bab5140', '5114100164', '0987654321', '2017-05-26', 55200000, 'Ani', 3, NULL, '5114100164', 10000),
('SO58d0e3eb48778', '5114100164', '74678833', '2017-05-28', 24000000, 'Paul', 3, NULL, '5114100164', 10000),
('SO58d0e4e752ff7', '5114100164', '0987654321', '2017-06-26', 48000000, 'Janet', 3, NULL, '5114100164', 10000),
('SO58d0e52775365', '5114100164', '74678833', '2017-06-28', 60000000, 'Ani', 3, NULL, '5114100164', 10000),
('SO58d0e5a694113', '5114100164', '0987654321', '2017-07-26', 72000000, 'Paul', 3, NULL, '5114100164', 10000),
('SO58d0e5e83c657', '5114100164', '74678833', '2017-07-30', 72000000, 'Ani', 3, NULL, '5114100164', 10000),
('SO58d0f0f7b4f06', '5114100017', '4114100017', '2017-07-30', 648000000, 'Pribadi', 3, NULL, '5114100017', 50000),
('SO58d11b825d0b0', '5114100001', '5114100177', '2017-03-05', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d11d0e1347e', '5114100001', '51141000170', '2017-03-07', 137500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d11da8c3cb4', '5114100001', '5114100097', '2017-03-06', 100000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d11e79ad08c', '5114100001', '5114100800', '2017-03-08', 137500000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d11ff766310', '5114100001', '5114100035', '2017-03-09', 112500000, 'Pribadi', 2, NULL, '5114100001', 0),
('SO58d1258698ff7', '5114100110', '7387136319', '2017-06-07', 450000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58d125a6a2e34', '5114100110', '123456', '2017-06-10', 462500000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58d125c8814fc', '5114100110', '123', '2017-06-13', 360000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58d125e5ce874', '5114100110', '34353256', '2017-06-15', 555000000, 'Akbar', 3, NULL, '5114100110', 25000),
('SO58d12671892b4', '5114100110', '432453254', '2017-06-20', 700000000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58d1268da758a', '5114100110', '123456', '2017-06-23', 540000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58d126bc7bcf2', '5114100110', '123', '2017-06-24', 540000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58d126db76cae', '5114100110', '34353256', '2017-06-28', 555000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58d127263d068', '5114100001', '5114100037', '2017-03-11', 150000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1286960481', '5114100172', '5114100135', '2017-03-02', 12000000, 'PD.Jaya', 0, NULL, '5114100172', 5000),
('SO58d12947edaf7', '5114100110', '432453254', '2017-07-05', 520000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58d129645361d', '5114100110', '123', '2017-07-08', 456000000, 'Akbar', 3, NULL, '5114100110', 50000),
('SO58d1299822fb5', '5114100172', '5114100135', '2017-04-02', 12000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d129c738541', '5114100110', '123456', '2017-07-12', 540000000, 'Akbar', 3, NULL, '5114100110', 25000),
('SO58d129ff552e2', '5114100110', '74776466628', '2017-07-14', 700000000, 'Akbar', 3, NULL, '5114100110', 25000),
('SO58d12a46d81d9', '5114100110', '34353256', '2017-07-18', 340000000, 'Akbar', 3, NULL, '5114100110', 50000);
INSERT INTO `penjualan` (`id_so`, `id_petugas`, `id_customer`, `tanggal`, `total`, `kurir`, `status`, `tanggal_keluar`, `id_pemilik`, `ongkir`) VALUES
('SO58d12a4978208', '5114100172', '5114100135', '2017-04-09', 12000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d12a5b69458', '5114100001', '5114100031', '2017-03-11', 100000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d12a632c092', '5114100110', '432453254', '2017-07-20', 360000000, 'Akbar', 3, NULL, '5114100110', 25000),
('SO58d12abec80fe', '5114100110', '123456', '2017-07-28', 555000000, 'Akbar', 3, NULL, '5114100110', 0),
('SO58d12b0094faa', '5114100172', '5114100191', '2017-04-03', 15000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d12b6fb5bb7', '5114100172', '5114100191', '2017-04-10', 15000000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d12ba3d0d86', '5114100172', '5114100185', '2017-04-04', 20000000, 'PD.Jaya', 3, NULL, '5114100172', 10000),
('SO58d12bc42cb9c', '5114100172', '5114100185', '2017-04-11', 20000000, 'PD.Jaya', 2, NULL, '5114100172', 10000),
('SO58d12d49e1c6c', '5114100172', '5114100185', '2017-04-12', 20000000, 'PD.Jaya', 4, NULL, '5114100172', 10000),
('SO58d1312e23794', '5114100172', '5114100085', '2017-04-17', 6650000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d131530dfe7', '5114100172', '5114100085', '2017-04-24', 6650000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d1317d9b470', '5114100172', '5114100069', '2017-04-18', 9500000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d131a5a7147', '5114100172', '5114100130', '2017-04-18', 9500000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d131f3b5174', '5114100172', '5114100176', '2017-04-19', 14250000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d13218a1b28', '5114100172', '5114100185', '2017-04-26', 23750000, 'PD.Jaya', 3, NULL, '5114100172', 10000),
('SO58d13246da610', '5114100172', '5114100135', '2017-04-25', 19000000, 'PD.Jaya', 3, NULL, '5114100172', 10000),
('SO58d1333c47dc3', '5114100001', '51141000170', '2017-03-12', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d133b9ccc8f', '5114100172', '5114100191', '2017-05-02', 28500000, 'PD.Jaya', 3, NULL, '5114100172', 15000),
('SO58d133d4efd58', '5114100172', '5114100191', '2017-05-09', 28500000, 'PD.Jaya', 3, NULL, '5114100172', 15000),
('SO58d133eb308b6', '5114100001', '5114100171', '2017-03-13', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1341ae10ee', '5114100001', '5114100097', '2017-03-14', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1342549d25', '5114100172', '5114100135', '2017-05-04', 23750000, 'PD.Jaya', 3, NULL, '5114100172', 10000),
('SO58d1346142c46', '5114100172', '5114100135', '2017-05-04', 23750000, 'PD.Jaya', 0, NULL, '5114100172', 10000),
('SO58d1352c62557', '5114100172', '5114100185', '2017-05-16', 33250000, 'PD.Jaya', 3, NULL, '5114100172', 15000),
('SO58d13558bcabb', '5114100172', '5114100185', '2017-05-24', 33250000, 'PD.Jaya', 3, NULL, '5114100172', 15000),
('SO58d13670ee217', '5114100172', '5114100135', '2017-06-02', 19000000, 'PD.Jaya', 3, NULL, '5114100172', 10000),
('SO58d13694781e4', '5114100172', '5114100191', '2017-06-09', 19000000, 'PD.Jaya', 3, NULL, '5114100172', 10000),
('SO58d136acbbc80', '5114100172', '5114100185', '2017-06-16', 19000000, 'PD.Jaya', 3, NULL, '5114100172', 10000),
('SO58d137179ef28', '5114100172', '5114100130', '2017-06-23', 0, 'PD.Jaya', 0, NULL, '5114100172', 10000),
('SO58d1372fd7dec', '5114100172', '5114100130', '2017-05-26', 19000000, 'PD.Jaya', 0, NULL, '5114100172', 10000),
('SO58d13772d4009', '5114100172', '5114100130', '2017-06-23', 19000000, 'PD.Jaya', 3, NULL, '5114100172', 10000),
('SO58d137c6720f7', '5114100172', '5114100069', '2017-06-27', 7220000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d1387ec5668', '5114100172', '5114100176', '2017-07-02', 13110000, 'PD.Jaya', 3, NULL, '5114100172', 5000),
('SO58d138ae7ded3', '5114100172', '5114100191', '2017-07-03', 36000000, 'PD.Jaya', 3, NULL, '5114100172', 20000),
('SO58d138b3ba8dd', '5114100001', '5114100044', '2017-03-15', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d138f936065', '5114100001', '5114100115', '2017-03-16', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13973d9582', '5114100172', '5114100135', '2017-07-17', 72000000, 'PD.Jaya', 3, NULL, '5114100172', 20000),
('SO58d139a11d55b', '5114100001', '5114100083', '2017-03-17', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13a0690036', '5114100001', '511510193302', '2017-03-18', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13a440210b', '5114100172', '5114100185', '2017-07-11', 30000000, 'PD.Jaya', 3, NULL, '5114100172', 15000),
('SO58d13a5607847', '5114100001', '5114100097', '2017-03-19', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13a8944651', '5114100001', '5114100031', '2017-03-20', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13a9f0cef2', '5114100172', '5114100135', '2017-07-12', 30000000, 'PD.Jaya', 3, NULL, '5114100172', 15000),
('SO58d13aecc2ccc', '5114100001', '5114100031', '2017-03-21', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13afda13d7', '5114100172', '5114100185', '2017-07-19', 45000000, 'PD.Jaya', 3, NULL, '5114100172', 15000),
('SO58d13b172acc6', '5114100001', '51141000121', '2017-03-22', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13b1a39eb7', '5114100172', '5114100069', '2017-07-25', 40000000, 'PD.Jaya', 4, NULL, '5114100172', 20000),
('SO58d13b71bbbf5', '5114100001', '51141000121', '2017-03-23', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13b7603c4c', '5114100172', '5114100069', '2017-07-25', 37800000, 'PD.Jaya', 3, NULL, '5114100172', 15000),
('SO58d13be1218bf', '5114100001', '5114100035', '2017-03-24', 0, 'Pribadi', 0, NULL, '5114100001', 0),
('SO58d13c325a8df', '5114100001', '5114100035', '2017-03-24', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13c69627ca', '5114100001', '51141000170', '2017-03-25', 0, 'Pribadi', 0, NULL, '5114100001', 0),
('SO58d13c864a2a1', '5114100001', '51141000170', '2017-03-25', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13cd5a839b', '5114100001', '5114100177', '2017-03-26', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13d04a3630', '5114100001', '5114100171', '2017-03-27', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13d444b3bd', '5114100001', '5114100115', '2017-03-28', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13d9168dd6', '5114100001', '5114100040', '2017-03-29', 0, 'Pribadi', 0, NULL, '5114100001', 0),
('SO58d13db151db8', '5114100001', '5114100040', '2017-03-29', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13e19c0a65', '5114100001', '5114100083', '2017-03-30', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13e6ce537b', '5114100001', '5114100044', '2017-03-31', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d13fd935d11', '5114100001', '5114100800', '2018-04-01', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1410ee1acd', '5114100001', '5114100173', '2018-04-02', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d14155d4246', '5114100051', '5116100016', '2017-03-22', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d1418a55289', '5114100001', '5114100037', '2018-04-03', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d141ed22a4d', '5114100051', '5116100007', '2017-03-22', 1550000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d1428ba5743', '5114100001', '5114100044', '2018-04-04', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d142dd20482', '5114100001', '5114100083', '2018-04-05', 125000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d143810b37e', '5114100051', '5116100003', '2017-03-22', 210000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d144942dd7d', '5114100051', '5116100015', '2017-03-23', 1581000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d144e64923b', '5114100001', '5114100040', '2017-03-06', 130000000, 'Pribadi', 0, NULL, '5114100001', 0),
('SO58d1451f1d5e0', '5114100001', '5114100040', '2018-04-06', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1475685ef7', '5114100001', '5114100115', '2018-04-07', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d147ae6399e', '5114100001', '5114100171', '2018-04-08', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d147fbce462', '5114100001', '5114100171', '2018-04-09', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1485aad391', '5114100001', '5114100177', '2018-04-10', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1496c96a1b', '5114100001', '51141000170', '2018-04-11', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d14a5ef239c', '5114100001', '5114100035', '2018-04-12', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d14b721f24d', '5114100001', '51141000121', '2018-04-13', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d14c3c6cd30', '5114100001', '5114100031', '2018-04-14', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d14d4e924e8', '5114100001', '5114100031', '2018-04-14', 130000000, 'Pribadi', 0, NULL, '5114100001', 0),
('SO58d14d7518653', '5114100001', '5114100097', '2018-04-15', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1513095447', '5114100001', '511510193302', '2018-04-16', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1516de4569', '5114100001', '5114100097', '2018-04-17', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1524fd6450', '5114100001', '5114100031', '2018-04-18', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d152e034f45', '5114100051', '5114100018', '2017-04-02', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d153281dd66', '5114100051', '5114100014', '2017-04-03', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d1538d8b4a2', '5114100051', '5116100008', '2017-04-04', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d153aa410d0', '5114100001', '5114100800', '2018-04-19', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d153d2c98f9', '5114100051', '5116100012', '2017-04-05', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d1542b1eba4', '5114100051', '5116100016', '2017-04-05', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d1547c85d16', '5114100001', '5114100173', '2018-04-20', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d154aa41ee7', '5114100051', '5116100002', '2017-04-06', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d154ebcb29a', '5114100051', '5116100009', '2017-04-09', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d1555322b10', '5114100001', '5114100037', '2018-04-21', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1556ad5523', '5114100051', '5116100010', '2017-04-10', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d155a436d23', '5114100051', '5116100005', '2017-04-10', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d155a934bf7', '5114100001', '5114100044', '2018-04-22', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d15614b540f', '5114100051', '5116100015', '2017-04-10', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d1561e120f2', '5114100001', '5114100083', '2018-04-23', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d1566ce087e', '5114100001', '5114100040', '2018-04-24', 130000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d157c3f11d0', '5114100051', '5116100017', '2017-05-02', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d15868782f8', '5114100051', '5114100018', '2017-05-02', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d3835050b32', '511400704', '5114100108', '2017-03-25', 9000000, 'iwan', 3, NULL, '511400704', 0),
('SO58d38394d3a68', '511400704', '521000001', '2017-03-27', 17400000, 'iwan', 3, NULL, '511400704', 50000),
('SO58d3855391075', '511400704', '5114100809', '2017-03-29', 3000000, 'irawan', 3, NULL, '511400704', 0),
('SO58d3866f5a4e0', '511400704', '5114100108', '2017-03-31', 11600000, 'iwan', 3, NULL, '511400704', 0),
('SO58d3912a34d6f', '511400704', '5114100108', '2018-03-01', 9000000, 'irawan', 3, NULL, '511400704', 0),
('SO58d3933776bdf', '511400704', '5114100108', '2019-06-01', 27000000, 'irawan', 3, NULL, '511400704', 0),
('SO58d5e0860cd0a', '511400704', '3112100002', '2020-03-01', 6000000, 'iwan', 3, NULL, '511400704', 0),
('SO58d604fb30a9f', '511400704', '5114100809', '2019-12-01', 26100000, 'iwan', 3, NULL, '511400704', 0),
('SO58d605866c13f', '511400704', '3112100002', '2021-01-01', 23200000, 'irawan', 3, NULL, '511400704', 0),
('SO58d605dc1c327', '511400704', '3112100002', '2021-01-01', 16800000, 'iwan', 3, NULL, '511400704', 0),
('SO58d609579898f', '511400704', '521000001', '2022-02-02', 14000000, 'iwan', 3, NULL, '511400704', 0),
('SO58d871d74cd82', '5113100028', '5113100160', '2017-03-10', 60000, 'lala', 2, NULL, '5113100028', 0),
('SO58d87378a24e3', '5113100028', '5113100160', '2017-04-07', 6000, 'lala', 2, NULL, '5113100028', 0),
('SO58d87c8104512', '5114100064', '13567', '2017-03-14', 3017700000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d87ce948a55', '5114100064', '17288', '2017-03-26', 3082000000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d87db89909b', '5114100064', '14070', '2017-04-04', 2550000000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d87e1a4b00b', '5114100064', '19023', '2017-04-23', 3186800000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d87eef6ed58', '5114100064', '100086', '2017-05-05', 2697000000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d87f5419226', '5114100064', '13567', '2017-05-19', 2607000000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d8803b80bbd', '5114100064', '19023', '2017-06-09', 1657500000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d880a435f97', '5114100064', '14070', '2017-06-23', 3622600000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d881ba6e8f1', '5114100064', '100086', '2017-07-11', 2632000000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d882182668f', '5114100064', '13567', '2017-07-26', 2485700000, 'Steven kurniawan', 3, NULL, '5114100064', 0),
('SO58d885b47e51a', '5114100051', '5114100014', '2017-05-10', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d885eea02c3', '5114100051', '5116100016', '2017-05-11', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d8862329359', '5114100051', '5116100001', '2017-05-12', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88659c597f', '5114100051', '5116100005', '2017-05-13', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d8868c0da3f', '5114100051', '5116100003', '2017-05-14', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d886bee3827', '5114100051', '5116100006', '2017-05-15', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d886f2e834d', '5114100051', '5116100010', '2017-05-16', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d8878fac340', '5114100051', '5116100002', '2017-06-05', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d887bca2a0c', '5114100051', '5114100013', '2017-06-05', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d887fbcf229', '5114100051', '5116100008', '2017-06-05', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d8882cbe090', '5114100051', '5116100012', '2017-06-05', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88862c408a', '5114100051', '5116100015', '2017-06-05', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d8888bd2a2b', '5114100051', '5116100001', '2017-06-05', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d888bb01291', '5114100051', '5116100017', '2017-06-05', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d888f18f63d', '5114100051', '5114100014', '2017-06-05', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d8891f08ff9', '5114100051', '5116100004', '2017-06-05', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d8894909c83', '5114100051', '5116100003', '2017-06-05', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d889be7d7bd', '5114100051', '5116100004', '2017-07-02', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d889e87fc58', '5114100051', '5116100011', '2017-07-02', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88a1e82af2', '5114100051', '5116100016', '2017-07-02', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88a4632ccb', '5114100051', '5116100002', '2017-07-02', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88a6e2e37e', '5114100051', '5116100003', '2017-07-02', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88a99654a0', '5114100051', '5116100009', '2017-07-02', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88ad0c7cf0', '5114100051', '5116100005', '2017-07-02', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88afad5039', '5114100051', '5116100010', '2017-07-02', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88b28385ca', '5114100051', '5116100008', '2017-07-02', 837000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88b512ad74', '5114100051', '5114100018', '2017-07-02', 810000000, 'Doomling', 3, NULL, '5114100051', 100000),
('SO58d88ddbc54d2', '5114100180', '5114100753', '2017-04-06', 11000000, 'Rujito', 3, NULL, '5114100180', 100000),
('SO58d88e0dd98ff', '5114100180', '5114100167', '2017-04-20', 11500000, 'Nuga', 3, NULL, '5114100180', 100000),
('SO58d88e8c80833', '5114100180', '5114100188', '2017-04-30', 7700000, 'Rujito', 3, NULL, '5114100180', 70000),
('SO58d88fbb5026c', '5114100180', '5114100167', '2017-05-13', 15750000, 'Nuga', 3, NULL, '5114100180', 150000),
('SO58d88ff21f5f8', '5114100180', '5114100753', '2017-05-27', 17500000, 'Nuga', 3, NULL, '5114100180', 170000),
('SO58d890c7b3639', '5114100180', '5114100753', '2017-06-09', 6700000, 'Nuga', 3, NULL, '5114100180', 30000),
('SO58d890f78f66d', '5114100180', '5114100188', '2017-06-16', 9490000, 'Nuga', 3, NULL, '5114100180', 40000),
('SO58d89129d8ea5', '5114100180', '5114100753', '2017-06-23', 7475000, 'Rujito', 3, NULL, '5114100180', 30000),
('SO58d8923718011', '5114100180', '5114100167', '2017-07-11', 39900000, 'Nuga', 3, NULL, '5114100180', 320000),
('SO58d8929830961', '5114100180', '5114100753', '2017-08-15', 31476000, 'Nuga', 3, NULL, '5114100180', 170000),
('SO58d895d3d66c0', '5114100001', '5114100044', '2018-04-29', 4680000000, 'Pribadi', 4, NULL, '5114100001', 0),
('SO58d896271495f', '5114100001', '5114100044', '2018-04-29', 468000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d89678ed1f7', '5114100001', '5114100083', '2019-05-01', 4050000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d896d842b98', '5114100181', '5114100172', '2017-07-04', 532500000, 'JNE', 3, NULL, '5114100181', 90000),
('SO58d89733ba872', '5114100181', '5113100002', '2017-07-16', 813050000, 'JNE', 3, NULL, '5114100181', 80000),
('SO58d8973b55c90', '5114100001', '5114100097', '2020-06-29', 4200000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d897b659458', '5114100181', '5114100170', '2017-07-19', 108000000, 'wahana', 3, NULL, '5114100181', 0),
('SO58d897d0c9eed', '5114100001', '5114100035', '2021-07-28', 4350000000, 'Pribadi', 3, NULL, '5114100001', 0),
('SO58d897d8c718a', '5114100181', '5114100170', '2017-07-20', 71600000, 'Tossa', 3, NULL, '5114100181', 89000),
('SO58d897d8ccfd9', '511400704', '521000001', '2021-01-01', 23200000, 'iwan', 3, NULL, '511400704', 0),
('SO58d898474a2c3', '511400704', '5114100108', '2018-03-01', 10500000, 'iwan', 2, NULL, '511400704', 0),
('SO58d898569d0ad', '5114100181', '5114100012', '2017-07-26', 630000000, 'Tossa', 3, NULL, '5114100181', 70000),
('SO58d8994999fb6', '5114100151', 'co002', '2017-03-05', 2400000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d899af5dcea', '5114100151', 'co001', '2017-03-05', 1360000000, 'wawan', 3, NULL, '5114100151', 50000),
('SO58d89a46796ae', '5114100151', 'co001', '2017-03-25', 3500000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d89acdbc8c2', '5114100014', '123456789', '2017-03-31', 8400000, 'adi', 2, NULL, '5114100014', 200000),
('SO58d89afd65ceb', '5114100014', '123456789', '2017-03-31', 4800000, 'ADI', 3, NULL, '5114100014', 200000),
('SO58d89b42e17bb', '5114100151', '34562345', '2018-04-18', 5400000000, 'wawan', 2, NULL, '5114100151', 100000),
('SO58d89b57a2897', '5114100014', '7655014565438895', '2017-03-31', 12000000, 'ADI', 3, NULL, '5114100014', 200000),
('SO58d89b851c0d1', '5114100014', '123456789', '2017-03-31', 12000000, 'Adi', 3, NULL, '5114100014', 200000),
('SO58d89b88f1737', '5114100151', 'co001', '2018-04-26', 4800000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d89bbd7f34d', '5114100151', 'co001', '2018-04-23', 6880000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d89e033122a', '5114100151', 'co001', '2019-05-22', 7800000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d8a038f1d77', '5114100151', 'co001', '2020-06-12', 5850000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d8a07f0f7de', '5114100151', 'co001', '2020-06-25', 4000000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d8a18ea0cd3', '5114100151', 'co002', '2021-07-14', 4000000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d8a1ce2f492', '5114100151', '34562345', '2021-07-27', 2280000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d8a28513efc', '5114100151', 'co001', '2021-07-29', 5200000000, 'wawan', 3, NULL, '5114100151', 100000),
('SO58d8a31d95fd9', '5114100151', '34562345', '2021-07-31', 400000000, 'wawan', 3, NULL, '5114100151', 100000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD KEY `FK_id_transaksi` (`id_so`),
  ADD KEY `fk_id_produk` (`id_item`),
  ADD KEY `fk_idSupDetPenjualan` (`id_suplier`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_so`),
  ADD KEY `FK_idPetugas` (`id_petugas`),
  ADD KEY `FK_id_custom` (`id_customer`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD CONSTRAINT `Fk_idItemSO` FOREIGN KEY (`id_item`) REFERENCES `item_master` (`id_item`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idSo` FOREIGN KEY (`id_so`) REFERENCES `penjualan` (`id_so`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_idSupDetPenjualan` FOREIGN KEY (`id_suplier`) REFERENCES `suplier` (`id_suplier`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `FK_idPetugas` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_id_custom` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
