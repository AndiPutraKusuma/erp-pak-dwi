    <div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="mt-150">
    <div class="hero-image">
    <div class="hero-image-inner" style="background-image: url('<?php echo base_url() ?>assets/images/tmp/slider-large-3.jpg');">
        <div class="hero-image-content">
            <div class="container">
                <h1>Retail</h1>

                <p>Memuat semua data keluar masuk barang</p>



            </div><!-- /.container -->
        </div><!-- /.hero-image-content -->

        <div class="hero-image-form-wrapper">
            <div class="container">
                <div class="row">

                    <div class="col-sm-4 col-sm-offset-8 col-lg-4 col-lg-offset-7">

                        <form method="post" action="<?php echo base_url() ?>petugas/addPetugas_act" method="post" enctype="multipart/form-data" style="margin-top:-400px;">
                          <div class="form-group">
                            <?php
                            if($this->session->flashdata('pesan')){
                              echo $this->session->flashdata('pesan');
                            }
                          ?>
                          </div>
                            <h2>Register</h2>


                            <div class="hero-image-keyword form-group">
                                <input name="email" type="email" class="form-control" placeholder="Email" required>
                            </div><!-- /.form-group -->
							<div class="hero-image-category form-group">
                                <input name="passwd" type="password" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="hero-image-keyword form-group">
                                <input name="nama" type="text" class="form-control" placeholder="Nama Lengkap" required>
                            </div>
                            <div class="hero-image-keyword form-group">
                                <input name="ktp" type="text" class="form-control" placeholder="Nomor KTM" required>
                            </div><!-- /.form-group -->
                            <!-- /.form-group -->

                            <button type="submit" class="btn btn-primary btn-block">Register</button>
                        </form>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.hero-image-form-wrapper -->
    </div><!-- /.hero-image-inner -->
</div><!-- /.hero-image -->

</div>
</div><!-- /.content -->
</div><!-- /.main-inner -->
</div><!-- /.main -->
