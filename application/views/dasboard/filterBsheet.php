  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Balance Sheet
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Keuangan</a></li>
            <li class="active">Balance Sheet</li>
          </ol>
        </section>
      
        <!-- Main content -->
        <section class="content">
         
          <div class="row">
            <div class="col-xs-12 col-lg-10">
              <div class="box">
                <div class="box-header">
                   <div class="row">
					<div class="co-lg-12">
                   <form method="post" action="<?php echo base_url(),"gl/filterBsheet" ?>" enctype="multipart/form-data">
				   <div class="col-lg-1">
						<label for="exampleInputEmail1">Filter</label>                   
					</div>
                    <div class="col-lg-2">
						<input name="tgl_awal" type="text" class="form-control datepicker"  placeholder="Periode" data-date-format="yyyy-mm" required>
                    </div>
					<!-- <div class="col-lg-2">
						<input name="tgl_akhir" type="text" class="form-control datepicker"  placeholder="Tanggal akhir" data-date-format="yyyy-mm-dd" required>
                    </div> -->
					<div class="col-lg-1">
						 <button type="submit" class="btn btn-primary">Filter</button>
					</div>
					 </form>
					 <div class="col-lg-1">
					 <form method="post" action="<?php echo base_url(),"gl/printBsheet" ?>" enctype="multipart/form-data">
					 <input name="tgl_awal" type="hidden" value="<?php echo $tgl_tampil; ?>">
					 <!-- <input name="tgl_akhir" type="hidden"  value="<?php echo $tgl_akhir; ?>">  -->
						 <button type="submit" class="btn btn-info"><li class="fa fa-print"></li>Print</button>
					</form>
					 </div>
                  </div>
               </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                <h4>Periode : <?php echo $tgl_tampil; ?></h4>
                  <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                            <th><b><i>Assets</i></b></th></tr>
                                        </thead>
                                        <tbody>
                                             <?php 
                                                 $total_asset=0;
                                                 if(!empty($isi)){ 
                                                foreach ($isi as $rows) {
                                                    if($rows->acc_id<2000){
                                                ?>
                                                        <tr>
                                                        <td><?php echo $rows->acc_id.' --- '.$rows->nama; ?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-";?></td> 
                                                        </tr>
                                            <?php $total_asset=$total_asset+($rows->total_dbt-$rows->total_crd);
                                             }}} ?>
                                                
                                                    <tr>
                                                        <td><b><i>Total Asset</i></b></td>
                                                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_asset,0,'','.').",-"; ?></i></b></td>
                                                    </tr>
                                        </tbody>    
                                        <thead>
                                            <tr>
                                            <th><b><i>Liability&Equity</i></b></th><td></td></tr>
                                            <tr>
                                            <th>Liability</th><td></td></tr>
                                        </thead>
                                        <tbody>

                                                <?php $total_lia=0;
                                                if(!empty($isi)){  
                                                foreach ($isi as $rows) {
                                                    if($rows->acc_id<3000&&$rows->acc_id>1999){
                                                ?>
                                                        <tr>
                                                        <td><?php echo $rows->acc_id.' --- '.$rows->nama; ?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-";?></td> 
                                                        </tr>
                                            <?php $total_lia=$total_lia+($rows->total_dbt-$rows->total_crd);
                                             }}} ?>
                                                
                                                    <tr>
                                                        <td><b>Total Liability</b></td>
                                                        <td style="text-align:right;"><b><?php echo "Rp ".number_format($total_lia,0,'','.').",-"; ?></b></td>
                                                    </tr>
                                        </tbody>
                                        <thead>
                                            <tr>
                                            <th>Equity</th><td></td></tr>
                                        </thead>
                                        <tbody>

                                                <?php  $total_equ=0;
                                                if(!empty($isi)){
                                                foreach ($isi as $rows) {
                                                    if($rows->acc_id<6000&&$rows->acc_id>2999||$rows->acc_id<8000&&$rows->acc_id>6999){
                                                      if($rows->acc_id==3000){ ?>
                                                        <tr>
                                                        <td><?php echo $rows->acc_id.' --- '.$rows->nama; ?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format(($rows->total_dbt-$rows->total_crd)/2,0,'','.').",-";
                                                        $total_equ=$total_equ+(($rows->total_dbt-$rows->total_crd))/2;}?></td> 
                                                        </tr>
                                                <?php if($rows->acc_id!=3000) {?>
                                                        <tr>
                                                        <td><?php echo $rows->acc_id.' --- '.$rows->nama; ?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format(($rows->total_dbt-$rows->total_crd),0,'','.').",-";
                                                        $total_equ=$total_equ+(($rows->total_dbt-$rows->total_crd));}?></td> 
                                                        </tr>
                                            <?php 
                                             }}} ?>
                                                
                                                    <tr>
                                                        <td><b>Total Equity</b></td>
                                                        <td style="text-align:right;"><b><?php echo "Rp ".number_format($total_equ,0,'','.').",-"; ?></b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><i>Total Liability&Equity</i></b></td>
                                                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_equ+$total_lia,0,'','.').",-"; ?></i></b></td>
                                                    </tr>
                                        </tbody>

                                    </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     