<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Edited By <a href="http://ahapedia.com">ahapedia</a></b>
        </div>
        <strong>Copyright &copy; 2017 <a href="#">PT.Retail</a></strong>
      </footer>



    </div><!-- ./wrapper -->


<!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url() ?>assets/js/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url() ?>assets/js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery-jvectormap-world-mill-en.js"></script>

    <!-- daterangepicker -->
    <script src="<?php echo base_url() ?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap3-wysihtml5.all.min.js"></script>

    <!-- Slimscroll -->
    <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() ?>assets/js/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>assets/js/app.min.js"></script>
	<!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>assets/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/datatables/dataTables.bootstrap.min.js"></script>
	<!-- AdminLTE for demo purposes -->
    <!--script src="<?php echo base_url() ?>assets/js/demo.js"></script>
	  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--script src="<?php echo base_url() ?>assets/js/dashboard.js"></script>

	<!-- Bootstrap-select -->
    <script src="<?php echo base_url() ?>assets/js/select2.min.js"></script>
	<script>
  /*$(function () {
    $("#dataTabel").DataTable();
    $('#dataTabel2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });*/
</script>
	<script type="text/javascript">
		 //Date picker
		$('.datepicker').datepicker({
		  autoclose: true

		});

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
		$.fn.modal.Constructor.prototype.enforceFocus = function() {};
		$(document).ready(function() {
		  $(".selecttree").select2();
		});
    </script>
  </body>
</html>
