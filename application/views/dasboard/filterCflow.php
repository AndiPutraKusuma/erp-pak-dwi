  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Cash Flow
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Keuangan</a></li>
            <li class="active">Cash Flow</li>
          </ol>
        </section>
      
        <!-- Main content -->
        <section class="content">
         
          <div class="row">
            <div class="col-xs-12 col-lg-10">
              <div class="box">
                <div class="box-header">
                   <div class="row">
					<div class="co-lg-12">
                   <form method="post" action="<?php echo base_url(),"gl/filterCflow" ?>" enctype="multipart/form-data">
				   <div class="col-lg-1">
						<label for="exampleInputEmail1">Filter</label>                   
					</div>
                    <div class="col-lg-2">
						<input name="tgl_awal" type="text" class="form-control datepicker"  placeholder="Periode" data-date-format="yyyy-mm" required>
                    </div>
					<!-- <div class="col-lg-2">
						<input name="tgl_akhir" type="text" class="form-control datepicker"  placeholder="Tanggal akhir" data-date-format="yyyy-mm-dd" required>
                    </div> -->
					<div class="col-lg-1">
						 <button type="submit" class="btn btn-primary">Filter</button>
					</div>
					 </form>
					 <div class="col-lg-1">
					 <form method="post" action="<?php echo base_url(),"gl/printCflow" ?>" enctype="multipart/form-data">
					 <input name="tgl_awal" type="hidden" value="<?php echo $tgl_tampil; ?>">
					 <!-- <input name="tgl_akhir" type="hidden"  value="<?php echo $tgl_akhir; ?>">  -->
						 <button type="submit" class="btn btn-info"><li class="fa fa-print"></li>Print</button>
					</form>
					 </div>
                  </div>
               </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                <h4>Periode : <?php echo $tgl_awal; ?></h4>
                  <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                            <th><b><i>Aktivitas Operasional</i></b></th></tr>
                                        </thead>
                                        <tbody>
                                            <th bgcolor="gray"><b>Pendapatan</b></th><th bgcolor="gray"></th>
                                              <?php $total_dapat=0;
                                              if(!empty($isi)){ 
                                              foreach ($isi as $rows) {
                                                if($rows->acc_id%4000<1000||$rows->acc_id==2100||$rows->acc_id==3000){ ?> 
                                                        <tr>
                                                  <td><?php echo $rows->nama;?></td>
                                                  <?php if($rows->acc_id==4000||$rows->acc_id==2100||$rows->acc_id==3000){?>
                                                            <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_crd-$rows->total_dbt,0,'','.').",-"; $total_dapat=$total_dapat+($rows->total_crd-$rows->total_dbt);?></td>
                                                        <?php } else{ ?>
                                                            <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt-$rows->total_crd,0,'',',').",-"; $total_dapat=$total_dapat+($rows->total_dbt-$rows->total_crd);?></td>
                                                        <?php } ?> 
                                                  </tr>
                                          <?php }}} ?>
                                            
                                              <tr>
                                                <th><b>Total Pendapatan Operasional</b></th>
                                                <td style="text-align:right;"><b><?php echo "Rp ".number_format($total_dapat,0,'',',').",-"; ?></b></td>
                                              </tr>
                                            <th bgcolor="gray"><b>Biaya Pengurangan</b></th><th bgcolor="gray"></th>
                                                <?php $total=0;
                                                if(!empty($isi)){ 
                                                foreach ($isi as $rows) {
                                                    if($rows->acc_id<7000&&$rows->acc_id>4999||$rows->acc_id==1400){ ?> 
                                                        <tr>
                                                        <td><?php echo $rows->nama;?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_dbt-$rows->total_crd,0,'',',').",-"; $total=$total+($rows->total_dbt-$rows->total_crd);?></td>
                                                        </tr>
                                            <?php }}} ?>
                                                
                                                    <tr>
                                                        <th><b>Total Biaya Operasional</b></th>
                                                        <td style="text-align:right;"><b><?php echo "Rp ".number_format($total,0,'','.').",-"; $total=$total_dapat-$total; ?></b></td>
                                                    </tr>
                                                    <tr>
                                                        <th><b><i>Total Aktivitas Operasional</i></b></th>
                                                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total,0,'',',').",-"; ?></i></b></td>
                                                    </tr>
                                      </tbody>    
                                      <thead>
                                            <tr>
                                            <th><b><i>Aktivitas Non Operasional</i></b></th></tr>
                                        </thead>
                                        <tbody>
                                            <th bgcolor="gray"><b>Lainnya</b></th><th bgcolor="gray"></th>
                                                <?php $total_lain=0;
                                                if(!empty($isi)){ 
                                                foreach ($isi as $rows) {
                                                    if($rows->lain==1){ ?> 
                                                        <tr>
                                                        <td><?php echo $rows->nama;?></td>
                                                        <td style="text-align:right;"><?php echo "Rp ".number_format($rows->total_crd-$rows->total_dbt,0,'','.').",-"; $total_lain=$total_lain+($rows->total_crd-$rows->total_dbt);?></td>
                                                        </tr>
                                            <?php }}} ?>
                                                
                                                        <tr><th><b><i>Total Aktivitas Non Operasional</i></b></th>
                                                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total_lain,0,'','.').",-"; ?></i></b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><i>Perpindahan Kas Bersih</i></b></td>
                                                        <td style="text-align:right;"><b><i><?php echo "Rp ".number_format($total=$total+$total_lain,0,'','.').",-";?></i></b></td>
                                                    </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                            <th><b><i>Ringkasan</i></b></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><b>Saldo Pembukaan</b></td>
                                                <td style="text-align:right;"><b>
                                                <?php $ctotal_dapat=0;
                                                $ctotal=0;
                                                if(!empty($last)){ 
                                                foreach ($last as $rows) {
                                                //     if($rows->acc_id%4000<1000){ 
                                                //         if($rows->acc_id==4000){ 
                                                //             $ctotal_dapat=$ctotal_dapat+($rows->total_crd-$rows->total_dbt);
                                                //         }else{
                                                //             $ctotal_dapat=$ctotal_dapat+($rows->total_dbt-$rows->total_crd);
                                                //         }
                                                //     }
                                                // } }
                                                // $ctotal=0;
                                                // if(!empty($last)){ 
                                                // foreach ($last as $rows) {
                                                //     if($rows->acc_id<7000&&$rows->acc_id>4999){  
                                                //         $ctotal=$ctotal+($rows->total_dbt-$rows->total_crd);
                                                //     }
                                                // }}
                                                // $ctotal=$ctotal_dapat-$ctotal;
                                                // $ctotal_lain=0; 
                                                // if(!empty($last)){
                                                // foreach ($last as $rows) {
                                                //     if($rows->lain==1){ 
                                                //         $ctotal_lain=$ctotal_lain+($rows->total_crd-$rows->total_dbt);
                                                //     }
                                                    if($rows->acc_id==1000){
                                                       $ctotal= ($rows->total_dbt-$rows->total_crd);
                                                    }
                                                    
                                                }} 
                                                // $ctotal=$ctotal+$ctotal_lain; 
                                                echo "Rp ".number_format($ctotal,0,'','.').",-";?></b></td>
                                            </tr>
                                            <tr>
                                                <td><b>Perpindahan Kas Bersih</b></td>
                                                <td style="text-align:right;"><b><?php echo "Rp ".number_format($total,0,'','.').",-";?></b></td>
                                            </tr>
                                            <tr>
                                                <td><b>Saldo Penutupan</b></td>
                                                <td style="text-align:right;"><b><?php echo "Rp ".number_format($total+$ctotal,0,'','.').",-";?></b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     