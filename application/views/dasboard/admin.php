  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
			<?php if($this->session->flashdata('pesan')){
				echo $this->session->flashdata('pesan');
			} ?>
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $pembelian ?><sub style="font-size: 20px">Transaksi</sub></h3>
                  <p>Pembelian Sukses</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="<?php echo base_url(),"pembelian/listPemesanan" ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $penjualan ?><sub style="font-size: 20px">Transaksi</sub></h3>
                  <p>Penjualan Sukses</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="<?php echo base_url(),"penjualan/listPenjualan" ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $customer ?></h3>
                  <p>Customer Terdaftar</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(),"customer/listCustomer" ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo $supplier ?></h3>
                  <p>Supplier Terdaftar</p>
                </div>
                <div class="icon">
                  <i class="fa fa-truck"></i>
                </div>
                <a href="<?php echo base_url(),"supplier/listSupplier" ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-6 connectedSortable">
               <!-- BAR CHART -->
			  <div class="box box-success">
        <!-- <div id="chartContainer1" style="width: 90%; height: 300px;display: inline-block;"></div> -->
        <!-- <canvas id="myChart"></canvas> -->
				<div class="box-header with-border">
				  <h3 class="box-title">Profit Margin</h3>
  <br />
  <!-- <div id="chartContainer3" style="width: 45%; height: 300px;display: inline-block;"></div>
  <div id="chartContainer4" style="width: 45%; height: 300px;display: inline-block;"></div> -->
				  <!-- <div class="box-tools pull-right"> -->
					
				  <!-- </div> -->
				</div>
				<div class="box-body">
				  <div class="chart">
					<canvas id="myChart"></canvas>
				  </div>
				</div>
				<!-- /.box-body -->
			  </div>
        <!-- <div class="box box-success">
        <div id="chartContainer2" style="width: 90%; height: 300px;display: inline-block;"></div></div> -->
          <!-- /.box -->

            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-6 connectedSortable">
			<!-- AREA CHART -->
					  <div class="box box-primary">
						<div class="box-header with-border">
						  <h3 class="box-title">Limit Stok</h3>

						  <div class="box-tools pull-right">
							
						  </div>
						</div>
						<div class="box-body">
						  <div class="">
						  <table class="table">
							<?php if(!empty($alert)){
							foreach($alert as $baris){ ?>
							<tr>
								<td>
									<?php echo $baris->nama_item; ?>
								</td>
								<td>
									<span class="label label-warning"><?php echo $baris->jumlah; ?></span>
								</td>
							</tr>
							<?php }} ?>
							</table>
						  </div>
						</div>
						<!-- /.box-body -->
					  </div>
					  <!-- /.box -->
            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  <!-- ChartJS 1.0.1 -->
    <script src="<?php echo base_url() ?>assets/js/Chart.min.js"></script>
    <script>
$(document).ready(function(){
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */
	 
	//var url="getposts.json"; // JSON File
	/*
	$.getJSON(url,function(data){
		console.log(data);
		$.each(data.isi, function(i,post){
			alert(post.jumlah);
			
		
		});
		
	});
		*/
		var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [' ', '<?php if(!empty($profit)){
              foreach($profit as $item){
          echo $item->month.' - '.$item->tahun; ?>',
          <?php }} ?>],
    datasets: [{
      label: 'profit margin',
      data: [0, <?php if(!empty($profit)){
              foreach($profit as $item){
          echo $item->margin; ?>,
          <?php }} ?>],
      backgroundColor: "rgba(153,255,51,0.4)"
    }]
  }
});

   
  });
</script>

