<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Gaji

        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">HRM</a></li>
          <li class="active">Gaji</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12 col-lg-10">
            <div class="box">
              <div class="box-header">
        <!-- <a style="cursor: pointer;" data-toggle="modal" data-target="#listAtur" ><i class="fa fa-gear"></i> <h3 class="box-title">Lihat Aturan Gaji Sekarang</h3></a><br>
         --><a style="cursor: pointer;" data-toggle="modal" data-target="#inputAtur" ><i class="fa fa-pencil"></i> <h3 class="box-title">Atur Gaji</h3></a>
                <div class="box-tools">
                  <!-- <form method="post" action="#" enctype="multipart/form-data">
                  <div class="input-group" style="width: 150px;">

                    <input type="text" name="cari" class="form-control input-sm pull-right" placeholder="Search">
                    <div class="input-group-btn">
                      <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                    </div>

                  </div>
         </form> -->
                </div>
              </div><!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <tr>
                    <th>Gaji Pokok</th>
                    <th>Tunjangan</th>
                    <th>Bonus/Satuan</th>
                    <th>Satuan</th>
                  </tr>
                  <?php  $temp=0;
                  if(!empty($atur)){
                    foreach($atur as $baris){ ?>
                  <tr>
                    <td><?php echo $baris->pokok; $temp1=$baris->pokok; ?></a></td>
                    <td><?php echo $baris->tunjangan; $temp2=$baris->tunjangan; ?></a></td>
                    <td><?php echo $baris->bonus; $temp3=$baris->bonus;  ?></a></td>
                    <!-- <td>lalalal</a></td> -->
                    <td><?php echo $baris->satuan_bonus; $temp4=$baris->satuan_bonus; ?></a></td>
                  </tr>
                  <?php }}   ?>
                </table>
              </div>
           
            </div><!-- /.box -->
      <div class="row">
        <div class="col-md-12 text-center">
          <?php //echo $paging; ?>
        </div>
      </div>

          </div>
        </div>
      <!-- Modal -->
        <div class="modal fade" id="inputAtur" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Form Atur Besar Gaji</h4>
            </div>
            <div class="modal-body">
             <!-- form start -->
              <form role="form" action="<?php echo base_url() ?>pegawai/updateAtur" method="post" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <label >Gaji Pokok</label>
                    <input name="pokok" type="number" class="form-control" placeholder="Gaji Pokok" required>
                  </div>
                  <div class="form-group">
                    <label >Gaji Tunjangan Keluarga (per tanggungan)</label>
                    <input name="tunjangan" type="number" class="form-control" placeholder="Gaji Tunjangan Keluarga" required>
                  </div>
                  <div class="form-group">
                    <label >Gaji Bonus (per Satuan)</label>
                    <input name="bonus" type="number" class="form-control" placeholder="Gaji Bonus" required>
                  </div>
                  <div class="form-group">
                    <label >Satuan Bonus (banyak penjualan dan kelipatan)</label>
                    <input name="satuan" type="number" class="form-control" placeholder="Satuan Bonus" required>
                  </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>
          </div>
        </div>

        <div class="row">
          <div class="col-xs-12 col-lg-10">
            <div class="box">
              <div class="box-header">
              <h3>Gaji Karyawan</h3><br>
                <form method="post" action="<?php echo base_url(),"pegawai/listGaji" ?>" enctype="multipart/form-data">
                  <div class="col-lg-2">
                    <input name="tanggal" type="text" class="form-control datepicker"  placeholder="Periode" data-date-format="yyyy-mm" required>
                  </div>
                  <div class="col-lg-1">
                    <button type="submit" class="btn btn-primary">Filter</button>
                  </div>
                </form>
              </div><!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
              <h4>Periode : <?php echo $tampil;?></h4>
                <table class="table table-hover">
                  <tr>
                    <th>Nama Pegawai</th>
                    <th>Total Penjualan</th>
                    <th>Gaji Pokok</th>
                    <th>Tunjangan</th>
                    <th>Bonus</th>
                    <th>Total Gaji</th>
                  </tr>
                  <?php $total=0;
                  if(!empty($gaji)){
                    foreach($gaji as $baris){ ?>
                  <tr>
                    <td><?php echo $baris->kurir  ?></td>
                    <td><?php echo $baris->penjualan  ?></td>
                    <td><?php echo "Rp ".number_format($temp1,0,'','.').",-"; ?></td>
                    <td><?php echo "Rp ".number_format($temp2*$baris->tanggungan,0,'','.').",-"; ?></td>
                    <td><?php echo "Rp ".number_format(($temp3/100)*$baris->penjualan,0,'','.').",-"; ?></td>
                    <td><?php echo "Rp ".number_format($total=$total+(($temp3/100)*$baris->penjualan)+($temp2*$baris->tanggungan)+($temp1),0,'','.').",-";  ?></td>
                  </tr>
                  <?php }}   ?>
                  <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th><form method="post" action="<?php echo base_url(),"pegawai/bayarGaji" ?>" enctype="multipart/form-data">
                <div class="col-lg-2">
                  <input name="tanggal" type="hidden" value="<?php echo $tampil; ?>" required>
                  <input name="total" type="hidden" value="<?php echo $total; ?>" required>
                </div>
                <div class="col-lg-1">
                  <button type="submit" class="btn btn-primary">Bayar Gaji</button>
                </div>
              </form></th>
                    <th>Total Seluruhnya</th>
                    <th><?php echo "Rp ".number_format($total,0,'','.').",-"; ?></th>
                  </tr>
                </table>
                
              </div>
              
            </div><!-- /.box -->
      <div class="row">
        <div class="col-md-12 text-center">
          <?php //echo $paging; ?>
        </div>
      </div>

          </div>
        </div>
      <!-- Modal -->
        <div class="modal fade" id="inputAtur" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Form Atur Besar Gaji</h4>
            </div>
            <div class="modal-body">
             <!-- form start -->
              <form role="form" action="<?php echo base_url() ?>pegawai/updateAtur" method="post" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <label >Gaji Pokok</label>
                    <input name="pokok" type="number" class="form-control" placeholder="Gaji Pokok" required>
                  </div>
                  <div class="form-group">
                    <label >Gaji Tunjangan Keluarga (per tanggungan)</label>
                    <input name="tunjangan" type="number" class="form-control" placeholder="Gaji Tunjangan Keluarga" required>
                  </div>
                  <div class="form-group">
                    <label >Gaji Bonus (per Satuan)</label>
                    <input name="bonus" type="number" class="form-control" placeholder="Gaji Bonus" required>
                  </div>
                  <div class="form-group">
                    <label >Satuan Bonus (banyak penjualan dan kelipatan)</label>
                    <input name="satuan" type="number" class="form-control" placeholder="Satuan Bonus" required>
                  </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>
          </div>
        </div>

      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
