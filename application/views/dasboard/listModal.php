<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          List Modal
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Modal</a></li>
          <li class="active">List Modal</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-body">
                Jumlah Modal : <?php echo "Rp "?> <?php echo $totalModal;?>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="box">
              <div class="box-header">
                <a style="cursor: pointer;" data-toggle="modal" data-target="#inputSatuan" ><i class="fa fa-plus"></i> <h3 class="box-title">Add</h3></a>
                <div class="box-tools">
                  <form method="post" action="#" enctype="multipart/form-data">
                  <div class="input-group" style="width: 150px;">

                    <input type="text" name="cari" class="form-control input-sm pull-right" placeholder="Search">
                    <div class="input-group-btn">
                      <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                    </div>

                  </div>
         </form>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
        <?php if($this->session->flashdata('pesan')){
          echo $this->session->flashdata('pesan');
        } ?>
                <table class="table table-hover">
                  <tr>
                    <th>ID</th>
                    <th>Jenis Modal</th>
                    <th>Jumlah</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
        <?php
            if(!empty($isi)){
            foreach($isi as $baris){ ?>
                  <tr>
                    <td><a href="<?php echo base_url(),"produk/viewSatuan/",$baris->id_modal  ?>"><?php echo $baris->id_modal  ?></a></td>
                    <td><?php if($baris->jenis_modal==1){
                      echo "Hutang Bank";
                    }else{
                      echo "Modal Pribadi";
                    }  ?></td>
                    <td>
                      <?php
                          $subtotal =  number_format($baris->jumlah, 2, ',', '.');
                          echo "Rp ",$subtotal;
                       ?>
                    </td>
                    <td><?php if($baris->status==0 && $baris->jenis_modal==1){
                      echo "Belum Lunas";
                    }else {
                      echo "Lunas";
                    } ?></td>

          <td style="width:150px">
           <div class="btn-group">
<!--edit -->
          <?php if ($baris->jenis_modal==1 && $baris->status==1) { ?>
            <span class="label label-success">Sudah Lunas</span>
            <?php
          }?>

          <?php if ($baris->jenis_modal==2 && $baris->status==1) { ?>
            <span class="label label-success">Sudah Lunas</span>
            <?php
          }?>

          <?php if ($baris->jenis_modal==1 && $baris->status==0) { ?>
            <div class="btn-group">
              <a href="<?php echo base_url();?>Modal/bayarModal/<?php echo $baris->id_modal?>" type="button" class="btn btn-primary btn-xs" name="button" title="Lunasi"><span class="fa fa-check-square"></span></a>
              <a data-toggle="modal" data-target="#modaledit-<?php echo $baris->id_modal?>-<?php echo $baris->jumlah?>" class="btn btn-warning btn-xs"  title="Edit"><span class="fa fa-pencil-square-o"></span></a>
              <a href="<?php echo base_url();?>Modal/hapus/<?php echo $baris->id_modal?>" type="button" class="btn btn-danger btn-xs" name="button" title="Hapus" onclick="return confirm('Yakin ? ')"><span class="fa fa-trash-o"></span></a>
            </div>
            <?php
          }?>
<!--edit -->
<!-- punya lama -->
          <!-- <a href="<?php echo base_url() ?>modal/bayarModal/<?php echo $baris->id_modal ?>">
            <button <?php if($baris->jenis_modal==2||$cek<$baris->jumlah||$baris->status==1){
              // echo "disabled";
            } ?> class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Lunasi Modal">
              <li class="fa fa-check-square-o" >
              </li>
            </button>
          </a> -->
<!-- punya lama -->

          </div>
          </td>
                  </tr>

<!--Modal edit -->
<div id="modaledit-<?php echo $baris->id_modal?>-<?php echo $baris->jumlah?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Edit Jumlah Modal</h4>
      </div>
      <div class="modal-body">
        <form action="<?php base_url();?>edit" method="post">
          <div class="form-group">
            <label for="jumlah">Jumlah:</label>
            <input class="form-control number" id="id_step2-number_2" name="jumlah" type="text" data-type="number" value="<?php echo $baris->jumlah?>"  required>
            <input type="hidden" name="id" value="<?php echo $baris->id_modal?>">
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </form>
      </div>

    </div>
  </div>
</div>
<!-- End of Modal edit -->
        <!-- Modal -->
        <div class="modal fade" id="myModal<?php echo $baris->id_modal ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Form Edit Data Satuan</h4>
            </div>
            <div class="modal-body">
             <!-- form start -->
      <form role="form" action="<?php echo base_url() ?>produk/updateSatuan" method="post" enctype="multipart/form-data">
                <div class="box-body">

        <div class="form-group">
          <label for="exampleInputEmail1">Nama Satuan</label>
          <input name="nama" type="text" class="form-control" id="exampleInput" placeholder="Nama Satuan" value="<?php echo $baris->jenis_modal; ?>" required>
          <input name="id" type="hidden" value="<?php echo $baris->id_modal; ?>">
        </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Kelas</label>
                    <input name="kelas" type="text" class="form-control" id="exampleInput" placeholder="Kelas Satuan" value="<?php echo $baris->jumlah; ?>"  required>
                  </div>
        <div class="form-group">
                    <label for="exampleInputPassword1">Deskripsi</label>
                    <input name="deskripsi" type="text" class="form-control" id="exampleInput" placeholder="Deskripsi Satuan" value="<?php echo $baris->status; ?>" required>
                  </div>

                </div><!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>
          </div>
        </div>


        <?php }}
          else{
            echo '<div class="text-center alert-info"><strong>Belum ada data Modal</strong></div>';
          }
        ?>
                </table>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
      <div class="row">
        <div class="col-md-12 text-center">
          <?php //echo $paging; ?>
        </div>
      </div>

          </div>
        </div>
      <!-- Modal -->
        <div class="modal fade" id="inputSatuan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Form Modal</h4>
            </div>
            <div class="modal-body">
             <!-- form start -->
      <form role="form" action="<?php echo base_url() ?>modal/addModal" method="post" enctype="multipart/form-data">
                <div class="box-body">

        <div class="form-group">
                <label >Jenis Modal</label>
                <select name="jenis_modal" class="form-control" required>
                  <option disabled selected value>- Pilih Jenis Modal</option>
                  <option value="1"> Hutang Bank</option>
                  <option value="2"> Modal Pribadi</option>
                </select>
        </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Jumlah</label>
                    <!-- <input name="jumlah" type="number" class="number form-control"  placeholder="Jumlah Modal"  required> -->
                    <input class="form-control number" id="id_step2-number_2" name="jumlah" type="text" data-type="number" placeholder="Jumlah Modal"  required>
                  </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
          </div>
        </div>

      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
<script type="text/javascript">
  $('input.number').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40){
     event.preventDefault();
    }

    $(this).val(function(index, value) {
        value = value.replace(/,/g,'');
        return numberWithCommas(value);
    });
  });

  function numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
  }

</script>
