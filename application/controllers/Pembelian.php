<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {
	public function index(){

		redirect('pembelian/listPemesanan');
	}
	//add data
	public function pemesanan()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			//$idPemilik=$this->session->userdata('id_retail');
			$this->load->model('mproduk');
			$data['isi']=$this->mproduk->list_item(2017);
			$gaga = $this->mproduk->list_item('2017');
			$this->load->model('msupplier');
			$data['suplier']=$this->msupplier->list_supplier('2017');
			$this->load->model('mmodal');
			$data['cek']=$this->mmodal->cek($idPemilik);
			// echo $idPemilik;
			// echo $data['test'];

			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/pemesanan2',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	//list data
	public function listPemesanan()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$this->load->model('mpembelian');

			//pagination
			$this->load->library('pagination');
			$config['base_url']=base_url().'pembelian/listPemesanan';
			$config['total_rows']=$this->mpembelian->countPembelian($idPemilik);
			$config["per_page"]=$per_page=25;
			$config["uri_segment"] = 3;
			//echo $config['total_rows'];
			//config for bootstrap pagination class integration
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);

			$data['paging']=$this->pagination->create_links();
			$page=($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$idPemilik=$this->session->userdata('id_retail');
			$data['isi']=$this->mpembelian->pageList_pembelian($page,$per_page,$idPemilik);

			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/listPemesanan',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	//update status
	public function updateStatus(){
		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
				'idTransaksi' => $this->input->post('idTransaksi'),
				'status' => $this->input->post('status')
            );
			$this->load->model('mpembelian');
			$this->mpembelian->updateStatus($data);
			redirect('pembelian/listPemesanan');
		}else{

			redirect('home');
		}
	}
	//rincian pembelian
	 public function rincianPemesanan($id){
		$cek=$this->session->userdata('username');
		if($cek){
			 $this->load->model('mpembelian');
			 $query=$this->mpembelian->rincianPemesanan($id);

			 if(!empty($query)){
				 foreach ($query as $row)
				{

						echo '<tr>

								<td><a href="',base_url(),'produk/viewProduk/',$row->id_item,'">',$row->nama_item,'</a></td>
								<td>',$row->nama_tipe_item,'</td>
								<td>',$row->nama_satuan,'</td>
								<td>',$row->hargaSatuan,'</td>
								<td>',$row->jumlah,'</td>
								<td>',$row->jml_keluar,'</td>
							</tr>';


				}
			 }
		 }else{

			redirect('home');
		}

	 }
	 public function rincianBarangPemesanan($id){

		$this->load->model('mproduk');
		$query=$this->mproduk->rincianProdukPesanan($id);
		//print_r($query);


		 foreach ($query as $person) {

		 $row = array();
		 $row['nama_suplier']=$person->nama_suplier;
		 $row['id_suplier'] = $person->id_suplier;
		 $row['nama_item'] = $person->nama_item;
		 $row['id_item'] = $person->id_item;
		 if($person->jumlah<1){
			 $row['hargaSatuan'] = $person->harga;
		 }else{
			 $row['hargaSatuan'] = $person->harga;
		 }
		 $row['nama'] = $person->nama_item;
		 if($person->jumlah<1){
			 $row['jumlah'] = "<span class='label label-danger'>Stok kosong</span>";
		 }else{
			 $row['jumlah'] = $person->jumlah;
		 }


		 $data[] = $row;
	 }
		 $out=array(
					 'isi'=>$data);
			echo json_encode($out);


	}

	//keranjang belanja
	public function addCart()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
					'id' => $this->input->post('id'),
					'name' => $this->input->post('nama'),
					'qty' => $this->input->post('jumlah'),
					'price' => $this->input->post('harga'),
					'options' =>array('idSuplier'=>$this->input->post('idSuplier'),
									'idHarga'=>$this->input->post('harga'))

				);

			$this->cart->insert($data);

			redirect("pembelian/pemesanan");
		}else{

			redirect('home');
		}

	}
	public function hapus($id){
		$cek=$this->session->userdata('username');
		if($cek){
			$data=array(
				'rowid'=>$id,
				'qty' =>0

			);
			$this->cart->update($data);
			redirect("pembelian/pemesanan");
		}else{

			redirect('home');
		}
	}
	public function addDataPurchasing()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$idtrans=$this->input->post('idtransaksi');
			$data = array(
					'idTransaksi' => $this->input->post('idtransaksi'),
					'id' => $this->input->post('id'),
					'qty' => $this->input->post('jumlah'),
					'price' => $this->input->post('harga'),
					'total' => $this->input->post('total'),
					'tgl' => $this->input->post('tgl'),
					'idSuplier' => $this->input->post('idSuplier'),
					'email' => $this->session->userdata('username'),
					'id_pemilik' => $idPemilik=$this->session->userdata('id_retail'),
					'ongkir' => $this->input->post('ongkir')

				);

			$this->load->model('mpembelian');
			$this->load->model('mpenjualan');
			$aye = $this->mpenjualan->cek();
    		$haha = $this->mpenjualan->cek2();
    		$idjurnalL = $aye->apa;
    		$idjurnalH = $haha->apa;
			$this->mpembelian->addPurchasing($data,$idjurnalL,$idjurnalH,$cek);
			redirect("pembelian/listPemesanan");
		}else{

			redirect('home');
		}

	}

}
?>
