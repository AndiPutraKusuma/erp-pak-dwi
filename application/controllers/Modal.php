<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modal extends CI_Controller {
	function index(){
		redirect('Modal/listModal');
	}
	//add data
	public function addModal(){
		$cek=$this->session->userdata('username');
		if($cek){

			$data = array(
					'jenis_modal' => $this->input->post('jenis_modal'),
					'jumlah' => $this->input->post('jumlah'),
					'id_pemilik' => $this->session->userdata('id_retail')

				);
			if($data['jumlah']<0)
			{
				$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Modal Tidak bisa negatif !!</div></div>");
				redirect("modal/listModal");
			}
			// return print_r($data['jumlah']);
			$myArray = explode(',', $data['jumlah']);
			$hasil="";
			foreach($myArray as $my_Array){
			    $hasil=$hasil.$my_Array;
			}
			$data['jumlah']=$hasil;
			$this->load->model('mmodal');
			$this->load->model('mpenjualan');
    		$haha = $this->mpenjualan->cek2();
    		$idjurnalH = $haha->apa;
			$this->mmodal->addModal($data,$idjurnalH);
				$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Input data berhasil !!</div></div>");
			redirect("modal/listModal");
		}else{

			redirect('home');
		}
	}

	public function bayarModal($id){
		$this->load->model('mmodal');
		$this->load->model('mpenjualan');
    		$haha = $this->mpenjualan->cek2();
    		$idjurnalH = $haha->apa;
		$jenis2 = $this->mmodal->cekjenis($id);
		//echo $jenis;
		//$jenis= $jenis2->jenis_modal;
		$this->mmodal->bayarModal($id,$jenis2,$idjurnalH);
		$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Pelunasan berhasil !!</div></div>");
	redirect("modal/listModal");
	}

	public function listModal(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$idPemilik=$this->session->userdata('id_retail');
			$this->load->model('mmodal');
			$data['isi']=$this->mmodal->list_modal($idPemilik);
			$data['cek']=$this->mmodal->cek($idPemilik);
			$total = $this->mmodal->total($idPemilik);
		  $data['totalModal'] = number_format($total,2,",",".");
			$this->load->view('dasboard/head',$user);
			$this->load->view('dasboard/header');
			$this->load->view('dasboard/sidebar');
			// echo $data['cek'];
			$this->load->view('dasboard/listModal',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}

	public function updateModal(){
		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
					'id' => $this->input->post('id'),
					'nama' => $this->input->post('nama'),
					'kelas' => $this->input->post('kelas'),
					'deskripsi' => $this->input->post('deskripsi')

				);

			$this->load->model('mproduk');
			$query=$this->mproduk->updateSatuan($data);
			if($query==1){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-check"></i> Update data berhasil</p></div>');
				redirect("produk/listSatuan");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-ban"></i>Update data gagal </p></div>');
				redirect("produk/listSatuan");
			}
		}else{

			redirect('home');
		}

	}

	public function hapus($id)
	{
		$this->load->model('mmodal');
		$this->mmodal->hapus($id);
		redirect("Modal");
	}

public function edit()
{
		$where = $this->input->post('id');
		$data = array('jumlah' =>  $this->input->post('jumlah') );

		$myArray = explode(',', $data['jumlah']);
		$hasil="";
		foreach($myArray as $my_Array){
				$hasil=$hasil.$my_Array;
		}
		$data['jumlah']=$hasil;

		$this->load->model('mmodal');
		$this->mmodal->edit($where,$data);
		redirect('Modal');
}

	public function deleteModal($id){
		$cek=$this->session->userdata('username');
		if($cek){
			$this->load->model('mproduk');
			$query=$this->mproduk->deleteSatuan($id);
			if($query==1){
				$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-check"></i> Hapus data berhasil</p></div>');
				redirect("produk/listSatuan");
			}else{
				$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p>	<i class="icon fa fa-ban"></i>Hapus data gagal </p></div>');
				redirect("produk/listSatuan");
			}
		}else{

			redirect('home');
		}
	}

}
?>
