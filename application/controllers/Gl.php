<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gl extends CI_Controller {
	public function index(){

	}
	// laporan barang keluar
	public function laporanBsheet(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);

			$this->load->model('mgl');
			// $data['isi']=$this->mlaporan->laporanPenjualan();
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/laporanBsheet');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	public function filterBsheet(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			$min = '1000-12-31';
			$this->db->reconnect();
			$data = array(

					'tgl_awal' => $min,
					'tgl_tampil' => $this->input->post('tgl_awal'), 
					'tgl_akhir' => $this->input->post('tgl_awal').'-31',
					'uid' => $this->session->userdata('id_retail')
				);
			$this->load->model('mgl');
			$data['isi']=$this->mgl->filterBalance($data);
			// echo $data['isi']->total_crd;
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/filterBsheet',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	public function printBsheet(){
		$cek=$this->session->userdata('username');
		if($cek){
			 $this->load->library(array('PHPExcel'));
			 $data = array(

					'tgl_awal' => $this->input->post('tgl_awal'),
					'tgl_akhir' => $this->input->post('tgl_awal').'-31',
					'uid' =>$this->session->userdata('id_retail')
				);

			$this->load->model('mgl');
			$ambildata = $this->mgl->filterBalance($data);

			if(count($ambildata)>0){
				$objPHPExcel = new PHPExcel();
				// Set properties
				$objPHPExcel->getProperties()
					  ->setCreator("ERP Retail") //creator
						->setTitle("Programmer - ");  //file title

				$objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
				$objget = $objPHPExcel->getActiveSheet();  //inisiasi get object

				$objget->setTitle('Balanced Sheet'); //sheet title

				$objget->getStyle("A5:B5")->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '92d050')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);

				//table header
				$cols = array("A","B");

				// $val = array("Kode Akun ","Saldo");
				$objPHPExcel->getActiveSheet()->mergeCells('A2:B2');
				$objPHPExcel->getActiveSheet()->setCellValue('A2','Laporan Keuangan Balanced Sheet');
				$objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
				$objPHPExcel->getActiveSheet()->setCellValue('A3','Periode :'.date('d/m/Y',strtotime($data['tgl_awal'])));

				for ($a=0;$a<2; $a++)
				{
					// $objset->setCellValue($cols[$a].'5', $val[$a]);

					//Setting lebar cell
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
					$style = array(
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						)
					);
					$objPHPExcel->getActiveSheet()->getStyle($cols[$a].'5')->applyFromArray($style);
				}

				$baris  = 5;
				$objset->setCellValue("A".$baris, 'Asset');
				$objget->getStyle("A5:B5")->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
				$baris++;
				$total_asset=0;
                if(!empty($ambildata)){
                	foreach ($ambildata as $rows) {
                		if($rows->acc_id<2000){
                			$objset->setCellValue("A".$baris, $rows->acc_id.' --- '.$rows->nama);
                			$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-");
                			$total_asset=$total_asset+($rows->total_dbt-$rows->total_crd);
                			$baris++;
                			}
                		}
                	}
                $objset->setCellValue("A".$baris, 'Total Asset');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total_asset,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
                $baris++;
                $baris++;
                $objset->setCellValue("A".$baris,'Liability & Equity');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris,'Liability');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
                $total_lia=0;
                if(!empty($ambildata)){
                	foreach ($ambildata as $rows) {
                		if($rows->acc_id<3000&&$rows->acc_id>1999){
                			$objset->setCellValue("A".$baris, $rows->acc_id.' --- '.$rows->nama);
                			$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-");
                			$total_lia=$total_lia+($rows->total_dbt-$rows->total_crd);
                			$baris++;
                		}
                	}
                }
                $objset->setCellValue("A".$baris,'Total Liability');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total_lia,0,'','.').",-"); 
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris,'Equity');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
                $total_equ=0;
                if(!empty($ambildata)){
                	foreach ($ambildata as $rows) {
                		if($rows->acc_id<6000&&$rows->acc_id>2999||$rows->acc_id<8000&&$rows->acc_id>6999){
                			if($rows->acc_id==3000){ 
		            			$objset->setCellValue("A".$baris, $rows->acc_id.' --- '.$rows->nama);
		            			$objset->setCellValue("B".$baris, 'Rp '.number_format(($rows->total_dbt-$rows->total_crd)/2,0,'','.').",-");
		            			$total_equ=$total_equ+(($rows->total_dbt-$rows->total_crd)/2);
		            			$baris++;
		            		}
		            		if($rows->acc_id!=3000){ 
		            			$objset->setCellValue("A".$baris, $rows->acc_id.' --- '.$rows->nama);
		            			$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-");
		            			$total_equ=$total_equ+(($rows->total_dbt-$rows->total_crd));
		            			$baris++;
		            		}
		            		
                		}
                	}
                }
                $objset->setCellValue("A".$baris,'Total Equity');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total_equ,0,'','.').",-"); 
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris, 'Total Liability & Equity');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total_equ+$total_lia,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
				// foreach ($ambildata as $frow){

				// 	//pemanggilan sesuaikan dengan nama kolom tabel
				// 	$objset->setCellValue("A".$baris, $baris-5);
				// 	$objset->setCellValue("B".$baris, $frow->id_so);
				// 	$objset->setCellValue("C".$baris, date('d/m/Y',strtotime($frow->tanggal))); //rubah format tanggal
				// 	$objset->setCellValue("D".$baris, $frow->nama_item);
				// 	$objset->setCellValue("E".$baris, $frow->nama_satuan);
				// 	$objset->setCellValue("F".$baris, $frow->jumlah);
				// 	$objset->setCellValue("G".$baris, $frow->kurir);
				// 	$objset->setCellValue("H".$baris, $frow->nama_petugas);
				// 	 //echo $frow->nama_petugas;
				// 	//Set number value
				// 	$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');

				// 	$baris++;
				// }

				//$objPHPExcel->getActiveSheet()->setTitle('Data Export');

				$objPHPExcel->setActiveSheetIndex(0);

				$filename = urlencode("B_Sheet".date('_d-M-Y_H-i-s').".xlsx");
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition:inline;filename="'.$filename.'"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
				$objWriter->save('php://output');

			}else{
				redirect('Excel');
			}
		}else{

			redirect('home');
		}
	}


	//laporan barang masuk
	public function laporanCflow(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$this->load->model('mgl');
			// $data['isi']=$this->mlaporan->laporanPembelian();
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/laporanCflow');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	public function filterCflow(){

		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$this->db->reconnect();
			$data = array(

					'tgl_awal' => $this->input->post('tgl_awal').'-1',
					'tgl_akhir' => $this->input->post('tgl_awal').'-31',
					'tgl_tampil' => $this->input->post('tgl_awal'),
					'uid' => $this->session->userdata('id_retail')
				);
			$this->load->model('mgl');
			$data['isi']=$this->mgl->filterCash($data);
			$min = '1000-12-31';
			$last = array(
					'tgl_awal' => $min,
					'tgl_akhir' => $data['tgl_awal'],
					'uid' => $idPemilik
				);
			$data['last']=$this->mgl->filterCash($last);
			// print_r($data['last']);
			// print_r($data['isi']);
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/filterCflow',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	public function printCflow(){
		$cek=$this->session->userdata('username');
		if($cek){
			 $this->load->library(array('PHPExcel'));
			 $data = array(

					'tgl_awal' => $this->input->post('tgl_awal'),
					'tgl_akhir' => $this->input->post('tgl_awal').'-31',
					'uid' =>$this->session->userdata('id_retail')
				);

			$this->load->model('mgl');
			$ambilcash = $this->mgl->filterCash($data);
			$min = '1000-12-31';
			$last = array(
					'tgl_awal' => $min,
					'tgl_akhir' => $this->input->post('tgl_awal'),
					'uid' => $this->session->userdata('id_retail')
				);
			$clast = $this->mgl->filterCash($last);
			if(count($ambilcash)>0){
				$objPHPExcel = new PHPExcel();
				// Set properties
				$objPHPExcel->getProperties()
					  ->setCreator("ERP Retail") //creator
						->setTitle("Programmer - ");  //file title

				$objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
				$objget = $objPHPExcel->getActiveSheet();  //inisiasi get object

				$objget->setTitle('Cash Flow'); //sheet title

				$objget->getStyle("A5:B5")->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '92d050')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);

				//table header
				$cols = array("A","B");

				// $val = array("Kode Akun ","Saldo");
				$objPHPExcel->getActiveSheet()->mergeCells('A2:B2');
				$objPHPExcel->getActiveSheet()->setCellValue('A2','Laporan Keuangan Cash Flow');
				$objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
				$objPHPExcel->getActiveSheet()->setCellValue('A3','Periode :'.date('d/m/Y',strtotime($data['tgl_awal'])));

				for ($a=0;$a<2; $a++)
				{
					// $objset->setCellValue($cols[$a].'5', $val[$a]);

					//Setting lebar cell
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
					$style = array(
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						)
					);
					$objPHPExcel->getActiveSheet()->getStyle($cols[$a].'5')->applyFromArray($style);
				}

				$baris  = 5;
				$objset->setCellValue("A".$baris, 'Aktivitas Operasional');
				$objget->getStyle("A5:B5")->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris,'Pendapatan');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
				$total_dapat=0;
                if(!empty($ambilcash)){
                	foreach ($ambilcash as $rows) {
                		if($rows->acc_id%4000<1000||$rows->acc_id==2100||$rows->acc_id==3000){
                			$objset->setCellValue("A".$baris, $rows->nama);
                			if($rows->acc_id==4000||$rows->acc_id==2100||$rows->acc_id==3000){
                				$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_crd-$rows->total_dbt,0,'','.').",-");
                				$total_dapat=$total_dapat+($rows->total_crd-$rows->total_dbt);
                				$baris++;
                			} else{
                				$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'',',').",-");
                				$total_dapat=$total_dapat+($rows->total_dbt-$rows->total_crd);
                				$baris++;
                			}
                		}
                	}
                }
                $objset->setCellValue("A".$baris, 'Total Pendapatan Operasional');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total_dapat,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
                $baris++;
                $objset->setCellValue("A".$baris, 'Biaya Pengurangan');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
                $baris++;
                $total=0;
                if(!empty($ambildata2)){
                	foreach ($ambildata2 as $rows) {
                		if($rows->acc_id<7000&&$rows->acc_id>4999||$rows->acc_id==1400){
                			$objset->setCellValue("A".$baris, $rows->nama);
                			$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'',',').",-");
                			$total=$total+($rows->total_dbt-$rows->total_crd);;
                			$baris++;
                		}
                	}
                }
                $objset->setCellValue("A".$baris, 'Total Biaya Operasional');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total,0,'','.').",-");
                $total=$total_dapat-$total;
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
                $baris++;
                $objset->setCellValue("A".$baris, 'Total Aktivitas Operasional');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total,0,'',',').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
                $baris++;
                $baris++;
                $objset->setCellValue("A".$baris,'Aktivitas Non Operasional');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris,'Lainnya');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
                $total_lain=0;
                if(!empty($ambildata2)){
                	foreach ($ambildata2 as $rows) {
                		if($rows->lain==1){
                			$objset->setCellValue("A".$baris, $rows->acc_id.' --- '.$rows->nama);
                			$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_crd-$rows->total_dbt,0,'','.').",-");
                			$total_lain=$total_lain+($rows->total_crd-$rows->total_dbt);
                			$baris++;
                		}
                	}
                }
				$objset->setCellValue("A".$baris, 'Total Aktivitas Non Operasional');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total_lain,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris, 'Perpindahan Kas Bersih');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total=$total+$total_lain,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
				$baris++;
				$objset->setCellValue("A".$baris,'Ringkasan');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris, 'Saldo Pembukaan');
				$ctotal_dapat=0;
                $ctotal=0;
                if(!empty($clast)){ 
                foreach ($clast as $rows) {
                    if($rows->acc_id==1000){
                       $ctotal= ($rows->total_dbt-$rows->total_crd);
                    }
                    
                }}
                $objset->setCellValue("B".$baris, 'Rp '.number_format($ctotal,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris, 'Perpindahan Kas Bersih');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris, 'Saldo Penutupan');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total+$ctotal,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);

				$objPHPExcel->setActiveSheetIndex(0);

				$filename = urlencode("C_flow".date('_d-M-Y_H-i-s').".xlsx");
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition:inline;filename="'.$filename.'"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
				$objWriter->save('php://output');

			}else{
				redirect('Excel');
			}
		}else{

			redirect('home');
		}
	}

	//Laporan Harian

	public function laporanHarian(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$this->load->model('mgl');
			// $data['isi']=$this->mlaporan->laporanPembelian();
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/laporanHarian');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	public function laporanIstate(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$this->load->model('mgl');
			// $data['isi']=$this->mlaporan->laporanService();
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/laporanIstate');
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	public function filterIstate(){
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$data = array(

					'tgl_awal' => $this->input->post('tgl_awal').'-1',
					'tgl_akhir' => $this->input->post('tgl_awal').'-31',
					'tgl_tampil' => $this->input->post('tgl_awal'),
					'uid' => $this->session->userdata('id_retail')
				);
				$this->load->model('mgl');
				$data['isi']=$this->mgl->filterIncome($data);
				// return print_r($data['isi']);
				$this->load->view('dasboard/head');
				$this->load->view('dasboard/header',$user);
				$this->load->view('dasboard/sidebar');
				$this->load->view('dasboard/filterIstate',$data);
				$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	public function printIstate(){
		$cek=$this->session->userdata('username');
		if($cek){
			 $this->load->library(array('PHPExcel'));
			 $data = array(

					'tgl_awal' => $this->input->post('tgl_awal'),
					'tgl_akhir' => $this->input->post('tgl_awal').'-31',
					'uid' =>$this->session->userdata('id_retail')
				);

			$this->load->model('mgl');
			$ambildata3 = $this->mgl->filterIncome($data);
			if(count($ambildata3)>0){
				$objPHPExcel = new PHPExcel();
				// Set properties
				$objPHPExcel->getProperties()
					  ->setCreator("ERP Retail") //creator
						->setTitle("Programmer - ");  //file title

				$objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
				$objget = $objPHPExcel->getActiveSheet();  //inisiasi get object

				$objget->setTitle('Income Statement'); //sheet title

				$objget->getStyle("A5:B5")->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '92d050')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);

				//table header
				$cols = array("A","B");

				// $val = array("Kode Akun ","Saldo");
				$objPHPExcel->getActiveSheet()->mergeCells('A2:B2');
				$objPHPExcel->getActiveSheet()->setCellValue('A2','Laporan Keuangan Income Statement');
				$objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
				$objPHPExcel->getActiveSheet()->setCellValue('A3','Periode :'.date('d/m/Y',strtotime($data['tgl_awal'])));

				for ($a=0;$a<2; $a++)
				{
					// $objset->setCellValue($cols[$a].'5', $val[$a]);

					//Setting lebar cell
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
					$style = array(
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						)
					);
					$objPHPExcel->getActiveSheet()->getStyle($cols[$a].'5')->applyFromArray($style);
				}

				$baris  = 5;
				$objset->setCellValue("A".$baris, 'Pendapatan');
				$objget->getStyle("A5:B5")->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
				$baris++;
				$objset->setCellValue("A".$baris,'Pendapatan dari Penjualan');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
				$total_dapat=0;
                if(!empty($ambildata3)){
                	foreach ($ambildata3 as $rows) {
                		if($rows->acc_id%4000<1000){
                			$objset->setCellValue("A".$baris, $rows->nama);
                			if($rows->acc_id==4000){
                				$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_crd-$rows->total_dbt,0,'','.').",-");
                				$total_dapat=$total_dapat+($rows->total_crd-$rows->total_dbt);
                			} else{
                				$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-");
                				$total_dapat=$total_dapat+($rows->total_dbt-$rows->total_crd);
                			}
                			$baris++;
                		}
                	}
                }
                $objset->setCellValue("A".$baris, 'Total Pendapatan dari Penjualan');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total_dapat,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
                $baris++;
                $objset->setCellValue("A".$baris, 'Harga Pokok penjualan');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
                $baris++;
                $total=0;
                if(!empty($ambildata3)){
                	foreach ($ambildata3 as $rows) {
                		if($rows->acc_id<6000&&$rows->acc_id>4999){
                			$objset->setCellValue("A".$baris, $rows->nama);
                			$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-");
                			$total=$total+($rows->total_dbt-$rows->total_crd);
                			$baris++;
                		}
                	}
                }
                $objset->setCellValue("A".$baris, 'Total Harga Pokok Penjualan');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total,0,'','.').",-");
                $total=$total_dapat-$total;
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
                $baris++;
                $objset->setCellValue("A".$baris, 'Laba Kotor');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
                $baris++;
                $baris++;
                $objset->setCellValue("A".$baris, 'Biaya Operasional');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
                $baris++;
                $total_oper=0;
                if(!empty($ambildata3)){
                	foreach ($ambildata3 as $rows) {
                		if($rows->acc_id<7000&&$rows->acc_id>5999){
                			$objset->setCellValue("A".$baris, $rows->nama);
                			$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-");
                			$total_oper=$total_oper+($rows->total_dbt-$rows->total_crd);
                			$baris++;
                		}
                	}
                }
                $objset->setCellValue("A".$baris, 'Total Biaya Operasional');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total_oper,0,'','.').",-");
                $total=$total-$total_oper;
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
                $objset->setCellValue("A".$baris,'Pendapatan Bersih Operasional');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);
				$baris++;
				$baris++;
				$objset->setCellValue("A".$baris,'Pendapatan Lainnya');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
                $total_lain=0;
                if(!empty($ambildata3)){
                	foreach ($ambildata3 as $rows) {
                		if($rows->acc_id<8000&&$rows->acc_id>6999){
                			$objset->setCellValue("A".$baris, $rows->acc_id.' --- '.$rows->nama);
                			$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-");
                			$total_lain=$total_lain+($rows->total_dbt-$rows->total_crd);
                			$baris++;
                		}
                	}
                }
				$objset->setCellValue("A".$baris, 'Total Pendapatan Lainnya');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total_lain,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
				$baris++;
				$objset->setCellValue("A".$baris, 'Biaya Lainnya');
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
                $total_lain=0;
                if(!empty($ambildata3)){
                	foreach ($ambildata3 as $rows) {
                		if($rows->acc_id<9000&&$rows->acc_id>7999){
                			$objset->setCellValue("A".$baris, $rows->acc_id.' --- '.$rows->nama);
                			$objset->setCellValue("B".$baris, 'Rp '.number_format($rows->total_dbt-$rows->total_crd,0,'','.').",-");
                			$total_lain=$total_lain-($rows->total_dbt-$rows->total_crd);
                			$baris++;
                		}
                	}
                }
				$objset->setCellValue("A".$baris,'Total Biaya Lainnya');
				$objset->setCellValue("B".$baris, 'Rp '.number_format($total_lain,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'c0c0c0')
						),
						'font' => array(
							'color' => array('rgb' => '000000')
						)
					)
				);
				$baris++;
				$baris++;
				$objset->setCellValue("A".$baris, 'Pendapatan Bersih');
                $objset->setCellValue("B".$baris, 'Rp '.number_format($total+$total_lain,0,'','.').",-");
                $objget->getStyle("A".$baris.":B".$baris)->applyFromArray(
					array(
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '000000')
						),
						'font' => array(
							'color' => array('rgb' => 'ffffff')
						)
					)
				);

				$objPHPExcel->setActiveSheetIndex(0);

				$filename = urlencode("I_state".date('_d-M-Y_H-i-s').".xlsx");
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition:inline;filename="'.$filename.'"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
				$objWriter->save('php://output');

			}else{
				redirect('Excel');
			}
		}else{

			redirect('home');
		}
	}



}
?>
