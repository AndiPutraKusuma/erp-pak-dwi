<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {
	public function index(){

		redirect('penjualan/list/Penjualan');
	}
	//add data
	public function addPenjualan()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik='2017';//$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			//$idPemilik=$this->session->userdata('id_retail');
			$id=$this->session->userdata('id_retail');
			$this->load->model('mproduk');
			$data['isi']=$this->mproduk->list_produk($id);
			$this->load->model('mcustomer');
			$data['customer']=$this->mcustomer->list_customer($id);
			$this->load->model('mpegawai');
			$data['pegawai']=$this->mpegawai->list_customer($id);
			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/penjualan',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	//update status
	public function updateStatus(){
		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
				'idTransaksi' => $this->input->post('idTransaksi'),
				'status' => $this->input->post('status')
            );
			$this->load->model('mpenjualan');
			$this->mpenjualan->updateStatus($data);
			redirect('penjualan/listPenjualan');
		}else{

			redirect('home');
		}
	}
	 public function rincianBarang($id){

		 $this->load->model('mproduk');
		 $query=$this->mproduk->rincianProduk($id);
		 //print_r($query);


			foreach ($query as $person) {

			$row = array();
			$row['nama_suplier']=$person->nama_suplier;
			$row['id_suplier'] = $person->id_suplier;
			$row['nama_item'] = $person->nama_item;
			$row['id_item'] = $person->id_item;
			if($person->jumlah<1){
				$row['hargaSatuan'] ="-";
			}else{
				$row['hargaSatuan'] = $person->hargaSatuan;
			}
			$row['nama'] = $person->nama_item;
			if($person->jumlah<1){
				$row['jumlah'] = "<span class='label label-danger'>Stok kosong</span>";
			}else{
				$row['jumlah'] = $person->jumlah;
			}


			$data[] = $row;
		}
			$out=array(
						'isi'=>$data);
			 echo json_encode($out);


	 }
	//list data
	public function listPenjualan()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			//data header
			$email=$this->session->userdata('username');
			$this->load->model('mpetugas');
			$idPet=$this->mpetugas->getId($email);
			$user['user']=$this->mpetugas->view_petugas($idPet);
			$this->load->model('mgudang');
			$idPemilik=$this->session->userdata('id_retail');
			$user['limit']=$this->mgudang->hitungAlertStok($idPemilik);
			$user['alert']=$this->mgudang->alertStok($idPemilik);
			//
			$this->load->model('mpenjualan');

			//pagination
			$this->load->library('pagination');
			$config['base_url']=base_url().'penjualan/listPenjualan';
			$idPemilik=$this->session->userdata('id_retail');
			$config['total_rows']=$this->mpenjualan->countPenjualan($idPemilik);
			$config["per_page"]=$per_page=25;
			$config["uri_segment"] = 3;
			//echo $config['total_rows'];
			//config for bootstrap pagination class integration
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = 'First';
			$config['last_link'] = 'Last';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);

			$data['paging']=$this->pagination->create_links();
			$page=($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$idPemilik= $this->session->userdata('id_retail');
			$data['isi']=$this->mpenjualan->pageList_penjualan($page,$per_page,$idPemilik);
			//

			$this->load->view('dasboard/head');
			$this->load->view('dasboard/header',$user);
			$this->load->view('dasboard/sidebar');
			$this->load->view('dasboard/listPenjualan',$data);
			$this->load->view('dasboard/footer');
		}else{

			redirect('home');
		}
	}
	//rincian penjualan
	 public function rincianPenjualan($id){

		 $this->load->model('mpenjualan');
		 $query=$this->mpenjualan->rincianPenjualan($id);

		 if(!empty($query)){
			 foreach ($query as $row)
			{

					echo '<tr>

							<td><a href="',base_url(),'produk/viewProduk/',$row->id_item,'">',$row->nama_item,'</a></td>
							<td>',$row->nama_tipe_item,'</td>
							<td>',$row->nama_satuan,'</td>
							<td>',$row->harga,'</td>
							<td>',$row->jumlah,'</td>
							<td>',$row->keluar,'</td>
						</tr>';


			}
		 }

	 }
	//keranjang belanja
	public function addCart()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$data = array(
					'id' => $this->input->post('id'),
					'name' => $this->input->post('nama'),
					'qty' => $this->input->post('jumlah'),
					'price' => $this->input->post('harga'),
					'options' =>array('idSuplier'=>$this->input->post('idSuplier'),
									'idHarga'=>$this->input->post('harga'))

				);
			$this->load->model('Mproduk');
			$cek = $this->Mproduk->hargaProduk($data['id'], $data['price']);
			// return print_r($cek['hargaSatuan']);
			if($cek['hargaSatuan']>$data['price'])
			{
				$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Harga jual kurang dari harga beli !!</div></div>");
				redirect("penjualan/addPenjualan");
			}
			$this->cart->insert($data);
			echo $data['id'];
			redirect("penjualan/addPenjualan");
		}else{

			redirect('home');
		}

	}
	public function hapus($id){
		$cek=$this->session->userdata('username');
		if($cek){

			$data=array(
				'rowid'=>$id,
				'qty' =>0

			);
			$this->cart->update($data);
			redirect("penjualan/addPenjualan");
		}else{

			redirect('home');
		}
	}
	public function addDataBelanja()
	{
		$cek=$this->session->userdata('username');
		if($cek){
			$idtrans=$this->input->post('idtransaksi');
			$this->load->model('mpenjualan');
			$pemilik = $this->session->userdata('id_retail');
			
			$data = array(
					'idTransaksi' => $this->input->post('idtransaksi'),
					'id' => $this->input->post('id'),
					'qty' => $this->input->post('jumlah'),
					'price' => $this->input->post('harga'),
					'total' => $this->input->post('total'),
					'tgl' => $this->input->post('tgl'),
					'kurir' => $this->input->post('kurir'),
					'idCustomer' => $this->input->post('id_customer'),
					'email' => $this->session->userdata('username'),
					'ongkir' => $this->input->post('ongkir'),
					'id_pemilik' => $this->session->userdata('id_retail')

				);
			$data2= $this->mpenjualan->total($data);
			$aye = $this->mpenjualan->cek();
    		$haha = $this->mpenjualan->cek2();
    		$idjurnalL = $aye->apa;
    		$idjurnalH = $haha->apa;
    		// $max = $this->mpenjualan->max();
    		// $maxi = $max->dipsy;
			$this->mpenjualan->addPenjualan($data,$idjurnalL,$idjurnalH,$cek,$data2);
				$this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Input data berhasil !!</div></div>");
			// // print_r($data2);
			// // var_dump($data2->jumlahjual) ;
			redirect("penjualan/listPenjualan");
		}else{

			redirect('home');
		}

	}
}
?>
