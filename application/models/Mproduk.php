<?php
class MProduk extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

	 //add data
	 public function additem($data){
		$this->db->reconnect();
		$query=$this->db->query("CALL sp_input_item('$data[id_item]','$data[nama_item]','$data[deskripsi]','$data[tipe]','$data[satuan]','$data[item_harga]','$data[id_pemilik]')");
		$row=$query->row();
		return $row->cek;
	 }
	  public function addSatuan($data){
		 $this->db->reconnect();
		$query=$this->db->query("CALL sp_input_satuan('$data[nama]','$data[kelas]','$data[deskripsi]','$data[id_pemilik]')");

	 }
	 public function addTipeItem($data){
		 $this->db->reconnect();
		$query=$this->db->query("CALL sp_input_tipe('$data[nama]','$data[deskripsi]')");

	 }
	//list data
	public function list_produk($id){
		$this->db->reconnect();
			$query = $this->db->query("SELECT id_suplier,nama_item,item_master.`id_item`,nama_tipe_item,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,link_photo,item_master.deskripsi
	FROM penerimaan_barang
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN item_master ON item_master.`id_item`=gudang.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	WHERE gudang.`id_pemilik`='$id'
	GROUP BY item_master.`id_item` ORDER BY nama_item ASC");
			if ($query->num_rows() > 0){
				return $query->result();
			}else{
				return 0;
			}



	}
	public function pageList_produk($start,$limit,$id_pemilik){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_pageList_produk($start,$limit,$id_pemilik)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function list_cariProduk($word){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_cariProduk('$word')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function list_produk_perSuplier($id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_listProduk_perSuplier('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}

	}

	public function rincianProduk($id){
		$this->db->reconnect();
		$idPemilik=$this->session->userdata('id_retail');
			$query = $this->db->query("SELECT gudang.id_pemilik,nama_suplier,detail_suplier.`id_suplier`,nama_item,detail_suplier.`id_item`,nama_satuan,SUM(gudang.`jumlah`) AS jumlah,hargaSatuan,item_master.deskripsi
	FROM penerimaan_barang
	INNER JOIN gudang ON gudang.`id_rec`=penerimaan_barang.`id_rec`
	RIGHT JOIN detail_suplier ON detail_suplier.`id_suplier`=penerimaan_barang.`id_suplier` AND detail_suplier.`id_item`=gudang.`id_item`
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN tipe_item ON tipe_item.`id_tipe_item`=item_master.`tipe`
	INNER JOIN suplier ON suplier.`id_suplier`=detail_suplier.`id_suplier`
	WHERE detail_suplier.`id_item`='$id' AND gudang.id_pemilik='$idPemilik' GROUP BY detail_suplier.`id_item`,hargaSatuan ORDER BY nama_item ASC");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}

	}

	public function hargaProduk($id, $price)
	{
		$this->db->reconnect();
		$this->db->select('*');
		$this->db->from('gudang');
		$this->db->where('id_item', $id);
		$query=$this->db->get();
		//return print_r($query);
		if($query)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function rincianProdukPesanan($id){
		$this->db->reconnect();
		$idPemilik=$this->session->userdata('id_retail');
			$query = $this->db->query("SELECT nama_suplier,detail_suplier.`id_suplier`,detail_suplier.harga,nama_item,detail_suplier.`id_item`,nama_satuan,'jumlah'
	FROM  detail_suplier
	INNER JOIN item_master ON item_master.`id_item`=detail_suplier.`id_item`
	INNER JOIN satuan sat ON sat.id_satuan=item_master.`satuan`
	INNER JOIN suplier ON suplier.`id_suplier`=detail_suplier.`id_suplier`
	WHERE detail_suplier.`id_item`='$id' ORDER BY nama_item ASC");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}

	}
	public function list_item($id){
		$this->db->reconnect();
	// 		$query = $this->db->query("select * from item_master
	// inner join satuan sat on sat.id_satuan=item_master.`satuan`
	// inner join tipe_item on tipe_item.`id_tipe_item`=item_master.`tipe` where item_master.`id_pemilik`=".$id." order by nama_item asc;");
			$query = $this->db->query("CALL sp_list_item($id)");
			if ($query->num_rows() > 0)
			 {
			 foreach ($query->result() as $row)
			 {
			 		$hasil[] = $row;
			 }
			 return $hasil;
			 }
			 else{
			 	return 0;
			 }
		//	return $query->num_rows();
	}
	public function list_satuan($id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_list_satuan($id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function list_tipeItem(){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_list_tipeItem()");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	//view data
	public function viewItem($id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_view_perItem('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	//update
	public function updateItem($data){
		 $this->db->reconnect();
		$query=$this->db->query("CALL sp_updateItem('$data[idItem]','$data[nama]','$data[tipe]','$data[satuan]','$data[deskripsi]')");
		$row=$query->row();
		return $row->cek;

	 }
	 public function updateSatuan($data){
		 $this->db->reconnect();
		$query=$this->db->query("CALL sp_updateSatuan('$data[id]','$data[nama]','$data[kelas]','$data[deskripsi]')");
		$row=$query->row();
		return $row->cek;

	 }
	 public function updateTipeItem($data){
		 $this->db->reconnect();
		$query=$this->db->query("CALL sp_updateTipe('$data[id]','$data[nama]','$data[deskripsi]')");
		$row=$query->row();
		return $row->cek;

	 }
	//delete item
	public function delete($id){
		$this->db->reconnect();
		$query = $this->db->query("CALL sp_delete_item('$id')");
		$row=$query->row();
		return $row->cek;

	}
	public function deleteSatuan($id){
		$this->db->reconnect();
		$query = $this->db->query("CALL sp_delete_satuan('$id')");
		$row=$query->row();
		return $row->cek;

	}
	public function deleteTipe($id){
		$this->db->reconnect();
		$query = $this->db->query("CALL sp_delete_tipe('$id')");
		$row=$query->row();
		return $row->cek;

	}
	public function cekItem($id){
		$this->db->reconnect();
		$query = $this->db->query("CALL sp_delete_tipe('$id')");
		$row=$query->row();
		return $row->cek;

	}
	 public function countProduk($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungProduk($id)");

				$row=$query->row();
				return $row->jumlah;


	}
}


?>
