<?php
class Mmodal extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

	  public function addModal($data,$idjurnalL){
		 $this->db->reconnect();


		if ($data[jenis_modal]==1) {
			$query=$this->db->query("INSERT INTO modal (jenis_modal,jumlah,status,id_pemilik) values('$data[jenis_modal]','$data[jumlah]',0,'$data[id_pemilik]')");
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES(NOW(),CONCAT('Peminjaman Modal','$idjurnalL'),'$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',2100,0,'$data[jumlah]','$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',1000,'$data[jumlah]',0,'$data[id_pemilik]')");
		}
		else{
			$query=$this->db->query("INSERT INTO modal (jenis_modal,jumlah,status,id_pemilik) values('$data[jenis_modal]','$data[jumlah]',1,'$data[id_pemilik]')");
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES(NOW(),CONCAT('Peminjaman Modal','$idjurnalL'),'$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',3000,0,'$data[jumlah]','$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',1000,'$data[jumlah]',0,'$data[id_pemilik]')");
		}

	 }
	public function cek($id){
		$this->db->reconnect();
		$query2=$this->db->query("SELECT (sum(l.line_debit) - sum(l.line_credit)) as total from gl_journal_l l where l.uid=$id and l.acc_id=1000");
		$row=$query2->row();
			$hasil=$row->total;
			return $hasil;
	}
	public function cekjenis($id)
	{
		$query = $this->db->query("SELECT * from modal WHERE id_modal=$id");
		return $query->row();
	}
	 public function bayarModal($id,$data,$idjurnalL){
		 $this->db->reconnect();
		$query=$this->db->query("UPDATE modal set status=1 where id_modal=$id");
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES(NOW(),CONCAT('Pembayaran Modal','$idjurnalL'),'$data->id_pemilik')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',2100,'$data->jumlah',0,'$data->id_pemilik')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',1000,0,'$data->jumlah','$data->id_pemilik')");

	 }

	public function list_modal($id){
		$this->db->reconnect();
			$query = $this->db->query("SELECT * FROM modal where id_pemilik= $id");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}

	public function hapus($id)
	{
		$this->db->delete('modal',array('id_modal' => $id));
	}

	public function edit($where,$data)
	{
		$this->db->where('id_modal',$where);
		$this->db->update('modal',$data);
	}

	public function total($idPemilik)
	{
		$this->db->select_sum('jumlah');
		$this->db->where('id_pemilik',$idPemilik);
		$query = $this->db->get('modal');
		// foreach ($query->result() as $value) {
		// 	$hasil[] = $value;
		// }
		return $query->row()->jumlah;
	}

}


?>
