<?php
class Mpenjualan extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }

	 public function cek()
	{
		$query=$this->db->query("SELECT MAX(id)+1 as apa FROM gl_journal_h ");
		return $query->row();
	}

	public function cek2()
	{
		$query=$this->db->query("SELECT MAX(id)+1 as apa FROM gl_journal_h ");
		return $query->row();
	}

	public function max()
	{
		$query=$this->db->query("SELECT MAX(id_so)+1 as dipsy FROM penjualan ");
		return $query->row();	
	}

	public function total($data)
	{
		$query = $this->db->query("SELECT c.harga*c.jumlah as jumlahjual , a.hargaSatuan*c.jumlah as jumlahbeli  FROM detail_purchasing a, detail_penjualan c,purchasing d , penjualan e WHERE d.id_po = id_purchasing and d.id_suplier =  c.id_suplier and d.id_pemilik='$data[id_pemilik]' and e.id_so='$data[idTransaksi]'");
		// if ($query->num_rows() > 0)
		// 	{
		// 	foreach ($query->result() as $row)
		// 	{
		// 			$hasil[] = $row;
		// 	}
		// 	return $hasil;
		// 	}
		// 	else{
		// 		return 0;
		// 	}
			//result return dibenerke (ketoke)

		// $query = "SELECT c.harga*c.jumlah as jumlahjual , a.hargaSatuan*c.jumlah as jumlahbeli  FROM detail_purchasing a, detail_penjualan c,purchasing d WHERE d.id_po = id_purchasing and d.id_suplier =  c.id_suplier and d.id_pemilik=$id";
		return $query->result();
		// print_r($query->result());
	}
	public function addPenjualan($data,$idjurnalL,$idjurnalH,$pemilik,$total){
		$this->db->reconnect();
		$query=$this->db->query("CALL sp_input_penjualan('$data[idTransaksi]','$data[email]','$data[idCustomer]','$data[total]','$data[tgl]','$data[kurir]','$data[ongkir]','$data[id_pemilik]')");
		foreach($this->cart->contents() as $item){
			$idSup=$item['options']['idSuplier'];
			$this->db->query("CALL sp_input_detailPenjualan('$data[idTransaksi]','$item[id]','$item[qty]','$item[price]','$idSup')");
			$detail = $this->db->query("SELECT distinct c.harga*c.jumlah as jumlahjual , a.hargaSatuan*c.jumlah as jumlahbeli FROM detail_purchasing a, detail_penjualan c,purchasing d WHERE d.id_po = id_purchasing and d.id_suplier = c.id_suplier and d.id_pemilik='$data[id_pemilik]' and c.id_so='$data[idTransaksi]'");
			$jual = $detail->row();
			// echo $jual->jumlahjual;
			$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$data[tgl]',CONCAT('Pembayaran Invoice','$idjurnalL'),'$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',4000,0,$jual->jumlahjual,'$data[id_pemilik]')");
			// $query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',2200,0,(1/11)*'$data[total]','$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',1000,'$jual->jumlahjual',0,'$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',1400,0,'$jual->jumlahbeli','$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$idjurnalL',5000,'$jual->jumlahbeli',0,'$data[id_pemilik]')");

		}
		$this->cart->destroy();
	 }

	public function list_penjualan(){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_list_penjualan()");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	 public function pageList_penjualan($start,$limit,$id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_pageList_penjualan($start,$limit,$id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	 //deatil pemesanan
	 public function rincianPenjualan($id){
		 $this->db->reconnect();
			$query = $this->db->query("CALL sp_rincianPenjualan('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	 }
	public function list_cariProduk($word){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_cari_suplierProduk('$word')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function list_produk_perSuplier($id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_listProduk_perSuplier('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}

	}
	//update status
	 public function updateStatus($data){
		 $this->db->reconnect();
		 $query=$this->db->query("CALL sp_updateStatus_penjualan('$data[idTransaksi]','$data[status]')");
	 }
	 public function countPenjualan($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungPenjualan($id)");

				$row=$query->row();
				return $row->jumlah;


	}
	 public function countPenjualanSukses($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungPenjualanSukses($id)");

				$row=$query->row();
				return $row->jumlah;


	}

}


?>
