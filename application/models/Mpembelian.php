<?php
class Mpembelian extends CI_Model{

	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }
	 //add data
	  public function addPurchasing($data,$idjurnalL,$idjurnalH,$pemilik){
		$temp=$idjurnalH;
		$this->db->reconnect();
		$query=$this->db->query("CALL sp_input_purchasing('$data[idTransaksi]','$data[idSuplier]','$data[email]','$data[tgl]','$data[total]','$data[id_pemilik]','$data[ongkir]')");
		foreach($this->cart->contents() as $item){
			$this->db->reconnect();
			$this->db->query("CALL sp_input_detailPurchasing('$data[idTransaksi]','$item[id]','$item[qty]','$item[price]')");

		}
		$query2=$this->db->query("INSERT INTO gl_journal_h(period_id,journal_name,uid) VALUES('$data[tgl]',CONCAT('Pembayaran Purchase ','$idjurnalH'),'$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$temp',1400,'$data[total]',0,'$data[id_pemilik]')");
			// $query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$temp',1900,(1/11)*'$data[total]',0,'$data[id_pemilik]')");
			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$temp',1000,0,'$data[total]','$data[id_pemilik]')");
// =======
// 			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$temp',5000,(10/11)*'$data[total]',0,'$data[id_pemilik]')");
// 			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$temp',1900,(1/11)*'$data[total]',0,'$data[id_pemilik]')");
// 			$query4=$this->db->query("INSERT INTO gl_journal_l(journal_id,acc_id,line_debit,line_credit,uid) VALUES('$temp',2000,0,'$data[total]','$data[id_pemilik]')");
// >>>>>>> a328f868158e0f29c10acef218529a6a153cb13b
		$this->cart->destroy();
	 }
	 //list pemesanan
	 public function listPemesanan($id){
		 $this->db->reconnect();
			$query = $this->db->query("CALL sp_list_purchasing($id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	 }
	 public function pageList_pembelian($start,$limit,$id){
		$this->db->reconnect();
			$query = $this->db->query("CALL sp_pageList_pembelian($start,$limit,$id)");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	 //deatil pemesanan
	 public function rincianPemesanan($id){
		 $this->db->reconnect();
			$query = $this->db->query("CALL sp_rincianPembelian('$id')");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row) 
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	 }
	 //update status
	 public function updateStatus($data){
		 $this->db->reconnect();
		 $query=$this->db->query("CALL sp_updateStatus_pembelian('$data[idTransaksi]','$data[status]')");
	 }
	 public function countPembelian($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungPembelian($id)");

				$row=$query->row();
				return $row->jumlah;


	}
	 public function countPembelianSukses($id){

		$this->db->reconnect();
			$query = $this->db->query("CALL sp_hitungPembelianSukses($id)");

				$row=$query->row();
				return $row->jumlah;


	}
}
?>
