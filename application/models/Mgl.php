<?php 
class Mgl extends CI_Model{
	
	 public function __construct(){
                // Call the CI_Model constructor
                parent::__construct();
	 }
	// public function laporanPenjualan(){
	// 	$this->db->reconnect();
	// 		$query = $this->db->query("CALL sp_laporan_penjualan()");
	// 		if ($query->num_rows() > 0)
	// 		{
	// 		foreach ($query->result() as $row)
	// 		{
	// 				$hasil[] = $row;
	// 		}
	// 		return $hasil;
	// 		}
	// 		else{
	// 			return 0;
	// 		}
	// }
	public function profitMargin($uid){
		$this->db->reconnect();
			$query = $this->db->query("select ((A.total-B.total)/B.total) as margin, (A.month) as month, (A.years) as tahun from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' group by month(h.period_id) DESC)A, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=5000 and h.uid='$uid' group by month(h.period_id) DESC)B WHERE A.month = B.month AND A.years=B.years GROUP BY A.month ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function asset($uid){
		$this->db->reconnect();
			$query2 = $this->db->query("SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id<2000 and h.uid='$uid' group by month(h.period_id) DESC");
			if ($query2->num_rows() > 0)
			{
			foreach ($query2->result() as $row)
			{
					$hasil2[] = $row;
			}
			return $hasil2;
			}
			else{
				return 0;
			}
	}
	public function roa($uid){
		$this->db->reconnect();
			$query = $this->db->query("select ((A.total-B.total)) as roa, (A.month) as month, (A.years) as tahun from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' group by month(h.period_id) DESC)A, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=5000 and h.uid='$uid' group by month(h.period_id) DESC)B WHERE A.month = B.month AND A.years=B.years  GROUP BY A.month ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function roe($uid){
		$this->db->reconnect();
			$query = $this->db->query("select ((A.total-B.total)/C.total) as roa, (A.month) as month, (A.years) as tahun from (SELECT (sum(l.line_credit)- sum(l.line_debit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=4000 and h.uid='$uid' group by month(h.period_id) DESC)A, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id=5000 and h.uid='$uid' group by month(h.period_id) DESC)B, (SELECT (sum(l.line_debit)- sum(l.line_credit)) as total, month(h.period_id) as month, year(h.period_id) as years from gl_journal_l l, gl_journal_h h where l.journal_id=h.id and l.acc_id<2000 and h.uid='$uid' group by month(h.period_id) DESC)C WHERE A.month = B.month AND B.month = C.month AND A.years=B.years AND B.years= C.years GROUP BY A.month ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function filterIncome($data){
		$this->db->reconnect();
			$query = $this->db->query("select l.acc_id,A.acc_name as nama, A.other as lain ,sum(l.line_debit) as total_dbt, sum(l.line_credit) as total_crd from gl_journal_l l, (select acc_name ,acc_code, other from gl_account)A where A.acc_code=l.acc_id and l.journal_id in (SELECT `id` FROM gl_journal_h where `period_id` >= '$data[tgl_awal]' and `period_id` <= '$data[tgl_akhir]' and (`uid`='$data[uid]' or `uid` is NULL)) group by l.acc_id order by l.acc_id ASC ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	public function filterCash($data){
		$this->db->reconnect();
			$query = $this->db->query("select l.acc_id,A.acc_name as nama, A.other as lain ,sum(l.line_debit) as total_dbt, sum(l.line_credit) as total_crd from gl_journal_l l, (select acc_name ,acc_code, other from gl_account where uid='$data[uid]' or uid is NULL) A where A.acc_code=l.acc_id and l.journal_id in (SELECT `id` FROM gl_journal_h where `period_id` >= '$data[tgl_awal]' and `period_id` <= '$data[tgl_akhir]' and (`uid`='$data[uid]' or `uid` is NULL))  group by l.acc_id order by l.acc_id ASC ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
	}
	// public function laporanService(){
	// 	$this->db->reconnect();
	// 		$query = $this->db->query("CALL sp_laporan_service()");
	// 		if ($query->num_rows() > 0)
	// 		{
	// 		foreach ($query->result() as $row)
	// 		{
	// 				$hasil[] = $row;
	// 		}
	// 		return $hasil;
	// 		}
	// 		else{
	// 			return 0;
	// 		}
	// }
	public function filterBalance($data){
		$this->db->reconnect();
			$query = $this->db->query("select l.acc_id,A.acc_name as nama, sum(l.line_credit) as total_crd ,sum(l.line_debit) as total_dbt from gl_journal_l l, (select acc_name ,acc_code from gl_account)A where A.acc_code=l.acc_id and l.journal_id in (SELECT `id` FROM gl_journal_h where `period_id` >= '$data[tgl_awal]' and `period_id` <= '$data[tgl_akhir]') and (`uid`='$data[uid]' or `uid` is NULL) group by l.acc_id order by l.acc_id ASC ");
			if ($query->num_rows() > 0)
			{
			foreach ($query->result() as $row)
			{
					$hasil[] = $row;
			}
			return $hasil;
			}
			else{
				return 0;
			}
			// return $query->result();
	}
	// public function laporanDefect(){
	// 	$this->db->reconnect();
	// 		$query = $this->db->query("CALL sp_laporan_defect()");
	// 		if ($query->num_rows() > 0)
	// 		{
	// 		foreach ($query->result() as $row)
	// 		{
	// 				$hasil[] = $row;
	// 		}
	// 		return $hasil;
	// 		}
	// 		else{
	// 			return 0;
	// 		}
	// }
	// public function filterLaporanDefect($data){
	// 	$this->db->reconnect();
	// 		$query = $this->db->query("CALL sp_filter_laporanDefect('$data[tgl_awal]','$data[tgl_akhir]')");
	// 		if ($query->num_rows() > 0)
	// 		{
	// 		foreach ($query->result() as $row)
	// 		{
	// 				$hasil[] = $row;
	// 		}
	// 		return $hasil;
	// 		}
	// 		else{
	// 			return 0;
	// 		}
	// }
	
}
?>